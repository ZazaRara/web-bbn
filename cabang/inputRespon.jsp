<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import = "java.text.SimpleDateFormat"%>
<%@page import="org.ikin.bbn.*"%>
<%
    String kode = request.getParameter("q");
    String nama = request.getParameter("n");
    String alasan = request.getParameter("a");
    String surat = request.getParameter("s");
%>

<html>
    <head>
        <title>Konfirmasi Progresif</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="/home/asset/img/icon.png">
        <link href="/home/asset/css/login.css" rel="stylesheet" type="text/css"/>
        <link href="/home/asset/css/userAdmin.css" rel="stylesheet" type="text/css"/>
        <link href="/home/asset/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/home/asset/css/animate.min.css" rel="stylesheet" type="text/css"/>
        <link href="/home/asset/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
        <link href="/home/asset/fonts/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="/home/asset/css/skp_home.css" rel="stylesheet" type="text/css"/>
        <link href="/home/asset/css/dataTeble.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    </head>
    <body>
        <div class="container">

            

                <div class="col-md-4 col-md-offset-4">

                    <div class="panel panel-default">
                        <div class="panel-heading">                                
                            <div class="row-fluid user-row">


                            </div>
                        </div>
                        <div class="panel-body">
                            <form action="/home/cabang/push/respon.jsp" autocomplete="on" method="post" accept-charset="UTF-8" role="form" class="form-signin">
                                <fieldset>
                                    <input type="hidden" id="kode" name="kode" value="<%=kode%>">
                                    <label class="panel-login">
                                        <div class="login_result">Nama Konsumen</div>
                                    </label>
                                    <input class="form-control" id="username" name="username" type="text" value="<%=nama%>" disabled>
                                    <label class="panel-login">
                                        <div class="login_result">No Surat Tugas</div>
                                    </label>
                                    <input class="form-control" placeholder="Nama Lengkap" id="nama" name="nama" type="text" value="<%=surat%>" disabled>
                                    <label class="panel-login">
                                        <div class="login_result">Alasan Tertunda</div>
                                    </label>
                                    <input class="form-control" placeholder="Kode" id="kode" name="kode" type="text" value="<%=alasan%>" disabled>
                                    <label class="panel-login">
                                        <div class="login_result">Respon Cabang</div>
                                    </label>
                                    <textarea class="form-control" placeholder="Masukkan Respon" id="respon" name="respon" type="text" value="" required="true"></textarea>

                                    <br />
                                    <input class="btn btn-lg btn-success btn-block" type="submit" id="login" value="Submit »">
                                </fieldset>
                            </form>
                        </div>
                    </div>
               
            </div>
        </div>
    </body>
</html>