/**
 * vizabi - Vizabi Framework, Interactive charts and visualization tools animated through time
 * @version v0.12.7
 * @build timestampThu Feb 18 2016 08:28:21 GMT+0000 (UTC)
 * @link http://vizabi.org
 * @license BSD
 */

(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
    typeof define === 'function' && define.amd ? define(factory) :
    global.Vizabi = factory();
}(this, function () { 'use strict';

    var globals = {};

    /*
     * A collection of interpolators
     * @param {Number} x1, x2, y1, y2 - boundary points
     * @param {Number} x - point of interpolation
     * @return {Number} y - interpolated value
     */
    //
    var interpolator = {
        linear: function(x1, x2, y1, y2, x) {
          return +y1 + (y2 - y1) * (x - x1) / (x2 - x1);
        },
        exp: function(x1, x2, y1, y2, x) {
          return Math.exp((Math.log(y1) * (x2 - x) - Math.log(y2) * (x1 - x)) / (x2 - x1));
        },
        stepBefore: function(x1, x2, y1, y2, x) {
            return y2;
        },
        stepAfter: function(x1, x2, y1, y2, x) {
            return y1;
        },
        stepMiddle: function(x1, x2, y1, y2, x) {
            return (x < (x1 + x2)/2) ? y1 : y2;
        }
    };

    /*
     * returns unique id with optional prefix
     * @param {String} prefix
     * @returns {String} id
     */
    var uniqueId = function() {
      var id = 0;
      return function(p) {
        return p ? p + (id += 1) : id += 1;
      };
    }();

    /*
     * checks whether obj is a DOM element
     * @param {Object} obj
     * @returns {Boolean}
     * from underscore: https://github.com/jashkenas/underscore/blob/master/underscore.js
     */
    var isElement = function(obj) {
      return !!(obj && obj.nodeType === 1);
    };

    /*
     * checks whether obj is an Array
     * @param {Object} obj
     * @returns {Boolean}
     * from underscore: https://github.com/jashkenas/underscore/blob/master/underscore.js
     */
    var isArray = Array.isArray || function(obj) {
      return toString.call(obj) === '[object Array]';
    };

    /*
     * checks whether obj is an object
     * @param {Object} obj
     * @returns {Boolean}
     * from underscore: https://github.com/jashkenas/underscore/blob/master/underscore.js
     */
    var isObject = function(obj) {
      var type = typeof obj;
      return type === 'object' && !!obj;
    };

    /*
     * checks whether arg is a date
     * @param {Object} arg
     * @returns {Boolean}
     */
    var isDate = function(arg) {
      return arg instanceof Date;
    };

    /*
     * checks whether arg is a string
     * @param {Object} arg
     * @returns {Boolean}
     */
    var isString = function(arg) {
      return typeof arg === 'string';
    };

    /*
     * checks whether arg is a NaN
     * @param {*} arg
     * @returns {Boolean}
     * from lodash: https://github.com/lodash/lodash/blob/master/lodash.js
     */
    var isNaN$1 = function(arg) {
      // A `NaN` primitive is the only number that is not equal to itself
      return isNumber(arg) && arg !== +arg;
    };

    /*
     * checks whether arg is a number. NaN is a number too
     * @param {*} arg
     * @returns {Boolean}
     * from lodash: https://github.com/lodash/lodash/blob/master/lodash.js
     * dependencies are resolved and included here
     */
    var isNumber = function(arg) {
      return typeof arg === 'number' || !!arg && typeof arg === 'object' && Object.prototype.toString.call(arg) ===
        '[object Number]';
    };

    /*
     * checks whether obj is a plain object {}
     * @param {Object} obj
     * @returns {Boolean}
     */
    var isPlainObject = function(obj) {
      return obj !== null && Object.prototype.toString.call(obj) === '[object Object]';
    };

    /*
     * checks whether two arrays are equal
     * @param {Array} a
     * @param {Array} b
     * @returns {Boolean}
     */
    var arrayEquals = function(a, b) {
      if(a === b) return true;
      if(a == null || b == null) return false;
      if(a.length != b.length) return false;
      for(var i = 0; i < a.length; ++i) {
        if(a[i] !== b[i]) return false;
      }
      return true;
    };


    /**
     * Object Comparison
     *
     * http://stamat.wordpress.com/2013/06/22/javascript-object-comparison/
     *
     * No version
     *
     * @param a
     * @param b
     * @returns {boolean} if objects are equal
     */
    var comparePlainObjects = function (a, b) {

        //Returns the object's class, Array, Date, RegExp, Object are of interest to us
        var getClass = function (val) {
            return Object.prototype.toString.call(val)
                .match(/^\[object\s(.*)\]$/)[1];
        };

        //Defines the type of the value, extended typeof
        var whatis = function (val) {

            if (val === undefined) {
                return 'undefined';
            }
            if (val === null) {
                return 'null';
            }

            var type = typeof val;

            if (type === 'object') {
                type = getClass(val).toLowerCase();
            }

            if (type === 'number') {
                if (val.toString().indexOf('.') > 0) {
                    return 'float';
                }
                else {
                    return 'integer';
                }
            }

            return type;
        };

        var compare = function (a, b) {
            if (a === b) {
                return true;
            }
            for (var i in a) {
                if (b.hasOwnProperty(i)) {
                    if (!equal(a[i], b[i])) {
                        return false;
                    }
                } else {
                    return false;
                }
            }

            for (var i in b) {
                if (!a.hasOwnProperty(i)) {
                    return false;
                }
            }
            return true;
        };

        var compareArrays = function (a, b) {
            if (a === b) {
                return true;
            }
            if (a.length !== b.length) {
                return false;
            }
            for (var i = 0; i < a.length; i++) {
                if (!equal(a[i], b[i])) {
                    return false;
                }
            }
            return true;
        };

        var _equal = {};
        _equal.array = compareArrays;
        _equal.object = compare;
        _equal.date = function (a, b) {
            return a.getTime() === b.getTime();
        };
        _equal.regexp = function (a, b) {
            return a.toString() === b.toString();
        };

        /**
         * Are two values equal, deep compare for objects and arrays.
         * @param a {any}
         * @param b {any}
         * @return {boolean} Are equal?
         */
        var equal = function (a, b) {
            if (a !== b) {
                var atype = whatis(a), btype = whatis(b);

                if (atype === btype) {
                    return _equal.hasOwnProperty(atype) ? _equal[atype](a, b) : a == b;
                }

                return false;
            }

            return true;
        };

        return compare(a, b);
    };


    var getViewportPosition = function(element) {
      var xPosition = 0;
      var yPosition = 0;

      while(element) {
        xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
        yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
        element = element.offsetParent;
      }

      return {
        x: xPosition,
        y: yPosition
      };
    };


    var findScrollableAncestor = function(node) {
      var scrollable = ["scroll", "auto"];
      while(node = node.parentNode) {
        var scrollHeight = node.scrollHeight,
          height = node.clientHeight;
          if (scrollHeight > height && scrollable.indexOf(d3.select(node).style("overflow")) !== -1) {
            return node;
          }
      }
      return null;
    };

    var roundStep = function(number, step) {
      return Math.round(number / step) * step;
    };

    /*
     * transforms a string into a validated fload value
     * @param {string} string to be transformed
     */
    var strToFloat = function(string) {
      return +string.replace(/[^\d.-]/g, '');
    };

    /*
     * loops through an object or array
     * @param {Object|Array} obj object or array
     * @param {Function} callback callback function
     * @param {Object} ctx context object
     */
    var forEach = function(obj, callback, ctx) {
      if(!obj) {
        return;
      }
      var i, size;
      if(isArray(obj)) {
        size = obj.length;
        for(i = 0; i < size; i += 1) {
          if(callback.apply(ctx, [
              obj[i],
              i
            ]) === false) {
            break;
          }
        }
      } else {
        var keys = Object.keys(obj);
        size = keys.length;
        for(i = 0; i < size; i += 1) {
          if(callback.apply(ctx, [
              obj[keys[i]],
              keys[i]
            ]) === false) {
            break;
          }
        }
      }
    };

    /*
     * extends an object
     * @param {Object} destination object
     * @returns {Object} extented object
     */
    var extend = function(dest) {
      //objects to overwrite dest are next arguments
      var objs = Array.prototype.slice.call(arguments, 1);
      //loop through each obj and each argument, left to right
      forEach(objs, function(obj, i) {
        forEach(obj, function(value, k) {
          if(obj.hasOwnProperty(k)) {
            dest[k] = value;
          }
        });
      });
      return dest;
    };

    // Deep extend and helper functions
    // https://github.com/unclechu/node-deep-extend/blob/master/lib/deep-extend.js

    function isSpecificValue(val) {
      return (
        val instanceof Date
        || val instanceof RegExp
      ) ? true : false;
    }

    function cloneSpecificValue(val) {
      if (val instanceof Date) {
        return new Date(val.getTime());
      } else if (val instanceof RegExp) {
        return new RegExp(val);
      } else {
        throw new Error('Unexpected situation');
      }
    }

    /**
     * Recursive cloning array.
     */
    function deepCloneArray(arr) {
      var clone = [];
      forEach(arr, function (item, index) {
        if (typeof item === 'object' && item !== null) {
          if (isArray(item)) {
            clone[index] = deepCloneArray(item);
          } else if (isSpecificValue(item)) {
            clone[index] = cloneSpecificValue(item);
          } else {
            clone[index] = deepExtend({}, item);
          }
        } else {
          clone[index] = item;
        }
      });
      return clone;
    }

    /**
     * Extening object that entered in first argument.
     *
     * Returns extended object or false if have no target object or incorrect type.
     *
     * If you wish to clone source object (without modify it), just use empty new
     * object as first argument, like this:
     *   deepExtend({}, yourObj_1, [yourObj_N]);
     */
    var deepExtend = function(/*obj_1, [obj_2], [obj_N]*/) {
      if (arguments.length < 1 || typeof arguments[0] !== 'object') {
        return false;
      }

      if (arguments.length < 2) {
        return arguments[0];
      }

      var target = arguments[0];

      // convert arguments to array and cut off target object
      var args = Array.prototype.slice.call(arguments, 1);

      var val, src, clone;

      forEach(args, function (obj) {
        // skip argument if it is array or isn't object
        if (typeof obj !== 'object' || isArray(obj)) {
          return;
        }

        forEach(Object.keys(obj), function (key) {
          src = target[key]; // source value
          val = obj[key]; // new value

          // recursion prevention
          if (val === target) {
            return;

          /**
           * if new value isn't object then just overwrite by new value
           * instead of extending.
           */
          } else if (typeof val !== 'object' || val === null) {
            target[key] = val;
            return;

          // just clone arrays (and recursive clone objects inside)
          } else if (isArray(val)) {
            target[key] = deepCloneArray(val);
            return;

          // custom cloning and overwrite for specific objects
          } else if (isSpecificValue(val)) {
            target[key] = cloneSpecificValue(val);
            return;

          // overwrite by new value if source isn't object or array
          } else if (typeof src !== 'object' || src === null || isArray(src)) {
            target[key] = deepExtend({}, val);
            return;

          // source value and new value is objects both, extending...
          } else {
            target[key] = deepExtend(src, val);
            return;
          }
        });
      });

      return target;
    }

    /*
     * merges objects instead of replacing
     * @param {Object} destination object
     * @returns {Object} merged object
     */
    var merge = function(dest) {

      // objects to overwrite dest are next arguments
      var objs = Array.prototype.slice.call(arguments, 1);

      // loop through each obj and each argument, left to right
      forEach(objs, function(obj, i) {
        forEach(obj, function(value, k) {
          if(obj.hasOwnProperty(k)) {
            if(dest.hasOwnProperty(k)) {
              if(!isArray(dest[k])) {
                dest[k] = [dest[k]];
              }
              dest[k].push(value);
            } else {
              dest[k] = value;
            }
          }
        });
      });
      return dest;

    };

    /*
     * clones an object (shallow copy)
     * @param {Object} src original object
     * @param {Array} arr filter keys
     * @returns {Object} cloned object
     */
    var clone = function(src, arr, exclude) {
      if(isArray(src)) {
        return src.slice(0);
      }
      var clone = {};
      forEach(src, function(value, k) {
        if((arr && arr.indexOf(k) === -1) || (exclude && exclude.indexOf(k) !== -1)) {
          return;
        }
        if(src.hasOwnProperty(k)) {
          clone[k] = value;
        }
      });
      return clone;
    };

    /*
     * deep clones an object (deep copy)
     * @param {Object} src original object
     * @returns {Object} cloned object
     */
    var deepClone = function(src) {
      var clone = {};
      if(isArray(src)) clone = [];

      forEach(src, function(value, k) {
        if(isObject(value) || isArray(value)) {
          clone[k] = deepClone(value);
        } else {
          clone[k] = value;
        }
      });
      return clone;
    };

    /*
     * Prints message to timestamp
     * @param {Arr} arr
     * @param {Object} el
     */
    var without = function(arr, el) {
      var idx = arr.indexOf(el);
      if(idx !== -1) {
        arr.splice(idx, 1);
      }
      return arr;
    };

    /*
     * unique items in an array
     * @param {Array} arr original array
     * @param {Function} func optional evaluation function
     * @returns {Array} unique items
     * Based on:
     * http://stackoverflow.com/questions/1960473/unique-values-in-an-array
     */
    var unique = function(arr, func) {
      var u = {};
      var a = [];
      if(!func) {
        func = function(d) {
          return d;
        };
      }
      for(var i = 0, l = arr.length; i < l; i += 1) {
        var key = func(arr[i]);
        if(u.hasOwnProperty(key)) {
          continue;
        }
        a.push(arr[i]);
        u[key] = 1;
      }
      return a;
    };

    /*
     * unique items in an array keeping the last item
     * @param {Array} arr original array
     * @param {Function} func optional evaluation function
     * @returns {Array} unique items
     * Based on the previous method
     */
    var uniqueLast = function(arr, func) {
      var u = {};
      var a = [];
      if(!func) {
        func = function(d) {
          return d;
        };
      }
      for(var i = 0, l = arr.length; i < l; i += 1) {
        var key = func(arr[i]);
        if(u.hasOwnProperty(key)) {
          a.splice(u[key], 1); //remove old item from array
        }
        a.push(arr[i]);
        u[key] = a.length - 1;
      }
      return a;
    };

    /*
     * returns first value that passes the test
     * @param {Array} arr original collection
     * @returns {Function} func test function
     */
    var find = function(arr, func) {
      var found;
      forEach(arr, function(i) {
        if(func(i)) {
          found = i;
          return false; //break
        }
      });
      return found;
    };

    /*
     * filters an array based on object properties
     * @param {Array} arr original array
     * @returns {Object} filter properties to use as filter
     */
    var filter = function(arr, filter) {
      var index = -1;
      var length = arr.length;
      var resIndex = -1;
      var result = [];
      var keys = Object.keys(filter);
      var s_keys = keys.length;
      var i;
      var f;
      while((index += 1) < length) {
        var value = arr[index];
        var match = true;
        for(i = 0; i < s_keys; i += 1) {
          f = keys[i];
          if(!value.hasOwnProperty(f) || value[f] !== filter[f]) {
            match = false;
            break;
          }
        }
        if(match) {
          result[resIndex += 1] = value;
        }
      }
      return result;
    };

    /*
     * filters an array based on object properties.
     * Properties may be arrays determining possible values
     * @param {Array} arr original array
     * @returns {Object} filter properties to use as filter
     */
    var filterAny = function(arr, filter, wildcard) {
      var index = -1;
      var length = arr.length;
      var resIndex = -1;
      var result = [];
      var keys = Object.keys(filter);
      var s_keys = keys.length;
      var i, f;
      while((index += 1) < length) {
        var value = arr[index];
        //normalize to array
        var match = true;
        for(i = 0; i < s_keys; i += 1) {
          f = keys[i];
          if(!value.hasOwnProperty(f) || !matchAny(value[f], filter[f], wildcard)) {
            match = false;
            break;
          }
        }
        if(match) {
          result[resIndex += 1] = value;
        }
      }
      return result;
    };

    /*
     * checks if the value matches the comparison value or any in array
     * compare may be an determining possible values
     * @param value original value
     * @param compare value or array
     * @param {String} wildc wildcard value
     * @returns {Boolean} try
     */
    var matchAny = function(values, compare, wildc) {
      //normalize value
      if(!isArray(values)) values = [values];
      if(!wildc) wildc = "*"; //star by default
      var match = false;
      for(var e = 0; e < values.length; e++) {
        var value = values[e];

        if(!isArray(compare) && value == compare) {
          match = true;
          break;
        } else if(isArray(compare)) {
          var found = -1;
          for(var i = 0; i < compare.length; i++) {
            var c = compare[i];
            if(!isArray(c) && (c == value || c === wildc)) {
              found = i;
              break;
            } else if(isArray(c)) { //range
              var min = c[0];
              var max = c[1] || min;
              if(value >= min && value <= max) {
                found = i;
                break;
              }
            }
          }
          if(found !== -1) {
            match = true;
            break;
          }
        }
      }
      return match;
    };

    /**
     * prevent scrolling parent scrollable elements for 2 second when element scrolled to end
     * @param node
     */

    var preventAncestorScrolling = function(element) {
      var preventScrolling = false;
      element.on('mousewheel', function(d, i) {
        var scrollTop = this.scrollTop,
          scrollHeight = this.scrollHeight,
          height = element.node().offsetHeight,
          delta = d3.event.wheelDelta,
          up = delta > 0;
        var prevent = function() {
          d3.event.stopPropagation();
          d3.event.preventDefault();
          d3.event.returnValue = false;
          return false;
        };

        var scrollTopTween = function(scrollTop) {
          return function () {
            var i = d3.interpolateNumber(this.scrollTop, scrollTop);
            return function (t) {
              this.scrollTop = i(t);
            };
          }
        };
        if (!up) {
          // Scrolling down
          if (-delta > scrollHeight - height - scrollTop && scrollHeight != height + scrollTop) {
            element.transition().delay(0).duration(0).tween("scrolltween", scrollTopTween(scrollHeight));
            //freeze scrolling on 2 seconds on bottom position
            preventScrolling = true;
            setTimeout(function() {
              preventScrolling = false;
            }, 2000);
          } else if (scrollTop == 0) { //unfreeze when direction changed
            preventScrolling = false;
          }
        } else if (up) {
          // Scrolling up
          if (delta > scrollTop && scrollTop > 0) { //
            //freeze scrolling on 2 seconds on top position
            element.transition().delay(0).duration(0).tween("scrolltween", scrollTopTween(0));
            preventScrolling = true;
            setTimeout(function() {
              preventScrolling = false;
            }, 2000);
          } else if (scrollHeight == height + scrollTop) { //unfreeze when direction changed
            preventScrolling = false;
          }
        }
        if (preventScrolling) {
          return prevent();
        }
      });
    };

    /*
     * maps all rows according to the formatters
     * @param {Array} original original dataset
     * @param {Object} formatters formatters object
     * @returns {Boolean} try
     */
    var mapRows = function(original, formatters) {

      function mapRow(value, fmt) {
        if(!isArray(value)) {
          return fmt(value);
        } else {
          var res = [];
          for(var i = 0; i < value.length; i++) {
            res[i] = mapRow(value[i], fmt);
          }
          return res;
        }
      }
       
      // default formatter turns empty strings in null and converts numeric values into number
      //TODO: default formatter is moved to utils. need to return it to hook prototype class, but retest #1212 #1230 #1253
      var defaultFormatter = function (val) {
          var newVal = val;
          if(val === ""){
            newVal = null;
          } else {
            // check for numberic
            var numericVal = parseFloat(val);
            if (!isNaN$1(numericVal) && isFinite(val)) {
              newVal = numericVal;
            }
          }  
          return newVal;
      }
      
      original = original.map(function(row) {
        var columns = Object.keys(row);
          
        for(var i = 0; i < columns.length; i++) {
          var col = columns[i];
          row[col] = mapRow(row[col], formatters[col] || defaultFormatter);
        }
        return row;
      });

      return original;
    };

    /*
     * Converts radius to area, simple math
     * @param {Number} radius
     * @returns {Number} area
     */
    var radiusToArea = function(r) {
      return r * r * Math.PI;
    };

    /*
     * Converts area to radius, simple math
     * @param {Number} area
     * @returns {Number} radius
     */
    var areaToRadius = function(a) {
      return Math.sqrt(a / Math.PI);
    };

    /*
     * Prints message to timestamp
     * @param {String} message
     */
    var timeStamp = function(message) {
      if(console && typeof console.timeStamp === 'function') {
        console.timeStamp(message);
      }
    };

    /*
     * Prints warning
     * @param {String} message
     */
    var warn = function(message) {
      message = Array.prototype.slice.call(arguments).join(' ');
      if(console && typeof console.warn === 'function') {
        console.warn(message);
      }
    };

    /*
     * Prints message for group
     * @param {String} message
     */
    var groupCollapsed = function(message) {
      message = Array.prototype.slice.call(arguments).join(' ');
      if(console && typeof console.groupCollapsed === 'function') {
        console.groupCollapsed(message);
      }
    };

    /*
     * Prints end of group
     * @param {String} message
     */
    var groupEnd = function() {
      if(console && typeof console.groupEnd === 'function') {
        console.groupEnd();
      }
    };

    /*
     * Prints error
     * @param {String} message
     */
    var error$1 = function(message) {
      message = Array.prototype.slice.call(arguments).join(' ');
      if(console && typeof console.error === 'function') {
        console.error(message);
      }
    };

    /*
     * Count the number of decimal numbers
     * @param {Number} number
     */
    var countDecimals = function(number) {
      if(Math.floor(number.valueOf()) === number.valueOf()) {
        return 0;
      }
      return number.toString().split('.')[1].length || 0;
    };

    /*
     * Adds class to DOM element
     * @param {Element} el
     * @param {String} className
     */
    var addClass = function(el, className) {
      if(el.classList) {
        el.classList.add(className);
      } else {
        //IE<10
        el.className += ' ' + className;
      }
    };

    /*
     * Remove class from DOM element
     * @param {Element} el
     * @param {String} className
     */
    var removeClass = function(el, className) {
      if(el.classList) {
        el.classList.remove(className);
      } else {
        //IE<10
        el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'),
          ' ');
      }
    };

    /*
     * Adds or removes class depending on value
     * @param {Element} el
     * @param {String} className
     * @param {Boolean} value
     */
    var classed = function(el, className, value) {
      if(value === true) {
        addClass(el, className);
      } else if(value === false) {
        removeClass(el, className);
      } else {
        return hasClass(el, className);
      }
    };

    /*
     * Checks whether a DOM element has a class or not
     * @param {Element} el
     * @param {String} className
     * @return {Boolean}
     */
    var hasClass = function(el, className) {
      if(el.classList) {
        return el.classList.contains(className);
      } else {
        //IE<10
        return new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
      }
    };

    /*
     * Throttles a function
     * @param {Function} func
     * @param {Number} ms duration
     * @return {Function}
     * Function recallLast was added to prototype of returned function.
     * Call Function.recallLast() - immediate recall func with last saved arguments,
     *                              else func will be called automaticly after ms duration
     */
    var throttle = function(func, ms) {

      var throttled = false,
        savedArgs,
        savedThis,
        nextTime,
        wrapper = function() {
          
          if(nextTime > Date.now()) {
            throttled = true;        
            savedArgs = arguments;
            savedThis = this;
            return;
          }

          nextTime = Date.now() + ms;
          throttled = false;
          
          func.apply(this, arguments);

          setTimeout(function() {
            __recallLast();          
          }, ms);

        },
        
        __recallLast = function() {
          if(throttled) {
            throttled = false;
            func.apply(savedThis, savedArgs);
          }     
        };

      wrapper.recallLast = __recallLast; 

      return wrapper;
    };


    /*
     * Returns keys of an object as array
     * @param {Object} arg
     * @returns {Array} keys
     */
    var keys = function(arg) {
      return Object.keys(arg);
    };

    /*
     * returns the values of an object in an array format
     * @param {Object} obj
     * @return {Array}
     */
    var values = function(obj) {
      var arr;
      var keys = Object.keys(obj);
      var size = keys.length;
      for(var i = 0; i < size; i += 1) {
        (arr = arr || []).push(obj[keys[i]]);
      }
      return arr;
    };


    /*
     * Computes the minumum value in an array
     * @param {Array} arr
     */
    var arrayMin = function(arr) {
      return arr.reduce(function(p, v) {
        return(p < v ? p : v);
      });
    };

    /*
     * Computes the minumum value in an array
     * @param {Array} arr
     */
    var arrayMax = function(arr) {
      return arr.reduce(function(p, v) {
        return(p > v ? p : v);
      });
    };

    /*
     * Computes the mean of an array
     * @param {Array} arr
     */
    var arrayMean = function(arr) {
      return arraySum(arr) / arr.length;
    };

    /*
     * Computes the sum of an array
     * @param {Array} arr
     */
    var arraySum = function(arr) {
      return arr.reduce(function(a, b) {
        return a + b;
      });
    };

    /*
     * Computes the median of an array
     * @param {Array} arr
     */
    var arrayMedian = function(arr) {
      arr = arr.sort(function(a, b) {
        return a - b;
      });
      var middle = Math.floor((arr.length - 1) / 2);
      if(arr.length % 2) {
        return arr[middle];
      } else {
        return(arr[middle] + arr[middle + 1]) / 2;
      }
    };

    /*
     * Returns the last value of array
     * @param {Array} arr
     */
    var arrayLast = function(arr) {
      if(!arr.length) return null;
      return arr[arr.length - 1];
    };

    /*
     * Returns the resulting object of the difference between two objects
     * @param {Object} obj2
     * @param {Object} obj1
     * @returns {Object}
     */
    var diffObject = function(obj2, obj1) {
      var diff = {};
      forEach(obj2, function(value, key) {
        if(!obj1.hasOwnProperty(key)) {
          diff[key] = value;
        } else if(value !== obj1[key]) {
          if(isPlainObject(value) && isPlainObject(obj1[key])) {
            var d = diffObject(value, obj1[key]);
            if(Object.keys(d).length > 0) {
              diff[key] = d;
            }
          } else if(!isArray(value) || !isArray(obj1[key]) || !arrayEquals(value, obj1[key])) {
            diff[key] = value;
          }
        }
      });
      return diff;
    };

    /*
     * Returns the resulting object without _defs_ leveling
     * @param {Object} obj
     * @returns {Object}
     */
    var flattenDefaults = function(obj) {
      var flattened = {};
      forEach(obj, function(val, key) {
        if(isPlainObject(val) && val._defs_) {
          flattened[key] = val._defs_;
        } else if(isPlainObject(val)) {
          flattened[key] = flattenDefaults(val);
        } else {
          flattened[key] = val;
        }
      });
      return flattened;
    };

    /*
     * Returns the resulting object without date objects for time
     * @param {Object} obj
     * @returns {Object}
     */
    var flattenDates = function(obj, timeFormat) {
      var flattened = {};
      forEach(obj, function(val, key) {
        //todo: hack to flatten time unit objects to strings
        if(key === 'time') {
          if(typeof val.value === 'object') {
            val.value = timeFormat(val.value);
          }
          if(typeof val.start === 'object') {
            val.start = timeFormat(val.start);
          }
          if(typeof val.end === 'object') {
            val.end = timeFormat(val.end);
          }
        }
        if(isPlainObject(val)) {
          flattened[key] = flattenDates(val, timeFormat);
        } else {
          flattened[key] = val;
        }
      });
      return flattened;
    }

    /*
     * Defers a function
     * @param {Function} func
     */
    var defer = function(func) {
      setTimeout(func, 1);
    };

    /*
     * Defers a function
     * @param {Function} func
     */
    var delay = function(func, delay) {
      setTimeout(func, delay);
    };

    /*
     * Creates a hashcode for a string or array
     * @param {String|Array} str
     * @return {Number} hashCode
     */
    var hashCode = function(str) {
      if(!isString(str)) {
        str = JSON.stringify(str);
      }
      var hash = 0;
      var size = str.length;
      var c;
      if(size === 0) {
        return hash;
      }
      for(var i = 0; i < size; i += 1) {
        c = str.charCodeAt(i);
        hash = (hash << 5) - hash + c;
        hash = hash & hash; // Convert to 32bit integer
      }
      return hash.toString();
    };


    /*
     * Converts D3 nest array into the object with key-value pairs, recursively
     * @param {Array} arr - array like this [{key: k, values: [a, b, ...]}, {...} ... {...}]
     * @return {Object} object like this {k: [a, b, ...], ...}
     */
    //
    var nestArrayToObj = function(arr) {
      if(!arr || !arr.length || !arr[0].key) return arr;
      var res = {};
      for(var i = 0; i < arr.length; i++) {
        res[arr[i].key] = nestArrayToObj(arr[i].values);
      };
      return res;
    }


    var interpolateVector = function(){
        
    }

    /**
     * interpolates the specific value 
     * @param {Array} items -- an array of items, sorted by "dimTime", filtered so that no item[which] is null
     * @param {String} use -- a use of hook that wants to interpolate. can be "indicator" or "property" or "constant"
     * @param {String} which -- a hook pointer to indicator or property, e.g. "lex"
     * @param {Number} next -- an index of next item in "items" array after the value to be interpolated. if omitted, then calculated here, but it's expensive
     * @param {String} dimTime -- a pointer to time dimension, usually "time"
     * @param {Date} time -- reference point for interpolation. here the valus is to be found
     * @param {String} method refers to which formula to use. "linear" or "exp". Falls back to "linear" if undefined
     * @param {Boolean} extrapolate indicates if we should use zero-order extrapolation outside the range of available data
     * @returns {Number} interpolated value
     */
    var interpolatePoint = function(items, use, which, next, dimTime, time, method, extrapolate){

        
      if(!items || items.length === 0) {
        warn('interpolatePoint failed because incoming array is empty. It was ' + which);
        return null;
      }
      // return constant for the use of "constant"
      if(use === 'constant') return which;
        
      // zero-order interpolation for the use of properties
      if(use === 'property') return items[0][which];

      // the rest is for the continuous measurements
        
      if (extrapolate){
        // check if the desired value is out of range. 0-order extrapolation
        if(time - items[0][dimTime] <= 0) return items[0][which];    
        if(time - items[items.length - 1][dimTime] >= 0) return items[items.length - 1][which];
      } else {
        // no extrapolation according to Ola's request
        if(time < items[0][dimTime] || time > items[items.length - 1][dimTime]) return null;
      }
        
      if(!next && next !== 0) next = d3.bisectLeft(items.map(function(m){return m[dimTime]}), time);
        
      if(next === 0) return items[0][which];
            
      //return null if data is missing
      if(items[next]===undefined || items[next][which] === null || items[next - 1][which] === null || items[next][which] === "") {
        warn('interpolatePoint failed because next/previous points are bad in ' + which);
        return null;
      }
        

      //do the math to calculate a value between the two points
      var result = interpolator[method||"linear"](
        items[next - 1][dimTime],
        items[next][dimTime],
        items[next - 1][which],
        items[next][which],
        time
      );

      // cast to time object if we are interpolating time
      if(which === dimTime) result = new Date(result);
      if(isNaN$1(result)) {
          warn('interpolatePoint failed because result is NaN. It was ' + which);
          result = null;
      }
        
      return result;

    }


    /*
     * Performs an ajax request
     * @param {Object} options
     * @param {String} className
     * @return {Boolean}
     */
    var ajax = function(options) {
      var request = new XMLHttpRequest();
      request.open(options.method, options.url, true);
      if(options.method === 'POST' && !options.json) {
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
      } else if(options.method === 'POST' && options.json) {
        request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
      }
      request.onload = function() {
        if(request.status >= 200 && request.status < 400) {
          // Success!
          var data = options.json ? JSON.parse(request.responseText) : request.responseText;
          if(options.success) {
            options.success(data);
          }
        } else {
          if(options.error) {
            options.error();
          }
        }
      };
      request.onerror = function() {
        if(options.error) {
          options.error();
        }
      };
      request.send(options.data);
    };

    /*
     * Performs a GET http request
     */
    var get = function(url, pars, success, error, json) {
      pars = pars || [];
      forEach(pars, function(value, key) {
        pars.push(key + '=' + value);
      });
      url = pars.length ? url + '?' + pars.join('&') : url;
      ajax({
        method: 'GET',
        url: url,
        success: success,
        error: error,
        json: json
      });
    };

    /*
     * Performs a POST http request
     */
    var post = function(url, pars, success, error, json) {
      ajax({
        method: 'POST',
        url: url,
        success: success,
        error: error,
        json: json,
        data: pars
      });
    };

    /**
     * Make function memoized
     * @param {Function} fn
     * @returns {Function}
     */
    var memoize = function(fn) {
      return function() {
        var args = Array.prototype.slice.call(arguments);
        var hash = '';
        var i = args.length;
        var currentArg = null;

        while(i--) {
          currentArg = args[i];
          hash += (currentArg === Object(currentArg)) ? JSON.stringify(currentArg) : currentArg;
          fn.memoize || (fn.memoize = {});
        }

        return(hash in fn.memoize) ? fn.memoize[hash] : fn.memoize[hash] = fn.apply(this, args);
      };
    };

    // Returns a function, that, as long as it continues to be invoked, will not
    // be triggered. The function will be called after it stops being called for
    // N milliseconds. If `immediate` is passed, trigger the function on the
    // leading edge, instead of the trailing.
    var debounce = function(func, wait, immediate) {
      var timeout;
      return function() {
        var context = this,
          args = arguments;
        var later = function() {
          timeout = null;
          if(!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if(callNow) func.apply(context, args);
      }
    };

    var isTouchDevice = function() {
      return !!(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch);
    };

    //return a pruneed tree
    var pruneTree = function(tree, filterCallback) {
      var filteredTree = {};
      var filteredChildrens = [];
      if(tree.hasOwnProperty("children")) {
        filteredChildrens = tree.children.map(function(childrenTree) {
          return pruneTree(childrenTree, filterCallback);
        }).filter(function(childrenTree) {
          return Object.keys(childrenTree).length !== 0;
        });
      }
      if(filteredChildrens.length != 0 || filterCallback(tree)) {
        filteredTree["id"] = tree.id;
      }
      if(filteredChildrens.length != 0) {
        filteredTree["children"] = filteredChildrens;
      }
      return filteredTree;
    };

    var setIcon = function(element, icon) {
      element.selectAll('*').remove();
      element.node().appendChild(
        element.node().ownerDocument.importNode(
          new DOMParser().parseFromString(
            icon, 'application/xml').documentElement, true)
      );
      return element;
    }


    var utils = {
      uniqueId: uniqueId,
      isElement: isElement,
      isArray: isArray,
      isObject: isObject,
      isDate: isDate,
      isString: isString,
      isNaN: isNaN$1,
      isNumber: isNumber,
      isPlainObject: isPlainObject,
      arrayEquals: arrayEquals,
      comparePlainObjects: comparePlainObjects,
      getViewportPosition: getViewportPosition,
      findScrollableAncestor: findScrollableAncestor,
      roundStep: roundStep,
      strToFloat: strToFloat,
      forEach: forEach,
      extend: extend,
      deepExtend: deepExtend,
      merge: merge,
      clone: clone,
      deepClone: deepClone,
      without: without,
      unique: unique,
      uniqueLast: uniqueLast,
      find: find,
      filter: filter,
      filterAny: filterAny,
      matchAny: matchAny,
      preventAncestorScrolling: preventAncestorScrolling,
      mapRows: mapRows,
      radiusToArea: radiusToArea,
      areaToRadius: areaToRadius,
      timeStamp: timeStamp,
      warn: warn,
      groupCollapsed: groupCollapsed,
      groupEnd: groupEnd,
      error: error$1,
      countDecimals: countDecimals,
      addClass: addClass,
      removeClass: removeClass,
      classed: classed,
      hasClass: hasClass,
      throttle: throttle,
      keys: keys,
      values: values,
      arrayMin: arrayMin,
      arrayMax: arrayMax,
      arrayMean: arrayMean,
      arraySum: arraySum,
      arrayMedian: arrayMedian,
      arrayLast: arrayLast,
      diffObject: diffObject,
      flattenDefaults: flattenDefaults,
      flattenDates: flattenDates,
      defer: defer,
      delay: delay,
      hashCode: hashCode,
      nestArrayToObj: nestArrayToObj,
      interpolateVector: interpolateVector,
      interpolatePoint: interpolatePoint,
      ajax: ajax,
      get: get,
      post: post,
      memoize: memoize,
      debounce: debounce,
      isTouchDevice: isTouchDevice,
      pruneTree: pruneTree,
      setIcon: setIcon
    };

    var initializing = false;
    var fnTest = /xyz/.test(function() {
      xyz;
    }) ? /\b_super\b/ : /.*/;

    function extend$1(name, extensions) {

      //in case there are two args
      extensions = arguments.length === 1 ? name : extensions;
      var _super = this.prototype;
      initializing = true;
      var prototype = new this();
      initializing = false;

      forEach(extensions, function(method, name) {
        if(typeof extensions[name] === 'function' && typeof _super[name] === 'function' && fnTest.test(extensions[name])) {
          prototype[name] = function(name, fn) {
            return function() {
              var tmp = this._super;
              this._super = _super[name];
              var ret = fn.apply(this, arguments);
              this._super = tmp;
              return ret;
            };
          }(name, extensions[name]);
        } else {
          prototype[name] = method;
        }
      });

      function Class() {
        if(!initializing && this.init) {
          this.init.apply(this, arguments);
        }
      }

      // Populate our constructed prototype object
      Class.prototype = prototype;
      Class.prototype.constructor = Class;
      Class.extend = extend$1;

      Class._collection = {};
      Class.register = function(name, code) {
        if(typeof this._collection[name] !== 'undefined') {
          warn('"' + name + '" is already registered. Overwriting...');
        }
        this._collection[name] = code;
      };

      Class.unregister = function(name) {
        this._collection[name] = void 0;
      };

      Class.getCollection = function() {
        return this._collection;
      };

      //define a method or field in this prototype
      Class.define = function(name, value) {
        this.prototype[name] = value;
      };

      //get an item of the collection from this class
      Class.get = function(name, silent) {
        if(this._collection.hasOwnProperty(name)) {
          return this._collection[name];
        }
        if(!silent) {
          warn('"' + name + '" was not found.');
        }
        return false;
      };
      //register extension by name
      if(arguments.length > 1 && this.register) {
        this.register(name, Class);
      }
      return Class;
    }

    var Class = function() {};
    Class.extend = extend$1;

    function Promise(resolver) {
      if(!(this instanceof Promise)) {
        return new Promise(resolver);
      }
      this.status = 'pending';
      this.value;
      this.reason;
      // then may be called multiple times on the same promise
      this._resolves = [];
      this._rejects = [];
      if(isFn(resolver)) {
        resolver(this.resolve.bind(this), this.reject.bind(this));
      }
      return this;
    }

    Promise.prototype.then = function(resolve, reject) {
      var next = this._next || (this._next = Promise());
      var status = this.status;
      var x;
      if('pending' === status) {
        isFn(resolve) && this._resolves.push(resolve);
        isFn(reject) && this._rejects.push(reject);
        return next;
      }
      if('resolved' === status) {
        if(!isFn(resolve)) {
          next.resolve(resolve);
        } else {
          try {
            x = resolve(this.value);
            resolveX(next, x);
          } catch(e) {
            this.reject(e);
          }
        }
        return next;
      }
      if('rejected' === status) {
        if(!isFn(reject)) {
          next.reject(reject);
        } else {
          try {
            x = reject(this.reason);
            resolveX(next, x);
          } catch(e) {
            this.reject(e);
          }
        }
        return next;
      }
    };
    Promise.prototype.resolve = function(value) {
      if('rejected' === this.status) {
        throw Error('Illegal call.');
      }
      this.status = 'resolved';
      this.value = value;
      this._resolves.length && fireQ(this);
      return this;
    };
    Promise.prototype.reject = function(reason) {
      if('resolved' === this.status) {
        throw Error('Illegal call. ' + reason);
      }
      this.status = 'rejected';
      this.reason = reason;
      this._rejects.length && fireQ(this);
      return this;
    };
    // shortcut of promise.then(undefined, reject)
    Promise.prototype.catch = function(reject) {
      return this.then(void 0, reject);
    };
    // return a promise with another promise passing in
    Promise.cast = function(arg) {
      var p = Promise();
      if(arg instanceof Promise) {
        return resolvePromise(p, arg);
      } else {
        return Promise.resolve(arg);
      }
    };
    // return a promise which resolved with arg
    // the arg maybe a thanable object or thanable function or other
    Promise.resolve = function(arg) {
      var p = Promise();
      if(isThenable(arg)) {
        return resolveThen(p, arg);
      } else {
        return p.resolve(arg);
      }
    };
    // accept a promises array,
    // return a promise which will resolsed with all promises's value,
    // if any promise passed rejectd, the returned promise will rejected with the same reason
    Promise.all = function(promises) {
      var len = promises.length;
      var promise = Promise();
      var r = [];
      var pending = 0;
      var locked;
      var test = promises;
      //modified
      forEach(promises, function(p, i) {
        p.then(function(v) {
          r[i] = v;
          if((pending += 1) === len && !locked) {
            promise.resolve(r);
          }
        }, function(e) {
          locked = true;
          promise.reject(e);
        });
      });
      return promise;
    };
    // accept a promises array,
    // return a promise which will resolsed with the first resolved promise passed,
    // if any promise passed rejectd, the returned promise will rejected with the same reason
    Promise.any = function(promises) {
      var promise = Promise();
      var called;
      //modified
      forEach(promises, function(p, i) {
        p.then(function(v) {
          if(!called) {
            promise.resolve(v);
            called = true;
          }
        }, function(e) {
          called = true;
          promise.reject(e);
        });
      });
      return promise;
    };
    // return a promise which reject with reason
    // reason must be an instance of Error object
    Promise.reject = function(reason) {
      if(!(reason instanceof Error)) {
        throw Error('reason must be an instance of Error');
      }
      var p = Promise();
      p.reject(reason);
      return p;
    };

    function resolveX(promise, x) {
      if(x === promise) {
        promise.reject(new Error('TypeError'));
      }
      if(x instanceof Promise) {
        return resolvePromise(promise, x);
      } else if(isThenable(x)) {
        return resolveThen(promise, x);
      } else {
        return promise.resolve(x);
      }
    }

    function resolvePromise(promise1, promise2) {
      var status = promise2.status;
      if('pending' === status) {
        promise2.then(promise1.resolve.bind(promise1), promise1.reject.bind(promise1));
      }
      if('resolved' === status) {
        promise1.resolve(promise2.value);
      }
      if('rejected' === status) {
        promise1.reject(promise2.reason);
      }
      return promise;
    }

    function resolveThen(promise, thanable) {
      var called;
      var resolve = once(function(x) {
        if(called) {
          return;
        }
        resolveX(promise, x);
        called = true;
      });
      var reject = once(function(r) {
        if(called) {
          return;
        }
        promise.reject(r);
        called = true;
      });
      try {
        thanable.then.call(thanable, resolve, reject);
      } catch(e) {
        if(!called) {
          throw e;
        } else {
          promise.reject(e);
        }
      }
      return promise;
    }

    function fireQ(promise) {
      var status = promise.status;
      var queue = promise['resolved' === status ? '_resolves' : '_rejects'];
      var arg = promise['resolved' === status ? 'value' : 'reason'];
      var fn;
      var x;
      while(fn = queue.shift()) {
        x = fn.call(promise, arg);
        x && resolveX(promise._next, x);
      }
      return promise;
    }

    function isFn(fn) {
      return 'function' === type(fn);
    }

    function type(obj) {
      var o = {};
      return o.toString.call(obj).replace(/\[object (\w+)\]/, '$1').toLowerCase();
    }

    function isThenable(obj) {
      return obj && obj.then && isFn(obj.then);
    }

    function once(fn) {
      var called;
      var r;
      return function() {
        if(called) {
          return r;
        }
        called = true;
        return r = fn.apply(this, arguments);
      };
    }

    /**
     * Initializes the reader.
     * @param {Object} reader_info Information about the reader
     */
    var Reader = Class.extend({
      init: function(reader_info) {
        this._name = this._name || reader_info.reader;
        this._data = reader_info.data || [];
        this._basepath = this._basepath || reader_info.path || null;
        this._parsers = reader_info.parsers;

        if(this._parsers) {
          this._data = mapRows(this._data, this._parsers);
        }
      },

      /**
       * Reads from source
       * @param {Array} queries Queries to be performed
       * @param {String} language language
       * @returns a promise that will be resolved when data is read
       */
      read: function(queries, language) {
        return new Promise.resolve();
      },

      /**
       * Gets the data
       * @returns all data
       */
      getData: function() {
        return this._data;
      }
    });

    var Data = Class.extend({

      init: function() {
        this._collection = {};
      },

      /**
       * Loads resource from reader or cache
       * @param {Array} query Array with queries to be loaded
       * @param {String} language Language
       * @param {Object} reader Which reader to use - data reader info
       * @param {*} evts ?
       */
      load: function(query, language, reader, evts) {
        var _this = this;
        var promise = new Promise();
        var wait = new Promise().resolve();
        var cached = query === true ? true : this.isCached(query, language, reader);
        var loaded = false;
        //if result is cached, dont load anything
        if(!cached) {
          timeStamp('Vizabi Data: Loading Data');
          if(evts && typeof evts.load_start === 'function') {
            evts.load_start();
          }
          wait = new Promise();
          this.loadFromReader(query, language, reader).then(function(queryId) {
            loaded = true;
            cached = queryId;
            wait.resolve();
          }, function(err) {
            warn(err);
            wait.reject();
          });
        }
        wait.then(function() {
          //pass the data forward
          var data = _this._collection[cached].data;
          //not loading anymore
          if(loaded && evts && typeof evts.load_end === 'function') {
            evts.load_end();
          }
          promise.resolve(cached);
        }, function() {
          //not loading anymore
          if(loaded && evts && typeof evts.load_end === 'function') {
            evts.load_end();
          }
          promise.reject();
        });
        return promise;
      },

      /**
       * Loads resource from reader
       * @param {Array} query Array with queries to be loaded
       * @param {String} lang Language
       * @param {Object} reader Which reader to use. E.g.: "json"
       * @param {String} path Where data is located
       */
      loadFromReader: function(query, lang, reader) {
        var _this = this;
        var promise = new Promise();
        var reader_name = reader.reader;
        var queryId = hashCode([
          query,
          lang,
          reader
        ]);

        // joining multiple queries
        // create a queue which this datamanager writes all queries to
        this.queryQueue = this.queryQueue || [];
        this.queryQueue.push({ query: query, queryId: queryId, promise: promise, reader: reader});

        // wait one execution round for the queue to fill up
        defer(function() {
          // now the query queue is filled with all queries from one execution round

          var mergedQueries = [];
          var willExecute = false;

          // check every query in the queue
          _this.queryQueue = _this.queryQueue.filter(function(queueItem) {
            if (queueItem.query == query) {
              // Query is still in the queue so this is the first deferred query with same requested rows (where & group) to reach here. 
              // This will be the base query which will be executed; It will be extended by other queries in the queue.
              mergedQueries.push(queueItem);
              willExecute = true;

              // remove so that other queries won't merge it
              return false;
            } else {
              // check if the requested rows are similar
              if (comparePlainObjects(queueItem.query.where, query.where)
               && comparePlainObjects(queueItem.query.grouping, query.grouping)
                ) {

                // if so, merge the selects to the base query
                Array.prototype.push.apply(query.select, queueItem.query.select);
                // merge parsers so the reader can parse the newly added columns
                extend(reader.parsers, queueItem.reader.parsers);

                // include query's promise to promises for base query
                mergedQueries.push(queueItem);

                // remove queueItem from queue as it's merged in the current query
                return false;
              }
            } 
            // otherwise keep it in the queue, so it can be joined with another query
            return true;
          });

          if (!willExecute) return;

          // make the promise a collection of all promises of merged queries
          // promise = promises.length ? Promise.all(promises) : new Promise.resolve();

          // remove double columns from select (resulting from merging)
          // no double columns in formatter because it's an object, extend would've overwritten doubles
          query.select = unique(query.select);

          //create hash for dimensions only query
          var dim, dimQ, dimQId = 0; 
          dimQ = clone(query);
          dim = keys(dimQ.grouping);
          if (arrayEquals(dimQ.select.slice(0, dim.length), dim)) {
            dimQ.select = dim;
            dimQId = hashCode([
              dimQ,
              lang,
              reader
            ]);
          }

          // Create a new reader for this query
          var readerClass = Reader.get(reader_name);
          if (!readerClass) {
            throw new Error('Unknown reader: ' + reader_name);
          }
          var r = new readerClass(reader);

          // execute the query with this reader
          r.read(query, lang).then(function() {

              //success reading
              var values$$ = r.getData();
              var q = query;

              //make sure all queried is returned
              values$$ = values$$.map(function(d) {
                for(var i = 0; i < q.select.length; i += 1) {
                  var col = q.select[i];
                  if(typeof d[col] === 'undefined') {
                    d[col] = null;
                  }
                }
                return d;
              });

              _this._collection[queryId] = {};
              var col = _this._collection[queryId];
              col.data = values$$;
              col.filtered = {};
              col.nested = {};
              col.unique = {};
              col.limits = {};
              col.limitsPerFrame = {};
              col.frames = {};
              col.query = q;
              // col.sorted = {}; // TODO: implement this for sorted data-sets, or is this for the server/(or file reader) to handle?

              // returning the query-id/values of the merged query without splitting the result up again per query
              // this is okay because the collection-object above will only be passed by reference to the cache and this will not take up more memory. 
              // On the contrary: it uses less because there is no need to duplicate the key-columns.
              forEach(mergedQueries, function(mergedQuery) {
                // set the cache-location for each seperate query to the combined query's cache
                _this._collection[mergedQuery.queryId] = _this._collection[queryId]; 
                // resolve the query
                mergedQuery.promise.resolve(mergedQuery.queryId);
              });
      
              //create cache record for dimension only query
              if(dimQId !== 0) {
                _this._collection[dimQId] = _this._collection[queryId];              
              }
              //promise.resolve(queryId);
            }, //error reading
            function(err) { 
              forEach(mergedQueries, function(mergedQuery) {
                mergedQuery.promise.reject(err);
              });
            }
          );

        })

        return promise;
      },

      /**
       * get data
       */
      get: function(queryId, what, whatId, args) {
        // if not specified data from what query, return nothing
        if(!queryId) return warn("Data.js 'get' method doesn't like the queryId you gave it: " + queryId);

        // if they want data, return the data
        if(!what || what == 'data') {
          return this._collection[queryId]['data'];
        }

        // if they didn't give an instruction, give them the whole thing
        // it's probably old code which modifies the data outside this class
        // TODO: move these methods inside (e.g. model.getNestedItems())
        if (!whatId) {
          return this._collection[queryId][what];
        }

        // if they want a certain processing of the data, see if it's already in cache
        var id = JSON.stringify(whatId);
        if(this._collection[queryId][what][id]) {
          return this._collection[queryId][what][id];
        }

        // if it's not cached, process the data and then return it
        switch(what) {
          case 'unique':
            this._collection[queryId][what][id] = this._getUnique(queryId, whatId);
            break;
          case 'filtered':
            this._collection[queryId][what][id] = this._getFiltered(queryId, whatId);
            break;
          case 'limits':
            this._collection[queryId][what][id] = this._getLimits(queryId, whatId);
            break;
          case 'limitsPerFrame':
            this._collection[queryId][what][id] = this._getLimitsPerFrame(queryId, whatId, args);
            break;
          case 'frames':
            this._collection[queryId][what][id] = this._getFrames(queryId, whatId, args);
            break;
          case 'nested':     
            this._collection[queryId][what][id] = this._getNested(queryId, whatId);
            break;
        }
        return this._collection[queryId][what][id];
      },

      getMetadata: function(which){
          if(!globals.metadata || !globals.metadata.indicatorsDB) return {};
          return which ? globals.metadata.indicatorsDB[which] : globals.metadata.indicatorsDB;
      },
        
      /**
       * Gets the metadata of all hooks
       * @returns {Object} metadata
       */
      getIndicatorsTree: function() {
        return globals.metadata && globals.metadata.indicatorsTree ? globals.metadata.indicatorsTree : {};
      },
        
      
      /**
       * Get regularised dataset (where gaps are filled)
       * @param {Number} queryId hash code for query
       * @param {Array} framesArray -- array of keyframes across animatable
       * @param {Object} indicatorsDB -- indicators DB from globals.metadata
       * @returns {Object} regularised dataset, nested by [animatable, column, key]
       */
      _getFrames: function(queryId, framesArray) {
          var _this = this;
          
          var indicatorsDB = this.getMetadata();
          
          if(!indicatorsDB) warn("_getFrames in data.js is missing indicatorsDB, it's needed for gap filling")
          if(!framesArray) warn("_getFrames in data.js is missing framesArray, it's needed so much")
                
          //TODO: thses should come from state or from outside somehow
          var KEY = "geo";
          var TIME = "time";
          var result = {};
          var filtered = {};
          var items, itemsIndex, oneFrame, method, use, next;
          
          // We _nest_ the flat dataset in two levels: first by “key” (example: geo), then by “animatable” (example: year)
          // See the _getNested function for more details
          var nested = this.get(queryId, 'nested', [KEY, TIME]);
          var keys = Object.keys(nested);
          
          // Get the list of columns that are in the dataset, exclude key column and animatable column
          // Example: [“lex”, “gdp”, “u5mr"]
          var query = this._collection[queryId].query;
          var columns = query.select.filter(function(f){return f != KEY && f != TIME && f !== "_default"});
          
          var fLength = framesArray.length;
          var kLength = keys.length;
          var cLength = columns.length;
          var frame, f, key, k, column, c;
          
          // FramesArray in the input contains the array of keyframes in animatable dimension. 
          // Example: array of years like [1800, 1801 … 2100]
          // these will be the points where we need data 
          // (some of which might already exist in the set. in regular datasets all the points would exist!)
          
          // Check if query.where clause is missing a time field
          if(!query.where.time){          
              // The query.where clause doesn't have time field for properties: 
              // we populate the regular set with a single value (unpack properties into constant time series)
              var dataset = _this._collection[queryId].data;
              
              for(f=0; f<fLength; f++){
                  frame = framesArray[f];
                  
                  result[frame] = {};
                  for(c=0; c<cLength; c++) result[frame][columns[c]] = {};
                  
                  for(var i=0; i<dataset.length; i++){   
                      var d = dataset[i];
                      for(c=0; c<cLength; c++) result[frame][columns[c]][d[KEY]] = d[columns[c]];
                  };
              };
              
              
          }else{
              // If there is a time field in query.where clause, then we are dealing with indicators in this request
              
              // Put together a template for cached filtered sets (see below what's needed)
              for(k=0; k<kLength; k++){
                  filtered[keys[k]] = {};
                  for(c=0; c<cLength; c++) filtered[keys[k]][columns[c]] = null;
              };

              // Now we run a 3-level loop: across frames, then across keys, then and across data columns (lex, gdp)
              for(f=0; f<fLength; f++){
                frame = framesArray[f];
                result[frame] = {};
                for(c=0; c<cLength; c++) result[frame][columns[c]] = {};

                for(k=0; k<kLength; k++){
                    key = keys[k];
                    
                    for(c=0; c<cLength; c++){
                        column = columns[c];       
                        
                        //If there are some points in the array with valid numbers, then
                        //interpolate the missing point and save it to the “clean regular set” 
                        method = indicatorsDB[column] ? indicatorsDB[column].interpolation : null;
                        use = indicatorsDB[column] ? indicatorsDB[column].use : "indicator";
                        

                        // Inside of this 3-level loop is the following: 
                        if(nested[key] && nested[key][frame] && (nested[key][frame][0][column] || nested[key][frame][0][column] === 0)){

                            // Check if the piece of data for [this key][this frame][this column] exists 
                            // and is valid. If so, then save it into a “clean regular set”
                            result[frame][column][key] = nested[key][frame][0][column];
                            
                        }else{
                            // If the piece of data doesn’t exist or is invalid, then we need to inter- or extapolate it

                            // Let’s take a slice of the nested set, corresponding to the current key nested[key] 
                            // As you remember it has the data nested further by frames. 
                            // At every frame the data in the current column might or might not exist. 
                            // Thus, let’s filter out all the frames which don’t have the data for the current column. 
                            // Let’s cache it because we will most likely encounter another gap in the same column for the same key

                            items = filtered[key][column];

                            if(items == null){                            
                                var givenFrames = Object.keys(nested[key]);
                                items = new Array(givenFrames.length);
                                itemsIndex = 0;

                                for(var z = 0, length = givenFrames.length; z<length; z++){
                                    oneFrame = nested[key][givenFrames[z]];
                                    if(oneFrame[0][column] || oneFrame[0][column] === 0) items[itemsIndex++] = oneFrame[0];
                                };
                                
                                //trim the length of the array
                                items.length = itemsIndex;
                            }


                            // Now we are left with a fewer frames in the filtered array. Let's check its length. 
                            //If the array is empty, then the entire column is missing for the key
                            //So we let the key have missing values in this column for all frames
                            if(items.length > 0) {
                                next = null;
                                result[frame][column][key] = interpolatePoint(items, use, column, next, TIME, frame, method);
                            }

                        }
                            

                    }; //loop across columns
                }; //loop across keys
              }; //loop across frameArray
          }
          
          return result;
      },


      _getNested: function(queryId, order) {
        // Nests are objects of key-value pairs
        // Example: 
        // 
        // order = ["geo", "time"];
        // 
        // original_data = [
        //   { geo: "afg", time: 1800, gdp: 23424, lex: 23}
        //   { geo: "afg", time: 1801, gdp: 23424, lex: null}
        //   { geo: "chn", time: 1800, gdp: 23587424, lex: 46}
        //   { geo: "chn", time: 1801, gdp: null, lex: null}
        // ];
        //  
        // nested_data = {
        //   afg: {
        //     1800: {gdp: 23424, lex: 23},
        //     1801: {gdp: 23424, lex: null}
        //   }
        //   chn: {
        //     1800: {gdp: 23587424, lex: 46 },
        //     1801: {gdp: null, lex: null }
        //   }
        // };

        var nest = d3.nest();
        for(var i = 0; i < order.length; i++) {
          nest = nest.key(
            (function(k) {
              return function(d) {
                return d[k];
              };
            })(order[i])
          );
        };

        return nestArrayToObj(nest.entries(this._collection[queryId]['data']));
      },
        

      _getUnique: function(queryId, attr) {
        var uniq;
        var items = this._collection[queryId].data;
        //if it's an array, it will return a list of unique combinations.
        if(isArray(attr)) {
          var values$$ = items.map(function(d) {
            return clone(d, attr); //pick attrs
          });
          uniq = unique(values$$, function(n) {
            return JSON.stringify(n);
          });
        } //if it's a string, it will return a list of values
        else {
          var values$$ = items.map(function(d) {
            return d[attr];
          });
          uniq = unique(values$$);
        }
        return uniq;
      },

      _getFiltered: function(queryId, filter$$) {
        return filter(this._collection[queryId].data, filter$$);
      },
        
        
      _getLimitsPerFrame: function(queryId, args) {
        var result = {};
        var values$$ = [];
          
        var frames = this.get(queryId, 'frames', args.framesArray);
          
        forEach(frames, function(frame, t){
            result[t] = {};
            
            values$$ = values(frame[args.which]);
            
            result[t] = !values$$ || !values$$.length ? {max: 0, min: 0} : {
                max: d3.max(values$$), 
                min: d3.min(values$$)
            }
        });    
        
        return result;
      },
        
      _getLimits: function(queryId, attr) {

        var items = this._collection[queryId].data;
        // get only column attr and only rows with number or date
        var filtered = items.reduce(function(filtered, d) {
          
          // check for dates
          var f = (isDate(d[attr])) ? d[attr] : parseFloat(d[attr]);

          // if it is a number
          if(!isNaN(f)) {
            filtered.push(f);
          }

          //filter
          return filtered;
        }, []);

        // get min/max for the filtered rows
        var min;
        var max;
        var limits = {};
        for(var i = 0; i < filtered.length; i += 1) {
          var c = filtered[i];
          if(typeof min === 'undefined' || c < min) {
            min = c;
          }
          if(typeof max === 'undefined' || c > max) {
            max = c;
          }
        }
        limits.min = min || 0;
        limits.max = max || 100;
        return limits;    
      },

      /**
       * checks whether this combination is cached or not
       */
      isCached: function(query, language, reader) {
        //encode in hashCode
        var q = hashCode([
          query,
          language,
          reader
        ]);
        //simply check if we have this in internal data
        if(Object.keys(this._collection).indexOf(q) !== -1) {
          return q;
        }
        return false;
      }
    });

    var _freezeAllEvents = false;
    var _frozenEventInstances = [];
    var _freezeAllExceptions = {};

    var DefaultEvent = Class.extend({

      source: '',
      type: 'default',

      init: function(source, type) {
        this.source = source;
        if (type) this.type = type;
      }

    });

    var ChangeEvent = DefaultEvent.extend('change', {

      type: 'change',

      init: function(source) {
        this._super(source);
      }

    })

    var EventSource = Class.extend({

      /**
       * Initializes the event class
       */
      init: function() {
        this._id = this._id || uniqueId('e');
        this._events = {};
        //freezing events
        this._freeze = false;
        this._freezer = [];
        this._freezeExceptions = {};
      },

      /**
       * Binds a callback function to an event: part 1: split grouped parameters in seperate calls
       * @param {String} type type of event
       * @param {String|Array} target path to object the event should be bound to or array of target paths
       * @param {Function|Array} func function to be bound with event or array with functions
       */
      on: function(type, path, func) {
        
        // if parameters had to be split up in seperate calls, don't continue with this call
        if (this.splitEventParameters(type, path, func, this.on))
          return;

        // get the target model
        var target = this.traversePath(path);
        if (!target) return;

        // register the event to this object
        target._events[type] = target._events[type] || [];
        if(typeof func === 'function') {
          target._events[type].push(func);
        } else {
          warn('Can\'t bind event \'' + type + '\'. It must be a function.');
        }
      },


      /**
       * Unbinds all events associated with a name or a specific one
       * @param {String|Array} name name of event or array with names
       */
      off: function(type, path, func) {

        // if no arguments, unbind all
        if (arguments.length == 0) {
          this._events = {};
          return;
        }

        // if parameters had to be split up in seperate calls, don't continue with this call
        if (this.splitEventparameters(type, path, func, this.off))
          return;

        // get target model
        var target = this.traversePath(path);
        if (!target) return;

        // unbind events
        if(this._events.hasOwnProperty(type)) {
          // if function not given, remove all events of type
          if (typeof func === 'undefined') {
            this._events[type] = [];
            return;
          }
          var index = this._events[type].indexOf(func);
          if (index > -1) {
            array.splice(index, 1);
          } else {
            warn('Could not unbind function ' + func.name + '. Function not in bound function list.');
          }
        }
      },

      /**
       * Split grouped event parameters to seperate calls to given funtion
       * @param {String|Object|Array} type type of event
       * @param {String|Array} target path to object the event should be bound to or array of target paths
       * @param {Function|Array} func function to be bound with event or array with functions
       * @param {Function} eventFunc function to further process the split up parameters 
       * @return {Boolean} true if the parameters where split up, false if nothing was split up
       * eventFunc is mostly arguments.callee but this is deprecated in ECMAscript 5: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/arguments/callee
       */
      splitEventParameters: function(type, path, func, eventFunc) {
        var i;
        var calls = [];

        // multiple at a time, array format: [{type: function}, {'type:path': function}, ... ]
        // seems redundant but used so that binding-sets won't be turned into models (which happens when it's a pure object). Used e.g. in Tool.init();
        if(isArray(type)) {
          for(i = 0; i < type.length; i += 1) {
            eventFunc.call(this, type[i], func);
          }
          return true;
        }

        //multiple at a time, object format: {type: function, 'type:path': function, ... }
        if(isObject(type)) {
          for(i in type) {
            eventFunc.call(this, i, type[i]);
          }
          return true;
        }

        // type and path are both in type: on('type:path', function)
        // or
        // path undefined: on('type', function)
        if(typeof path === 'function') {
          func = path; // put callback function in func variable
          // on('type:path', func)
          if (type.indexOf(':') !== -1) {
            var split = type.split(':');  
            type = split[0];
            path = split[1];
          } 
          // on(type, func)
          else {
            path = undefined;
          }
          eventFunc.call(this, type, path, func);
          return true;
        }

        // bind multiple paths at a time to one function: on(type, [path1, path2], function)
        if(isArray(path)) {
          for(i = 0; i < path.length; i += 1) {
            eventFunc.call(this, type, path[i], func);
          }
          return true;
        }

        //bind multiple functions at the same time to one path: on(type, path, [function1, function2])
        if(func && isArray(func)) {
          for(i = 0; i < func.length; i += 1) {
            eventFunc.call(this, type, path, func[i]);
          }
          return true;
        }   
        return false;
      },

      /**
       * // TODO: if events will not be strictly model-bound, this might have to move to model.
       * Traverse path down the model tree
       * @param {String|Array} target path to object that should be returned. Either in string or array form
       */
      traversePath: function(path) {

        // if there's no path to traverse
        if (typeof path === 'undefined' || isArray(path) && path.length == 0) {
          return this; 
        }

        // prepare path to array
        if (typeof path === 'string') {
          path = path.split('.');
        }
        
        // check if path is an array
        if (!isArray(path)) {
          error$1('Path is wrong type. Path should be a string or array but is ' + typeof path + '.');
          return null;
        }

        // descent to next child to find target object
        var currentTarget = path.shift();
        if (this[currentTarget] === undefined)
          warn('Can\'t find child "' + currentTarget + '" of the model ' + this._name + '.');
        else
          return this.getModelObject(currentTarget).traversePath(path);
      },

      /**
       * Triggers an event, adding it to the buffer
       * @param {String|Array} name name of event or array with names
       * @param args Optional arguments (values to be passed)
       */
      trigger: function(evtType, args) {
        var i;
        var size;

        // split up eventType-paremeter for multiple event-triggers
        if(isArray(evtType)) {
          for(i = 0, size = evtType.length; i < size; i += 1) {
            this.trigger(evtType[i], args);
          }
          return;
        }

        // create an event-object if necessary
        var evt;
        if ((evtType instanceof DefaultEvent)) {
          evt = evtType;
        } else {
          var eventClass = DefaultEvent.get(evtType, true); // silent
          if(eventClass) {
            evt = new eventClass(this);
          } else {
            evt = new DefaultEvent(this, evtType);
          }
        } 

        // if this eventType has no events registered
        if(!this._events.hasOwnProperty(evt.type)) {
          return;
        }

        // for each function registered to this eventType on this object
        var _this = this;
        forEach(this._events[evt.type], function(func) {

          // prepare execution
          var execute = function() {
            var msg = 'Vizabi Event: ' + evt.type; // + ' - ' + eventPath;
            timeStamp(msg);
            func.apply(_this, [
              evt,
              args
            ]);
          };

          //TODO: improve readability of freezer code
          //only execute if not frozen and exception doesnt exist
          if(_this._freeze || _freezeAllEvents) {
            //if exception exists for freezing, execute
            if(_freezeAllEvents && _freezeAllExceptions.hasOwnProperty(name) || !_freezeAllEvents && _this._freeze &&
              _this._freezeExceptions.hasOwnProperty(name)) {
              execute();
            } //otherwise, freeze it
            else {
              _this._freezer.push(execute);
              if(_freezeAllEvents && !_frozenEventInstances[_this._id]) {
                _this.freeze();
                _frozenEventInstances[_this._id] = _this;
              }
            }
          } else {
            execute();
          }
        })    
      },

      /**
       * Prevents all events from being triggered, buffering them
       */
      freeze: function(exceptions) {
        this._freeze = true;
        if(!exceptions) {
          return;
        }
        if(!isArray(exceptions)) {
          exceptions = [exceptions];
        }
        for(var i = 0; i < exceptions.length; i += 1) {
          this._freezeExceptions[exceptions[i]] = true;
        }
      },

      /**
       * triggers all frozen events
       */
      unfreeze: function() {
        this._freeze = false;
        this._freezeExceptions = {};
        //execute old frozen events
        while(this._freezer.length) {
          var execute = this._freezer.shift();
          execute();
        }
      },

      /**
       * clears all frozen events
       */
      clearFrozen: function() {
        this._freeze = false;
        this._freezeExceptions = {};
        this._freezer = [];
      }
    });

    EventSource.freezeAll = freezeAll;
    EventSource.unfreezeAll = unfreezeAll;

    //generic event functions
    /**
     * freezes all events
     */
    function freezeAll(exceptions) {
      _freezeAllEvents = true;
      if(!exceptions) {
        return;
      }
      if(!isArray(exceptions)) {
        exceptions = [exceptions];
      }
      forEach(exceptions, function(e) {
        _freezeAllExceptions[e] = true;
      });
    };

    /**
     * triggers all frozen events form all instances
     */
    function unfreezeAll() {
      _freezeAllEvents = false;
      _freezeAllExceptions = {};
      //unfreeze all instances
      var keys = Object.keys(_frozenEventInstances);
      for(var i = 0; i < keys.length; i++) {
        var instance = _frozenEventInstances[keys[i]];
        if(!instance) {
          continue;
        }
        instance.unfreeze();
      }
      _frozenEventInstances = {};
    };

    var Intervals = Class.extend({

      /**
       * Initializes intervals
       */
      init: function() {
        this.intervals = {};
      },

      /**
       * Sets an interval
       * @param {String} name name of interval
       * @param {Function} func function to be executed
       * @param {Number} duration duration in milliseconds
       */
      setInterval: function(name, func, duration) {
        this.clearInterval(name);
        this.intervals[name] = setInterval(func, duration);
      },

      /**
       * Clears an interval
       * @param {String} name name of interval to be removed
       */
      clearInterval: function(name) {
        return name ? clearInterval(this.intervals[name]) : this.clearAllIntervals();
      },

      /**
       * Clears all intervals
       */
      clearAllIntervals: function() {
        for(var i in this.intervals) {
          this.clearInterval(i);
        }
      }
    });

    var _DATAMANAGER = new Data();

    var ModelLeaf = EventSource.extend({

      _name: '',
      _parent: null,
      _persistent: true,

      init: function(name, value, parent, binds) {

        // getter and setter for the value
        Object.defineProperty(this, 'value', {
          get: this.get,
          set: this.set
        });
        Object.defineProperty(this, 'persistent', {
          get: function() { return this._persistent; }
        });

        this._super();

        this._name = name;
        this._parent = parent;
        this.value = value;
        this.on(binds); // after super so there is an .events object
      },

      // if they want a persistent value and the current value is not persistent, return the last persistent value
      get: function(persistent) {
        return (persistent && !this._persistent) ? this._persistentVal : this._val;
      },

      set: function(val, force, persistent) {
        if (force || (this._val !== val && JSON.stringify(this._val) !== JSON.stringify(val))) {

          // persistent defaults to true
          persistent = (typeof persistent !== 'undefined') ? persistent : true;
     
          // set leaf properties
          if (persistent) this._persistentVal = val; // set persistent value if change is persistent.
          this._val = val;
          this._persistent = persistent;

          // trigger change event
          this.trigger(new ChangeEvent(this), this._name);
        }
      },

      // duplicate from Model. Should be in a shared parent class.
      setTreeFreezer: function(freezerStatus) {
        if (freezerStatus) {
          this.freeze();
        } else {
          this.unfreeze();
        }
      }

    })

    var Model = EventSource.extend({
      /**
       * Initializes the model.
       * @param {Object} values The initial values of this model
       * @param {Object} parent reference to parent
       * @param {Object} bind Initial events to bind
       * @param {Boolean} freeze block events from being dispatched
       */
      init: function(name, values, parent, bind) {
        this._type = this._type || 'model';
        this._id = this._id || uniqueId('m');
        this._data = {};
        //holds attributes of this model
        this._parent = parent;
        this._name = name;
        this._ready = false;
        this._readyOnce = false;
        //has this model ever been ready?
        this._loadedOnce = false;
        this._loading = [];
        //array of processes that are loading
        this._intervals = getIntervals(this);
        //holds the list of dependencies for virtual models
        this._deps = {
          parent: [],
          children: []
        };
        //will the model be hooked to data?
        this._space = {};
        this._spaceDims = {};

        this._dataId = false;
        this._limits = {};
        //stores limit values
        this._super();

        //initial values
        if(values) {
          this.set(values);
        }
        // bind initial events
        // bind after setting, so no events are fired by setting initial values
        if(bind) {
          this.on(bind);
        }
      },

      /* ==========================
       * Getters and Setters
       * ==========================
       */

      /**
       * Gets an attribute from this model or all fields.
       * @param attr Optional attribute
       * @returns attr value or all values if attr is undefined
       */
      get: function(attr) {
        if(!attr) {
          return this._data;
        }
        if (isModel(this._data[attr]))
          return this._data[attr];
        else
          return this._data[attr].value; // return leaf value
      },

      /**
       * Sets an attribute or multiple for this model (inspired by Backbone)
       * @param attr property name
       * @param val property value (object or value)
       * @param {Boolean} force force setting of property to value and triggers set event
       * @param {Boolean} persistent true if the change is a persistent change
       * @returns defer defer that will be resolved when set is done
       */
      set: function(attr, val, force, persistent) {
        var setting = this._setting;
        var attrs;
        var freezeCall = false; // boolean, indicates if this .set()-call froze the modelTree
        
        //expect object as default
        if(!isPlainObject(attr)) {
          (attrs = {})[attr] = val;
        } else {
          // move all arguments one place
          attrs = attr;
          persistent = force;
          force = val;
        }

        //we are currently setting the model
        this._setting = true;

        // Freeze the whole model tree if not frozen yet, so no events are fired while setting
        if (!this._freeze) {
          freezeCall = true;
          this.setTreeFreezer(true);
        }

        // init/set all given values
        var newSubmodels = false;
        for(var a in attrs) {
          val = attrs[a];

          var bothModel = isPlainObject(val) && this._data[a] instanceof Model;
          var bothModelLeaf = !isPlainObject(val) && this._data[a] instanceof ModelLeaf;
          
          if (this._data[a] && (bothModel || bothModelLeaf)) {
            // data type does not change (model or leaf and can be set through set-function)
            this._data[a].set(val, force, persistent);
          } else {
            // data type has changed or is new, so initializing the model/leaf
            this._data[a] = initSubmodel(a, val, this);
            newSubmodels = true;
          }
        }

        // only if there's new submodels, we have to set new getters/setters
        if (newSubmodels)
          bindSettersGetters(this);

        if(this.validate && !setting) {
          this.validate();
        }

        if(!setting || force) {
          this._setting = false;
          if(!this.isHook()) {
            this.setReady();
          }
        }
        
        // if this set()-call was the one freezing the tree, now the tree can be unfrozen (i.e. all setting is done)
        if (freezeCall) {
          this.setTreeFreezer(false);
        }

      },


      setTreeFreezer: function(freezerStatus) {
        // first traverse down
        // this ensures deepest events are triggered first
        forEach(this._data, function(submodel) {
          submodel.setTreeFreezer(freezerStatus);
        });

        // then freeze/unfreeze
        if (freezerStatus) {
          this.freeze();
        } else {
          this.unfreeze();
        }
      },

      /**
       * Gets the type of this model
       * @returns {String} type of the model
       */
      getType: function() {
        return this._type;
      },



      /**
       * Gets all submodels of the current model
       * @param {Object} object [object=false] Should it return an object?
       * @param {Function} fn Validation function
       * @returns {Array} submodels
       */
      getSubmodels: function(object, fn) {
        var submodels = (object) ? {} : [];
        var fn = fn || function() {
          return true;
        };
        var _this = this;
        forEach(this._data, function(s, name) {
          if(s && typeof s._id !== 'undefined' && isModel(s) && fn(s)) {
            if(object) {
              submodels[name] = s;
            } else {
              submodels.push(s);
            }
          }
        });
        return submodels;
      },

      /**
       * Gets the current model and submodel values as a JS object
       * @returns {Object} All model as JS object, leafs will return their values
       */
      getPlainObject: function(persistent) {
        var obj = {};
        forEach(this._data, function(dataItem, i) {
          // if it's a submodel
          if(dataItem instanceof Model) {
            obj[i] = dataItem.getPlainObject(persistent);
          } 
          // if it's a modelLeaf
          else {
            obj[i] = dataItem.get(persistent);
          }
        });
        return obj;
      },


      /**
       * Gets the requested object, including the leaf-object, not the value
       * @returns {Object} Model or ModelLeaf object.
       */
      getModelObject: function(name) {
        if (name)
          return this._data[name];
        else
          return this;
      },

      /**
       * Clears this model, submodels, data and events
       */
      clear: function() {
        var submodels = this.getSubmodels();
        for(var i in submodels) {
          submodels[i].clear();
        }
        this._spaceDims = {};
        this.setReady(false);
        this.off();
        this._intervals.clearAllIntervals();
        this._data = {};
      },

      /**
       * Validates data.
       * Interface for the validation function implemented by a model
       * @returns Promise or nothing
       */
      validate: function() {},

      /* ==========================
       * Model loading
       * ==========================
       */

      /**
       * checks whether this model is loading anything
       * @param {String} optional process id (to check only one)
       * @returns {Boolean} is it loading?
       */
      isLoading: function(p_id) {
        if(this.isHook() && (!this._loadedOnce || this._loadCall)) {
          return true;
        }
        if(p_id) {
          return this._loading.indexOf(p_id) !== -1;
        } //if loading something
        else if(this._loading.length > 0) {
          return true;
        } //if not loading anything, check submodels
        else {
          var submodels = this.getSubmodels();
          var i;
          for(i = 0; i < submodels.length; i += 1) {
            if(submodels[i].isLoading()) {
              return true;
            }
          }
          for(i = 0; i < this._deps.children.length; i += 1) {
            var d = this._deps.children[i];
            if(d.isLoading() || !d._ready) {
              return true;
            }
          }
          return false;
        }
      },

      /**
       * specifies that the model is loading data
       * @param {String} id of the loading process
       */
      setLoading: function(p_id) {
        //if this is the first time we're loading anything
        if(!this.isLoading()) {
          this.trigger('load_start');
        }
        //add id to the list of processes that are loading
        this._loading.push(p_id);
      },

      /**
       * specifies that the model is done with loading data
       * @param {String} id of the loading process
       */
      setLoadingDone: function(p_id) {
        this._loading = without(this._loading, p_id);
        this.setReady();
      },

      /**
       * Sets the model as ready or not depending on its loading status
       */
      setReady: function(value) {
        if(value === false) {
          this._ready = false;
          if(this._parent && this._parent.setReady) {
            this._parent.setReady(false);
          }
          return;
        }
        //only ready if nothing is loading at all
        var prev_ready = this._ready;
        this._ready = !this.isLoading() && !this._setting && !this._loadCall;
        // if now ready and wasn't ready yet
        if(this._ready && prev_ready !== this._ready) {
          if(!this._readyOnce) {
            this._readyOnce = true;
            this.trigger('readyOnce');
          }
          this.trigger('ready');
        }
      },

      /**
       * loads data (if hook)
       * Hooks loads data, models ask children to load data
       * Basically, this method:
       * loads is theres something to be loaded:
       * does not load if there's nothing to be loaded
       * @param {Object} options (includes splashScreen)
       * @returns defer
       */
      load: function(opts) {

        opts = opts || {};
        var splashScreen = opts.splashScreen || false;

        var _this = this;
        var data_hook = this._dataModel;
        var language_hook = this._languageModel;
        var query = this.getQuery(splashScreen);
        var promiseLoad = new Promise();
        var promises = [];
        //useful to check if in the middle of a load call
        this._loadCall = true;

        //load hook
        //if its not a hook, the promise will not be created
        if(this.isHook() && data_hook && query) {
          //hook changes, regardless of actual data loading
          this.trigger('hook_change');
          //get reader info
          var reader = data_hook.getPlainObject();
          reader.parsers = this._getAllParsers();

          var lang = language_hook ? language_hook.id : 'en';
          var promise = new Promise();
          var evts = {
            'load_start': function() {
              _this.setLoading('_hook_data');
              EventSource.freezeAll([
                'load_start',
                'resize',
                'dom_ready'
              ]);
            }
          };

          timeStamp('Vizabi Model: Loading Data: ' + _this._id);
          _DATAMANAGER.load(query, lang, reader, evts).then(function(dataId) {
            _this._dataId = dataId;
            timeStamp('Vizabi Model: Data loaded: ' + _this._id);
            _this.afterLoad();
            promise.resolve();
          }, function(err) {
            warn('Problem with query: ', JSON.stringify(query));
            promise.reject(err);
          });
          promises.push(promise);
        }

        //load submodels as well
        forEach(this.getSubmodels(true), function(sm, name) {
          promises.push(sm.load(opts));
        });

        //when all promises/loading have been done successfully
        //we will consider this done
        var wait = promises.length ? Promise.all(promises) : new Promise.resolve();
        wait.then(function() {

          //only validate if not showing splash screen to avoid fixing the year
          if(_this.validate) {
            _this.validate();
          }
          timeStamp('Vizabi Model: Model loaded: ' + _this._id);
          //end this load call
          _this._loadedOnce = true;

          //we need to defer to make sure all other submodels
          //have a chance to call loading for the second time
          _this._loadCall = false;
          promiseLoad.resolve();
          defer(function() {
            _this.setReady();
          });
        }, function() {
          _this.trigger('load_error');
          promiseLoad.reject();
        });

        return promiseLoad;
      },

      /**
       * executes after preloading processing is done
       */
      afterPreload: function() {
        var submodels = this.getSubmodels();
        forEach(submodels, function(s) {
          s.afterPreload();
        });
      },

      /**
       * executes after data has actually been loaded
       */
      afterLoad: function() {
        EventSource.unfreezeAll();
        this.setLoadingDone('_hook_data');
      },

      /**
       * removes all external dependency references
       */
      resetDeps: function() {
        this._deps.children = [];
      },

      /**
       * add external dependency ref to this model
       */
      addDep: function(child) {
        this._deps.children.push(child);
        child._deps.parent.push(this);
      },

      /**
       * gets query that this model/hook needs to get data
       * @returns {Array} query
       */
      getQuery: function(splashScreen) {

        var dimensions, filters, select, grouping, orderBy, q;

        //if it's not a hook, no query is necessary
        if(!this.isHook()) return true;
        //error if there's nothing to hook to
        if(Object.keys(this._space).length < 1) {
          error$1('Error:', this._id, 'can\'t find the space');
          return true;
        }

        var prop = (this.use === "property");
        var exceptions = (prop) ? { exceptType: 'time' } : {};

        // select
        dimensions = this._getAllDimensions(exceptions);
        if(this.use !== 'constant') dimensions = dimensions.concat([this.which]);
        select = unique(dimensions);

        // where 
        filters = this._getAllFilters(exceptions, splashScreen);
        
        // grouping
        grouping = this._getGrouping();

        // order by
        orderBy = (!prop) ? this._space.time.dim : null;

        //return query
        return {
          'select': select,
          'where': filters,
          'grouping': grouping,
          'orderBy': orderBy // should be _space.animatable, but that's time for now
        };
      },

      /* ===============================
       * Hooking model to external data
       * ===============================
       */

      /**
       * is this model hooked to data?
       */
      isHook: function() {
        return this.use ? true : false;
      },
      /**
       * Hooks all hookable submodels to data
       */
      setHooks: function() {
        if(this.isHook()) {
          //what should this hook to?
          this.hookModel();
        } else {
          //hook submodels
          var submodels = this.getSubmodels();
          forEach(submodels, function(s) {
            s.setHooks();
          });
        }
      },

      /**
       * Hooks this model to data, entities and time
       * @param {Object} h Object containing the hooks
       */
      hookModel: function() {
        var _this = this;
        var spaceRefs = getSpace(this);
        // assuming all models will need data and language support
        this._dataModel = getClosestModel(this, 'data');
        this._languageModel = getClosestModel(this, 'language');
        //check what we want to hook this model to
        forEach(spaceRefs, function(name) {
          //hook with the closest prefix to this model
          _this._space[name] = getClosestModel(_this, name);
          //if hooks change, this should load again
          //TODO: remove hardcoded 'show"
          if(_this._space[name].show) {
            _this._space[name].on('change:show', function(evt) {
              //hack for right size of bubbles
              if(_this._type === 'size' && _this.which === _this.which_1) {
                _this.which_1 = '';
              };
              //defer is necessary because other events might be queued.
              //load right after such events
              defer(function() {
                _this.load().then(function() {

                }, function(err) {
                  warn(err);
                });
              });
            });
          }
        });
        //this is a hook, therefore it needs to reload when data changes
        this.on('change:which', function(evt) {
          //defer is necessary because other events might be queued.
          //load right after such events
          _this.load();
        });
        //this is a hook, therefore it needs to reload when data changes
        this.on('hook_change', function() {
          _this._spaceDims = {};
          _this.setReady(false);
        });
      },

      /**
       * Gets all submodels of the current model that are hooks
       * @param object [object=false] Should it return an object?
       * @returns {Array|Object} hooks array or object
       */
      getSubhooks: function(object) {
        return this.getSubmodels(object, function(s) {
          return s.isHook();
        });
      },

      /**
       * gets all sub values for a certain hook
       * only hooks have the "hook" attribute.
       * @param {String} type specific type to lookup
       * @returns {Array} all unique values with specific hook use
       */
      getHookWhich: function(type) {
        var values = [];
        if(this.use && this.use === type) {
          values.push(this.which);
        }
        //repeat for each submodel
        forEach(this.getSubmodels(), function(s) {
          values = unique(values.concat(s.getHookWhich(type)));
        });
        //now we have an array with all values in a type of hook for hooks.
        return values;
      },

      /**
       * gets all sub values for indicators in this model
       * @returns {Array} all unique values of indicator hooks
       */
      getIndicators: function() {
        return this.getHookWhich('indicator');
      },

      /**
       * gets all sub values for indicators in this model
       * @returns {Array} all unique values of property hooks
       */
      getProperties: function() {
        return this.getHookWhich('property');
      },

      /**
       * Gets the dimension of this model if it has one
       * @returns {String|Boolean} dimension
       */
      getDimension: function() {
        return this.dim || false; //defaults to dim if it exists
      },

      /**
       * Gets the dimension (if entity) or which (if hook) of this model
       * @returns {String|Boolean} dimension
       */
      getDimensionOrWhich: function() {
        return this.dim || (this.use != 'constant' ? this.which : false); //defaults to dim or which if it exists
      },

      /**
       * Gets the filter for this model if it has one
       * @returns {Object} filters
       */
      getFilter: function() {
        return {}; //defaults to no filter
      },


      /**
       * maps the value to this hook's specifications
       * @param value Original value
       * @returns hooked value
       */
      mapValue: function(value) {
        return value;
      },


      /**
       * gets filtered dataset with fewer keys
       * @param {Object} filter
       * @returns {Object} filtered items object
       */
      getFilteredItems: function(filter) {
        if(!filter) return warn("No filter provided to getFilteredItems(<filter>)");
        return _DATAMANAGER.get(this._dataId, 'filtered', filter);
      },
        
      /**
       * gets nested dataset
       * @param {Array} keys define how to nest the set
       * @returns {Object} hash-map of key-value pairs
       */
      getNestedItems: function(keys) {
        if(!keys) return warn("No keys provided to getNestedItems(<keys>)");
        return _DATAMANAGER.get(this._dataId, 'nested', keys);
      },


      /**
       * Gets formatter for this model
       * @returns {Function|Boolean} formatter function
       */
      getParser: function() {
        //TODO: default formatter is moved to utils. need to return it to hook prototype class, but retest #1212 #1230 #1253
        return null;
      },

      getDataManager: function(){
        return _DATAMANAGER;
      },

      /**
       * Gets limits
       * @param {String} attr parameter
       * @returns {Object} limits (min and max)
       */
      getLimits: function(attr) {
        return _DATAMANAGER.get(this._dataId, 'limits', attr);
      },

      /**
       * gets all hook dimensions
       * @param {Object} opts options with exceptType or onlyType
       * @returns {Array} all unique dimensions
       */
      _getAllDimensions: function(opts) {

        var optsStr = JSON.stringify(opts);
        if(optsStr in this._spaceDims) {
          return this._spaceDims[optsStr];
        }

        opts = opts || {};
        var dims = [];
        var dim;

        var models = this._space;
        //in case it's a parent of hooks
        if(!this.isHook() && this.space) {
          models = [];
          var _this = this;
          forEach(this.space, function(name) {
            models.push(getClosestModel(_this, name));
          });
        }

        forEach(models, function(m) {
          if(opts.exceptType && m.getType() === opts.exceptType) {
            return true;
          }
          if(opts.onlyType && m.getType() !== opts.onlyType) {
            return true;
          }
          if(dim = m.getDimension()) {
            dims.push(dim);
          }
        });

        this._spaceDims[optsStr] = dims;

        return dims;
      },

      /**
       * gets first dimension that matches type
       * @param {Object} options
       * @returns {Array} all unique dimensions
       */
      _getFirstDimension: function(opts) {
        opts = opts || {};

        var models = this._space;
        //in case it's a parent of hooks
        if(!this.isHook() && this.space) {
          models = [];
          var _this = this;
          forEach(this.space, function(name) {
            models.push(getClosestModel(_this, name));
          });
        }

        var dim = false;
        forEach(models, function(m) {
          if(opts.exceptType && m.getType() !== opts.exceptType) {
            dim = m.getDimension();
            return false;
          } else if(opts.type && m.getType() === opts.type) {
            dim = m.getDimension();
            return false;
          } else if(!opts.exceptType && !opts.type) {
            dim = m.getDimension();
            return false;
          }
        });
        return dim;
      },

      /**
       * gets all hook filters
       * @param {Boolean} splashScreen get filters for first screen only
       * @returns {Object} filters
       */
      _getAllFilters: function(opts, splashScreen) {
        opts = opts || {};
        var filters = {};
        forEach(this._space, function(h) {
          if(opts.exceptType && h.getType() === opts.exceptType) {
            return true;
          }
          if(opts.onlyType && h.getType() !== opts.onlyType) {
            return true;
          }
          filters = extend(filters, h.getFilter(splashScreen));
        });
        return filters;
      },

      /**
       * gets grouping for each of the used entities
       * @param {Boolean} splashScreen get filters for first screen only
       * @returns {Object} filters
       */
      _getGrouping: function() {
        var groupings = {};
        forEach(this._space, function(h) {
          groupings[h.dim] = h.grouping || undefined;
        });
        return groupings;
      },

      /**
       * gets all hook filters
       * @returns {Object} filters
       */
      _getAllParsers: function() {

        var parsers = {};

        function addParser(model) {
          // get parsers from model
          var parser = model.getParser();
          var column = model.getDimensionOrWhich();
          if (parser && column) {
            parsers[column] = parser;
          }
        }

        // loop through all models which can have filters
        forEach(this._space, function(h) {
          addParser(h);
        });
        addParser(this);

        return parsers;
      },

      getDefaults: function() {
        // if defaults are set, does not care about defaults from children
        if(this._defaults) return this._defaults;
        var d = {};
        forEach(this.getSubmodels(true), function(model, name) {
          d[name] = model.getDefaults();
        });
        return d;
      }

    });

    /* ===============================
     * Private Helper Functions
     * ===============================
     */

    /**
     * Checks whether an object is a model or not
     * if includeLeaf is true, a leaf is also seen as a model
     */
    function isModel(model, includeLeaf) {
      return model && (model.hasOwnProperty('_data') || (includeLeaf &&  model.hasOwnProperty('_val')));
    }

    /**
     * Binds all attributes in _data to magic setters and getters
     */
    function bindSettersGetters(model) {
      for(var prop in model._data) {
        Object.defineProperty(model, prop, {
          configurable: true,
          //allow reconfiguration
          get: function(p) {
            return function() {
              return model.get(p);
            };
          }(prop),
          set: function(p) {
            return function(value) {
              return model.set(p, value);
            };
          }(prop)
        });
      }
    }

    /**
     * Loads a submodel, when necessaary
     * @param {String} attr Name of submodel
     * @param {Object} val Initial values
     * @param {Object} ctx context / parent model
     * @returns {Object} model new submodel
     */
    function initSubmodel(attr, val, ctx) {

      var submodel;

      // if value is a value -> leaf
      if(!isPlainObject(val) || isArray(val)) {  

        var binds = {
          //the submodel has changed (multiple times)
          'change': onChange
        }
        submodel = new ModelLeaf(attr, val, ctx, binds);
      }

      // if value is an object -> model
      else {

        var binds = {
          //the submodel has changed (multiple times)
          'change': onChange,
          //loading has started in this submodel (multiple times)
          'hook_change': onHookChange,
          //loading has started in this submodel (multiple times)
          'load_start': onLoadStart,
          //loading has failed in this submodel (multiple times)
          'load_error': onLoadError,
            //loading has ended in this submodel (multiple times)
          'ready': onReady
        };

        // if the value is an already instantiated submodel (Model or ModelLeaf)
        // this is the case for example when a new componentmodel is made (in Component._modelMapping)
        // it takes the submodels from the toolmodel and creates a new model for the component which refers 
        // to the instantiated submodels (by passing them as model values, and thus they reach here)
        if (isModel(val, true)) {
          submodel = val;
          submodel.on(binds);
        } 
        // if it's just a plain object, create a new model
        else {
          // construct model
          var modelType = attr.split('_')[0];
          var Modl = Model.get(modelType, true) || models[modelType] || Model;
          submodel = new Modl(attr, val, ctx, binds);
          // model is still frozen but will be unfrozen at end of original .set()
        }
      }

      return submodel;

      // Default event handlers for models
      function onChange(evt, path) {
        if(!ctx._ready) return; //block change propagation if model isnt ready
        path = ctx._name + '.' + path
        ctx.trigger(evt, path);    
      }
      function onHookChange(evt, vals) {
        ctx.trigger(evt, vals);
      }
      function onLoadStart(evt, vals) {
        ctx.setReady(false);
        ctx.trigger(evt, vals);
      }
      function onLoadError(evt, vals) {
        ctx.trigger(evt, vals);
      }
      function onReady(evt, vals) {
        //trigger only for submodel
        ctx.setReady(false);
        //wait to make sure it's not set false again in the next execution loop
        defer(function() {
          ctx.setReady();
        });
        //ctx.trigger(evt, vals);
      }
    }

    /**
     * gets closest interval from this model or parent
     * @returns {Object} Intervals object
     */
    function getIntervals(ctx) {
      if(ctx._intervals) {
        return ctx._intervals;
      } else if(ctx._parent) {
        return getIntervals(ctx._parent);
      } else {
        return new Intervals();
      }
    }

    /**
     * gets closest prefix model moving up the model tree
     * @param {String} prefix
     * @returns {Object} submodel
     */
    function getClosestModel(ctx, name) {
      var model = findSubmodel(ctx, name);
      if(model) {
        return model;
      } else if(ctx._parent) {
        return getClosestModel(ctx._parent, name);
      }
    }

    /**
     * find submodel with name that starts with prefix
     * @param {String} prefix
     * @returns {Object} submodel or false if nothing is found
     */
    function findSubmodel(ctx, name) {
      for(var i in ctx._data) {
        //found submodel
        if(i === name && isModel(ctx._data[i])) {
          return ctx._data[i];
        }
      }
    }

    /**
     * Learn what this model should hook to
     * @returns {Array} space array
     */
    function getSpace(model) {
      if(isArray(model.space)) {
        return model.space;
      } else if(model._parent) {
        return getSpace(model._parent);
      } else {
        error$1(
          'ERROR: space not found.\n You must specify the objects this hook will use under the "space" attribute in the state.\n Example:\n space: ["entities", "time"]'
        );
      }
    }

    /*!
     * VIZABI Time Model
     */

    // short-cut for developers to get UTC date strings
    // not meant to be used in code!!!
    Date.prototype.utc = Date.prototype.toUTCString;

    /*
     * Time formats for internal data
     * all in UTC
     */
    var formats = {
      'year':    d3.time.format.utc('%Y'),
      'month':   d3.time.format.utc('%Y%m'),
      'day':     d3.time.format.utc('%Y%m%d'),
      'hour':    d3.time.format.utc("%Y-%m-%d %H"),
      'minute':  d3.time.format.utc("%Y-%m-%d %H:%M"),
      'second':  d3.time.format.utc("%Y-%m-%d %H:%M:%S"),
      'week':    weekFormat(),   // %Yw%W d3 week format does not comply with ISO
      'quarter': quarterFormat() // %Yq%Q d3 does not support quarters
    };

    var TimeModel = Model.extend({

      /**
       * Default values for this model
       */
      _defaults: {
        dim: "time",
        value: "2015",
        start: "1800",
        end: "2015",
        playable: true,
        playing: false,
        loop: false,
        round: 'round',
        delay: 300,
        delayStart: 1200,
        delayEnd: 75,
        delayThresholdX2: 300,
        delayThresholdX4: 150,
        delaySet: false,
        unit: "year",
        step: 1, //step must be integer
        adaptMinMaxZoom: false, //TODO: remove from here. only for bubble chart
        xLogStops: [], //TODO: remove from here. only for mountain chart
        yMaxMethod: "latest", //TODO: remove from here. only for mountain chart
        record: false,
        probeX: 0, //TODO: remove from here. only for mountain chart
        tailFatX: 1, //TODO: remove from here. only for mountain chart
        tailCutX: 0, //TODO: remove from here. only for mountain chart
        tailFade: 1, //TODO: remove from here. only for mountain chart
        xScaleFactor: 1, //TODO: remove from here. only for mountain chart
        xScaleShift: 0, //TODO: remove from here. only for mountain chart
        xPoints: 50 //TODO: remove from here. only for mountain chart
      },

      /**
       * Initializes the language model.
       * @param {Object} values The initial values of this model
       * @param parent A reference to the parent model
       * @param {Object} bind Initial events to bind
       */
      init: function(name, values, parent, bind) {

        this._type = "time";
        //default values for time model
        var defaults = deepClone(this._defaults);
        values = extend(defaults, values);

        //same constructor
        this._super(name, values, parent, bind);

        var _this = this;
        this.timeFormat = formats[this.unit];
        this.dragging = false;
        this.postponePause = false;
        this.allSteps = {};
        this.delayAnimations = this.delay;

        //bing play method to model change
        this.on({

          "change:playing": function() {
            if(_this.playing === true) {
              _this._startPlaying();
            } else {
              _this._stopPlaying();
            }
          },

          "change:unit": function() {
            this.timeFormat = formats[this.unit];
          }

        });
      },

      /**
       * Formats value, start and end dates to actual Date objects
       */
      _formatToDates: function() {

        var date_attr = ["value", "start", "end"];
        for(var i = 0; i < date_attr.length; i++) {
          var attr = date_attr[i];
          if(!isDate(this[attr])) {
            var date = this.parseToUnit(this[attr].toString(), this.unit);
            this.set(attr, date);
          }
        }
      },

      /*
       * Formatting and parsing functions
       * @param {Date} date
       * @param {String} unit
       * @returns {String}
       */
      format: function(dateObject, unit) {
        unit = unit || this.unit;
        return formats[unit] ? formats[unit](dateObject) : formats['year'](dateObject);
      },

      parseToUnit: function(timeString, unit) {
        unit = unit || this.unit;
        return formats[unit] ? formats[unit].parse(timeString) : null;
      },

      parse: function(timeString) {
        var keys = Object.keys(formats), i = 0; 
        for (; i < keys.length; i++) {
          var dateObject = formats[keys[i]].parse(timeString);
          if (date) return { unit: keys[i], time: dateObject };
        }
        return null;
      },


      /**
       * Validates the model
       */
      validate: function() {

        //unit has to be one of the available_time_units
        if(!formats[this.unit]) {
          warn(this.unit + ' is not a valid time unit, using "year" instead.');
          this.unit = "year";
        }

        if(this.step < 1) {
          this.step = 1;
        }

        //make sure dates are transformed into dates at all times
        if(!isDate(this.start) || !isDate(this.end) || !isDate(this.value)) {
          this._formatToDates();
        }

        //end has to be >= than start
        if(this.end < this.start) {
          this.end = new Date(this.start);
        }
        //value has to be between start and end
        if(this.value < this.start) {
          this.value = new Date(this.start);
        } else if(this.value > this.end) {
          this.value = new Date(this.end);
        }

        if(this.playable === false && this.playing === true) {
          this.playing = false;
        }
      },

      /**
       * Plays time
       */
      play: function() {
        this._startPlaying();
      },

      /**
       * Pauses time
       */
      pause: function(soft) {
        if(soft) {
          this.postponePause = true;
        } else {
          this.playing = false;
        }
      },

      /**
       * Indicates dragging of time
       */
      dragStart: function() {
        this.dragging = true;
      },
      dragStop: function() {
        this.dragging = false;
      },


      /**
       * gets time range
       * @returns range between start and end
       */
      getRange: function() {
        var is = this.getIntervalAndStep();
        return d3.time[is.interval].utc.range(this.start, this.end, is.step);
      },

      /** 
       * gets the d3 interval and stepsize for d3 time interval methods
       * D3's week-interval starts on sunday and it does not support a quarter interval
       * 
       **/
      getIntervalAndStep: function() {
        var d3Interval, step;
        switch (this.unit) {
          case 'week': d3Interval = 'monday'; step = this.step; break;
          case 'quarter': d3Interval = 'month'; step = this.step*3; break;
          default: d3Interval = this.unit; step = this.step; break;
        }
        return { interval: d3Interval, step: step };
      },

      /**
       * Gets filter for time
       * @param {Boolean} firstScreen get filter for current year only
       * @returns {Object} time filter
       */
      getFilter: function(firstScreen) {
        var start = this.timeFormat(this.start);
        var end = this.timeFormat(this.end);
        var value = this.timeFormat(this.value);
        var dim = this.getDimension();
        var filter = {};

        filter[dim] = (firstScreen) ? [
          [value]
        ] : [
          [start, end]
        ];
        return filter;
      },

      /**
       * Gets formatter for this model
       * @returns {Function} formatter function
       */
      getParser: function() {
        return this.timeFormat.parse;
      },

      /**
       * Gets an array with all time steps for this model
       * @returns {Array} time array
       */
      getAllSteps: function() {
        var hash = "" + this.start + this.end + this.step;
        
        //return if cached
        if(this.allSteps[hash]) return this.allSteps[hash];
          
        this.allSteps[hash] = [];
        var curr = this.start;
        while(curr <= this.end) {
          var is = this.getIntervalAndStep();
          this.allSteps[hash].push(curr);
          curr = d3.time[is.interval].utc.offset(curr, is.step);
        }
        return this.allSteps[hash];
      },

      /**
       * Snaps the time to integer
       * possible inputs are "start", "end", "value". "value" is default
       */
      snap: function(what) {
        if(!this.round) return;
        if(what == null) what = "value";
        var op = 'round';
        if(this.round === 'ceil') op = 'ceil';
        if(this.round === 'floor') op = 'floor';
        var is = this.getIntervalAndStep();
        var time = d3.time[is.interval].utc[op](this[what]);
        this.set(what, time, true); //3rd argumennt forces update
      },

      /**
       * Starts playing the time, initializing the interval
       */
      _startPlaying: function() {
        //don't play if it's not playable
        if(!this.playable) return;

        var _this = this;

        //go to start if we start from end point
        if(this.value >= this.end) {
          _this.getModelObject('value').set(_this.start, null, false /*make change non-persistent for URL and history*/);
        } else {
          //the assumption is that the time is already snapped when we start playing
          //because only dragging the timeslider can un-snap the time, and it snaps on drag.end
          //so we don't need this line. let's see if we survive without.
          //as a consequence, the first time update in playing sequence will have this.playing flag up
          //so the bubble chart will zoom in smoothly. Closes #1213
          //this.snap();
        }
        this.playing = true;
        this.playInterval();

        this.trigger("play");
      },

      playInterval: function(){
        if(!this.playing) return;
        var _this = this;
        this.delayAnimations = this.delay;
        if(this.delay < this.delayThresholdX2) this.delayAnimations*=2;
        if(this.delay < this.delayThresholdX4) this.delayAnimations*=2;

        this._intervals.setInterval('playInterval_' + this._id, function() {
          // when time is playing and it reached the end
          if(_this.value >= _this.end) {
            // if looping
            if(_this.loop) {
              // reset time to start, silently
              _this.getModelObject('value').set(_this.start, null, false /*make change non-persistent for URL and history*/);
            } else {
              _this.playing = false;
            }
            return;
          } else {

            _this._intervals.clearInterval('playInterval_' + _this._id);

            if(_this.postponePause || !_this.playing) {
              _this.playing = false;
              _this.postponePause = false;
              _this.getModelObject('value').set(_this.value, true, true /*force the change and make it persistent for URL and history*/);
            } else {
              var is = _this.getIntervalAndStep();
              if(_this.delay < _this.delayThresholdX2) is.step*=2;
              if(_this.delay < _this.delayThresholdX4) is.step*=2;
              var time = d3.time[is.interval].utc.offset(_this.value, is.step);
              if(time >= _this.end) {
                // if no playing needed anymore then make the last update persistent and not overshooting
                _this.getModelObject('value').set(_this.end, null, true /*force the change and make it persistent for URL and history*/);
              }else{
                _this.getModelObject('value').set(time, null, false /*make change non-persistent for URL and history*/);
              }
              _this.playInterval();
            }
          }
        }, this.delayAnimations);

      },

      /**
       * Stops playing the time, clearing the interval
       */
      _stopPlaying: function() {
        this._intervals.clearInterval('playInterval_' + this._id);
        //this.snap();
        this.trigger("pause");
      }

    });

    /*
     * Week Format to format and parse dates
     * Conforms with ISO8601
     * Follows format: YYYYwWW: 2015w04, 3845w34, 0020w53
     */ 
    function weekFormat() {

      var format = function(d) {
        return formatWeekYear(d) + 'w' + formatWeek(d);
      }
      
      format.parse = function parse(dateString) {
        var matchedDate = dateString.match(/^(\d{4})w(\d{2})$/);
        return matchedDate ? getDateFromWeek(matchedDate[1], matchedDate[2]): null;
      };
      
      var formatWeekYear = function(d) {
          var origin = +d;
          return new Date(origin + ((4 - (d.getUTCDay() || 7)) * 86400000)).getUTCFullYear();
      };
      
      var formatWeek = function(d) {
        var origin = +d;
        var quote = new Date(origin + ((4 - (d.getUTCDay() || 7)) * 86400000))
        var week = Math.ceil(((quote.getTime() - quote.setUTCMonth(0, 1)) / 86400000 + 1) / 7);
        return week < 10 ? '0' + week : week;
      };
      
      var getDateFromWeek = function(p1, p2) {
        var week = parseInt(p2);
        var year = p1;
        var startDateOfYear = new Date(); // always 4th of January (according to standard ISO 8601)
        startDateOfYear.setUTCFullYear(year);
        startDateOfYear.setUTCMonth(0);
        startDateOfYear.setUTCDate(4);
        var startDayOfWeek = startDateOfYear.getUTCDay() || 7;
        var dayOfWeek = 1; // Monday === 1
        var dayOfYear = week * 7 + dayOfWeek - (startDayOfWeek + 4);

        var date = formats['year'].parse(year);
        date = new Date(date.getTime() + dayOfYear * 24 * 60 * 60 * 1000);

        return date;
      }
      
      return format;
      
    };

    /*
     * Quarter Format to format and parse quarter dates
     * A quarter is the month%3
     * Follows format: YYYYqQ: 2015q4, 5847q1, 0040q2
     */ 
    function quarterFormat() {
      
      var format = function(d) {
        return formats['year'](d) + 'q' + formatQuarter(d)
      }
      
      format.parse = function(dateString) {
        var matchedDate = dateString.match(/^(\d{4})q(\d)$/);
        return matchedDate ? getDateFromQuarter(matchedDate[1], matchedDate[2]): null;
      }

      var formatQuarter = function(d) {
        return ((d.getUTCMonth() / 3) | 0) + 1;
      };
     
      var getDateFromQuarter = function(p1, p2) {
        var quarter = parseInt(p2);
        var month = 3 * quarter - 2; // first month in quarter
        var year = p1;
        return formats['month'].parse([year, (month < 9 ? '0': '') + month].join(''));
      }   
      
      return format;
    }

    /*!
     * HOOK MODEL
     */


    var Hook = Model.extend({
        /**
       * Gets tick values for this hook
       * @returns {Number|String} value The value for this tick
       */
      tickFormatter: function(x, formatterRemovePrefix) {

        // Assumption: a hook has always time in its space
        if(isDate(x)) return this._space.time.timeFormat(x);
        if(isString(x)) return x;

        var format = "f";
        var prec = 0;
        if(Math.abs(x) < 1) {
          prec = 1;
          format = "r"
        };

        var prefix = "";
        if(formatterRemovePrefix) return d3.format("." + prec + format)(x);

        //---------------------
        // BEAUTIFIERS GO HOME!
        // don't break formatting please
        //---------------------
        switch(Math.floor(Math.log(Math.abs(x))/Math.LN10)) {
          case -13: x = x * 1000000000000; prefix = "p"; break; //0.1p
          case -10: x = x * 1000000000; prefix = "n"; break; //0.1n
          case -7: x = x * 1000000; prefix = "µ"; break; //0.1µ
          case -6: x = x * 1000000; prefix = "µ"; break; //1µ
          case -5: x = x * 1000000; prefix = "µ"; break; //10µ
          case -4: break; //0.0001
          case -3: break; //0.001
          case -2: break; //0.01
          case -1: break; //0.1
          case 0:  break; //1
          case 1:  break; //10
          case 2:  break; //100
          case 3:  break; //1000
          case 4:  break; //10000
          case 5:  x = x / 1000; prefix = "k"; break; //0.1M
          case 6:  x = x / 1000000; prefix = "M"; prec = 1; break; //1M
          case 7:  x = x / 1000000; prefix = "M"; break; //10M
          case 8:  x = x / 1000000; prefix = "M"; break; //100M
          case 9:  x = x / 1000000000; prefix = "B"; prec = 1; break; //1B
          case 10: x = x / 1000000000; prefix = "B"; break; //10B
          case 11: x = x / 1000000000; prefix = "B"; break; //100B
          case 12: x = x / 1000000000000; prefix = "T"; prec = 1; break; //1T
          //use the D3 SI formatting for the extreme cases
          default: return(d3.format("." + prec + "s")(x)).replace("G", "B");
        }

        // use manual formatting for the cases above
        return(d3.format("." + prec + format)(x) + prefix).replace("G", "B");

      },
        
      /**
       * Gets the d3 scale for this hook. if no scale then builds it
       * @returns {Array} domain
       */
      getScale: function(margins) {
        if(!this.scale) {
          this.buildScale(margins);
        }
        return this.scale;
      },

      /**
       * Gets the domain for this hook
       * @returns {Array} domain
       */
      buildScale: function() {
        if(!this.isHook()) {
          return;
        }
        var domain;
        var scaleType = this.scaleType || 'linear';
        switch(this.use) {
          case 'indicator':
            var limits = this.getLimits(this.which);
            domain = [
              limits.min,
              limits.max
            ];
            break;
          case 'property':
            domain = this.getUnique(this.which);
            break;
          default:
            domain = [this.which];
            break;
        }
        //TODO: d3 is global?
        this.scale = scaleType === 'time' ? d3.time.scale.utc().domain(domain) : d3.scale[scaleType]().domain(domain);
      },
        
          //TODO: this should go down to datamanager, hook should only provide interface
      /**
       * gets maximum, minimum and mean values from the dataset of this certain hook
       */
      gerLimitsPerFrame: function() {
          
        if(this.use === "property") return warn("getMaxMinMean: strange that you ask min max mean of a property"); 
        if(!this.isHook) return warn("getMaxMinMean: only works for hooks");
          
        var result = {};
        var values = [];
        var value = null;
          
        var steps = this._parent._parent.time.getAllSteps();
          
        if(this.use === "constant") {
            steps.forEach(function(t){ 
                value = this.which;
                result[t] = {
                    min: value,
                    max: value
                }
            });

        }else if(this.which==="time"){
            steps.forEach(function(t){ 
                value = new Date(t);
                result[t] = {
                    min: value,
                    max: value
                }
            });

        }else{
            var args = {framesArray: steps, which: this.which};
            result = this.getDataManager().get(this._dataId, 'limitsPerFrame', args);   
        }
          
        return result;
      },
        
        
         /**
         * Gets unique values in a column
         * @param {String|Array} attr parameter
         * @returns {Array} unique values
         */
        getUnique: function(attr) {
            if(!this.isHook()) return;
            if(!attr) attr = this._getFirstDimension({type: "time"});
            return this.getDataManager().get(this._dataId, 'unique', attr);
        },
        
        
      /**
       * gets the items associated with this hook without values
       * @param filter filter
       * @returns hooked value
       */
      getKeys: function(filter) {
          // If there is no _dataId, then no data was loaded
          if (!this._dataId) return warn("hook:getKeys() -- returning nothing since no data is loaded");
          
          //all dimensions except time (continuous)
          var dimensions = this._getAllDimensions({exceptType: 'time'});
          var excluded = this._getAllDimensions({onlyType: 'time'});

          return this.getUnique(dimensions).map(function(item) {
            forEach(excluded, function(e) {
              if(filter && filter[e]) {
                item[e] = filter[e];
              }
            });
            return item;
          });
      },
        
      /**
       * Gets the metadata of the hook's "which"
       * @returns {Object} metadata
       */
      getMetadata: function() {
        return this.use !== 'constant' ? this.getDataManager().getMetadata(this.which) : {};
      }    
    });

    /*
     * VIZABI Stack Model
     */

    var palettes = {
      'ALL': "all",
      _default: "none"
    };

    var StackModel = Hook.extend({

      /**
       * Default values for this model
       */
      _defaults: {
        use: null,
        which: null,
        merge: false
      },
      /**
       * Initializes the stack hook
       * @param {Object} values The initial values of this model
       * @param parent A reference to the parent model
       * @param {Object} bind Initial events to bind
       */
      init: function(name, values, parent, bind) {

        this._type = "model";
        //TODO: add defaults extend to super
        var defaults = deepClone(this._defaults);
        values = extend(defaults, values);
        this._super(name, values, parent, bind);
      },

      /**
       * Validates a color hook
       */
      validate: function() {
        //there must be no scale
        if(this.scale) this.scale = null;

        //use must not be "indicator" 
        if(this.use === "indicator") {
          warn("stack model: use must not be 'indicator'. Resetting use to 'constant' and which to '" + palettes._default)
          this.use = "constant";
          this.which = palettes._default;
        }

        //if use is "constant"
        if(this.use === "constant" && values(palettes).indexOf(this.which) == -1) {
          warn("stack model: the requested value '" + this.which + "' is not allowed. resetting to '" +
            palettes._default)
          this.which == palettes._default;
        }
      },

      /**
       * Get the above constants
       */
      getPalettes: function() {
        return palettes;
      },

      /**
       * There must be no scale
       */
      buildScale: function() {}

    });

    /*
     * VIZABI Size Model
     */

    var SizeModel = Hook.extend({

      /**
       * Default values for this model
       */
      _defaults: {
        use: null,
        domainMin: 0,
        domainMax: 1,
        which: null
      },

      /**
       * Initializes the size hook
       * @param {Object} values The initial values of this model
       * @param parent A reference to the parent model
       * @param {Object} bind Initial events to bind
       */
      init: function(name, values, parent, bind) {

        this._type = "size";
        //TODO: add defaults extend to super
        var defaults = deepClone(this._defaults);
        values = extend(defaults, values);
        this._super(name, values, parent, bind);
      },

      /**
       * Validates a size hook
       */
      validate: function() {
        //there must be a min and a max
        if(typeof this.domainMin === 'undefined' || this.domainMin < 0) this.domainMin = 0;
        if(typeof this.domainMax === 'undefined' || this.domainMax > 1) this.domainMax = 1;

        if(this.domainMax < this.domainMin) this.set('domainMin', this.domainMax, true);

        //value must always be between min and max
        if(this.use === "constant" && this.which > this.domainMax) this.which = this.domainMax;
        if(this.use === "constant" && this.which < this.domainMin) this.which = this.domainMin;
        
        if(!this.scaleType) this.scaleType = 'linear';
        if(this.use === "property") this.scaleType = 'ordinal';
        
        //TODO a hack that kills the scale, it will be rebuild upon getScale request in model.js
        if(this.which_1 != this.which || this.scaleType_1 != this.scaleType) this.scale = null;
        this.which_1 = this.which;
        this.scaleType_1 = this.scaleType;
      },

      /**
       * Gets the domain for this hook
       * @returns {Array} domain
       */
      buildScale: function(margins) {
        var domain;

        if(this.scaleType == "time") {
          var limits = this.getLimits(this.which);
          this.scale = d3.time.scale.utc().domain([limits.min, limits.max]);
          return;
        }

        switch(this.use) {
          case "indicator":
            var limits = this.getLimits(this.which);
            //default domain is based on limits
            domain = [limits.min, limits.max];
            //domain from metadata can override it if defined
            domain = this.getMetadata().domain ? this.getMetadata().domain : domain;
            break;
          case "property":
            domain = this.getUnique(this.which);
            break;
          case "constant":
          default:
            domain = [this.which];
            break;
        }
        
        var scaletype = (d3.min(domain)<=0 && d3.max(domain)>=0 && this.scaleType === "log")? "genericLog" : this.scaleType;;
        this.scale = d3.scale[scaletype || "linear"]().domain(domain).clamp(true);
      }

    });

    /*!
     * HOOK MODEL
     */


    var Marker = Model.extend({

      /**
       * Gets limits
       * @param {String} attr parameter
       * @returns {Object} limits (min and max)
       * this function is only needed to route the "time" to some indicator, to adjust time start and end to the max and min time available in data
       */
      getLimits: function(attr) {
        if(!this.isHook()) {
          //if there's subhooks, find the one which is an indicator
          var limits = {};
          forEach(this.getSubhooks(), function(s) {
            var prop = (s.use === "property");
            if(!prop) {
              limits = s.getLimits(attr);
              return false;
            }
          });
          return limits;
        }

      },
        
        
        
        
      /**
       * gets the items associated with this hook without values
       * @param filter filter
       * @returns hooked value
       */
      getKeys: function(filter$$) {
          var sub = this.getSubhooks();
          var found = [];
          if(sub.length > 1) {
            forEach(sub, function(s) {
              found = s.getKeys();
              return false;
            });
          }
          return found;
      },
        
        getFrame: function(time) {
            var _this = this;
            var steps = this._parent.time.getAllSteps();

            var cachePath = "";
            forEach(this._dataCube, function(hook, name) {
                cachePath = cachePath + "," + name + ":" + hook.which + " " + _this._parent.time.start + " " + _this._parent.time.end;
            });
            if(!this.cachedFrames || !this.cachedFrames[cachePath]) this.getFrames();

            if(this.cachedFrames[cachePath][time]) return this.cachedFrames[cachePath][time];

            if(time < steps[0] || time > steps[steps.length - 1]) {
                //if the requested time point is out of the known range
                //then send nulls in the response
                var pValues = this.cachedFrames[cachePath][steps[0]];
                var curr = {};
                forEach(pValues, function(keys, hook) {
                    curr[hook] = {};
                    forEach(keys, function(val, key) {
                        curr[hook][key] = null;
                    });
                });
                return curr;
            }


            var next = d3.bisectLeft(steps, time);

            if(next === 0) return this.cachedFrames[cachePath][steps[0]];

            var fraction = (time - steps[next - 1]) / (steps[next] - steps[next - 1]);

            var pValues = this.cachedFrames[cachePath][steps[next - 1]];
            var nValues = this.cachedFrames[cachePath][steps[next]];

            var curr = {};
            forEach(pValues, function(values, hook) {
                curr[hook] = {};
                forEach(values, function(val, id) {
                    var val2 = nValues[hook][id];
                    curr[hook][id] = (!isNumber(val)) ? val : val + ((val2 - val) *
                        fraction);
                });
            });

            return curr;
        },

        getFrames: function() {
            var _this = this;

            var cachePath = "";
            forEach(this._dataCube, function(hook, name) {
                cachePath = cachePath + "," + name + ":" + hook.which + " " + _this._parent.time.start + " " + _this._parent.time.end;
            });

            if(!this.cachedFrames) this.cachedFrames = {};
            if(this.cachedFrames[cachePath]) return this.cachedFrames[cachePath];

            var steps = this._parent.time.getAllSteps();

            this._dataCube = this._dataCube || this.getSubhooks(true)

            var result = {};
            var resultKeys = [];

            // Assemble the list of keys as an intersection of keys in all queries of all hooks
            forEach(this._dataCube, function(hook, name) {

                // If hook use is constant, then we can provide no additional info about keys
                // We can just hope that we have something else than constants =) 
                if(hook.use === "constant") return;

                // Get keys in data of this hook
                var nested = _this.getDataManager().get(hook._dataId, 'nested', ["geo", "time"]);
                var keys = Object.keys(nested);

                if(resultKeys.length == 0) {
                    // If ain't got nothing yet, set the list of keys to result
                    resultKeys = keys;
                } else {
                    // If there is result accumulated aleready, remove the keys from it that are not in this hook
                    resultKeys = resultKeys.filter(function(f) {
                        return keys.indexOf(f) > -1;
                    })
                }
            });

            steps.forEach(function(t) {
                result[t] = {};
            });

            forEach(this._dataCube, function(hook, name) {

                if(hook.use === "constant") {
                    steps.forEach(function(t) {
                        result[t][name] = {};
                        resultKeys.forEach(function(key) {
                            result[t][name][key] = hook.which;
                        });
                    });

                } else if(hook.which === "geo") {
                    steps.forEach(function(t) {
                        result[t][name] = {};
                        resultKeys.forEach(function(key) {
                            result[t][name][key] = key;
                        });
                    });

                } else if(hook.which === "time") {
                    steps.forEach(function(t) {
                        result[t][name] = {};
                        resultKeys.forEach(function(key) {
                            result[t][name][key] = new Date(t);
                        });
                    });

                } else {
                    var frames = _this.getDataManager().get(hook._dataId, 'frames', steps);
                    forEach(frames, function(frame, t) {
                        result[t][name] = frame[hook.which];
                    });
                }
            });

            this.cachedFrames[cachePath] = result;
            return result;
        },
        
        

      /**
       * gets multiple values from the hook
       * @param {Object} filter Reference to the row. e.g: {geo: "swe", time: "1999", ... }
       * @param {Array} group_by How to nest e.g: ["geo"]
       * @param {Boolean} [previous = false] previous Append previous data points
       * @returns an array of values
       */
      getValues: function(filter$$, group_by, previous) {
        var _this = this;

        if(this.isHook()) {
          return [];
        }

        var dimTime, time, filtered, next, method, u, w, value, method;
        this._dataCube = this._dataCube || this.getSubhooks(true);
        filter$$ = clone(filter$$, this._getAllDimensions());
        dimTime = this._getFirstDimension({
          type: 'time'
        });
        time = new Date(filter$$[dimTime]); //clone date
        filter$$ = clone(filter$$, null, dimTime);

        var response = {};
        var f_keys = Object.keys(filter$$);
        var f_values = f_keys.map(function(k) {
          return filter$$[k];
        });

        //if there's a filter, interpolate only that
        if(f_keys.length) {
          forEach(this._dataCube, function(hook, name) {
            u = hook.use;
            w = hook.which;

            if(hook.use !== "property") next = next || d3.bisectLeft(hook.getUnique(dimTime), time);        

            method = hook.getMetadata().interpolation;
            filtered = _this.getDataManager().get(hook._dataId, 'nested', f_keys);
            forEach(f_values, function(v) {
              filtered = filtered[v]; //get precise array (leaf)
            });
            value = interpolatePoint(filtered, u, w, next, dimTime, time, method);
            response[name] = hook.mapValue(value);

            //concat previous data points
            if(previous) {
              var values = filter(filtered, filter$$).filter(function(d) {
                return d[dimTime] <= time;
              }).map(function(d) {
                return hook.mapValue(d[w]);
              }).concat(response[name]);
              response[name] = values;
            }
          });
        }
        //else, interpolate all with time
        else {
          forEach(this._dataCube, function(hook, name) {
              
            filtered = _this.getDataManager().get(hook._dataId, 'nested', group_by);
                
            response[name] = {};
            //find position from first hook
            u = hook.use;
            w = hook.which;
              
            if(hook.use !== "property") next = (typeof next === 'undefined') ? d3.bisectLeft(hook.getUnique(dimTime), time) : next;
            
            method = hook.getMetadata().interpolation;


            forEach(filtered, function(arr, id) {
              //TODO: this saves when geos have different data length. line can be optimised. 
              next = d3.bisectLeft(arr.map(function(m){return m.time}), time);
                
              value = interpolatePoint(arr, u, w, next, dimTime, time, method);
              response[name][id] = hook.mapValue(value);

              //concat previous data points
              if(previous) {
                var values = filter(arr, filter$$).filter(function(d) {
                  return d[dimTime] <= time;
                }).map(function(d) {
                  return hook.mapValue(d[w]);
                }).concat(response[name][id]);
                response[name][id] = values;
              }

            });
          });
        }

        return response;
      },

      /**
       * Gets the metadata of all hooks
       * @returns {Object} metadata
       */
      getMetadata: function() {
        return this.getDataManager().getMetadata();
      },
        
      /**
       * Gets the metadata of all hooks
       * @returns {Object} metadata
       */
      getIndicatorsTree: function() {
        return this.getDataManager().getIndicatorsTree();
      } 
        

    });

    var LanguageModel = Model.extend({

      /**
       * Default values for this model
       */
      _defaults: {
        id: "en",
        strings: {}
      },

      /**
       * Initializes the language model.
       * @param {Object} values The initial values of this model
       * @param parent A reference to the parent model
       * @param {Object} bind Initial events to bind
       */
      init: function(name, values, parent, bind) {

        this._type = "language";
        //default values for state model
        var defaults = deepClone(this._defaults);
        values = extend(defaults, values);

        //same constructor, with same arguments
        this._super(name, values, parent, bind);
      },

      /**
       * Gets a certain UI string
       * @param {String} id string identifier
       * @param {String} lang language
       * @param {Object} ui_strings ui_strings object or model
       * @returns {string} translated string
       */
      getUIString: function(id, lang, strings) {
        lang = lang || this.id;
        strings = strings || this.strings;

        if(strings && strings.hasOwnProperty(lang) && strings[lang].hasOwnProperty(id)) {
          return strings[lang][id];
        } else {
          return id;
        }
      },

      /**
       * Gets the translation function
       * @returns {string} translation function
       */
      getTFunction: function() {
        var lang = this.id,
          strings = this.strings,
          _this = this;

        return function(string) {
          return _this.getUIString(string, lang, strings);
        }
      }

    });

    /*
     * VIZABI Data Model (options.data)
     */

    var LabelModel = Hook.extend({

      /**
       * Default values for this model
       */
      _defaults: {
      },

      /**
       * Initializes the size hook
       * @param {Object} values The initial values of this model
       * @param parent A reference to the parent model
       * @param {Object} bind Initial events to bind
       */
      init: function(name, values, parent, bind) {

        this._type = "label";
        //TODO: add defaults extend to super
        var defaults = deepClone(this._defaults);
        values = extend(defaults, values);
        this._super(name, values, parent, bind);
      }


    });

    /*
     * VIZABI Group Model
     */

    var GroupModel = Hook.extend({

      /**
       * Default values for this model
       */
      _defaults: {
        use: null,
        which: null,
        merge: false,
        manualSorting: null
      },

      /**
       * Initializes the group hook
       * @param {Object} values The initial values of this model
       * @param parent A reference to the parent model
       * @param {Object} bind Initial events to bind
       */
      init: function(name, values, parent, bind) {

        this._type = "model";
        //TODO: add defaults extend to super
        var defaults = deepClone(this._defaults);
        values = extend(defaults, values);
        this._super(name, values, parent, bind);
      },

      /**
       * Validates a color hook
       */
      validate: function() {
        //there must be no scale
        if(this.scale) this.scale = null;

        //use must be "property" 
        if(this.use != "property") {
          warn("group model: use must be 'property'. Resetting...")
          this.use = "property";
        }
      },

      /**
       * There must be no scale
       */
      buildScale: function() {}

    });

    /*!
     * VIZABI Entities Model
     */

    var EntitiesModel = Model.extend({

      /**
       * Default values for this model
       */
      _defaults: {
        show: {},
        select: [],
        highlight: [],
        opacitySelectDim: .3,
        opacityRegular: 1
      },

      /**
       * Initializes the entities model.
       * @param {Object} values The initial values of this model
       * @param parent A reference to the parent model
       * @param {Object} bind Initial events to bind
       */
      init: function(name, values, parent, bind) {

        this._type = "entities";
        //TODO: add defaults extend to super
        var defaults = deepClone(this._defaults);
        values = extend(defaults, values);

        this._visible = [];
        this._multiple = true;

        this._super(name, values, parent, bind);
      },

      /**
       * Validates the model
       * @param {boolean} silent Block triggering of events
       */
      validate: function(silent) {
        var _this = this;
        var dimension = this.getDimension();
        var visible_array = this._visible.map(function(d) {
          return d[dimension]
        });

        if(visible_array.length) {
          this.select = this.select.filter(function(f) {
            return visible_array.indexOf(f[dimension]) !== -1;
          });
          this.setHighlight(this.highlight.filter(function(f) {
            return visible_array.indexOf(f[dimension]) !== -1;
          }));
        }
      },

      /**
       * Sets the visible entities
       * @param {Array} arr
       */
      setVisible: function(arr) {
        this._visible = arr;
      },

      /**
       * Gets the visible entities
       * @returns {Array} visible
       */
      getVisible: function(arr) {
        return this._visible;
      },

      /**
       * Determines whether multiple entities can be selected
       * @param {Boolean} bool
       */
      selectMultiple: function(bool) {
        this._multiple = bool;
      },

      /**
       * Gets the dimensions in this entities
       * @returns {String} String with dimension
       */
      getDimension: function() {
        return this.dim;
      },

      /**
       * Gets the filter in this entities
       * @returns {Array} Array of unique values
       */
      getFilter: function() {
        return this.show.getPlainObject();
      },

      /**
       * Gets the selected items
       * @returns {Array} Array of unique selected values
       */
      getSelected: function() {
        var dim = this.getDimension();
        return this.select.map(function(d) {
          return d[dim];
        });
      },

      /**
       * Selects or unselects an entity from the set
       */
      selectEntity: function(d, timeDim, timeFormatter) {
        var dimension = this.getDimension();
        var value = d[dimension];
        if(this.isSelected(d)) {
          this.select = this.select.filter(function(d) {
            return d[dimension] !== value;
          });
        } else {
          var added = {};
          added[dimension] = value;
          added["labelOffset"] = [0, 0];
          if(timeDim && timeFormatter) {
            added["trailStartTime"] = timeFormatter(d[timeDim]);
          }
          this.select = (this._multiple) ? this.select.concat(added) : [added];
        }
      },
        
      /**
       * Shows or unshows an entity from the set
       */
      showEntity: function(d) {
        //clear selected countries when showing something new
        this.clearSelected();
        
        var dimension = this.getDimension();
        var value = d[dimension];
        var show = this.show[dimension];
          
        if(!show || show[0] === "*") show = [];
          
        show = show.concat([]); //clone array
          
        if(this.isShown(d)) {
          show = show.filter(function(d) { return d !== value; });
        } else {
          show = show.concat(value);
        }
          
        if(show.length === 0) show = ["*"];
        this.show[dimension] = show.concat([]);

      },

      setLabelOffset: function(d, xy) {
        var dimension = this.getDimension();
        var value = d[dimension];

        find(this.select, function(d) {
          return d[dimension] === value;
        }).labelOffset = xy;

        //force the model to trigger events even if value is the same
        this.set("select", this.select, true);
      },

      /**
       * Selects an entity from the set
       * @returns {Boolean} whether the item is selected or not
       */
      isSelected: function(d) {
        var dimension = this.getDimension();
        var value = d[this.getDimension()];

        return this.select
            .map(function(d) {return d[dimension];})
            .indexOf(value) !== -1;
      },
        
      /**
       * Selects an entity from the set
       * @returns {Boolean} whether the item is shown or not
       */
      isShown: function(d) {
        var dimension = this.getDimension();
        return this.show[dimension] && this.show[dimension].indexOf(d[dimension]) !== -1;
      },

      /**
       * Clears selection of items
       */
      clearSelected: function() {
        this.select = [];
      },
      /**
       * Clears showing of items
       */
      clearShow: function() {
        var dimension = this.getDimension();
        this.show[dimension] = ["*"];
      },


      setHighlight: function(arg) {
        if (!isArray(arg))
          this.setHighlight([].concat(arg));
        this.getModelObject('highlight').set(arg, false, false); // highlights are always non persistent changes
      },

      //TODO: join the following 3 methods with the previous 3

      /**
       * Highlights an entity from the set
       */
      highlightEntity: function(d, timeDim, timeFormatter) {
        var dimension = this.getDimension();
        var value = d[dimension];
        if(!this.isHighlighted(d)) {
          var added = {};
          added[dimension] = value;
          if(timeDim && timeFormatter) {
            added["trailStartTime"] = timeFormatter(d[timeDim]);
          }
          this.setHighlight(this.highlight.concat(added));
        }
      },

      /**
       * Unhighlights an entity from the set
       */
      unhighlightEntity: function(d) {
        var dimension = this.getDimension();
        var value = d[dimension];
        if(this.isHighlighted(d)) {
          this.setHighlight(this.highlight.filter(function(d) {
            return d[dimension] !== value;
          }));
        }
      },

      /**
       * Checks whether an entity is highlighted from the set
       * @returns {Boolean} whether the item is highlighted or not
       */
      isHighlighted: function(d) {
        var dimension = this.getDimension();
        var value = d[this.getDimension()];

        var highlight_array = this.highlight.map(function(d) {
          return d[dimension];
        });

        return highlight_array.indexOf(value) !== -1;
      },

      /**
       * Clears selection of items
       */
      clearHighlighted: function() {
        this.setHighlight([]);
      }
        
    });

    /*
     * VIZABI Data Model (model.data)
     */

    var DataModel = Model.extend({

      /**
       * Default values for this model
       */
      _defaults: {
        reader: "csv",
        splash: false
      },


      /**
       * Initializes the data model.
       * @param {Object} values The initial values of this model
       * @param parent A reference to the parent model
       * @param {Object} bind Initial events to bind
       */
      init: function(name, values, parent, bind) {

        this._type = "data";
        //TODO: add defaults extend to super
        var defaults = deepClone(this._defaults);
        values = extend(defaults, values);

        //same constructor as parent, with same arguments
        this._super(name, values, parent, bind);
      }

    });

    /*!
     * VIZABI Color Model (hook)
     */

    var defaultPalettes = {
      "_continuous": {
        "0": "#F77481",
        "1": "#E1CE00",
        "2": "#B4DE79"
      },
      "_discrete": {
        "0": "#bcfa83",
        "1": "#4cd843",
        "2": "#ff8684",
        "3": "#e83739",
        "4": "#ffb04b",
        "5": "#ff7f00",
        "6": "#f599f5",
        "7": "#c027d4",
        "8": "#f4f459",
        "9": "#d66425",
        "10": "#7fb5ed",
        "11": "#0ab8d8"
      },
      "_default": {
        "_default": "#93daec"
      }
    };

    var ColorModel = Hook.extend({

      /**
       * Default values for this model
       */
      _defaults: {
        use: null,
        palette: {},
        scaleType: null,
        which: null
      },

      /**
       * Initializes the color hook
       * @param {Object} values The initial values of this model
       * @param parent A reference to the parent model
       * @param {Object} bind Initial events to bind
       */
      init: function(name, values, parent, bind) {

        this._type = "color";
        //TODO: add defaults extend to super
        var defaults = deepClone(this._defaults);
        values = extend(defaults, values);

        this._original_palette = values.palette;

        this._super(name, values, parent, bind);

        this._firstLoad = true;
        this._hasDefaultColor = false;
      },

      // args: {colorID, shadeID}
      getColorShade: function(args){
        var palette = this.getPalette();
          
        if(!args) return warn("getColorShade() is missing arguments");  
          
        // if colorID is not given or not found in the palette, replace it with default color
        if(!args.colorID || !palette[args.colorID]) args.colorID = "_default";
        
        // if the resolved colr value is not an array (has only one shade) -- return it
        if( !isArray(palette[args.colorID]) ) return palette[args.colorID];
          
        var colorMeta = this.getMetadata().color;
        var shade = args.shadeID && colorMeta && colorMeta.shades && colorMeta.shades[args.shadeID] ? colorMeta.shades[args.shadeID] : 0;
            
        return palette[args.colorID][shade];
        
      },
        

      afterPreload: function() {
        this._super();
      },
      
      /**
       * Get the above constants
       */
      isUserSelectable: function() {
        var metaColor = this.getMetadata().color;
        return metaColor == null || metaColor.selectable == null || metaColor.selectable;
      },

      /**
       * Validates a color hook
       */
      validate: function() {

        var possibleScales = ["log", "genericLog", "linear", "time", "pow"];
        if(!this.scaleType || (this.use === "indicator" && possibleScales.indexOf(this.scaleType) === -1)) {
          this.scaleType = 'linear';
        }
        if(this.use !== "indicator" && this.scaleType !== "ordinal") {
          this.scaleType = "ordinal";
        }

        // reset palette and scale in the following cases: indicator or scale type changed
        if(this._firstLoad === false && (this.which_1 != this.which || this.scaleType_1 != this.scaleType)) {

          //TODO a hack that kills the scale and palette, it will be rebuild upon getScale request in model.js
          if(this.palette) this.palette._data = {};
          this.scale = null;
        }

        this.which_1 = this.which;
        this.scaleType_1 = this.scaleType;
        this._firstLoad = false;
      },

      /**
       * set color
       */
      setColor: function(value, pointer) {
        var temp = this.getPalette();
        temp[pointer] = value;
        this.scale.range(values(temp));
        this.palette[pointer] = value;
      },


      /**
       * maps the value to this hook's specifications
       * @param value Original value
       * @returns hooked value
       */
      mapValue: function(value) {
        //if the property value does not exist, supply the _default
        // otherwise the missing value would be added to the domain
        if(this.scale != null && this.use == "property" && this._hasDefaultColor && this.scale.domain().indexOf(value) == -1) value = "_default";
        return this._super(value);
      },


      getDefaultPalette: function() {     
          var metaColor = this.getMetadata().color;
          var palette;
          
          if(metaColor && metaColor.palette) {
            //specific color palette from hook metadata
            palette = clone(metaColor.palette);
          } else if(defaultPalettes[this.which]) {
            //color palette for this.which exists in palette defaults
            palette = clone(defaultPalettes[this.which]);
          } else if(this.use === "constant" && /^#([0-9a-f]{3}|[0-9a-f]{6})$/.test(this.which)) {
            //an explicit hex color constant #abc or #adcdef is provided
            palette = {"_default": this.which};
          } else if(this.use === "indicator") {
            palette = clone(defaultPalettes["_continuous"]);
          } else if(this.use === "property") {
            palette = clone(defaultPalettes["_discrete"]);
          } else {
            palette = clone(defaultPalettes["_default"]);
          }
          
          return palette;
      },
        
      getPalette: function(){
        //rebuild palette if it's empty
        if (!this.palette || Object.keys(this.palette._data).length===0) this.palette = this.getDefaultPalette();
        return this.palette.getPlainObject(); 
      },
        
      /**
       * Gets the domain for this hook
       * @returns {Array} domain
       */
      buildScale: function() {
        var _this = this;

        var paletteObject = _this.getPalette();
        var domain = Object.keys(paletteObject);
        var range = values(paletteObject);

        this._hasDefaultColor = domain.indexOf("_default") > -1;

        if(this.scaleType == "time") {
          
          var timeMdl = this._parent._parent.time;
          var limits = timeMdl.beyondSplash ? 
              {min: timeMdl.beyondSplash.start, max: timeMdl.beyondSplash.end}
              :
              {min: timeMdl.start, max: timeMdl.end};
            
          var step = ((limits.max.valueOf() - limits.min.valueOf()) / (range.length - 1));
          domain = d3.range(limits.min.valueOf(), limits.max.valueOf(), step).concat(limits.max.valueOf());

          if(step === 0) {
            domain.push(domain[0]);
            range = [range[range.length - 1]];             
          }
          
          this.scale = d3.time.scale.utc()
            .domain(domain)
            .range(range)
            .interpolate(d3.interpolateRgb);
          return;
        }

        switch(this.use) {
          case "indicator":
            var limits = this.getLimits(this.which);
            //default domain is based on limits
            domain = [limits.min, limits.max];
            //domain from metadata can override it if defined
            domain = this.getMetadata().domain ? this.getMetadata().domain : domain;
              
            var limitMin = domain[0];
            var limitMax = domain[1];
            var step = (limitMax - limitMin) / (range.length - 1);
            domain = d3.range(limitMin, limitMax, step).concat(limitMax);
            if (domain.length > range.length) domain.pop();
            domain = domain.reverse();
            if(this.scaleType == "log") {
              var s = d3.scale.log()
                .domain([limitMin === 0 ? 1 : limitMin, limitMax])
                .range([limitMin, limitMax]);
              domain = domain.map(function(d) {
                return s.invert(d)
              });
            }

            this.scale = d3.scale[this.scaleType]()
              .domain(domain)
              .range(range)
              .interpolate(d3.interpolateRgb);
            return;

          default:
            range = range.map(function(m){ return isArray(m)? m[0] : m; });
                
            this.scale = d3.scale["ordinal"]()
              .domain(domain)
              .range(range);
            return;
        }
      }

    });

    /*!
     * VIZABI Axis Model (hook)
     */

    var allowTypes = {
        "indicator": ["linear", "log", "genericLog", "time", "pow"],
        "property": ["ordinal"],
        "value": ["ordinal"]
    };

    var AxisModel = Hook.extend({

      /**
       * Default values for this model
       */
      _defaults: {
        use: null,
        which: null,
        domainMin: null,
        domainMax: null,
        zoomedMin: null,
        zoomedMax: null
      },

      /**
       * Initializes the color hook
       * @param {Object} values The initial values of this model
       * @param parent A reference to the parent model
       * @param {Object} bind Initial events to bind
       */
      init: function(name, values, parent, bind) {

        this._type = "axis";
        //TODO: add defaults extend to super
        var defaults = deepClone(this._defaults);
        values = extend(defaults, values);      
        this._super(name, values, parent, bind);
      },

      /**
       * Validates a color hook
       */
      validate: function() {

        //only some scaleTypes are allowed depending on use. reset to default if inappropriate
        if(allowTypes[this.use].indexOf(this.scaleType) === -1) this.scaleType = allowTypes[this.use][0];

        //kill the scale if indicator or scale type have changed
        //the scale will be rebuild upon getScale request in model.js
        if(this.which_1 != this.which || this.scaleType_1 != this.scaleType) this.scale = null;
        this.which_1 = this.which;
        this.scaleType_1 = this.scaleType;

        //here the modified min and max may change the domain, if the scale is defined
        if(this.scale && this._readyOnce && this.use === "indicator") {

          //min and max nonsense protection
          if(this.domainMin == null || this.domainMin <= 0 && this.scaleType === "log") this.domainMin = this.scale.domain()[0];
          if(this.domainMax == null || this.domainMax <= 0 && this.scaleType === "log") this.domainMax = this.scale.domain()[1];

          //zoomedmin and zoomedmax nonsense protection    
          if(this.zoomedMin == null || this.zoomedMin <= 0 && this.scaleType === "log") this.zoomedMin = this.scale.domain()[0];
          if(this.zoomedMax == null || this.zoomedMax <= 0 && this.scaleType === "log") this.zoomedMax = this.scale.domain()[1];

          this.scale.domain([this.domainMin, this.domainMax]);
        }
      },

      /**
       * Gets the domain for this hook
       * @returns {Array} domain
       */
      buildScale: function(margins) {
        var domain;

        if(this.scaleType == "time") {
          var limits = this.getLimits(this.which);
          this.scale = d3.time.scale.utc().domain([limits.min, limits.max]);
          return;
        }

        switch(this.use) {
          case "indicator":
            var limits = this.getLimits(this.which);
            //default domain is based on limits
            domain = [limits.min, limits.max];
            //domain from metadata can override it if defined
            domain = this.getMetadata().domain ? this.getMetadata().domain : domain;
            //min and max can override the domain if defined
            domain = this.domainMin!=null && this.domainMax!=null ? [+this.domainMin, +this.domainMax] : domain;
            break;
          case "property":
            domain = this.getUnique(this.which);
            break;
          case "constant":
          default:
            domain = [this.which];
            break;
        }
        
        var scaletype = (d3.min(domain)<=0 && d3.max(domain)>=0 && this.scaleType === "log")? "genericLog" : this.scaleType;;
        this.scale = d3.scale[scaletype || "linear"]().domain(domain);
      }
    });

    var _index$2 = {
    axis : AxisModel,
    color : ColorModel,
    data : DataModel,
    entities : EntitiesModel,
    group : GroupModel,
    hook : Hook,
    label : LabelModel,
    language : LanguageModel,
    marker : Marker,
    size : SizeModel,
    stack : StackModel,
    time : TimeModel,
    };

    var models = {
    	axis: AxisModel,
    	color: ColorModel,
    	data: DataModel,
    	entities: EntitiesModel,
    	group: GroupModel,
    	hook: Hook,
    	label: LabelModel,
    	language: LanguageModel,
    	marker: Marker,
    	size: SizeModel,
    	stack: StackModel,
    	time: TimeModel,
    	default: _index$2
    };

    var class_loading$1 = 'vzb-loading';
    var class_loading_first$1 = 'vzb-loading-first';
    var class_error = 'vzb-error';

    var templates$1 = {};
    var Component = EventSource.extend({

      /**
       * Initializes the component
       * @param {Object} config Initial config, with name and placeholder
       * @param {Object} parent Reference to tool
       */
      init: function(config, parent) {
        this._id = this._id || uniqueId('c');
        this._ready = false;
        this._readyOnce = false;
        this.name = this.name || config.name;
        this.template = this.template || '<div></div>';
        this.placeholder = this.placeholder || config.placeholder;
        this.template_data = this.template_data || {
          name: this.name
        };
        //make sure placeholder is DOM element
        if(this.placeholder && !isElement(this.placeholder)) {
          try {
            this.placeholder = parent.placeholder.querySelector(this.placeholder);
          } catch(e) {
            error$1('Error finding placeholder \'' + this.placeholder + '\' for component \'' + this.name + '\'');
          }
        }
        this.parent = parent || this;
        this.root = this.parent.root || this;

        this.components = this.components || [];
        this._components_config = this.components.map(function(x) {
          return clone(x);
        });
        this._frameRate = 10;
        //define expected models for this component
        this.model_expects = this.model_expects || [];
        this.model_binds = this.model_binds || {};
        this.ui = this.ui || config.ui;
        this._super();
        //readyOnce alias
        var _this = this;
        this.on({
          'readyOnce': function() {
            if(typeof _this.readyOnce === 'function') {
              _this.readyOnce();
            }
          },
          'ready': function() {
            if(typeof _this.ready === 'function') {
              _this.ready();
            }
          },
          'domReady': function() {
            if(typeof _this.domReady === 'function') {
              _this.domReady();
            }
          },
          'resize': function() {
            if(typeof _this.resize === 'function') {
              _this.resize();
            }
          }
        });
        this.triggerResize = throttle(this.triggerResize, 100);
      },

      /**
       * Preloads data before anything else
       */
      preload: function(promise) {
        promise.resolve(); //by default, load nothing
      },

      /**
       * Executes after preloading is finished
       */
      afterPreload: function() {
        if(this.model) {
          this.model.afterPreload();
        }
      },

      /**
       * Renders the component (after data is ready)
       */
      render: function() {
        var _this = this;
        this.loadTemplate();
        this.loadComponents();
        //render each subcomponent
        forEach(this.components, function(subcomp) {
          subcomp.render();
          _this.on('resize', function() {
            subcomp.trigger('resize');
          });
        });

        //if it's a root component with model
        if(this.isRoot() && this.model) {
          this.model.on('ready', function() {
            done();
          });
          this.model.setHooks();

          var splashScreen = this.model && this.model.data && this.model.data.splash;

          preloader(this).then(function() {
            var timeMdl = _this.model.state.time;
            if(splashScreen) {

              //TODO: cleanup hardcoded splash screen
              timeMdl.splash = true;
              timeMdl.beyondSplash = clone(timeMdl.getPlainObject(), ['start', 'end']);

              _this.model.load({
                splashScreen: true
              }).then(function() {
                //delay to avoid conflicting with setReady
                delay(function() {
                  //force loading because we're restoring time.
                  _this.model.setLoading('restore_orig_time');
                  //restore because validation kills the original start/end
                  timeMdl.start = timeMdl.beyondSplash.start;
                  timeMdl.end = timeMdl.beyondSplash.end;
                  delete timeMdl.beyondSplash;

                  _this.model.load().then(function() {
                    _this.model.setLoadingDone('restore_orig_time');
                    timeMdl.splash = false;
                    timeMdl.trigger('change', timeMdl.getPlainObject());
                  });
                }, 300);

              }, function() {
                renderError();
              });
            } else {
              _this.model.load().then(function() {
                delay(function() {
                  timeMdl.trigger('change');
                }, 300);
              }, function() {
                renderError();
              });
            }
          });

        } else if(this.model && this.model.isLoading()) {
          this.model.on('ready', function() {
            done();
          });
        } else {
          done();
        }

        function renderError() {
          removeClass(_this.placeholder, class_loading$1);
          addClass(_this.placeholder, class_error);
          _this.setError({
            type: 'data'
          });
        }

        function done() {
          removeClass(_this.placeholder, class_loading$1);
          removeClass(_this.placeholder, class_loading_first$1);
          _this.setReady();
        }
      },

      setError: function(opts) {
        if(typeof this.error === 'function') {
          this.error(opts);
        }
      },

      setReady: function(value) {
        if(!this._readyOnce) {
          this._readyOnce = true;
          this.trigger('readyOnce');
        }

        this._ready = true;
        this.trigger('ready');
      },

      /**
       * Loads the template
       * @returns defer a promise to be resolved when template is loaded
       */
      loadTemplate: function() {
        var tmpl = this.template;
        var data = this.template_data;
        var _this = this;
        var rendered = '';
        if(!this.placeholder) {
          return;
        }
        //todo: improve t function getter + generalize this
        data = extend(data, {
          t: this.getTranslationFunction(true)
        });
        if(this.template) {
          try {
            rendered = templateFunc(tmpl, data);
          } catch(e) {
            error$1('Templating error for component: \'' + this.name +
              '\' - Check if template name is unique and correct. E.g.: \'bubblechart\'');

            removeClass(this.placeholder, class_loading$1);
            addClass(this.placeholder, class_error);
            this.setError({
              type: 'template'
            });
          }
        }
        //add loading class and html
        addClass(this.placeholder, class_loading$1);
        addClass(this.placeholder, class_loading_first$1);
        this.placeholder.innerHTML = rendered;
        this.element = this.placeholder.children[0];
        //only tools have layout (manage sizes)
        if(this.layout) {
          this.layout.setContainer(this.element);
          this.layout.on('resize', function() {
            if(_this._ready) {
              _this.triggerResize();
            }
          });
        }
        //template is ready
        this.trigger('domReady');
      },

      triggerResize: function() {
        this.trigger('resize');
      },

      getActiveProfile: function(profiles, presentationProfileChanges) {
        // get layout values
        var layoutProfile = this.getLayoutProfile();
        var presentationMode = this.getPresentationMode();
        var activeProfile = deepClone(profiles[layoutProfile]); // clone so it can be extended without changing the original profile

        // extend the profile with presentation mode values
        if (presentationMode && presentationProfileChanges[layoutProfile]) {
          deepExtend(activeProfile, presentationProfileChanges[layoutProfile]);
        }

        return activeProfile;
      },

      /*
       * Loads all subcomponents
       */
      loadComponents: function() {
        var _this = this;
        var config;
        var comp;
        //use the same name for collection
        this.components = [];
        //external dependencies let this model know what it
        //has to wait for
        if(this.model) {
          this.model.resetDeps();
        }
        // Loops through components, loading them.
        forEach(this._components_config, function(c) {
          if(!c.component) {
            error$1('Error loading component: name not provided');
            return;
          }
          comp = (isString(c.component)) ? Component.get(c.component) : c.component;

          if(!comp) return;

          config = extend(c, {
            name: c.component,
            ui: _this._uiMapping(c.placeholder, c.ui)
          });
          //instantiate new subcomponent
          var subcomp = new comp(config, _this);
          var c_model = c.model || [];
          subcomp.model = _this._modelMapping(subcomp.name, c_model, subcomp.model_expects, subcomp.model_binds);
          _this.components.push(subcomp);
        });
      },

      /**
       * Checks whether this is the root component
       * @returns {Boolean}
       */
      isRoot: function() {
        return this.parent === this;
      },

      /**
       * Returns subcomponent by name
       * @returns {Boolean}
       */
      findChildByName: function(name) {
        return find(this.components, function(f) {
          return f.name === name
        });
      },

      /**
       * Get layout profile of the current resolution
       * @returns {String} profile
       */
      getLayoutProfile: function() {
        //get profile from parent if layout is not available
        if(this.layout) {
          return this.layout.currentProfile();
        } else {
          return this.parent.getLayoutProfile();
        }
      },

      /**
       * Get if presentation mode is set of the current tool
       * @returns {Bool} presentation mode
       */
      getPresentationMode: function() {
        //get profile from parent if layout is not available
        if(this.layout) {
          return this.layout.getPresentationMode();
        } else {
          return this.parent.getPresentationMode();
        }
      },

      //TODO: make ui mapping more powerful
      /**
       * Maps the current ui to the subcomponents
       * @param {String} id subcomponent id (placeholder)
       * @param {Object} ui Optional ui parameters to overwrite existing
       * @returns {Object} the UI object
       */
      _uiMapping: function(id, ui) {
        //if overwritting UI
        if(ui) {
          return new Model('ui', ui);
        }
        if(id && this.ui) {
          id = id.replace('.', '');
          //remove trailing period
          var sub_ui = this.ui[id];
          if(sub_ui) {
            return sub_ui;
          }
        }
        return this.ui;
      },

      /**
       * Maps the current model to the subcomponents
       * @param {String} subcomponentName name of the subcomponent
       * @param {String|Array} model_config Configuration of model
       * @param {String|Array} model_expects Expected models
       * @param {Object} model_binds Initial model bindings
       * @returns {Object} the model
       */
      _modelMapping: function(subcomponentName, model_config, model_expects, model_binds) {
        var _this = this;
        var values = {};
        //If model_config is an array, we map it
        if(isArray(model_config) && isArray(model_expects)) {

          //if there's a different number of models received and expected
          if(model_expects.length !== model_config.length) {
            groupCollapsed('DIFFERENCE IN NUMBER OF MODELS EXPECTED AND RECEIVED');
            warn('Please, configure the \'model_expects\' attribute accordingly in \'' + subcomponentName +
              '\' or check the models passed in \'' + _this.name + '\'.\n\nComponent: \'' + _this.name +
              '\'\nSubcomponent: \'' + subcomponentName + '\'\nNumber of Models Expected: ' + model_expects.length +
              '\nNumber of Models Received: ' + model_config.length);
            groupEnd();
          }
          forEach(model_config, function(m, i) {
            var model_info = _mapOne(m);
            var new_name;
            if(model_expects[i]) {
              new_name = model_expects[i].name;
              if(model_expects[i].type && model_info.type !== model_expects[i].type && (!isArray(
                    model_expects[i].type) ||
                  model_expects[i].type.indexOf(model_info.type) === -1)) {

                groupCollapsed('UNEXPECTED MODEL TYPE: \'' + model_info.type + '\' instead of \'' +
                  model_expects[i].type + '\'');
                warn('Please, configure the \'model_expects\' attribute accordingly in \'' + subcomponentName +
                  '\' or check the models passed in \'' + _this.name + '\'.\n\nComponent: \'' + _this.name +
                  '\'\nSubcomponent: \'' + subcomponentName + '\'\nExpected Model: \'' + model_expects[i].type +
                  '\'\nReceived Model\'' + model_info.type + '\'\nModel order: ' + i);
                groupEnd();
              }
            } else {

              groupCollapsed('UNEXPECTED MODEL: \'' + model_config[i] + '\'');
              warn('Please, configure the \'model_expects\' attribute accordingly in \'' + subcomponentName +
                '\' or check the models passed in \'' + _this.name + '\'.\n\nComponent: \'' + _this.name +
                '\'\nSubcomponent: \'' + subcomponentName + '\'\nNumber of Models Expected: ' + model_expects.length +
                '\nNumber of Models Received: ' + model_config.length);
              groupEnd();
              new_name = model_info.name;
            }
            values[new_name] = model_info.model;
          });

          // fill the models that weren't passed with empty objects
          // e.g. if expected = [ui, language, color] and passed/existing = [ui, language]
          // it will fill values up to [ui, language, {}]
          var existing = model_config.length;
          var expected = model_expects.length;
          if(expected > existing) {
            //skip existing
            model_expects.splice(0, existing);
            //adds new expected models if needed
            forEach(expected, function(m) {
              values[m.name] = {};
            });
          }
        } else {
          return;
        }
        //return a new model with the defined submodels
        return new Model(subcomponentName, values, null, model_binds);
        /**
         * Maps one model name to current submodel and returns info
         * @param {String} name Full model path. E.g.: "state.marker.color"
         * @returns {Object} the model info, with name and the actual model
         */
        function _mapOne(name) {
          var parts = name.split('.');
          var current = _this.model;
          var current_name = '';
          while(parts.length) {
            current_name = parts.shift();
            current = current[current_name];
          }
          return {
            name: name,
            model: current,
            type: current ? current.getType() : null
          };
        }
      },

      /**
       * Get translation function for templates
       * @param {Boolean} wrap wrap in spam tags
       * @returns {Function}
       */
      getTranslationFunction: function(wrap) {
        var t_func;
        try {
          t_func = this.model.get('language').getTFunction();
        } catch(err) {
          if(this.parent && this.parent !== this) {
            t_func = this.parent.getTranslationFunction();
          }
        }
        if(!t_func) {
          t_func = function(s) {
            return s;
          };
        }
        if(wrap) {
          return this._translatedStringFunction(t_func);
        } else {
          return t_func;
        }
      },

      /**
       * Get function for translated string
       * @param {Function} translation_function The translation function
       * @returns {Function}
       */
      _translatedStringFunction: function(translation_function) {
        return function(string) {
          var translated = translation_function(string);
          return '<span data-vzb-translate="' + string + '">' + translated + '</span>';
        };
      },

      /**
       * Translate all strings in the template
       */
      translateStrings: function() {
        var t = this.getTranslationFunction();
        var strings = this.placeholder.querySelectorAll('[data-vzb-translate]');
        if(strings.length === 0) {
          return;
        }
        forEach(strings, function(str) {
          if(!str || !str.getAttribute) {
            return;
          }
          str.innerHTML = t(str.getAttribute('data-vzb-translate'));
        });
      },

      /**
       * Checks whether this component is a tool or not
       * @returns {Boolean}
       */
      isTool: function() {
        return this._id[0] === 't';
      },

      /**
       * Executes after the template is loaded and rendered.
       * Ideally, it contains HTML instantiations related to template
       * At this point, this.element and this.placeholder are available
       * as DOM elements
       */
      readyOnce: function() {},

      /**
       * Executes after the template and model (if any) are ready
       */
      ready: function() {},

      /**
       * Executes when the resize event is triggered.
       * Ideally, it only contains operations related to size
       */
      resize: function() {},

      /**
       * Clears a component
       */
      clear: function() {
        this.freeze();
        if(this.model) this.model.freeze();
        forEach(this.components, function(c) {
          c.clear();
        });
      }
    });

    /**
     * Preloader implementation with promises
     * @param {Object} comp any component
     * @returns {Promise}
     */
    function preloader(comp) {
      var promise = new Promise();
      var promises = []; //holds all promises

      //preload all subcomponents first
      forEach(comp.components, function(subcomp) {
        promises.push(preloader(subcomp));
      });

      var wait = promises.length ? Promise.all(promises) : new Promise.resolve();
      wait.then(function() {
        comp.preload(promise);
      }, function(err) {
        error$1("Error preloading data:", err);
      });

      return promise.then(function() {
        comp.afterPreload();
        return true;
      });
    }

    // Based on Simple JavaScript Templating by John Resig
    //generic templating function
    function templateFunc(str, data) {

      var func = function(obj) {
        return str.replace(/<%=([^\%]*)%>/g, function(match) {
          //match t("...")
          var s = match.match(/t\s*\(([^)]+)\)/g);
          //replace with translation
          if(s.length) {
            s = obj.t(s[0].match(/\"([^"]+)\"/g)[0].split('"').join(''));
          }
          //use object[name]
          else {
            s = match.match(/([a-z\-A-Z]+([a-z\-A-Z0-9]?[a-zA-Z0-9]?)?)/g)[0];
            s = obj[s] || s;
          }
          return s;
        });
      }
      // Figure out if we're getting a template, or if we need to
      // load the template - and be sure to cache the result.
      var fn = !/<[a-z][\s\S]*>/i.test(str) ? templates$1[str] = templates$1[str] || templateFunc(document.getElementById(
          str).innerHTML) : func;

      // Provide some basic currying to the user
      return data ? fn(data) : fn;
    }

    //utility function to check if a component is a component
    //TODO: Move to utils?
    Component.isComponent = function(c) {
      return c._id && (c._id[0] === 't' || c._id[0] === 'c');
    };

    //classes are vzb-portrait, vzb-landscape...
    var class_prefix = 'vzb-';
    var class_presentation = 'presentation';
    var class_portrait = 'vzb-portrait';
    var class_lansdcape = 'vzb-landscape';

    var Layout = EventSource.extend({

      screen_profiles: {
        small: {
          min_width: 0,
          min_height: 0
        },
        medium: {
          min_width: 600,
          min_height: 400
        },
        large: {
          min_width: 1000,
          min_height: 700
        }
      },

      /**
       * Initializes the layout manager
       */
      init: function(ui) {
        this.ui = ui || {};

        this._container = null;
        //dom element
        this._curr_profile = null;
        this._prev_size = {};
        //resize when window resizes
        var _this = this;

        this.resizeHandler = this.resizeHandler || resize$1.bind(this);

        window.addEventListener('resize', this.resizeHandler);
        this._super();
      },

      /**
       * Calculates the size of the newly resized container
       */
      setSize: function() {
        var _this = this;
        var width = this._container.clientWidth;
        var height = this._container.clientHeight;

        /**
         * issue #1118
         * check if device is iPhone then add top margin for searchbar if it visible
         */
        if (/^((?!chrome|android).)*safari/i.test(navigator.userAgent) // browser is safari
          && navigator.userAgent.match(/iPhone/i) // checking device
        ) {
          this._container.style.top =  0;
          if (this._container.clientWidth > this._container.clientHeight // landscape mode
            && this._container.clientWidth < 700) {  // small device
            var bodyHeight = this._container.clientHeight;
            var windowHeight = window.innerHeight;
            if (2 < (bodyHeight - windowHeight) && (bodyHeight - windowHeight) <= 45) { // check searchbar is visible
              this._container.style.top =  44 + "px";
              document.body.scrollTop = 44; // scrolling empty space
            }
          }
        }

        if(this._prev_size && this._prev_size.width === width && this._prev_size.height === height) {
          return;
        }

        // choose profile depending on size
        forEach(this.screen_profiles, function(range, size) {
          //remove class
          removeClass(_this._container, class_prefix + size);
          //find best fit
          if(width >= range.min_width && height >= range.min_height) {
            _this._curr_profile = size;
          }
        });

        //update size class
        addClass(this._container, class_prefix + this._curr_profile);
        //toggle, untoggle classes based on orientation
        if(width < height) {
          addClass(this._container, class_portrait);
          removeClass(this._container, class_lansdcape);
        } else {
          addClass(this._container, class_lansdcape);
          removeClass(this._container, class_portrait);
        }
        this._prev_size.width = width;
        this._prev_size.height = height;
        this.trigger('resize');
      },

      /**
       * Sets the container for this layout
       * @param container DOM element
       */
      setContainer: function(container) {
        this._container = container;
        this.setSize();
        this.updatePresentation();
      },

      /**
       * Sets the presentation mode for this layout
       * @param {Bool} presentation mode on or off
       */
      updatePresentation: function() {
        if (this.ui.presentation) {
            addClass(this._container, class_prefix + class_presentation);
        } else {
            removeClass(this._container, class_prefix + class_presentation);
        }
      },

      getPresentationMode: function() {
        return this.ui.presentation;
      },

      /**
       * Gets the current selected profile
       * @returns {String} name of current profile
       */
      currentProfile: function() {
        return this._curr_profile;
      },

      clear: function() {
        window.removeEventListener('resize', this.resizeHandler);
      }

    });

    function resize$1() {
      if(this._container) {
        this.setSize();
      }
    }

    // source https://github.com/encharm/Font-Awesome-SVG-PNG/tree/master/black/svg

    var paintbrush =
      '<svg class="vzb-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792"><path d="M1615 0q70 0 122.5 46.5t52.5 116.5q0 63-45 151-332 629-465 752-97 91-218 91-126 0-216.5-92.5t-90.5-219.5q0-128 92-212l638-579q59-54 130-54zm-909 1034q39 76 106.5 130t150.5 76l1 71q4 213-129.5 347t-348.5 134q-123 0-218-46.5t-152.5-127.5-86.5-183-29-220q7 5 41 30t62 44.5 59 36.5 46 17q41 0 55-37 25-66 57.5-112.5t69.5-76 88-47.5 103-25.5 125-10.5z"/></svg>';
    var search =
      '<svg class="vzb-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792"><path d="M1216 832q0-185-131.5-316.5t-316.5-131.5-316.5 131.5-131.5 316.5 131.5 316.5 316.5 131.5 316.5-131.5 131.5-316.5zm512 832q0 52-38 90t-90 38q-54 0-90-38l-343-342q-179 124-399 124-143 0-273.5-55.5t-225-150-150-225-55.5-273.5 55.5-273.5 150-225 225-150 273.5-55.5 273.5 55.5 225 150 150 225 55.5 273.5q0 220-124 399l343 343q37 37 37 90z"/></svg>';
    var circle$1 =
      '<svg class="vzb-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792"><path d="M1664 896q0 209-103 385.5t-279.5 279.5-385.5 103-385.5-103-279.5-279.5-103-385.5 103-385.5 279.5-279.5 385.5-103 385.5 103 279.5 279.5 103 385.5z"/></svg>';
    var expand =
      '<svg class="vzb-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792"><path d="M883 1056q0 13-10 23l-332 332 144 144q19 19 19 45t-19 45-45 19h-448q-26 0-45-19t-19-45v-448q0-26 19-45t45-19 45 19l144 144 332-332q10-10 23-10t23 10l114 114q10 10 10 23zm781-864v448q0 26-19 45t-45 19-45-19l-144-144-332 332q-10 10-23 10t-23-10l-114-114q-10-10-10-23t10-23l332-332-144-144q-19-19-19-45t19-45 45-19h448q26 0 45 19t19 45z"/></svg>';
    var asterisk =
      '<svg class="vzb-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792"><path d="M1546 1050q46 26 59.5 77.5t-12.5 97.5l-64 110q-26 46-77.5 59.5t-97.5-12.5l-266-153v307q0 52-38 90t-90 38h-128q-52 0-90-38t-38-90v-307l-266 153q-46 26-97.5 12.5t-77.5-59.5l-64-110q-26-46-12.5-97.5t59.5-77.5l266-154-266-154q-46-26-59.5-77.5t12.5-97.5l64-110q26-46 77.5-59.5t97.5 12.5l266 153v-307q0-52 38-90t90-38h128q52 0 90 38t38 90v307l266-153q46-26 97.5-12.5t77.5 59.5l64 110q26 46 12.5 97.5t-59.5 77.5l-266 154z"/></svg>';
    var trails =
      '<svg class="vzb-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792"><path d="M 1381.375 17.1875 C 1375.7825 17.176804 1370.1216 17.316078 1364.4375 17.5625 C 1273.4913 21.505489 1197.0982 57.199956 1135.2188 124.6875 C 1076.5961 188.62338 1047.6964 263.96059 1048.5312 350.65625 L 835.71875 433 C 797.77288 391.67699 749.96961 361.96416 692.3125 343.84375 C 604.96227 316.39162 520.95691 323.70366 440.25 365.8125 C 359.5432 407.92133 305.45225 472.64985 278 560 C 250.54783 647.35004 257.89117 731.38694 300 812.09375 C 342.10886 892.80075 406.83755 946.89147 494.1875 974.34375 C 576.9404 1000.3512 657.38873 994.58645 735.5625 957.09375 L 959.28125 1171.4375 L 972.375 1184.4062 C 966.2931 1198.3454 961.94845 1209.2226 959.34375 1217.0625 C 956.73915 1224.9024 953.7186 1236.224 950.25 1251.0312 L 711.03125 1285.1875 C 669.59175 1209.0324 607.72526 1157.2863 525.40625 1129.9375 C 438.51381 1101.0693 354.34933 1107.021 272.96875 1147.8125 C 191.58796 1188.6039 136.49335 1252.4513 107.625 1339.3438 C 78.756758 1426.2362 84.708528 1510.3694 125.5 1591.75 C 166.29138 1673.1307 230.1387 1728.2567 317.03125 1757.125 C 403.92369 1785.9933 488.05682 1780.0415 569.4375 1739.25 C 650.81799 1698.4587 705.94425 1634.6111 734.8125 1547.7188 C 737.41718 1539.8788 740.43763 1528.5573 743.90625 1513.75 L 983.125 1479.5938 C 1024.5644 1555.7487 1086.4309 1607.4948 1168.75 1634.8438 C 1255.6425 1663.7119 1339.8069 1657.7603 1421.1875 1616.9688 C 1502.5682 1576.1772 1557.6631 1512.3299 1586.5312 1425.4375 C 1615.3996 1338.5451 1609.4477 1254.4119 1568.6562 1173.0312 C 1527.8647 1091.6506 1464.0174 1036.5244 1377.125 1007.6562 C 1294.9259 980.34721 1214.5066 984.74084 1135.8438 1020.8125 L 1120.2812 1005.9062 L 898.0625 785.96875 C 902.79653 774.40321 906.33847 765.03422 908.5 758.15625 C 920.42249 720.22 925.7916 682.90194 924.59375 646.21875 L 1130.9688 566.34375 C 1141.2015 577.59424 1149.3796 586.0106 1155.4688 591.59375 C 1222.9566 653.47326 1302.1474 682.44278 1393.0938 678.5 C 1484.04 674.55731 1560.4642 638.83151 1622.3438 571.34375 C 1684.2232 503.85591 1713.1929 424.6337 1709.25 333.6875 C 1705.3072 242.74139 1669.5816 166.34819 1602.0938 104.46875 C 1538.8238 46.456824 1465.2625 17.347946 1381.375 17.1875 z "/></svg>';
    var lock =
      '<svg class="vzb-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792"><path d="M640 768h512v-192q0-106-75-181t-181-75-181 75-75 181v192zm832 96v576q0 40-28 68t-68 28h-960q-40 0-68-28t-28-68v-576q0-40 28-68t68-28h32v-192q0-184 132-316t316-132 316 132 132 316v192h32q40 0 68 28t28 68z"/></svg>';
    var unlock =
      '<svg class="vzb-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792"><path d="M1376 768q40 0 68 28t28 68v576q0 40-28 68t-68 28h-960q-40 0-68-28t-28-68v-576q0-40 28-68t68-28h32v-320q0-185 131.5-316.5t316.5-131.5 316.5 131.5 131.5 316.5q0 26-19 45t-45 19h-64q-26 0-45-19t-19-45q0-106-75-181t-181-75-181 75-75 181v320h736z"/></svg>';
    var unexpand =
      '<svg class="vzb-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792"><path d="M896 960v448q0 26-19 45t-45 19-45-19l-144-144-332 332q-10 10-23 10t-23-10l-114-114q-10-10-10-23t10-23l332-332-144-144q-19-19-19-45t19-45 45-19h448q26 0 45 19t19 45zm755-672q0 13-10 23l-332 332 144 144q19 19 19 45t-19 45-45 19h-448q-26 0-45-19t-19-45v-448q0-26 19-45t45-19 45 19l144 144 332-332q10-10 23-10t23 10l114 114q10 10 10 23z"/></svg>';
    var axes =
      '<svg class="vzb-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 500"><path d="M430.25,379.655l-75.982-43.869v59.771H120.73V151.966h59.774l-43.869-75.983L92.767,0L48.898,75.983L5.029,151.966h59.775 v271.557c0,15.443,12.52,27.965,27.963,27.965h261.5v59.773l75.982-43.869l75.982-43.867L430.25,379.655z"/></svg>';
    var gear =
      '<svg class="vzb-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792"><path d="M1152 896q0-106-75-181t-181-75-181 75-75 181 75 181 181 75 181-75 75-181zm512-109v222q0 12-8 23t-20 13l-185 28q-19 54-39 91 35 50 107 138 10 12 10 25t-9 23q-27 37-99 108t-94 71q-12 0-26-9l-138-108q-44 23-91 38-16 136-29 186-7 28-36 28h-222q-14 0-24.5-8.5t-11.5-21.5l-28-184q-49-16-90-37l-141 107q-10 9-25 9-14 0-25-11-126-114-165-168-7-10-7-23 0-12 8-23 15-21 51-66.5t54-70.5q-27-50-41-99l-183-27q-13-2-21-12.5t-8-23.5v-222q0-12 8-23t19-13l186-28q14-46 39-92-40-57-107-138-10-12-10-24 0-10 9-23 26-36 98.5-107.5t94.5-71.5q13 0 26 10l138 107q44-23 91-38 16-136 29-186 7-28 36-28h222q14 0 24.5 8.5t11.5 21.5l28 184q49 16 90 37l142-107q9-9 24-9 13 0 25 10 129 119 165 170 7 8 7 22 0 12-8 23-15 21-51 66.5t-54 70.5q26 50 41 98l183 28q13 2 21 12.5t8 23.5z"/></svg>';
    var stack =
      '<svg class="vzb-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 54.849 54.849"><g><path d="M54.497,39.614l-10.363-4.49l-14.917,5.968c-0.537,0.214-1.165,0.319-1.793,0.319c-0.627,0-1.254-0.104-1.79-0.318     l-14.921-5.968L0.351,39.614c-0.472,0.203-0.467,0.524,0.01,0.716L26.56,50.81c0.477,0.191,1.251,0.191,1.729,0L54.488,40.33     C54.964,40.139,54.969,39.817,54.497,39.614z"/><path d="M54.497,27.512l-10.364-4.491l-14.916,5.966c-0.536,0.215-1.165,0.321-1.792,0.321c-0.628,0-1.256-0.106-1.793-0.321     l-14.918-5.966L0.351,27.512c-0.472,0.203-0.467,0.523,0.01,0.716L26.56,38.706c0.477,0.19,1.251,0.19,1.729,0l26.199-10.479     C54.964,28.036,54.969,27.716,54.497,27.512z"/><path d="M0.361,16.125l13.662,5.465l12.537,5.015c0.477,0.191,1.251,0.191,1.729,0l12.541-5.016l13.658-5.463     c0.477-0.191,0.48-0.511,0.01-0.716L28.277,4.048c-0.471-0.204-1.236-0.204-1.708,0L0.351,15.41     C-0.121,15.614-0.116,15.935,0.361,16.125z"/></g></svg>';
    var iconDrag =
      '<svg class="vzb-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792"><path d="M896 384q-53 0-90.5 37.5t-37.5 90.5v128h-32v-93q0-48-32-81.5t-80-33.5q-46 0-79 33t-33 79v429l-32-30v-172q0-48-32-81.5t-80-33.5q-46 0-79 33t-33 79v224q0 47 35 82l310 296q39 39 39 102 0 26 19 45t45 19h640q26 0 45-19t19-45v-25q0-41 10-77l108-436q10-36 10-77v-246q0-48-32-81.5t-80-33.5q-46 0-79 33t-33 79v32h-32v-125q0-40-25-72.5t-64-40.5q-14-2-23-2-46 0-79 33t-33 79v128h-32v-122q0-51-32.5-89.5t-82.5-43.5q-5-1-13-1zm0-128q84 0 149 50 57-34 123-34 59 0 111 27t86 76q27-7 59-7 100 0 170 71.5t70 171.5v246q0 51-13 108l-109 436q-6 24-6 71 0 80-56 136t-136 56h-640q-84 0-138-58.5t-54-142.5l-308-296q-76-73-76-175v-224q0-99 70.5-169.5t169.5-70.5q11 0 16 1 6-95 75.5-160t164.5-65q52 0 98 21 72-69 174-69z"/></svg>';
    var iconWarn =
      '<svg class="vzb-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512.209 512.209"><path d="M507.345,439.683L288.084,37.688c-3.237-5.899-7.71-10.564-13.429-13.988c-5.705-3.427-11.893-5.142-18.554-5.142   s-12.85,1.718-18.558,5.142c-5.708,3.424-10.184,8.089-13.418,13.988L4.859,439.683c-6.663,11.998-6.473,23.989,0.57,35.98   c3.239,5.517,7.664,9.897,13.278,13.128c5.618,3.237,11.66,4.859,18.132,4.859h438.529c6.479,0,12.519-1.622,18.134-4.859   c5.62-3.23,10.038-7.611,13.278-13.128C513.823,463.665,514.015,451.681,507.345,439.683z M292.655,411.132   c0,2.662-0.91,4.897-2.71,6.704c-1.807,1.811-3.949,2.71-6.427,2.71h-54.816c-2.474,0-4.616-0.899-6.423-2.71   c-1.809-1.807-2.713-4.042-2.713-6.704v-54.248c0-2.662,0.905-4.897,2.713-6.704c1.807-1.811,3.946-2.71,6.423-2.71h54.812   c2.479,0,4.62,0.899,6.428,2.71c1.803,1.807,2.71,4.042,2.71,6.704v54.248H292.655z M292.088,304.357   c-0.198,1.902-1.198,3.47-3.001,4.709c-1.811,1.238-4.046,1.854-6.711,1.854h-52.82c-2.663,0-4.947-0.62-6.849-1.854   c-1.908-1.243-2.858-2.807-2.858-4.716l-4.853-130.47c0-2.667,0.953-4.665,2.856-5.996c2.474-2.093,4.758-3.14,6.854-3.14h62.809   c2.098,0,4.38,1.043,6.854,3.14c1.902,1.331,2.851,3.14,2.851,5.424L292.088,304.357z"/></svg>';
    var iconPin =
      '<svg class="vzb-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792"><path d="M800 864v-448q0-14-9-23t-23-9-23 9-9 23v448q0 14 9 23t23 9 23-9 9-23zm672 352q0 26-19 45t-45 19h-429l-51 483q-2 12-10.5 20.5t-20.5 8.5h-1q-27 0-32-27l-76-485h-404q-26 0-45-19t-19-45q0-123 78.5-221.5t177.5-98.5v-512q-52 0-90-38t-38-90 38-90 90-38h640q52 0 90 38t38 90-38 90-90 38v512q99 0 177.5 98.5t78.5 221.5z"/></svg>';
    var iconQuestion =
      '<svg class="vzb-icon" xmlns="http://www.w3.org/2000/svg" viewBox="17 17 483 483"> <circle stroke-width="40" cx="258.57" cy="258.57" r="220"/> <path d="M299.756,413.021v-61.78c0-3.003-0.966-5.472-2.896-7.401s-4.398-2.896-7.401-2.896h-61.78 c-3.003,0-5.47,0.965-7.4,2.896c-1.932,1.931-2.896,4.398-2.896,7.401v61.78c0,3.002,0.965,5.47,2.896,7.399 c1.931,1.931,4.396,2.896,7.4,2.896h61.779c3.003,0,5.472-0.967,7.401-2.896S299.756,416.021,299.756,413.021z"/> <path d="M382.128,196.789c0-18.877-5.952-36.36-17.856-52.449c-11.905-16.088-26.762-28.53-44.566-37.325 c-17.804-8.795-36.037-13.192-54.7-13.192c-52.127,0-91.919,22.845-119.377,68.537c-3.218,5.148-2.359,9.653,2.574,13.514 l42.474,32.177c1.502,1.287,3.54,1.931,6.114,1.931c3.433,0,6.115-1.287,8.044-3.861c11.369-14.587,20.594-24.454,27.672-29.603 c7.294-5.148,16.519-7.723,27.673-7.723c10.297,0,19.468,2.789,27.513,8.366c8.044,5.578,12.065,11.906,12.065,18.985 c0,8.151-2.146,14.694-6.437,19.628c-4.29,4.934-11.583,9.76-21.881,14.479c-13.514,6.006-25.901,15.284-37.164,27.834 c-11.263,12.549-16.894,26.01-16.894,40.382v11.583c0,3.004,0.965,5.472,2.896,7.401c1.931,1.93,4.396,2.896,7.4,2.896h61.779 c3.003,0,5.471-0.965,7.401-2.896c1.93-1.931,2.896-4.397,2.896-7.401c0-4.075,2.306-9.385,6.917-15.928 c4.612-6.542,10.458-11.852,17.537-15.927c6.863-3.861,12.119-6.918,15.768-9.171c3.646-2.252,8.579-6.008,14.802-11.263 c6.22-5.255,10.993-10.402,14.317-15.443c3.325-5.042,6.328-11.53,9.01-19.467C380.788,214.916,382.128,206.228,382.128,196.789z"/> </svg>';
    var iconClose =
      '<svg class="vzb-icon vzb-icon-pin" viewBox="-150 -250 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1149 414q0 26 -19 45l-181 181l181 181q19 19 19 45q0 27 -19 46l-90 90q-19 19 -46 19q-26 0 -45 -19l-181 -181l-181 181q-19 19 -45 19q-27 0 -46 -19l-90 -90q-19 -19 -19 -46q0 -26 19 -45l181 -181l-181 -181q-19 -19 -19 -45q0 -27 19 -46l90 -90q19 -19 46 -19 q26 0 45 19l181 181l181 -181q19 -19 45 -19q27 0 46 19l90 90q19 19 19 46z"/></svg>';
    var presentation =
      '<svg class="vzb-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path id="flip-chart-1" d="M334.549,393.834l58.607,68.666h-45.096l-58.709-68.666H334.549z M240.333,462.5h34.333v-68.666h-34.333 V462.5z M360.5,153.5h-34.334v137.334H360.5V153.5z M121.566,462.5h45.113l58.709-68.666h-45.197L121.566,462.5z M206,273.666 h-34.333v17.168H206V273.666z M257.5,239.333h-34.333v51.5H257.5V239.333z M309,205h-34.334v85.834H309V205z M446.334,102h-17.168 v257.5H85.833V102H68.667V50.5h377.667V102z M394.834,102H120.167v223.166h274.667V102z"/></svg>';
    var about =
      '<svg class="vzb-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792"><path d="M1088 1256v240q0 16-12 28t-28 12h-240q-16 0-28-12t-12-28v-240q0-16 12-28t28-12h240q16 0 28 12t12 28zm316-600q0 54-15.5 101t-35 76.5-55 59.5-57.5 43.5-61 35.5q-41 23-68.5 65t-27.5 67q0 17-12 32.5t-28 15.5h-240q-15 0-25.5-18.5t-10.5-37.5v-45q0-83 65-156.5t143-108.5q59-27 84-56t25-76q0-42-46.5-74t-107.5-32q-65 0-108 29-35 25-107 115-13 16-31 16-12 0-25-8l-164-125q-13-10-15.5-25t5.5-28q160-266 464-266 80 0 161 31t146 83 106 127.5 41 158.5z"/></svg>'


    var iconset = {
      paintbrush: paintbrush,
      search: search,
      circle: circle$1,
      expand: expand,
      asterisk: asterisk,
      trails: trails,
      lock: lock,
      unlock: unlock,
      unexpand: unexpand,
      axes: axes,
      gear: gear,
      stack: stack,
      drag: iconDrag,
      warn: iconWarn,
      pin: iconPin,
      question: iconQuestion,
      close: iconClose,
      presentation: presentation,
      about: about
    };

    var class_loading_first = 'vzb-loading-first';
    var class_loading_data = 'vzb-loading';
    var class_loading_error = 'vzb-loading-error';
    var class_placeholder = 'vzb-placeholder';
    var class_buttons_off = 'vzb-buttonlist-off';

    //tool model is quite simple and doesn't need to be registered
    var ToolModel = Model.extend({
      /**
       * Initializes the tool model.
       * @param {Object} values The initial values of this model
       * @param {Object} binds contains initial bindings for the model
       * @param {Function|Array} validade validate rules
       */
      init: function(name, values, defaults, binds, validate) {
        this._id = uniqueId('tm');
        this._type = 'tool';
        //generate validation function
        this.validate = generateValidate(this, validate);
        //default submodels
        values = values || {};
        defaults = defaults || {};
        values = defaultModel(values, defaults);
        //constructor is similar to model
        this._super(name, values, null, binds);
        // change language
        if(values.language) {
          var _this = this;
          this.on('change:language.id', function() {
            _this.trigger('translate');
          });
        }
      }
    });
    //tool
    var Tool = Component.extend({
      /**
       * Initializes the tool
       * @param {Object} placeholder object
       * @param {Object} external_model External model such as state, data, etc
       */
      init: function(placeholder, external_model) {
        this._id = uniqueId('t');
        this.template = this.template || 
          '<div class="vzb-tool vzb-tool-' + this.name + '">' + 
            '<div class="vzb-tool-stage">' + 
              '<div class="vzb-tool-viz">' + 
              '</div>' + 
              '<div class="vzb-tool-timeslider">' + 
              '</div>' + 
            '</div>' + 
            '<div class="vzb-tool-sidebar">' + 
              '<div class="vzb-tool-dialogs">' + 
              '</div>' +
              '<div class="vzb-tool-buttonlist">' + 
              '</div>' + 
            '</div>' +         
            '<div class="vzb-tool-treemenu vzb-hidden">' + 
            '</div>' + 
            '<div class="vzb-tool-datawarning vzb-hidden">' + 
            '</div>' + 
          '</div>';
        this.model_binds = this.model_binds || {};
        
        external_model = external_model || {}; //external model can be undefined
        external_model.bind = external_model.bind || {}; //bind functions can be undefined

        
        //bind the validation function with the tool
        var validate = this.validate.bind(this);
        var _this = this;

        // callbacks has to be an array so that it will not be turned into a submodel when the toolmodel is made.
        var callbacks = {
          'change': function(evt, path) {
            if(_this._ready) {
              _this.model.validate();

              if (evt.source.persistent)
                _this.model.trigger(new DefaultEvent(evt.source, 'persistentChange'), _this.getMinModel());
            }
          },
          'change:ui.presentation': function() {
            _this.layout.updatePresentation();
            _this.trigger('resize');
          },
          'translate': function(evt, val) {
            if(_this._ready) {
              Promise.all([_this.preloadLanguage(), _this.model.load()])
                .then(function() {
                  _this.model.validate();
                  _this.translateStrings();
                });
            }
          },
          'load_start': function() {
            _this.beforeLoading();
          },
          'ready': function(evt) {
            if(_this._ready) {
              _this.afterLoading();
            }
          }
        };
        extend(callbacks, this.model_binds, external_model.bind);
        delete external_model.bind;

        this.model = new ToolModel(this.name, external_model, this.default_model, callbacks, validate);

        // default model is the model set in the tool
        this.default_model = this.default_model || {};

        this.ui = this.model.ui || {};

        this.layout = new Layout(this.ui);
        //splash
        this.ui.splash = this.model && this.model.data && this.model.data.splash;
        this._super({
          name: this.name || this._id,
          placeholder: placeholder
        }, this);
        this.render();
        this._setUIModel();
      },

      getMinModel: function() {
        var currentToolModel = this.model.getPlainObject(true); // true = get only persistent model values
        var defaultToolModel = this.default_model;
        var defaultsFromModels = this.model.getDefaults();
        //flattens _defs_ object
        defaultToolModel = flattenDefaults(defaultToolModel);
        // compares with chart default model
        var modelChanges = diffObject(currentToolModel, defaultToolModel);
        // change date object to string according to current format
        modelChanges = flattenDates(modelChanges, this.model.state.time.timeFormat);
        //compares with model's defaults
        return diffObject(modelChanges, defaultsFromModels);
      },

      /**
       * Clears a tool
       */

      clear: function() {
        this.layout.clear();
        this.setModel = this.getModel = function() {
          return;
        };
        this._super();
      },

      /**
       * Visually display errors
       */
      error: function(opts) {

        var msg = (opts && opts.type === "data") ? "Error loading chart data. <br>Please, try again soon." : "Error loading chart";

        this.placeholder.innerHTML = '<div class="vzb-error-message"><h1>'+iconWarn+'</h1><p>'+msg+'</p></div>';
      },

      /**
       * Sets model from external page
       * @param {Object} JSONModel new model in JSON format
       * @param {Boolean} overwrite overwrite everything instead of extending
       */
      setModel: function(newModelJSON, overwrite) {
        if(overwrite) {
          this.model.reset(newModelJSON);
        } else {
          this.model.set(newModelJSON);
        }
        this._setUIModel();
      },

      /**
       * get model
       * @return {Object} JSON object of model
       */
      getModel: function() {
        return this.model.getPlainObject() || {};
      },
      /**
       * Displays loading class
       */
      beforeLoading: function() {
        if(!this._readyOnce) {
          addClass(this.placeholder, class_loading_first);
        }
        if(!hasClass(this.placeholder, class_loading_data)) {
          addClass(this.placeholder, class_loading_data);
        }
      },
      /**
       * Removes loading class
       */
      afterLoading: function() {
        removeClass(this.placeholder, class_loading_data);
        removeClass(this.placeholder, class_loading_first);
      },
      /**
       * Adds loading error class
       */
      errorLoading: function() {
        addClass(this.placeholder, class_loading_error);
      },
      /* ==========================
       * Validation and query
       * ==========================
       */
      /**
       * Validating the tool model
       * @param model the current tool model to be validated
       */
      validate: function(model) {

        model = this.model || model;

        if(!model || !model.state) {
          warn("tool validation aborted: model.state looks wrong: " + model);
          return;
        };

        var time = model.state.time;
        var marker = model.state.marker;

        if(!time) {
          warn("tool validation aborted: time looks wrong: " + time);
          return;
        };
        if(!marker) {
          warn("tool validation aborted: marker looks wrong: " + marker);
          return;
        };

        if(!marker) {
          warn("tool validation aborted: marker looks wrong: " + label);
          return;
        };

        //don't validate anything if data hasn't been loaded
        if(model.isLoading() || !marker.getKeys() || marker.getKeys().length < 1) return;

        var dateMin = marker.getLimits(time.getDimension()).min;
        var dateMax = marker.getLimits(time.getDimension()).max;

        if(!isDate(dateMin)) warn("tool validation: min date looks wrong: " + dateMin);
        if(!isDate(dateMax)) warn("tool validation: max date looks wrong: " + dateMax);

        // change is not persistent if it's splashscreen change
        if(time.start < dateMin && isDate(dateMin)) time.getModelObject('start').set(dateMin, false, !time.splash);
        if(time.end > dateMax && isDate(dateMax)) time.getModelObject('end').set(dateMax, false, !time.splash);
      },

      _setUIModel: function() {
        //add placeholder class
        addClass(this.placeholder, class_placeholder);
        //add-remove buttonlist class
        if(!this.ui || !this.ui.buttons || !this.ui.buttons.length) {
          addClass(this.element, class_buttons_off);
        } else {
          removeClass(this.element, class_buttons_off);
        }
      },

      preloadLanguage: function() {
        return Promise.resolve();
      }
    });

    /* ==========================
     * Validation methods
     * ==========================
     */

    /**
     * Generates a validation function based on specific model validation
     * @param {Object} m model
     * @param {Function} validate validation function
     * @returns {Function} validation
     */
    function generateValidate(m, validate) {
      var max = 10;

      function validate_func() {
        var model = JSON.stringify(m.getPlainObject());
        var c = arguments[0] || 0;
        //TODO: remove validation hotfix
        //while setting this.model is not available
        if(!this._readyOnce) {
          validate(this);
        } else {
          validate();
        }
        var model2 = JSON.stringify(m.getPlainObject());
        if(c >= max) {
          error$1('Max validation loop.');
        } else if(model !== model2) {
          validate_func.call(this, [c += 1]);
        }
      }

      return validate_func;
    }

    /* ==========================
     * Default model methods
     * ==========================
     */

    /**
     * Generates a valid state based on default model
     */
    function defaultModel(values, defaults) {
      var keys = Object.keys(defaults);
      var size = keys.length;
      var field;
      var blueprint;
      var original;
      var type;
      for(var i = 0; i < size; i += 1) {
        field = keys[i];
        if(field === '_defs_') {
          continue;
        }
        blueprint = defaults[field];
        original = values[field];
        type = typeof blueprint;
        if(type === 'object') {
          type = isPlainObject(blueprint) && blueprint._defs_ ? 'object' : isArray(blueprint) ? 'array' :
            'model';
        }
        if(typeof original === 'undefined') {
          if(type !== 'object' && type !== 'model') {
            values[field] = blueprint;
          } else {
            values[field] = defaultModel({}, blueprint);
          }
        }
        original = values[field];
        if(type === 'number' && isNaN(original)) {
          values[field] = 0;
        } else if(type === 'string' && typeof original !== 'string') {
          values[field] = '';
        } else if(type === 'array' && !isArray(original)) {
          values[field] = [];
        } else if(type === 'model') {
          if(!isObject(original)) {
            values[field] = {};
          }
          values[field] = defaultModel(values[field], blueprint);
        } else if(type === 'object') {
          if(!isObject(original) || Object.keys(original).length === 0) {
            original = false; //will be overwritten
          }
          if(!isObject(blueprint._defs_)) {
            blueprint._defs_ = {};
          }
          values[field] = original || blueprint._defs_;
        }
      }
      return values;
    }

    //utility function to check if a component is a tool
    //TODO: Move to utils?
    Tool.isTool = function(c) {
      return c._id && c._id[0] === 't';
    };

    //d3.svg.axisSmart

    function axisSmart() {

      return function d3_axis_smart(_super) {

        var VERTICAL = 'vertical axis';
        var HORIZONTAL = 'horizontal axis';
        var X = 'labels stack side by side';
        var Y = 'labels stack top to bottom';

        var OPTIMISTIC = 'optimistic approximation: labels have different lengths';
        var PESSIMISTIC = 'pessimistic approximation: all labels have the largest length';
        var DEFAULT_LOGBASE = 10;

        function onlyUnique(value, index, self) {
          return self.indexOf(value) === index;
        }

        function axis(g) {
          if(highlightValue != null) {
            axis.highlightValueRun(g);
            return;
          }

          // measure the width and height of one digit
          var widthSampleG = g.append("g").attr("class", "tick widthSampling");
          var widthSampleT = widthSampleG.append('text').text('0');

          options.cssMarginTop = widthSampleT.style("margin-top");
          options.cssMarginBottom = widthSampleT.style("margin-bottom");
          options.cssMarginLeft = widthSampleT.style("margin-left");
          options.cssMarginRight = widthSampleT.style("margin-right");
          options.widthOfOneDigit = widthSampleT[0][0].getBBox().width;
          options.heightOfOneDigit = widthSampleT[0][0].getBBox().height;
          widthSampleG.remove();


          // run label factory - it will store labels in tickValues property of axis
          axis.labelFactory(options);

          //if(axis.orient()=="bottom") console.log("ordered", axis.tickValues())
          // construct the view (d3 constructor is used)
          if(options.transitionDuration > 0) {
            _super(g.transition().duration(options.transitionDuration));
          } else {
            _super(g);
          }
          //if(axis.orient()=="bottom") console.log("received", g.selectAll("text").each(function(d){console.log(d)}))

          var orient = axis.orient() == "top" || axis.orient() == "bottom" ? HORIZONTAL : VERTICAL;
          var dimension = (orient == HORIZONTAL && axis.pivot() || orient == VERTICAL && !axis.pivot()) ? Y : X;

          g.selectAll('.vzb-axis-value')
            .data([null])
            .enter().append('g')
            .attr("class", 'vzb-axis-value')
            .classed("vzb-hidden", true)
            .append("text");

          // patch the label positioning after the view is generated
          g.selectAll("text")
            .each(function(d, i) {
              var view = d3.select(this);

              if(axis.pivot() == null) return;
              view.attr("transform", "rotate(" + (axis.pivot() ? -90 : 0) + ")");
              view.style("text-anchor", dimension == X ? "middle" : "end");
              view.attr("x", dimension == X ? 0 : (-axis.tickPadding() - axis.tickSize()));
              view.attr("y", dimension == X ? (orient == VERTICAL ? -1 : 1) * (axis.tickPadding() + axis.tickSize()) :
                0);
              view.attr("dy", dimension == X ? (orient == VERTICAL ? 0 : ".72em") : ".32em");

              if(axis.repositionLabels() == null) return;
              var shift = axis.repositionLabels()[i] || {
                x: 0,
                y: 0
              };
              view.attr("x", +view.attr("x") + shift.x);
              view.attr("y", +view.attr("y") + shift.y);
            })

          if(axis.tickValuesMinor() == null) axis.tickValuesMinor([]);
          // add minor ticks
          var minorTicks = g.selectAll(".tickMinor").data(tickValuesMinor);
          minorTicks.exit().remove();
          minorTicks.enter().append("line")
            .attr("class", "tickMinor");

          var tickLengthOut = axis.tickSizeMinor().outbound;
          var tickLengthIn = axis.tickSizeMinor().inbound;
          var scale = axis.scale();
          minorTicks
            .attr("y1", orient == HORIZONTAL ? (axis.orient() == "top" ? 1 : -1) * tickLengthIn : scale)
            .attr("y2", orient == HORIZONTAL ? (axis.orient() == "top" ? -1 : 1) * tickLengthOut : scale)
            .attr("x1", orient == VERTICAL ? (axis.orient() == "right" ? -1 : 1) * tickLengthIn : scale)
            .attr("x2", orient == VERTICAL ? (axis.orient() == "right" ? 1 : -1) * tickLengthOut : scale)

          if(options.bump){

              g.selectAll("path").remove();
              var rake = g.selectAll(".vzb-axis-line").data([0]);
              rake.exit().remove();
              rake.enter().append("line")
                  .attr("class", "vzb-axis-line");
              rake
                .attr("x1", orient == VERTICAL ? 0 : d3.min(scale.range()) - options.bump - 1)
                .attr("x2", orient == VERTICAL ? 0 : d3.max(scale.range()) + options.bump)
                .attr("y1", orient == HORIZONTAL ? 0 : d3.min(scale.range()) - options.bump)
                .attr("y2", orient == HORIZONTAL ? 0 : d3.max(scale.range()) + options.bump)
          }

        };


        axis.highlightValueRun = function(g) {
          var orient = axis.orient() == "top" || axis.orient() == "bottom" ? HORIZONTAL : VERTICAL;

          g.select('.vzb-axis-value')
            .classed("vzb-hidden", highlightValue == "none")
            .select("text")
            .text(axis.tickFormat()(highlightValue == "none" ? 0 : highlightValue));
            
          var getTransform = function(){
            return highlightValue == "none" ? "translate(0,0)" : 
                "translate(" 
                + (orient == HORIZONTAL ? axis.scale()(highlightValue) : 0) + "," 
                + (orient == VERTICAL ? axis.scale()(highlightValue) : 0) 
                + ")"
          }
          
          var getOpacity = function(d, t){
            return highlightValue == "none" ? 1 : 
                Math.min(1, Math.pow( Math.abs(axis.scale()(d) - axis.scale()(highlightValue)) / (axis.scale().range()[1] - axis.scale().range()[0]) * 5, 2))
          }
            
          if(highlightTransDuration){
            g.selectAll(".tick").each(function(d, t) {
              d3.select(this).select("text")
                .transition()
                .duration(highlightTransDuration)
                .ease("linear")
                .style("opacity", getOpacity(d,t))
            })
              
            g.select('.vzb-axis-value')
              .transition()
              .duration(highlightTransDuration)
              .ease("linear")
              .attr("transform", getTransform);
              
          }else{
              
            g.selectAll(".tick").each(function(d, t) {
              d3.select(this).select("text")
                .style("opacity", getOpacity(d,t))
            })
              
            g.select('.vzb-axis-value')
              .attr("transform", getTransform);
              
          }

          highlightValue = null;
        }


        var highlightValue = null;
        axis.highlightValue = function(arg) {
          if(!arguments.length) return highlightValue;
          highlightValue = arg;
          return axis;
        }

        var highlightTransDuration = 0;
        axis.highlightTransDuration = function(arg) {
          if(!arguments.length) return highlightTransDuration;
          highlightTransDuration = arg;
          return axis;
        }

        var repositionLabels = null;
        axis.repositionLabels = function(arg) {
          if(!arguments.length) return repositionLabels;
          repositionLabels = arg;
          return axis;
        };

        var pivot = false;
        axis.pivot = function(arg) {
          if(!arguments.length) return pivot;
          pivot = !!arg;
          return axis;
        };

        var tickValuesMinor = [];
        axis.tickValuesMinor = function(arg) {
          if(!arguments.length) return tickValuesMinor;
          tickValuesMinor = arg;
          return axis;
        };

        var tickSizeMinor = {
          outbound: 0,
          inbound: 0
        };
        axis.tickSizeMinor = function(arg1, arg2) {
          if(!arguments.length) return tickSizeMinor;
          tickSizeMinor = {
            outbound: arg1,
            inbound: arg2 || 0
          };
          meow("setting", tickSizeMinor)
          return axis;
        };

        var options = {};
        axis.labelerOptions = function(arg) {
          if(!arguments.length) return options;
          options = arg;
          return axis;
        };

        axis.METHOD_REPEATING = 'repeating specified powers';
        axis.METHOD_DOUBLING = 'doubling the value';

        axis.labelFactory = function(options) {
          if(options == null) options = {}
          if(options.scaleType != "linear" &&
            options.scaleType != "time" &&
            options.scaleType != "genericLog" &&
            options.scaleType != "log" &&
            options.scaleType != "ordinal") {
            return axis.ticks(ticksNumber)
              .tickFormat(null)
              .tickValues(null)
              .tickValuesMinor(null)
              .pivot(null)
              .repositionLabels(null);
          };
          if(options.scaleType == 'ordinal') return axis.tickValues(null);

          if(options.logBase == null) options.logBase = DEFAULT_LOGBASE;
          if(options.stops == null) options.stops = [1, 2, 5, 3, 7, 4, 6, 8, 9];



          if(options.removeAllLabels == null) options.removeAllLabels = false;

          if(options.formatterRemovePrefix == null) options.formatterRemovePrefix = false;

          if(options.formatter == null) options.formatter = function(d) {

            if(options.scaleType == "time") {
              if(!(d instanceof Date)) d = new Date(d);
              return options.timeFormat(d);
            }

            var format = "f";
            var prec = 0;
            if(Math.abs(d) < 1) {
              prec = 1;
              format = "r"
            };

            var prefix = "";
            if(options.formatterRemovePrefix) return d3.format("." + prec + format)(d);
            //switch(Math.floor(Math.log10(Math.abs(d)))) {
            switch(Math.floor(Math.log(Math.abs(d))/Math.LN10)) {
              case -13:
                d = d * 1000000000000;
                prefix = "p";
                break; //0.1p
              case -10:
                d = d * 1000000000;
                prefix = "n";
                break; //0.1n
              case -7:
                d = d * 1000000;
                prefix = "µ";
                break; //0.1µ
              case -6:
                d = d * 1000000;
                prefix = "µ";
                break; //1µ
              case -5:
                d = d * 1000000;
                prefix = "µ";
                break; //10µ
              case -4:
                break; //0.0001
              case -3:
                break; //0.001
              case -2:
                break; //0.01
              case -1:
                break; //0.1
              case 0:
                break; //1
              case 1:
                break; //10
              case 2:
                break; //100
              case 3:
                break; //1000
              case 4:
                break; //10000
              case 5:
                d = d / 1000;
                prefix = "k";
                break; //0.1M
              case 6:
                d = d / 1000000;
                prefix = "M";
                prec = 1;
                break; //1M
              case 7:
                d = d / 1000000;
                prefix = "M";
                break; //10M
              case 8:
                d = d / 1000000;
                prefix = "M";
                break; //100M
              case 9:
                d = d / 1000000000;
                prefix = "B";
                prec = 1;
                break; //1B
              case 10:
                d = d / 1000000000;
                prefix = "B";
                break; //10B
              case 11:
                d = d / 1000000000;
                prefix = "B";
                break; //100B
              case 12:
                d = d / 1000000000000;
                prefix = "T";
                prec = 1;
                break; //1T
                //use the D3 SI formatting for the extreme cases
              default:
                return(d3.format("." + prec + "s")(d)).replace("G", "B");
            }


            // use manual formatting for the cases above
            return(d3.format("." + prec + format)(d) + prefix).replace("G", "B");
          }
          options.cssLabelMarginLimit = 5; //px
          if(options.cssMarginLeft == null || parseInt(options.cssMarginLeft) < options.cssLabelMarginLimit) options.cssMarginLeft =
            options.cssLabelMarginLimit + "px";
          if(options.cssMarginRight == null || parseInt(options.cssMarginRight) < options.cssLabelMarginLimit) options.cssMarginRight =
            options.cssLabelMarginLimit + "px";
          if(options.cssMarginTop == null || parseInt(options.cssMarginTop) < options.cssLabelMarginLimit) options.cssMarginTop =
            options.cssLabelMarginLimit + "px";
          if(options.cssMarginBottom == null || parseInt(options.cssMarginBottom) < options.cssLabelMarginLimit) options
            .cssMarginBottom = options.cssLabelMarginLimit + "px";
          if(options.toolMargin == null) options.toolMargin = {
            left: 30,
            bottom: 30,
            right: 30,
            top: 30
          };
          if(options.bump == null) options.bump = 0;

          if(options.pivotingLimit == null) options.pivotingLimit = options.toolMargin[this.orient()];

          if(options.showOuter == null) options.showOuter = false;
          if(options.limitMaxTickNumber == null) options.limitMaxTickNumber = 0; //0 is unlimited

          var orient = this.orient() == "top" || this.orient() == "bottom" ? HORIZONTAL : VERTICAL;

          if(options.isPivotAuto == null) options.isPivotAuto = orient == VERTICAL;

          if(options.cssFontSize == null) options.cssFontSize = "13px";
          if(options.widthToFontsizeRatio == null) options.widthToFontsizeRatio = .75;
          if(options.heightToFontsizeRatio == null) options.heightToFontsizeRatio = 1.20;
          if(options.widthOfOneDigit == null) options.widthOfOneDigit =
            parseInt(options.cssFontSize) * options.widthToFontsizeRatio;
          if(options.heightOfOneDigit == null) options.heightOfOneDigit =
            parseInt(options.cssFontSize) * options.heightToFontsizeRatio;



          meow("********** " + orient + " **********");

          var domain = axis.scale().domain();
          var range = axis.scale().range();
          var lengthDomain = Math.abs(domain[domain.length - 1] - domain[0]);
          var lengthRange = Math.abs(range[range.length - 1] - range[0]);

          var min = d3.min([domain[0], domain[domain.length - 1]]);
          var max = d3.max([domain[0], domain[domain.length - 1]]);
          var bothSidesUsed = (min < 0 && max > 0) && options.scaleType != "time";

          if(bothSidesUsed && options.scaleType == "log") console.error("It looks like your " + orient +
            " log scale domain is crossing ZERO. Classic log scale can only be one-sided. If need crossing zero try using genericLog scale instead"
          )

          var tickValues = options.showOuter ? [min, max] : [];
          var tickValuesMinor = []; //[min, max];
          var ticksNumber = 5;

          function getBaseLog(x, base) {
            if (x == 0 || base == 0) {
              return 0;
            }
            if(base == null) base = options.logBase;
            return Math.log(x) / Math.log(base);
          };

          // estimate the longest formatted label in pixels
          var estLongestLabelLength =
            //take 17 sample values and measure the longest formatted label
            d3.max(d3.range(min, max, (max - min) / 17).concat(max).map(function(d) {
              return options.formatter(d).length
            })) * options.widthOfOneDigit + parseInt(options.cssMarginLeft);

          var pivot = options.isPivotAuto && (
            (estLongestLabelLength + axis.tickPadding() + axis.tickSize() > options.pivotingLimit) && (orient ==
              VERTICAL) ||
            !(estLongestLabelLength + axis.tickPadding() + axis.tickSize() > options.pivotingLimit) && !(orient ==
              VERTICAL)
          );

          var labelsStackOnTop = (orient == HORIZONTAL && pivot || orient == VERTICAL && !pivot);




          // conditions to remove labels altogether
          var labelsJustDontFit = (!labelsStackOnTop && options.heightOfOneDigit > options.pivotingLimit);
          if(options.removeAllLabels) return axis.tickValues([]);

          // return a single tick if have only one point in the domain
          if(min == max) return axis.tickValues([min]).ticks(1).tickFormat(options.formatter);






          // LABELS FIT INTO SCALE
          // measure if all labels in array tickValues can fit into the allotted lengthRange
          // approximationStyle can be OPTIMISTIC or PESSIMISTIC
          // in optimistic style the length of every label is added up and then we check if the total pack of symbols fit
          // in pessimistic style we assume all labels have the length of the longest label from tickValues
          // returns TRUE if labels fit and FALSE otherwise
          var labelsFitIntoScale = function(tickValues, lengthRange, approximationStyle, rescalingLabels) {
            if(tickValues == null || tickValues.length <= 1) return true;
            if(approximationStyle == null) approximationStyle = PESSIMISTIC;
            if(rescalingLabels == null) scaleType = "none";



            if(labelsStackOnTop) {
              //labels stack on top of each other. digit height matters
              return lengthRange >
                tickValues.length * (
                  options.heightOfOneDigit +
                  parseInt(options.cssMarginTop) +
                  parseInt(options.cssMarginBottom)
                );
            } else {
              //labels stack side by side. label width matters
              var marginsLR = parseInt(options.cssMarginLeft) + parseInt(options.cssMarginRight);
              var maxLength = d3.max(tickValues.map(function(d) {
                return options.formatter(d).length
              }));

              // log scales need to rescale labels, so that 9 takes more space than 2
              if(rescalingLabels == "log") {
                // sometimes only a fragment of axis is used. in this case we want to limit the scope to that fragment
                // yes, this is hacky and experimental
                lengthRange = Math.abs(axis.scale()(d3.max(tickValues)) - axis.scale()(d3.min(tickValues)));

                return lengthRange >
                  d3.sum(tickValues.map(function(d) {
                    return(
                        options.widthOfOneDigit * (approximationStyle == PESSIMISTIC ? maxLength : options.formatter(
                          d).length) + marginsLR
                      )
                      // this is a logarithmic rescaling of labels
                      * (1 + Math.log(d.toString().replace(/([0\.])/g, "")[0])/Math.LN10)
                  }))

              } else {
                return lengthRange >
                  tickValues.length * marginsLR + (approximationStyle == PESSIMISTIC ?
                    options.widthOfOneDigit * tickValues.length * maxLength : 0) + (approximationStyle == OPTIMISTIC ?
                    options.widthOfOneDigit * (
                      tickValues.map(function(d) {
                        return options.formatter(d)
                      }).join("").length
                    ) : 0);
              }
            }
          }





          // COLLISION BETWEEN
          // Check is there is a collision between labels ONE and TWO
          // ONE is a value, TWO can be a value or an array
          // returns TRUE if collision takes place and FALSE otherwise
          var collisionBetween = function(one, two) {
            if(two == null || two.length == 0) return false;
            if(!(two instanceof Array)) two = [two];

            for(var i = 0; i < two.length; i++) {
              if(
                one != two[i] && one != 0 &&
                Math.abs(axis.scale()(one) - axis.scale()(two[i])) <
                (labelsStackOnTop ?
                  (options.heightOfOneDigit) :
                  (options.formatter(one).length + options.formatter(two[i]).length) * options.widthOfOneDigit / 2
                )
              ) return true;

            }
            return false;
          }

          if(options.scaleType == "genericLog" || options.scaleType == "log") {
            var eps = axis.scale().eps ? axis.scale().eps() : 0;

            var spawnZero = bothSidesUsed ? [0] : [];

            // check if spawn positive is needed. if yes then spawn!
            var spawnPos = max < eps ? [] : (
              d3.range(
                Math.floor(getBaseLog(Math.max(eps, min))),
                Math.ceil(getBaseLog(max)),
                1)
              .concat(Math.ceil(getBaseLog(max)))
              .map(function(d) {
                return Math.pow(options.logBase, d)
              })
            );

            // check if spawn negative is needed. if yes then spawn!
            var spawnNeg = min > -eps ? [] : (
              d3.range(
                Math.floor(getBaseLog(Math.max(eps, -max))),
                Math.ceil(getBaseLog(-min)),
                1)
              .concat(Math.ceil(getBaseLog(-min)))
              .map(function(d) {
                return -Math.pow(options.logBase, d)
              })
            );


            // automatic chosing of method if it's not explicitly defined
            if(options.method == null) {
              var coverage = bothSidesUsed ?
                Math.max(Math.abs(max), Math.abs(min)) / eps :
                Math.max(Math.abs(max), Math.abs(min)) / Math.min(Math.abs(max), Math.abs(min));
              options.method = 10 <= coverage && coverage <= 1024 ? this.METHOD_DOUBLING : this.METHOD_REPEATING;
            };


            //meow('spawn pos/neg: ', spawnPos, spawnNeg);


            if(options.method == this.METHOD_DOUBLING) {
              var doublingLabels = [];
              if(bothSidesUsed) tickValues.push(0);
              var avoidCollidingWith = [].concat(tickValues);

              // start with the smallest abs number on the scale, rounded to nearest nice power
              //var startPos = max<eps? null : Math.pow(options.logBase, Math.floor(getBaseLog(Math.max(eps,min))));
              //var startNeg = min>-eps? null : -Math.pow(options.logBase, Math.floor(getBaseLog(Math.max(eps,-max))));

              var startPos = max < eps ? null : 4 * spawnPos[Math.floor(spawnPos.length / 2) - 1];
              var startNeg = min > -eps ? null : 4 * spawnNeg[Math.floor(spawnNeg.length / 2) - 1];

              //meow('starter pos/neg: ', startPos, startNeg);

              if(startPos) {
                for(var l = startPos; l <= max; l *= 2) doublingLabels.push(l);
              }
              if(startPos) {
                for(var l = startPos / 2; l >= Math.max(min, eps); l /= 2) doublingLabels.push(l);
              }
              if(startNeg) {
                for(var l = startNeg; l >= min; l *= 2) doublingLabels.push(l);
              }
              if(startNeg) {
                for(var l = startNeg / 2; l <= Math.min(max, -eps); l /= 2) doublingLabels.push(l);
              }

              doublingLabels = doublingLabels
                .sort(d3.ascending)
                .filter(function(d) {
                  return min <= d && d <= max
                });

              tickValuesMinor = tickValuesMinor.concat(doublingLabels);

              doublingLabels = groupByPriorities(doublingLabels, false); // don't skip taken values

              var tickValues_1 = tickValues;
              for(var j = 0; j < doublingLabels.length; j++) {

                // compose an attempt to add more axis labels
                var trytofit = tickValues_1.concat(doublingLabels[j])
                  .filter(function(d) {
                    return !collisionBetween(d, avoidCollidingWith);
                  })
                  .filter(onlyUnique)

                // stop populating if labels don't fit
                if(!labelsFitIntoScale(trytofit, lengthRange, PESSIMISTIC, "none")) break;

                // apply changes if no blocking instructions
                tickValues = trytofit
              }
            }


            if(options.method == this.METHOD_REPEATING) {

              var spawn = spawnZero.concat(spawnPos).concat(spawnNeg).sort(d3.ascending);

              options.stops.forEach(function(stop, i) {
                tickValuesMinor = tickValuesMinor.concat(spawn.map(function(d) {
                  return d * stop
                }));
              });

              spawn = groupByPriorities(spawn);
              var avoidCollidingWith = spawnZero.concat(tickValues);

              var stopTrying = false;

              options.stops.forEach(function(stop, i) {
                if(i == 0) {
                  for(var j = 0; j < spawn.length; j++) {

                    // compose an attempt to add more axis labels
                    var trytofit = tickValues
                      .concat(spawn[j].map(function(d) {
                        return d * stop
                      }))
                      // throw away labels that collide with "special" labels 0, min, max
                      .filter(function(d) {
                        return !collisionBetween(d, avoidCollidingWith);
                      })
                      .filter(function(d) {
                        return min <= d && d <= max
                      })
                      .filter(onlyUnique);

                    // stop populating if labels don't fit
                    if(!labelsFitIntoScale(trytofit, lengthRange, PESSIMISTIC, "none")) break;

                    // apply changes if no blocking instructions
                    tickValues = trytofit;
                  }

                  //flatten the spawn array
                  spawn = [].concat.apply([], spawn);
                } else {
                  if(stopTrying) return;

                  // compose an attempt to add more axis labels
                  var trytofit = tickValues
                    .concat(spawn.map(function(d) {
                      return d * stop
                    }))
                    .filter(function(d) {
                      return min <= d && d <= max
                    })
                    .filter(onlyUnique);

                  // stop populating if the new composition doesn't fit
                  if(!labelsFitIntoScale(trytofit, lengthRange, PESSIMISTIC, "log")) {
                    stopTrying = true;
                    return;
                  }
                  // stop populating if the number of labels is limited in options
                  if(tickValues.length > options.limitMaxTickNumber && options.limitMaxTickNumber != 0) {
                    stopTrying = true;
                    return;
                  }

                  // apply changes if no blocking instructions
                  tickValues = trytofit;
                }
              })


            } //method


          } //logarithmic




          if(options.scaleType == "linear" || options.scaleType == "time") {
            if(bothSidesUsed) tickValues.push(0);
            var avoidCollidingWith = [].concat(tickValues);

            if(labelsStackOnTop){
                ticksNumber = Math.max(Math.floor(lengthRange / (options.heightOfOneDigit + parseInt(options.cssMarginTop))), 2);
            }else{
                ticksNumber = Math.max(Math.floor(lengthRange / estLongestLabelLength), 2);
            }

            // limit maximum ticks number
            if(options.limitMaxTickNumber != 0 && ticksNumber > options.limitMaxTickNumber) ticksNumber = options.limitMaxTickNumber;

            var addLabels = axis.scale().ticks.apply(axis.scale(), [ticksNumber])
              .sort(d3.ascending)
              .filter(function(d) {
                return min <= d && d <= max
              });

            tickValuesMinor = tickValuesMinor.concat(addLabels);

            addLabels = groupByPriorities(addLabels, false);

            var tickValues_1 = tickValues;
            for(var j = 0; j < addLabels.length; j++) {

              // compose an attempt to add more axis labels
              var trytofit = tickValues_1.concat(addLabels[j])
                .filter(function(d) {
                  return !collisionBetween(d, avoidCollidingWith);
                })
                .filter(onlyUnique);

              // stop populating if labels don't fit
              if(!labelsFitIntoScale(trytofit, lengthRange, PESSIMISTIC, "none")) break;

              // apply changes if no blocking instructions
              tickValues = trytofit
            }

            tickValues = tickValues //.concat(addLabels)
              .filter(function(d) {
                return !collisionBetween(d, avoidCollidingWith);
              })
              .filter(onlyUnique);


          }




          if(tickValues != null && tickValues.length <= 2 && !bothSidesUsed) tickValues = [min, max];

          if(tickValues != null && tickValues.length <= 3 && bothSidesUsed) {
            if(!collisionBetween(0, [min, max])) {
              tickValues = [min, 0, max];
            } else {
              tickValues = [min, max];
            }
          }

          if(tickValues != null) tickValues.sort(function(a, b) {
            return(orient == HORIZONTAL ? -1 : 1) * (axis.scale()(b) - axis.scale()(a))
          });

          if(labelsJustDontFit) tickValues = [];
          tickValuesMinor = tickValuesMinor.filter(function(d) {
            return tickValues.indexOf(d) == -1 && min <= d && d <= max
          });


          meow("final result", tickValues);

          return axis
            .ticks(ticksNumber)
            .tickFormat(options.formatter)
            .tickValues(tickValues)
            .tickValuesMinor(tickValuesMinor)
            .pivot(pivot)
            .repositionLabels(
              repositionLabelsThatStickOut(tickValues, options, orient, axis.scale(), labelsStackOnTop ? "y" : "x")
            );
        };











        // GROUP ELEMENTS OF AN ARRAY, SO THAT...
        // less-prio elements are between the high-prio elements
        // Purpose: enable adding axis labels incrementally, like this for 9 labels:
        // PRIO 1: +--------, concat result: +-------- first we show only 1 label
        // PRIO 2: ----+---+, concat result: +---+---+ then we add 2 more, that are maximally spaced
        // PRIO 3: --+---+--, concat result: +-+-+-+-+ then we fill spaces with 2 more labels
        // PRIO 4: -+-+-+-+-, concat result: +++++++++ then we fill the remaing spaces and show all labels
        // exception: zero jumps to the front, if it's on the list
        // example1: [1 2 3 4 5 6 7] --> [[1][4 7][2 3 5 6]]
        // example2: [1 2 3 4 5 6 7 8 9] --> [[1][5 9][3 7][2 4 6 8]]
        // example3: [-4 -3 -2 -1 0 1 2 3 4 5 6 7] --> [[0][-4][2][-1 5][-3 -2 1 3 4 6 7]]
        // inputs:
        // array - the source array to be processed. Only makes sense if sorted
        // removeDuplicates - return incremental groups (true, default), or return concatinated result (false)
        // returns:
        // the nested array
        function groupByPriorities(array, removeDuplicates) {
          if(removeDuplicates == null) removeDuplicates = true;

          var result = [];
          var taken = [];

          //zero is an exception, if it's present we manually take it to the front
          if(array.indexOf(0) != -1) {
            result.push([0]);
            taken.push(array.indexOf(0));
          }

          for(var k = array.length; k >= 1; k = k < 4 ? k - 1 : k / 2) {
            // push the next group of elements to the result
            result.push(array.filter(function(d, i) {
              if(i % Math.floor(k) == 0 && (taken.indexOf(i) == -1 || !removeDuplicates)) {
                taken.push(i);
                return true;
              }
              return false;
            }));
          }

          return result;
        }








        // REPOSITION LABELS THAT STICK OUT
        // Purpose: the outer labels can easily be so large, they stick out of the allotted area
        // Example:
        // Label is fine:    Label sticks out:    Label sticks out more:    Solution - label is shifted:
        //      12345 |           1234|                123|5                   12345|
        // _______.   |      _______. |           _______.|                 _______.|
        //
        // this is what the function does on the first step (only outer labels)
        // on the second step it shifts the inner labels that start to overlap with the shifted outer labels
        //
        // requires tickValues array to be sorted from tail-first
        // tail means left or bottom, head means top or right
        //
        // dimension - which dimension requires shifting
        // X if labels stack side by side, Y if labels stack on top of one another
        //
        // returns the array of recommended {x,y} shifts

        function repositionLabelsThatStickOut(tickValues, options, orient, scale, dimension) {
          if(tickValues == null) return null;

          // make an abstraction layer for margin sizes
          // tail means left or bottom, head means top or right
          var margin =
            orient == VERTICAL ? {
              head: options.toolMargin.top,
              tail: options.toolMargin.bottom
            } : {
              head: options.toolMargin.right,
              tail: options.toolMargin.left
            };


          var result = {};


          // STEP 1:
          // for outer labels: avoid sticking out from the tool margin
          tickValues.forEach(function(d, i) {
            if(i != 0 && i != tickValues.length - 1) return;

            // compute the influence of the axis head
            var repositionHead = margin.head + options.bump + (orient == HORIZONTAL ? 1 : 0) * d3.max(scale.range()) -
              (orient == HORIZONTAL ? 0 : 1) * d3.min(scale.range()) + (orient == HORIZONTAL ? -1 : 1) * scale(d) - (
                dimension == "x") * options.formatter(d).length * options.widthOfOneDigit / 2 - (dimension == "y") *
              options.heightOfOneDigit / 2
              // we may consider or not the label margins to give them a bit of spacing from the edges
              - (dimension == "x") * parseInt(options.cssMarginRight) 
              - (dimension == "y") * parseInt(options.cssMarginTop);

            // compute the influence of the axis tail
            var repositionTail = Math.min(margin.tail, options.widthOfOneDigit) + options.bump + (orient == VERTICAL ?
                1 : 0) * d3.max(scale.range()) - (orient == VERTICAL ? 0 : 1) * d3.min(scale.range()) + (orient ==
                VERTICAL ? -1 : 1) * scale(d) - (dimension == "x") * options.formatter(d).length * options.widthOfOneDigit /
              2 - (dimension == "y") * options.heightOfOneDigit / 2
              // we may consider or not the label margins to give them a bit of spacing from the edges
              - (dimension == "x") * parseInt(options.cssMarginLeft) 
              - (dimension == "y") * parseInt(options.cssMarginBottom);

            // apply limits in order to cancel repositioning of labels that are good
            if(repositionHead > 0) repositionHead = 0;
            if(repositionTail > 0) repositionTail = 0;

            // add them up with appropriate signs, save to the axis
            result[i] = {
              x: 0,
              y: 0
            };
            result[i][dimension] = (dimension == "y" && orient == VERTICAL ? -1 : 1) * (repositionHead -
              repositionTail);
          });


          // STEP 2:
          // for inner labels: avoid collision with outer labels
          tickValues.forEach(function(d, i) {
            if(i == 0 || i == tickValues.length - 1) return;

            // compute the influence of the head-side outer label
            var repositionHead =
              // take the distance between head and the tick at hand
              Math.abs(scale(d) - scale(tickValues[tickValues.length - 1]))
            
              // substract the shift of the head TODO: THE SIGN CHOICE HERE MIGHT BE WRONG. NEED TO TEST ALL CASES
              - (dimension == "y") * (orient == HORIZONTAL ? -1 : 1) * result[tickValues.length - 1][dimension]
              - (dimension == "x") * (orient == HORIZONTAL ? 1 : -1) * result[tickValues.length - 1][dimension]
            
              // substract half-length of the overlapping labels
              - (dimension == "x") * options.widthOfOneDigit / 2 * options.formatter(d).length 
              - (dimension == "x") * options.widthOfOneDigit / 2 * options.formatter(tickValues[tickValues.length - 1]).length 
              - (dimension == "y") * options.heightOfOneDigit * .7 //TODO remove magic constant - relation of actual font height to BBox-measured height
              
              // we may consider or not the label margins to give them a bit of spacing from the edges
              - (dimension == "x") * parseInt(options.cssMarginLeft) 
              - (dimension == "y") * parseInt(options.cssMarginBottom);
              
            // compute the influence of the tail-side outer label
            var repositionTail =
              // take the distance between tail and the tick at hand
              Math.abs(scale(d) - scale(tickValues[0]))
            
              // substract the shift of the tail TODO: THE SIGN CHOICE HERE MIGHT BE WRONG. NEED TO TEST ALL CASES
              - (dimension == "y") * (orient == VERTICAL ? -1 : 1) * result[0][dimension]
              - (dimension == "x") * (orient == VERTICAL ? 1 : -1) * result[0][dimension]
            
              // substract half-length of the overlapping labels
              - (dimension == "x") * options.widthOfOneDigit / 2 * options.formatter(d).length 
              - (dimension == "x") * options.widthOfOneDigit / 2 * options.formatter(tickValues[0]).length 
              - (dimension == "y") * options.heightOfOneDigit * .7 //TODO remove magic constant - relation of actual font height to BBox-measured height
            
              // we may consider or not the label margins to give them a bit of spacing from the edges
              - (dimension == "x") * parseInt(options.cssMarginLeft) 
              - (dimension == "y") * parseInt(options.cssMarginBottom);

            // apply limits in order to cancel repositioning of labels that are good
            if(repositionHead > 0) repositionHead = 0;
            if(repositionTail > 0) repositionTail = 0;

            // add them up with appropriate signs, save to the axis
            result[i] = {
              x: 0,
              y: 0
            };
            result[i][dimension] = (dimension == "y" && orient == VERTICAL ? -1 : 1) * (repositionHead -
              repositionTail);
          });


          return result;
        } // function repositionLabelsThatStickOut()




        axis.copy = function() {
          return d3_axis_smart(d3.svg.axis());
        };

        return d3.rebind(axis, _super,
          "scale", "orient", "ticks", "tickValues", "tickFormat",
          "tickSize", "innerTickSize", "outerTickSize", "tickPadding",
          "tickSubdivide"
        );


        function meow(l1, l2, l3, l4, l5) {
          if(!axis.labelerOptions().isDevMode) return;
          if(l5 != null) {
            console.log(l1, l2, l3, l4, l5);
            return;
          }
          if(l4 != null) {
            console.log(l1, l2, l3, l4);
            return;
          }
          if(l3 != null) {
            console.log(l1, l2, l3);
            return;
          }
          if(l2 != null) {
            console.log(l1, l2);
            return;
          }
          if(l1 != null) {
            console.log(l1);
            return;
          }
        }

      }(d3.svg.axis());

    };

    //BAR CHART COMPONENT
    var BarComponent = Component.extend({

      /**
       * Initializes the component (Bar Chart).
       * Executed once before any template is rendered.
       * @param {Object} config The options passed to the component
       * @param {Object} context The component's parent
       */
      init: function(config, context) {
        this.name = 'barchart';
        this.template = 'barchart.html';

        //define expected models for this component
        this.model_expects = [{
          name: "time",
          type: "time"
        }, {
          name: "entities",
          type: "entities"
        }, {
          name: "marker",
          type: "model"
        }, {
          name: "language",
          type: "language"
        }];

        var _this = this;

        this.model_binds = {
          "change:time.value": function(evt) {
            if(!_this._readyOnce) return;
            _this.updateEntities();
          },
          'change:marker': function(evt, path) {
            if(!_this._readyOnce) return;
            if(path.indexOf("color.palette") > -1) return;
            if(path.indexOf("which") > -1 || path.indexOf("use") > -1) return;

            _this.ready();
          },
          'change:marker.color.palette': debounce(function(evt) {
            if(!_this._readyOnce) return;
            _this.updateEntities();
          }, 200)
        };

        //contructor is the same as any component
        this._super(config, context);

        this.xScale = null;
        this.yScale = null;
        this.cScale = d3.scale.category10();

        this.xAxis = axisSmart();
        this.yAxis = axisSmart();
      },

      /**
       * DOM is ready
       */
      readyOnce: function() {
        this.element = d3.select(this.element);

        this.graph = this.element.select('.vzb-bc-graph');
        this.yAxisEl = this.graph.select('.vzb-bc-axis-y');
        this.xAxisEl = this.graph.select('.vzb-bc-axis-x');
        this.yTitleEl = this.graph.select('.vzb-bc-axis-y-title');
        this.xTitleEl = this.graph.select('.vzb-bc-axis-x-title');
        this.bars = this.graph.select('.vzb-bc-bars');
        this.year = this.element.select('.vzb-bc-year');

        var _this = this;
        this.on("resize", function() {
          _this.updateEntities();
        });
      },

      /*
       * Both model and DOM are ready
       */
      ready: function() {
        this.updateIndicators();
        this.resize();
        this.updateEntities();
      },

      /**
       * Changes labels for indicators
       */
      updateIndicators: function() {

        var _this = this;
        this.translator = this.model.language.getTFunction();
        this.duration = this.model.time.delayAnimations;

        var titleStringY = this.translator("indicator/" + this.model.marker.axis_y.which);
        var titleStringX = this.translator("indicator/" + this.model.marker.axis_x.which);

        var yTitle = this.yTitleEl.selectAll("text").data([0]);
        yTitle.enter().append("text");
        yTitle
          .attr("y", "-6px")
          .attr("x", "-9px")
          .attr("dx", "-0.72em")
          .text(titleStringY)
          .on("click", function() {
            //TODO: Optimise updateView
            _this.parent
              .findChildByName("gapminder-treemenu")
              .markerID("axis_y")
              .alignX("left")
              .alignY("top")
              .updateView()
              .toggle();
          });

        var xTitle = this.xTitleEl.selectAll("text").data([0]);
        xTitle.enter().append("text");
        xTitle
          .attr("y", "-3px")
          .attr("dx", "-0.72em")
          .text(titleStringX)
          .on("click", function() {
            //TODO: Optimise updateView
            _this.parent
              .findChildByName("gapminder-treemenu")
              .markerID("axis_x")
              .alignY("bottom")
              .alignX("center")
              .updateView()
              .toggle();
          });

        this.yScale = this.model.marker.axis_y.getScale();
        this.xScale = this.model.marker.axis_x.getScale();
        this.cScale = this.model.marker.color.getScale();

        var xFormatter = this.model.marker.axis_x.which == "geo.region"?
            function(x){return _this.translator("region/" + x)}
            :
            _this.model.marker.axis_x.tickFormatter;

        this.yAxis.tickFormat(_this.model.marker.axis_y.tickFormatter);
        this.xAxis.tickFormat(xFormatter);

      },

      /**
       * Updates entities
       */
      updateEntities: function() {

        var _this = this;
        var time = this.model.time;
        var timeDim = time.getDimension();
        var entityDim = this.model.entities.getDimension();
        var duration = (time.playing) ? time.delayAnimations : 0;
        var filter = {};
        filter[timeDim] = time.value;
        var items = this.model.marker.getKeys(filter);
        var values = this.model.marker.getFrame(time.value);

        this.entityBars = this.bars.selectAll('.vzb-bc-bar')
          .data(items);

        //exit selection
        this.entityBars.exit().remove();

        //enter selection -- init circles
        this.entityBars.enter().append("rect")
          .attr("class", "vzb-bc-bar")
          .on("mousemove", function(d, i) {})
          .on("mouseout", function(d, i) {})
          .on("click", function(d, i) {});

        //positioning and sizes of the bars

        var bars = this.bars.selectAll('.vzb-bc-bar');
        var barWidth = this.xScale.rangeBand();

        this.bars.selectAll('.vzb-bc-bar')
          .attr("width", barWidth)
          .attr("fill", function(d) {
            return _this.cScale(values.color[d[entityDim]]);
          })
          .attr("x", function(d) {
            return _this.xScale(values.axis_x[d[entityDim]]);
          })
          .transition().duration(duration).ease("linear")
          .attr("y", function(d) {
            return _this.yScale(values.axis_y[d[entityDim]]);
          })
          .attr("height", function(d) {
            return _this.height - _this.yScale(values.axis_y[d[entityDim]]);
          });
          this.year.text(this.model.time.timeFormat(this.model.time.value));
      },

      /**
       * Executes everytime the container or vizabi is resized
       * Ideally,it contains only operations related to size
       */
      resize: function() {

        var _this = this;

        this.profiles = {
          "small": {
            margin: {
              top: 30,
              right: 20,
              left: 40,
              bottom: 50
            },
            padding: 2,
            minRadius: 2,
            maxRadius: 40
          },
          "medium": {
            margin: {
              top: 30,
              right: 60,
              left: 60,
              bottom: 60
            },
            padding: 2,
            minRadius: 3,
            maxRadius: 60
          },
          "large": {
            margin: {
              top: 30,
              right: 60,
              left: 60,
              bottom: 80
            },
            padding: 2,
            minRadius: 4,
            maxRadius: 80
          }
        };

        this.activeProfile = this.profiles[this.getLayoutProfile()];
        var margin = this.activeProfile.margin;


        //stage
        this.height = parseInt(this.element.style("height"), 10) - margin.top - margin.bottom;
        this.width = parseInt(this.element.style("width"), 10) - margin.left - margin.right;

        this.graph
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        //update scales to the new range
        if(this.model.marker.axis_y.scaleType !== "ordinal") {
          this.yScale.range([this.height, 0]);
        } else {
          this.yScale.rangePoints([this.height, 0], _this.activeProfile.padding).range();
        }
        if(this.model.marker.axis_x.scaleType !== "ordinal") {
          this.xScale.range([0, this.width]);
        } else {
          this.xScale.rangePoints([0, this.width], _this.activeProfile.padding).range();
        }

        //apply scales to axes and redraw
        this.yAxis.scale(this.yScale)
          .orient("left")
          .tickSize(6, 0)
          .tickSizeMinor(3, 0)
          .labelerOptions({
            scaleType: this.model.marker.axis_y.scaleType,
            timeFormat: this.model.time.timeFormat,
            toolMargin: {top: 5, right: margin.right, left: margin.left, bottom: margin.bottom},
            limitMaxTickNumber: 6
          });

        this.xAxis.scale(this.xScale)
          .orient("bottom")
          .tickSize(6, 0)
          .tickSizeMinor(3, 0)
          .labelerOptions({
            scaleType: this.model.marker.axis_x.scaleType,
            timeFormat: this.model.time.timeFormat,
            toolMargin: margin
          });

        this.xAxisEl.attr("transform", "translate(0," + this.height + ")")
          .call(this.xAxis);

        this.xScale.rangeRoundBands([0, this.width], .1, .2);

        this.yAxisEl.call(this.yAxis);
        this.xAxisEl.call(this.xAxis);

        var xAxisSize = this.xAxisEl.node().getBoundingClientRect();
        var xTitleSize = this.xTitleEl.node().getBoundingClientRect();
        var xTitleXPos = xAxisSize.width / 2 - xTitleSize.width / 2;
        var xTitleYPos = this.height + xAxisSize.height + xTitleSize.height;
        this.xTitleEl.attr("transform", "translate(" + xTitleXPos + "," + xTitleYPos + ")");
        this.year.attr('x', this.width).attr('y', 0);
      }
    });

    var MENU_HORIZONTAL = 1;
    var MENU_VERTICAL = 2;

    //css custom classes
    var css = {
      wrapper: 'vzb-treemenu-wrap',
      background: 'vzb-treemenu-background',
      close: 'vzb-treemenu-close',
      search: 'vzb-treemenu-search',
      list: 'vzb-treemenu-list',
      list_item: 'vzb-treemenu-list-item',
      hasChild: 'vzb-treemenu-list-item-children',
      list_item_label: 'vzb-treemenu-list-item-label',
      list_top_level: 'vzb-treemenu-list-top',
      search_wrap: 'vzb-treemenu-search-wrap',
      isSpecial: 'vzb-treemenu-list-item-special',
      hidden: 'vzb-hidden',
      title: 'vzb-treemenu-title',
      scaletypes: 'vzb-treemenu-scaletypes',
      scaletypesDisabled: 'vzb-treemenu-scaletypes-disabled',
      scaletypesActive: 'vzb-treemenu-scaletypes-active',
      alignYt: 'vzb-align-y-top',
      alignYb: 'vzb-align-y-bottom',
      alignXl: 'vzb-align-x-left',
      alignXr: 'vzb-align-x-right',
      alignXc: 'vzb-align-x-center',
      menuHorizontal: 'vzb-treemenu-horizontal',
      menuVertical: 'vzb-treemenu-vertical',
      absPosVert: 'vzb-treemenu-abs-pos-vert',
      absPosHoriz: 'vzb-treemenu-abs-pos-horiz',
      menuOpenLeftSide: 'vzb-treemenu-open-left-side',
      noTransition: 'notransition'   
    };

    //options and globals
    var OPTIONS$1 = {
      MOUSE_LOCS: [], //contains last locations of mouse
      MOUSE_LOCS_TRACKED: 3, //max number of locations of mouse
      DELAY: 200, //amazons multilevel delay
      TOLERANCE: 150, //this parameter is used for controlling the angle of multilevel dropdown
      LAST_DELAY_LOC: null, //this is cached location of mouse, when was a delay
      TIMEOUT: null, //timeout id
      SEARCH_PROPERTY: 'id', //property in input data we we'll search by
      SUBMENUS: 'children', //property for submenus (used by search)
      SEARCH_MIN_STR: 1, //minimal length of query string to start searching
      RESIZE_TIMEOUT: null, //container resize timeout
      MOBILE_BREAKPOINT: 400, //mobile breakpoint
      CURRENT_PATH: [], //current active path
      MIN_COL_WIDTH: 50, //minimal column size
      MENU_DIRECTION: MENU_HORIZONTAL,
      MAX_MENU_WIDTH: 300,
      MENU_OPEN_LEFTSIDE: false
    };

    var Menu = Class.extend({
      init: function (parent, menu) {
        var _this = this;
        this.parent = parent;
        this.entity = menu;
        this.width = OPTIONS$1.MIN_COL_WIDTH;
        this.direction = OPTIONS$1.MENU_DIRECTION;
        this._setDirectionClass();
        this.menuItems = [];
        menu.selectAll('.' + css.list_item)
          .filter(function() {
            return this.parentNode == _this.entity.node();
          })
          .each(function() {
            _this.addSubmenu(d3.select(this));
          });
        return this;
      },
      setWidth: function(width, recursive) {
        if (this.width != width) {
          this.width = width;
          if (this.entity.classed('active')) {
            this.entity.transition()
              .delay(0)
              .duration(100)
              .style('width', this.width + "px")
          }
          if (recursive) {
            for (var i = 0; i < this.menuItems.length; i++) {
              this.menuItems[i].setWidth(this.width, recursive);
            }
          }
          return this;
        }
      },
      /**
       * configure menu type (horizontal or vertical)
       * @param direction MENU_HORIZONTAL or MENU_VERTICAL
       * @param recursive change direction over menu sublevels
       * @returns {Menu}
       */
      setDirection: function(direction, recursive) {
        this.direction = direction;
        this.entity
          .style('width', '')
          .style('height', '');
        if (recursive) {
          for (var i = 0; i < this.menuItems.length; i++) {
            this.menuItems[i].setDirection(this.direction, recursive);
          }
        }
        this._setDirectionClass();
        return this;
      },
      _setDirectionClass: function() {
        if (this.direction == MENU_HORIZONTAL) {
          this.entity.classed(css.menuVertical, false);
          this.entity.classed(css.menuHorizontal, true);
        } else {
          this.entity.classed(css.menuHorizontal, false);
          this.entity.classed(css.menuVertical, true);
        }
      },
      addSubmenu: function(item) {
          this.menuItems.push(new MenuItem(this, item))
      },
      open: function() {
        var _this = this;
        if (!this.isActive()) {
          this.closeNeighbors(function() {
            if (_this.direction == MENU_HORIZONTAL) {
              _this._openHorizontal();
              _this.calculateMissingWidth(0);
            } else {
              _this._openVertical();
            }
          });
        }
        return this;
      },
      /**
       * recursively calculate missed width for last menu level
       * @param width
       * @param cb
       */
      calculateMissingWidth: function(width, cb) {
        var _this = this;
        if (this.entity.classed(css.list_top_level)) {
          if (width > OPTIONS$1.MAX_MENU_WIDTH) {
            cb(width - OPTIONS$1.MAX_MENU_WIDTH);
          }
        } else {
          this.parent.parentMenu.calculateMissingWidth(width + this.width, function(widthToReduce) {
              if (widthToReduce > 0) {
                _this.reduceWidth(widthToReduce, function(newWidth) {
                  if (typeof cb === "function") cb(); // callback is not defined if it is emitted from this level
                });
              }
          });
        }
      },
      /**
       * restore width (if it was reduced before)
       * @param width
       * @param isClosedElement (parameter for check if curent element emit this action)
       * @param cb
       */
      restoreWidth: function(width, isClosedElement, cb) {
        var _this = this;
        if (isClosedElement) {
          this.parent.parentMenu.restoreWidth(width, false, cb);
        } else if (width <= 0) {
          if (typeof cb === "function") cb();
        } else if (!this.entity.classed(css.list_top_level)) {
          var currentElementWidth =  this.entity.node().offsetWidth;
          if (currentElementWidth < _this.width) {
            var duration = 250*(currentElementWidth / _this.width);
            this.entity.transition()
              .delay(0)
              .duration(duration)
              .style('width', _this.width + "px")
              .each('end', function() {
              });
            _this.parent.parentMenu.restoreWidth(width - _this.width + currentElementWidth, false, cb);
          } else {
            this.parent.parentMenu.restoreWidth(width, false, cb);
          }
        } else {
          if (typeof cb === "function") cb();
        }
      },
      /**
       * made element narrower to free space for other element
       * @param width
       * @param cb
       */
      reduceWidth: function(width, cb) {
        var _this = this;
        var currWidth = this.entity.node().offsetWidth;

        if (currWidth <= OPTIONS$1.MIN_COL_WIDTH) {
          cb(width);
        } else {

          var newElementWidth = Math.max(OPTIONS$1.MIN_COL_WIDTH, Math.abs(_this.width - width));
          var duration = 250 / (_this.width / newElementWidth);
          this.entity.transition()
            .delay(0)
            .duration(duration)
            .style('width', newElementWidth + "px")
            .each('end', function() {
              cb(width - _this.width + newElementWidth);
            });
        }
      },
      _openHorizontal: function() {
        var _this = this;
        _this.entity.transition()
          .delay(0)
          .duration(250)
          .style('width', _this.width + "px")
          .each('end', function() {
            _this.marqueeToggle(true);
          });
        _this.entity.classed('active', true);
      },
      _openVertical: function() {
        var _this = this;
        _this.entity.transition()
          .delay(0)
          .duration(250)
          .style('height', (35*_this.menuItems.length) + "px")
          .each('end', function() {
            _this.entity.style('height', 'auto');
            _this.marqueeToggle(true);
          });
        _this.entity.classed('active', true);
      },
      closeAllChildren: function(cb) {
        var callbacks = 0;
        for (var i = 0; i < this.menuItems.length; i++) {
          if (this.menuItems[i].isActive()) {
            ++callbacks;
            this.menuItems[i].submenu.close(function() {
              if (--callbacks == 0) {
                if (typeof cb === "function") cb();
              }
            });
          }
        }
        if (callbacks == 0) {
          if (typeof cb === "function") cb();
        }
      },
      closeNeighbors: function(cb) {
        if (this.parent) {
          this.parent.closeNeighbors(cb);
        } else {
          cb();
        }
      },
      close: function(cb) {
        var _this = this;
        this.closeAllChildren(function() {
          if (_this.direction == MENU_HORIZONTAL) {
            _this._closeHorizontal(cb);
          } else {
            _this._closeVertical(cb);
          }
        })
      },
      _closeHorizontal: function(cb) {
        var elementWidth = this.entity.node().offsetWidth;
        var _this = this;
        _this.entity.transition()
          .delay(0)
          .duration(20)
          .style('width', 0 + "px")
          .each('end', function() {
            _this.marqueeToggle(false);
            _this.entity.classed('active', false);
            _this.restoreWidth(elementWidth, true, function() {
              if (typeof cb === "function") cb();
            });
          });
      },
      _closeVertical: function(cb) {
        var _this = this;
        _this.entity.transition()
          .delay(0)
          .duration(10)
          .style('height', 0 + "px")
          .each('end', function() {
            _this.marqueeToggle(false);
            _this.entity.classed('active', false);
            if (typeof cb === "function") cb();
          });
      },
      isActive: function() {
        return this.entity.classed('active');
      },
      marqueeToggle: function(toggle) {
        for (var i = 0; i < this.menuItems.length; i++) {
          this.menuItems[i].marqueeToggle(toggle);
        }
      }
    });

    var MenuItem = Class.extend({
      init: function (parent, item) {
        var _this = this;
        this.parentMenu = parent;
        this.entity = item;
        var submenu = item.select('.' + css.list);
        if (submenu.node()) {
          this.submenu = new Menu(this, submenu);
        }
        this.entity.on('mouseenter', function() {
          if(isTouchDevice()) return;
          if (_this.parentMenu.direction == MENU_HORIZONTAL) {
            _this.openSubmenu();
          }
        }).on('click', function() {
          if(isTouchDevice()) return;
          d3.event.stopPropagation();
          _this.toggleSubmenu();
        }).onTap(function() {
            d3.event.stopPropagation();
            _this.toggleSubmenu();
        });
        return this;
      },
      setWidth: function(width, recursive) {
        if (this.submenu && recursive) {
          this.submenu.setWidth(width, recursive);
        }
        return this;
      },
      setDirection: function(direction, recursive) {
        if (this.submenu && recursive) {
          this.submenu.setDirection(direction, recursive);
        }
        return this;
      },
      toggleSubmenu: function() {
        if (this.submenu) {
          if (this.submenu.isActive()) {
            this.submenu.close();
          } else {
            this.submenu.open();
          }
        }
      },
      openSubmenu: function() {
        if (this.submenu) {
          this.submenu.open();
        } else {
          this.closeNeighbors();
        }
      },
      closeNeighbors: function(cb) {
        this.parentMenu.closeAllChildren(cb);
      },
      isActive: function() {
        return this.submenu && this.submenu.isActive();
      },

      marqueeToggle: function(toggle) {
        if(toggle) {
          var label = this.entity.select('.' + css.list_item_label);
          if(label.node().scrollWidth > this.entity.node().offsetWidth) {
            label.attr("data-content", label.text());
            this.entity.classed('marquee', true);
          }
        } else {
          this.entity.classed('marquee', false);
        }
      }
    });

    //default callback
    var callback = function(indicator) {
      console.log("Indicator selector: stub callback fired. New indicator is ", indicator);
    };

    var tree;
    var langStrings;
    var lang;
    var markerID;
    var alignX = "center";
    var alignY = "center";
    var top;
    var left;

    var TreeMenu = Component.extend({

      //setters-getters
      tree: function(input) {
        if(!arguments.length) return tree;
        tree = input;
        return this;
      },
      lang: function(input) {
        if(!arguments.length) return lang;
        lang = input;
        return this;
      },
      langStrings: function(input) {
        if(!arguments.length) return langStrings;
        langStrings = input;
        return this;
      },
      callback: function(input) {
        if(!arguments.length) return callback;
        callback = input;
        return this;
      },
      markerID: function(input) {
        if(!arguments.length) return markerID;
        markerID = input;
        return this;
      },
      alignX: function(input) {
        if(!arguments.length) return alignX;
        alignX = input;
        return this;
      },
      alignY: function(input) {
        if(!arguments.length) return alignY;
        alignY = input;
        return this;
      },
      top: function(input) {
        if(!arguments.length) return top;
        top = input;
        return this;
      },
      left: function(input) {
        if(!arguments.length) return left;
        left = input;
        return this;
      },

      init: function(config, context) {

        var _this = this;

        this.name = 'gapminder-treemenu';
        this.model_expects = [{
          name: "marker",
          type: "model"
        }, {
          name: "language",
          type: "language"
        }];

        this.context = context;
        // object for manipulation with menu representation level
        this.menuEntity = null;
        this.model_binds = {
          "change:marker": function(evt, path) {
            if(path.indexOf(markerID)==-1) return;
            _this.updateView();
          },
          "change:language.strings": function(evt) {
            _this.updateView();
          }
        };

        //contructor is the same as any component
        this._super(config, context);

        this.ui = extend({
          //...add properties here
        }, this.ui);

      },

      ready: function() {
        this.updateView();
      },

      readyOnce: function() {
        //this function is only called once at start, when both DOM and this.model are ready
        //this.element contains the view where you can append the menu
        this.element = d3.select(this.placeholder);
        //menu class private
        var _this = this;

        this.element.selectAll("div").remove();

        //general markup

        this.element.append("div")
          .attr("class", css.background)
          .on("click", function() {
            d3.event.stopPropagation();
            _this.toggle()
          });

        this.wrapper = this.element
          .append('div')
          .classed(css.wrapper, true)
          .classed(css.noTransition, true)
          .classed("vzb-dialog-scrollable", true);

        this.wrapper
          .on("click", function() {
            d3.event.stopPropagation();
          })

        this.wrapper.append("div")
          .html(iconClose)
          .on("click", function() {
            d3.event.stopPropagation();
            _this.toggle()
          })
          .select("svg")
          .attr("width", "0px")
          .attr("height", "0px")
          .attr("class", css.close)

        this.wrapper.append('div')
          .classed(css.title, true)
          .append('span');

        this.wrapper.append('div')
          .classed(css.scaletypes, true)
          .append('span');

        this.wrapper.append('div')
          .classed(css.search_wrap, true)
          .append('input')
          .classed(css.search, true)
          .attr('type', 'text')
          .attr('id', css.search);


        //init functions
        d3.select('body').on('mousemove', _this._mousemoveDocument);
        this.wrapper.on('mouseleave', function() {
          _this.menuEntity.closeAllChildren();
        });

        _this._enableSearch();

        _this.resize();
      },

      //happens on resizing of the container
      resize: function() {
        var _this = this;

        this.profiles = {
          "small": {
            col_width: 200
          },
          "medium": {
            col_width: 200
          },
          "large": {
            col_width: 200
          }
        };
        this.activeProfile = this.profiles[this.getLayoutProfile()];
        
        this.wrapper.classed(css.noTransition, true);        
        
        this.width = _this.element.node().offsetWidth;
        this.height = _this.element.node().offsetHeight;
        var rect = this.wrapper.node().getBoundingClientRect();
        var containerWidth = rect.width;
        var containerHeight = rect.height;
        OPTIONS$1.IS_MOBILE = this.getLayoutProfile() === "small";
        if (containerWidth) {
          if(top || left) {
            if(OPTIONS$1.IS_MOBILE) {
              this.clearPos();
            } else {
              if( this.wrapper.node().offsetTop < 0) {
                this.wrapper.style('top', '10px'); 
              }
              if(this.height - _this.wrapper.node().offsetTop - containerHeight < 0) {
                this.wrapper.style('top', (this.height - containerHeight - 10) + 'px');
              }
            }
          }
          
          this.wrapper.classed(css.alignXc, alignX === "center");
          this.wrapper.style("margin-left",alignX === "center"? "-" + containerWidth/2 + "px" : null);
          if (alignX === "center") {
            OPTIONS$1.MAX_MENU_WIDTH = this.width/2 - containerWidth * 0.5;
          } else {
            OPTIONS$1.MAX_MENU_WIDTH = this.width - this.wrapper.node().offsetLeft - containerWidth - 10; // 10 - padding around wrapper
            OPTIONS$1.MENU_OPEN_LEFTSIDE = OPTIONS$1.MAX_MENU_WIDTH < this.activeProfile.col_width + OPTIONS$1.MIN_COL_WIDTH;
            if(OPTIONS$1.MENU_OPEN_LEFTSIDE) OPTIONS$1.MAX_MENU_WIDTH = _this.wrapper.node().offsetLeft - 10; // 10 - padding around wrapper
            this.wrapper.classed('vzb-treemenu-open-left-side', !OPTIONS$1.IS_MOBILE && OPTIONS$1.MENU_OPEN_LEFTSIDE);
          }
        }
        
        this.wrapper.classed(css.noTransition, false);        
       
        if (this.menuEntity) {
          this.menuEntity.setWidth(this.activeProfile.col_width, true);
          if (OPTIONS$1.IS_MOBILE) {
            if (this.menuEntity.direction != MENU_VERTICAL) {
              this.menuEntity.setDirection(MENU_VERTICAL, true);
            }
          } else {
            if (this.menuEntity.direction != MENU_HORIZONTAL) {
              this.menuEntity.setDirection(MENU_HORIZONTAL, true);
            }
          }
        }
        return this;
      },

      toggle: function() {
        var _this = this;
        var hidden = !this.element.classed(css.hidden);
        this.element.classed(css.hidden, hidden);

        if(hidden) {
          if(top || left) this.clearPos();
          this.menuEntity.marqueeToggle(false);
        } else {
          if(top || left) this.setPos();
          this.resize();
        }

        this.wrapper.classed(css.noTransition, hidden);    

        this.parent.components.forEach(function(c) {
          if(c.name == "gapminder-dialogs") {
            d3.select(c.placeholder.parentNode).classed("vzb-blur", !hidden);
          } else 
          if(c.element.classed) {
            c.element.classed("vzb-blur", c != _this && !hidden);
          } else {
            d3.select(c.element).classed("vzb-blur", c != _this && !hidden);
          }
        });

        this.width = _this.element.node().offsetWidth;
      },
      
      setPos: function() {
        var rect = this.wrapper.node().getBoundingClientRect();      
        
        if(top) {
          this.wrapper.style({'top': top + 'px', 'bottom': 'auto'});
          this.wrapper.classed(css.absPosVert, top);
        }
        if(left) {
          var right = this.element.node().offsetWidth - left - rect.width; 
          this.wrapper.style({'right': right + 'px', 'left': 'auto'});    
          this.wrapper.classed(css.absPosHoriz, right);
        }

      },

      clearPos: function() {
        top = '';
        left = '';
        this.wrapper.attr("style", "");
        this.wrapper.classed(css.absPosVert, '');
        this.wrapper.classed(css.absPosHoriz, '');
        this.wrapper.classed(css.menuOpenLeftSide, '');
      },
      //search listener
      _enableSearch: function() {
        var _this = this;

        var input = this.wrapper.select('.' + css.search);

        //it forms the array of possible queries
        var getMatches = function(value) {
          var matches = {
            _id: 'root',
            children: []
          };

          //translation integration
          var translationMatch = function(value, data, i) {
            var arr = [];
            if(_this.langStrings()) {
              for(var language in _this.langStrings()) {
                for(var key in _this.langStrings()[language]) {
                  if(key.indexOf('indicator/') == 0 &&
                    key.replace(/indicator\//g,"") == data[i][OPTIONS$1.SEARCH_PROPERTY] &&
                    _this.langStrings()[language][key].toLowerCase().indexOf(
                      value.toLowerCase()) >= 0) {
                    return true;
                  };
                };
              };
            };
            return false;
          };

          var matching = function(data) {
            for(var i = 0; i < data.length; i++) {
              var match = false;
              match =  translationMatch(value, data, i);
              if(match) {
                matches.children.push(data[i]);
              }
              if(!match && data[i][OPTIONS$1.SUBMENUS]) {
                matching(data[i][OPTIONS$1.SUBMENUS]);
              }
            }
          };
          matching(tree.children);
          return matches;
        };

        var searchIt = function() {
          var value = input.node().value;

          if(value.length >= OPTIONS$1.SEARCH_MIN_STR) {
            _this.redraw(getMatches(value));
          } else {
            _this.redraw();
          }
        };

        input.on('keyup', searchIt);
      },


      //this function process click on list item
      _selectIndicator: function(item, view) {

        view = d3.select(view);

        //only for leaf nodes
        if(view.attr("children")) return;
        callback("which", view.attr("info"), markerID);
        this.toggle();
      },




      //function is redrawing data and built structure
      redraw: function(data) {
        var _this = this;
        this.element.select('.' + css.title).select("span")
          .text(this.translator("buttons/" + markerID));

        this.element.select('.' + css.search)
          .attr("placeholder", this.translator("placeholder/search") + "...");


        if(data == null) data = tree;
        this.wrapper.select('ul').remove();

        var indicatorsDB = _this.model.marker.getMetadata();         

        var allowedIDs = keys(indicatorsDB).filter(function(f) {
          //check if indicator is denied to show with allow->names->!indicator
          if(_this.model.marker[markerID].allow && _this.model.marker[markerID].allow.names
            && _this.model.marker[markerID].allow.names.indexOf('!' + f) != -1) return false;
          //keep indicator if nothing is specified in tool properties
          if(!_this.model.marker[markerID].allow || !_this.model.marker[markerID].allow.scales) return true;
          //keep indicator if any scale is allowed in tool properties
          if(_this.model.marker[markerID].allow.scales[0] == "*") return true;

          //check if there is an intersection between the allowed tool scale types and the ones of indicator
          for(var i = indicatorsDB[f].scales.length - 1; i >= 0; i--) {
            if(_this.model.marker[markerID].allow.scales.indexOf(indicatorsDB[f].scales[i]) > -1) return true;
          }

          return false;
        })

        var dataFiltered = pruneTree(data, function(f) {
          return allowedIDs.indexOf(f.id) > -1
        });

        var createSubmeny = function(select, data, toplevel) {
          if(!data.children) return;
          var li = select.append('ul')
            .classed(css.list, !toplevel)
            .classed(css.list_top_level, toplevel)
            .selectAll('li')
            .data(data.children, function(d) {
              return d['id'];
            })
            .enter()
            .append('li');

          li.append('span')
            .classed(css.list_item_label, true)
            .text(function(d) {
              return _this.translator("indicator/" + d.id);
            })
            .attr("info", function(d) {
              return d.id;
            })
            .attr("children", function(d) {
              return d.children ? "true" : null;
            })
            .on('click', function(d) {
              _this._selectIndicator(d, this)
            });
          li.append('div')
            .classed(css.list_item_label + '-mask', true);

          li.classed(css.list_item, true)
            .classed(css.hasChild, function(d) {
              return d['children'];
            })
            .classed(css.isSpecial, function(d) {
              return d['special'];
            })
            .each(function(d) {
              var view = d3.select(this);
              createSubmeny(view, d);
            });
        };
        if (OPTIONS$1.IS_MOBILE) {
          OPTIONS$1.MENU_DIRECTION = MENU_VERTICAL;
        }
        createSubmeny(this.wrapper, dataFiltered, true);
        this.menuEntity = new Menu(null, this.wrapper.select('.' + css.list_top_level))
          .setWidth(this.activeProfile.col_width, true)
          .setDirection(OPTIONS$1.MENU_DIRECTION);

        var pointer = "_default";
        if(allowedIDs.indexOf(this.model.marker[markerID].which) > -1) pointer = this.model.marker[markerID].which;
        var scaleTypesData = indicatorsDB[pointer].scales.filter(function(f) {
          if(!_this.model.marker[markerID].allow || !_this.model.marker[markerID].allow.scales) return true;
          if(_this.model.marker[markerID].allow.scales[0] == "*") return true;
          return _this.model.marker[markerID].allow.scales.indexOf(f) > -1;
        });

        var scaleTypes = this.element.select('.' + css.scaletypes).selectAll("span")
            .data(scaleTypesData, function(d){return d});

        scaleTypes.exit().remove();

        scaleTypes.enter().append("span")
            .on("click", function(d){
              d3.event.stopPropagation();
              _this._setModel("scaleType", d, markerID)
            });

        scaleTypes
            .classed(css.scaletypesDisabled, scaleTypesData.length < 2)
            .classed(css.scaletypesActive, function(d){
                return d == _this.model.marker[markerID].scaleType && scaleTypesData.length > 1;
            })
            .text(function(d){
                return _this.translator("scaletype/" + d);
            });


        return this;
      },






      updateView: function() {
        var _this = this;
        var languageID = _this.model.language.id;

        if(!markerID) return;

          this.wrapper.classed(css.absPosVert, top);
          this.wrapper.classed(css.alignYt, alignY === "top");
          this.wrapper.classed(css.alignYb, alignY === "bottom");
          this.wrapper.classed(css.absPosHoriz, left);
          this.wrapper.classed(css.alignXl, alignX === "left");
          this.wrapper.classed(css.alignXr, alignX === "right");
        

        var strings = langStrings ? langStrings : {};
        strings[languageID] = _this.model.language.strings[languageID];

        this.translator = this.model.language.getTFunction();

        var setModel = this._setModel.bind(this);
        this.langStrings(strings)
          .lang(languageID)
          .callback(setModel)
          .tree(this.model.marker.getIndicatorsTree())
          .redraw();

        return this;
      },

      _setModel: function(what, value, markerID) {

        var indicatorsDB = this.model.marker.getMetadata();

        var mdl = this.model.marker[markerID];

        var obj = {};

        obj[what] = value;

        if(what == "which") {
          obj.use = indicatorsDB[value].use;
          obj.scaleType = indicatorsDB[value].scales[0];
        }

        if(mdl.getType() == 'axis') {
          obj.domainMin = null;
          obj.domainMax = null;
          obj.zoomedMin = null;
          obj.zoomedMax = null;
        }

        mdl.set(obj);

      }


    });

    var precision = 1;

    //constants
    var class_playing = "vzb-playing";
    var class_loading$2 = "vzb-ts-loading";
    var class_hide_play = "vzb-ts-hide-play-button";
    var class_dragging = "vzb-ts-dragging";
    var class_axis_aligned = "vzb-ts-axis-aligned";
    var class_show_value = "vzb-ts-show-value";
    var class_show_value_when_drag_play = "vzb-ts-show-value-when-drag-play";

    //margins for slider
    var profiles$1 = {
      small: {
        margin: {
          top: 7,
          right: 15,
          bottom: 10,
          left: 15
        },
        radius: 8,
        label_spacing: 10
      },
      medium: {
        margin: {
          top: 16,
          right: 15,
          bottom: 10,
          left: 15
        },
        radius: 9,
        label_spacing: 12
      },
      large: {
        margin: {
          top: 14,
          right: 15,
          bottom: 10,
          left: 15
        },
        radius: 11,
        label_spacing: 14
      }
    };


    var presentationProfileChanges = {
      "medium": {
        margin: {
          top: 9
        }
      },
      "large": {
        margin: {
        }
      }
    }

    var TimeSlider = Component.extend({
      /**
       * Initializes the timeslider.
       * Executed once before any template is rendered.
       * @param model The model passed to the component
       * @param context The component's parent
       */
      init: function(model, context) {

        this.name = "gapminder-timeslider";
        this.template = this.template || "timeslider.html";
        this.prevPosition = null;
        //define expected models/hooks for this component
        this.model_expects = [{
          name: "time",
          type: "time"
        }];

        var _this = this;

        //starts as splash if this is the option
        this._splash = model.ui.splash;

        //binds methods to this model
        this.model_binds = {
          'change:time': function(evt, path) {

            //TODO: readyOnce CANNOT be run twice
            if(_this._splash !== _this.model.time.splash) {
              _this._splash = _this.model.time.splash;
              _this.readyOnce();
              _this.ready();
            }

            if(!_this._splash) {

              if((['time.start', 'time.end']).indexOf(path) !== -1) {
                _this.changeLimits();
              }
              _this._optionClasses();
            }
          },
          'change:time.value': function(evt, path) {
            if(!_this._splash) {
              //only set handle position if change is external
              if(!_this.model.time.dragging) _this._setHandle(_this.model.time.playing);
            }
          }
        };

        this.ui = extend({
          show_limits: false,
          show_value: false,
          show_value_when_drag_play: true,
          show_button: true,
          class_axis_aligned: false
        }, model.ui, this.ui);

        // Same constructor as the superclass
        this._super(model, context);

        //defaults
        this.width = 0;
        this.height = 0;

        this.getValueWidth = memoize(this.getValueWidth);
        this._setTime = throttle(this._setTime, 50);
      },

      //template is ready
      readyOnce: function () {

        if(this._splash) return;

        var _this = this;

        //DOM to d3
        //TODO: remove this ugly hack
        this.element = isArray(this.element) ? this.element : d3.select(this.element);
        this.element.classed(class_loading$2, false);

        //html elements
        this.slider_outer = this.element.select(".vzb-ts-slider");
        this.slider = this.slider_outer.select("g");
        this.axis = this.element.select(".vzb-ts-slider-axis");
        this.slide = this.element.select(".vzb-ts-slider-slide");
        this.handle = this.slide.select(".vzb-ts-slider-handle");
        this.valueText = this.slide.select('.vzb-ts-slider-value');
        //Scale
        this.xScale = d3.time.scale.utc()
          .clamp(true);

        //Axis
        this.xAxis = d3.svg.axis()
          .orient("bottom")
          .tickSize(0);
        //Value
        this.valueText.attr("text-anchor", "middle").attr("dy", "-0.7em");

        var brushed = _this._getBrushed(),
          brushedEnd = _this._getBrushedEnd();

        //Brush for dragging
        this.brush = d3.svg.brush()
          .x(this.xScale)
          .extent([0, 0])
          .on("brush", function () {
            brushed.call(this);
          })
          .on("brushend", function () {
            brushedEnd.call(this);
          });

        //Slide
        this.slide.call(this.brush);
        this.slide.selectAll(".extent,.resize")
          .remove();


        this.parent.on('myEvent', function (evt, arg) {
          var layoutProfile = _this.getLayoutProfile();

          if (arg.profile && arg.profile.margin) {
            profiles$1[layoutProfile].margin = arg.profile.margin;
          }

          // set the right margin that depends on longest label width
          _this.element.select(".vzb-ts-slider-wrapper")
            .style("right", (arg.mRight - profiles$1[layoutProfile].margin.right) + "px");

          _this.xScale.range([0, arg.rangeMax]);
          _this.resize();
        });
      },

      //template and model are ready
      ready: function () {
        if(this._splash) return;

        var play = this.element.select(".vzb-ts-btn-play");
        var pause = this.element.select(".vzb-ts-btn-pause");
        var _this = this;
        var time = this.model.time;

        play.on('click', function () {

          _this.model.time.play();
        });

        pause.on('click', function () {
          _this.model.time.pause("soft");
        });

        this.changeLimits();
        this.changeTime();
        this.resize();

      },

      changeLimits: function() {
        var minValue = this.model.time.start;
        var maxValue = this.model.time.end;
        //scale
        this.xScale.domain([minValue, maxValue]);
        //axis
        this.xAxis.tickValues([minValue, maxValue])
          .tickFormat(this.model.time.timeFormat);
      },

      changeTime: function() {
        this.ui.format = this.model.time.unit;
        //time slider should always receive a time model
        var time = this.model.time.value;
        //special classes
        this._optionClasses();
      },

      /**
       * Executes everytime the container or vizabi is resized
       * Ideally,it contains only operations related to size
       */
      resize: function () {

        this.model.time.pause();

        this.profile = this.getActiveProfile(profiles$1, presentationProfileChanges);

        var slider_w = parseInt(this.slider_outer.style("width"), 10);
        var slider_h = parseInt(this.slider_outer.style("height"), 10);
        this.width = slider_w - this.profile.margin.left - this.profile.margin.right;
        this.height = slider_h - this.profile.margin.bottom - this.profile.margin.top;
        var _this = this;

        //translate according to margins
        this.slider.attr("transform", "translate(" + this.profile.margin.left + "," + this.profile.margin.top + ")");

        //adjust scale width if it was not set manually before
        if (this.xScale.range()[1] = 1) this.xScale.range([0, this.width]);

        //adjust axis with scale
        this.xAxis = this.xAxis.scale(this.xScale)
          .tickPadding(this.profile.label_spacing);

        this.axis.attr("transform", "translate(0," + this.height / 2 + ")")
          .call(this.xAxis);

        this.slide.select(".background")
          .attr("height", this.height);

        //size of handle
        this.handle.attr("transform", "translate(0," + this.height / 2 + ")")
          .attr("r", this.profile.radius);

        this.sliderWidth = _this.slider.node().getBoundingClientRect().width;

        this._setHandle();

      },

      /**
       * Returns width of slider text value.
       * Parameters in this function needed for memoize function, so they are not redundant.
       */
      getValueWidth: function(layout, value) {
        return this.valueText.node().getBoundingClientRect().width;
      },

      /**
       * Gets brushed function to be executed when dragging
       * @returns {Function} brushed function
       */
      _getBrushed: function() {
        var _this = this;
        return function() {

          if (_this.model.time.playing)
            _this.model.time.pause();

          _this._optionClasses();
          _this.element.classed(class_dragging, true);

          var value = _this.brush.extent()[0];

          //set brushed properties

          if(d3.event.sourceEvent) {
            _this.model.time.dragStart();
            var posX = roundStep(Math.round(d3.mouse(this)[0]), precision);
            value = _this.xScale.invert(posX);
            var maxPosX = _this.width;

            if(posX > maxPosX) {
              posX = maxPosX;
            } else if(posX < 0) {
              posX = 0;
            }

            //set handle position
            _this.handle.attr("cx", posX);
            _this.valueText.attr("transform", "translate(" + posX + "," + (_this.height / 2) + ")");
            _this.valueText.text(_this.model.time.timeFormat(value));
          }

          //set time according to dragged position
          if(value - _this.model.time.value !== 0) {
            _this._setTime(value);
          }
        };
      },

      /**
       * Gets brushedEnd function to be executed when dragging ends
       * @returns {Function} brushedEnd function
       */
      _getBrushedEnd: function() {
        var _this = this;
        return function() {
          _this._setTime.recallLast();
          _this.element.classed(class_dragging, false);
          _this.model.time.dragStop();
          _this.model.time.snap();
        };
      },

      /**
       * Sets the handle to the correct position
       * @param {Boolean} transition whether to use transition or not
       */
      _setHandle: function(transition) {
        var _this = this;
        var value = this.model.time.value;
        this.slide.call(this.brush.extent([value, value]));
        this.valueText.text(this.model.time.timeFormat(value));

    //    var old_pos = this.handle.attr("cx");
        var new_pos = this.xScale(value);
        if(_this.prevPosition == null) _this.prevPosition = new_pos;
        var delayAnimations = new_pos > _this.prevPosition ? this.model.time.delayAnimations : 0;
        if(transition) {
          this.handle.attr("cx", _this.prevPosition)
            .transition()
            .duration(delayAnimations)
            .ease("linear")
            .attr("cx", new_pos);

          this.valueText.attr("transform", "translate(" + _this.prevPosition + "," + (this.height / 2) + ")")
            .transition()
            .duration(delayAnimations)
            .ease("linear")
            .attr("transform", "translate(" + new_pos + "," + (this.height / 2) + ")");
        } else {
          this.handle
            //cancel active transition
            .interrupt()
            .attr("cx", new_pos);

          this.valueText
            //cancel active transition
            .interrupt()
            .attr("transform", "translate(" + new_pos + "," + (this.height / 2) + ")");
        }
        _this.prevPosition = new_pos;

      },

      /**
       * Sets the current time model to time
       * @param {number} time The time
       */
      _setTime: function(time) {
        //update state
        var _this = this;
        //  frameRate = 50;

        //avoid updating more than once in "frameRate"
        //var now = new Date();
        //if (this._updTime != null && now - this._updTime < frameRate) return;
        //this._updTime = now;
        var persistent = !this.model.time.dragging && !this.model.time.playing;
        _this.model.time.getModelObject('value').set(time, false, persistent); // non persistent
      },


      /**
       * Applies some classes to the element according to options
       */
      _optionClasses: function() {
        //show/hide classes

        var show_limits = this.ui.show_limits;
        var show_value = this.ui.show_value;
        var show_value_when_drag_play = this.ui.show_value_when_drag_play;
        var axis_aligned = this.ui.axis_aligned;
        var show_play = (this.ui.show_button) && (this.model.time.playable);

        if(!show_limits) {
          this.xAxis.tickValues([]).ticks(0);
        }

        this.element.classed(class_hide_play, !show_play);
        this.element.classed(class_playing, this.model.time.playing);
        this.element.classed(class_show_value, show_value);
        this.element.classed(class_show_value_when_drag_play, show_value_when_drag_play);
        this.element.classed(class_axis_aligned, axis_aligned);
      }
    });

    /*!
     * VIZABI GENERIC SLIDER CONTROL
     * Reusable SLIDER
     */

    var SimpleSlider = Component.extend({

        init: function (config, context) {
          this.template = '<div class="vzb-ss-holder"><input type="range" id="vzb-ss-slider" class="vzb-ss-slider" step="1"></div>';

          this.model_expects = [{
            name: "submodel"
          }];

          var _this = this;
          this.name = 'gapminder-simpleSlider';

          this.arg = config.arg;
          this.thumb_size = config.thumb_size;
          this.slider_properties = config.properties;

          this.model_binds = {};
          this.model_binds["change:submodel." + this.arg] = function (evt) {
            _this.updateView();
          };

          //contructor is the same as any component
          this._super(config, context);
            
          this._setModel = throttle(this._setModel, 50);
        },

        /**
         * Executes after the template is loaded and rendered.
         * Ideally, it contains HTML instantiations related to template
         * At this point, this.element and this.placeholder are available as a d3 object
         */
        readyOnce: function () {

          //default values
          var min = 0;
          var max = 1;
          var step = 0.1;
          var value = min;

          //selecting elements
          var _this = this;
          this.element = d3.select(this.element);
          this.slider = this.element.selectAll('#vzb-ss-slider');

          this.elementSize = this.element.node().getBoundingClientRect();
          this.sliderSize = this.slider.node().getBoundingClientRect();
          this.slider.style('left', (this.elementSize.left - this.sliderSize.left) + 'px');

          //TODO: replace this with utils.extend
          if(this.slider_properties){
            if(this.slider_properties.min != null) min = this.slider_properties.min;
            if(this.slider_properties.max != null) max = this.slider_properties.max;
            if(this.slider_properties.step != null) step = this.slider_properties.step;

            if(this.slider_properties.scale){
              value = this.slider_properties.scale(min);
            }
          }

          //check and change the slider's thumb size
          if(this.thumb_size){
            this.slider.classed('vzb-ss-slider', false);
            this.slider.classed('vzb-ss-slider-'+this.thumb_size, true);
          }

          this.slider
            .attr('min', min)
            .attr('max', max)
            .attr('step', step)
            .attr('value', value)
            .on('input', function () {
              var value = +d3.event.target.value;
              _this._setModel(value, false, false); // on drag - non-persistent changes while dragging
            })
            .on('change', function() {
              var value = +d3.event.target.value;
              _this._setModel(value, true); // on drag end - value is probably same as last 'input'-event, so force change
            });

          this.updateView();
        },

        updateView: function () {
          var value = this.model.submodel[this.arg];
          var slider_properties = this.slider_properties;
          var scale;

          if(slider_properties){
            scale = slider_properties.scale;
          }
          if (scale){
            value = scale.invert(value);
          }

          //this.slider.attr('value', value);
          this.slider.node().value = value;
        },

        _setModel: function (value, force, persistent) {
          // rescale value if scale is supplied in slider_properties 
          if(this.slider_properties && this.slider_properties.scale) value = this.slider_properties.scale(value);
          
          this.model.submodel.getModelObject(this.arg).set(value, force, persistent);
        }

      });

    var simplecheckbox = Component.extend({

      init: function(config, context) {
        this.template =
          '<span class="vzb-sc-holder vzb-dialog-checkbox"><input type="checkbox"><label></label></span>';
        var _this = this;
        this.name = 'gapminder-simplecheckbox';

        this.checkbox = config.checkbox;
        this.submodel = config.submodel;

        this.model_expects = [{
          name: "mdl"
            //TODO: learn how to expect model "axis" or "size" or "color"
        }, {
          name: "language",
          type: "language"
        }];


        this.model_binds = {
          "change:mdl": function(evt) {
            _this.updateView();
          },
          "change:language.strings": function(evt) {
            _this.updateView();
          }
        };

        var submodel = (this.submodel) ? this.submodel + ':' : '';
        this.model_binds["change:mdl." + submodel + this.checkbox] = function() {
          _this.updateView();
        };

        //contructor is the same as any component
        this._super(config, context);
      },

      ready: function() {
        this.parentModel = (this.submodel) ? this.model.mdl[this.submodel] : this.model.mdl;
        this.updateView();
      },

      readyOnce: function() {
        var _this = this;
        this.element = d3.select(this.element);
        var id = "-check-" + Math.random() * 1000;
        this.labelEl = this.element.select('label').attr("for", id);
        this.checkEl = this.element.select('input').attr("id", id)
          .on("change", function() {
            _this._setModel(d3.select(this).property("checked"));
          });
      },

      updateView: function() {
        this.translator = this.model.language.getTFunction();
        this.labelEl.text(this.translator("check/" + this.checkbox));
        this.checkEl.property("checked", !!this.parentModel[this.checkbox]);
      },

      _setModel: function(value) {
        this.parentModel[this.checkbox] = value;
      }

    });

    /*!
     * VIZABI MIN MAX INPUT FIELDS
     */

    var DOMAINMIN = "domainMin";
    var DOMAINMAX = "domainMax";
    var ZOOMEDMIN = "zoomedMin";
    var ZOOMEDMAX = "zoomedMax";

    var MinMaxInputs = Component.extend({

        /**
         * Initializes the Component.
         * Executed once before any template is rendered.
         * @param config The options passed to the component
         * @param context The component's parent
         */
        init: function(config, context) {

            this.name = 'gapminder-minmaxinputs';
            this.template = 'minmaxinputs.html';

            var _this = this;

            this.model_expects = [{
                name: "marker",
                type: "model"
            }, {
                name: "language",
                type: "language"
            }];

            this.markerID = config.markerID;
            if(!config.markerID) warn("minmaxinputs.js complains on 'markerID' property: " + config.markerID);

            this.model_binds = {};
            this.model_binds["change:language.strings"] = function(evt) {
                _this.updateView();
            };
            this.model_binds["change:marker." + this.markerID] = function(evt) {
                _this.updateView();
            };
            this.model_binds["ready"] = function(evt) {
                _this.updateView();
            };

            //contructor is the same as any component
            this._super(config, context);

            this.ui = extend({
                selectDomainMinMax: false,
                selectZoomedMinMax: false
            }, this.ui.getPlainObject());

        },

        ready: function() {
            this.updateView();
        },

        readyOnce: function() {
            var _this = this;

            this.element = d3.select(this.element);

            this.el_domain_labelMin = this.element.select('.vzb-mmi-domainmin-label');
            this.el_domain_labelMax = this.element.select('.vzb-mmi-domainmax-label');
            this.el_domain_fieldMin = this.element.select('.vzb-mmi-domainmin');
            this.el_domain_fieldMax = this.element.select('.vzb-mmi-domainmax');

            this.el_break = this.element.select('.vzb-mmi-break');
            
            this.el_zoomed_labelMin = this.element.select('.vzb-mmi-zoomedmin-label');
            this.el_zoomed_labelMax = this.element.select('.vzb-mmi-zoomedmax-label');
            this.el_zoomed_fieldMin = this.element.select('.vzb-mmi-zoomedmin');
            this.el_zoomed_fieldMax = this.element.select('.vzb-mmi-zoomedmax');


            _this.el_domain_fieldMin.on("change", function() {
                _this._setModel(DOMAINMIN, this.value)
            });
            _this.el_domain_fieldMax.on("change", function() {
                _this._setModel(DOMAINMAX, this.value)
            });

            _this.el_zoomed_fieldMin.on("change", function() {
                _this._setModel(ZOOMEDMIN, this.value)
            });
            _this.el_zoomed_fieldMax.on("change", function() {
                _this._setModel(ZOOMEDMAX, this.value)
            });
            
            this.element.selectAll("input")
                .on("keypress", function(e) {
                    if(d3.event.which == 13) document.activeElement.blur();
                });
        },

        updateView: function() {
            var _this = this;
            this.translator = this.model.language.getTFunction();

            this.el_domain_labelMin.text(this.translator("min") + ":");
            this.el_domain_labelMax.text(this.translator("max") + ":");
            this.el_zoomed_labelMin.text(this.translator("min") + ":");
            this.el_zoomed_labelMax.text(this.translator("max") + ":");

            this.el_domain_labelMin.classed('vzb-hidden', !this.ui.selectDomainMinMax);
            this.el_domain_labelMax.classed('vzb-hidden', !this.ui.selectDomainMinMax);
            this.el_domain_fieldMin.classed('vzb-hidden', !this.ui.selectDomainMinMax);
            this.el_domain_fieldMax.classed('vzb-hidden', !this.ui.selectDomainMinMax);

            this.el_break.classed('vzb-hidden', !(this.ui.selectDomainMinMax && this.ui.selectZoomedMinMax));

            this.el_zoomed_labelMin.classed('vzb-hidden', !this.ui.selectZoomedMinMax);
            this.el_zoomed_labelMax.classed('vzb-hidden', !this.ui.selectZoomedMinMax);
            this.el_zoomed_fieldMin.classed('vzb-hidden', !this.ui.selectZoomedMinMax);
            this.el_zoomed_fieldMax.classed('vzb-hidden', !this.ui.selectZoomedMinMax);

            var formatter = d3.format(".2r");
            this.el_domain_fieldMin.property("value", formatter(this.model.marker[this.markerID].getScale().domain()[0]));
            this.el_domain_fieldMax.property("value", formatter(this.model.marker[this.markerID].getScale().domain()[1]));

            this.el_zoomed_fieldMin.property("value", formatter(this.model.marker[this.markerID].zoomedMin));
            this.el_zoomed_fieldMax.property("value", formatter(this.model.marker[this.markerID].zoomedMax));
        },

        _setModel: function(what, value) {
            this.model.marker[this.markerID][what] = strToFloat(value);
        }

    });

    /*!
     * VIZABI INDICATOR PICKER
     * Reusable indicator picker component
     */

    var IndPicker = Component.extend({

        /**
         * Initializes the Indicator Picker.
         * Executed once before any template is rendered.
         * @param config The options passed to the component
         * @param context The component's parent
         */
        init: function(config, context) {

            this.name = 'gapminder-indicatorpicker';
            this.template = '<span class="vzb-ip-select"></span>';

            var _this = this;

            this.model_expects = [{
                name: "marker",
                type: "model"
            }, {
                name: "language",
                type: "language"
            }];

            this.markerID = config.markerID;
            if(!config.markerID) warn("indicatorpicker.js complains on 'markerID' property: " + config.markerID);

            this.model_binds = {
                "change:language.strings": function(evt) {
                    _this.updateView();
                },
                "change:marker": function(evt) {
                    _this.updateView();
                },
                "ready": function(evt) {
                    _this.updateView();
                }
            };


            //contructor is the same as any component
            this._super(config, context);
        },

        ready: function() {
            this.updateView();
        },


        readyOnce: function() {
            var _this = this;

            this.el_select = d3.select(this.element);

            this.el_select.on("click", function() {
                var rect = _this.el_select.node().getBoundingClientRect();
                var rootRect = _this.root.element.getBoundingClientRect();
                var treemenuComp = _this.root.findChildByName("gapminder-treemenu");
                var treemenuColWidth = treemenuComp.activeProfile.col_width; 
                var treemenuPaddLeft = parseInt(treemenuComp.wrapper.style('padding-left'), 10) || 0; 
                var treemenuPaddRight = parseInt(treemenuComp.wrapper.style('padding-right'), 10) || 0; 
                var topPos = rect.bottom - rootRect.top;
                var leftPos = rect.left - rootRect.left - (treemenuPaddLeft + treemenuPaddRight + treemenuColWidth - rect.width) * .5;
                
                treemenuComp
                    .markerID(_this.markerID)
                    .alignX("left")
                    .alignY("top")
                    .top(topPos)
                    .left(leftPos)
                    .updateView()
                    .toggle();
            });
        },

        
        updateView: function() {
            if(!this._readyOnce) return;

            var _this = this;
            this.translator = this.model.language.getTFunction();
            this.el_select.text(this.translator("indicator/" + this.model.marker[this.markerID].which));
        }
        
    });

    var DraggableList = Component.extend({

      init: function(config, context) {
        this.template = '<span class="vzb-dl-holder"><ul class="vzb-draggable list vzb-dialog-scrollable"></ul></span>';
        var _this = this;
        this.name = 'draggableList';

        this.dataArrFn = config.dataArrFn;
        this.lang = config.lang;

        this.model_expects = [{
          name: "group",
          type: "model"
        }, {
          name: "language",
          type: "language"
        }];
        
        this.groupID = config.groupID;
        if(!config.groupID) warn("draggablelist.js complains on 'groupID' property: " + config.groupID);

        this.model_binds = {
          "change:language.strings": function(evt) {
            _this.updateView();
          }
        };
        
        this.model_binds["change:group." + this.groupID] = function(evt) {
            _this.updateView();
        };
        

        this._super(config, context);

        this.updateData = debounce(this.updateData, 1000);
        
        this.itemDragger = d3.behavior.drag()
          .on('dragstart', function(draggedData, i) {
            if(_this.dataUpdateFlag) return;
            d3.event.sourceEvent.stopPropagation();
            _this.parentBoundRect = _this.element.node().getBoundingClientRect();
            _this.element
              .selectAll('div')
              .each(function(d, i) {
                var boundRect = this.getBoundingClientRect();
                d._y = boundRect.top;
                d._top = 0; 
                if(draggedData.data === d.data) {
                  d._height = boundRect.height;
                  _this.selectedNode = this;
                }
              })
            d3.select(_this.selectedNode)
              .classed('dragged', true)
          })
          
          .on('drag', function(draggedData, draggedIndex) {
            if(_this.dataUpdateFlag) return;
            draggedData._top += d3.event.dy;
            var newDraggedY = draggedData._y + draggedData._top;
            if(newDraggedY > _this.parentBoundRect.top 
              && newDraggedY + draggedData._height < _this.parentBoundRect.top + _this.parentBoundRect.height)
            {
              _this.itemsEl
                .style('top', function(d, i) {
                  var top = 0;
                  
                  if (i < draggedIndex && d._y + draggedData._height * .5 > newDraggedY) {
                    top = draggedData._height;
                  }
                  else if(i > draggedIndex && d._y - draggedData._height * .5 < newDraggedY) {
                    top = -draggedData._height;
                  }
                  
                  if (i != draggedIndex) d._top = top;
                  return d._top + "px";
               })
            }
          })
          
          .on('dragend', function(d, i) {
            if(_this.dataUpdateFlag) return;
            _this.getData();     
          })
          
      },

      ready: function() {
        var _this = this;

        this.updateView();

        this.itemsEl = this.element
          .selectAll('div')
        
        this.itemsEl
          .call(_this.itemDragger);
          
        var test = this.itemsEl.select('li')
          .on('mouseover', function() {
            d3.select(this).classed('hover', true);
          })
          .on('mouseout', function() {
            d3.select(this).classed('hover', false);        
          })
          .on('touchstart', function() {
            d3.event.preventDefault();
          })
          
      },

      updateView: function() {
        var _this = this;
        this.translator = this.model.language.getTFunction();

        this.items = this.element.selectAll('div').data(function() {
          return _this.dataArrFn().map( function(d) { return {data:d};})});
        this.items.enter()
          .append('div')
          .attr('draggable', true)
          .append('li');
        this.items.select('li').classed('hover', false).each(function(val, index) {
            d3.select(this).attr('data', val['data']).text(_this.translator(_this.lang + val['data']));
          });
        this.items.exit().remove();
        this.element.selectAll('div')
          .style('top', '')
          .classed('dragged', false);
        this.dataUpdateFlag = false;
         
      },
      
      getData: function() {
        var dataArr = [];
        var data = this.element
          .selectAll('div').data();

        dataArr = data
          .sort(function(a, b) {
            return (a._y + a._top) - (b._y + b._top);
          })
          .map(function(d) {
            return d.data        
          })
        if(arrayEquals(this.dataArrFn(), dataArr)) {
          this.updateView();
        } else {
          this.dataUpdateFlag = true;
          this.updateData(dataArr);
        }
      },
      
      updateData: function(dataArr) {
        this.dataArrFn(dataArr);    
      },

      readyOnce: function() {
        var _this = this;

        this.element = d3.select(this.element).select('.list');
        
      }

    });

    /*!
     * VIZABI DIALOG
     * Reusable Dialog component
     */

    var Dialog = Component.extend({
      /**
       * Initializes the dialog
       * @param {Object} config Initial config, with name and placeholder
       * @param {Object} parent Reference to tool
       */
      init: function(config, parent) {
        this.name = this.name || '';

        this.model_expects = this.model_expects || [{
          name: "state",
          type: "model"
        }, {
          name: "ui",
          type: "model"
        }, {
          name: "language",
          type: "language"
        }];

        this.template = this.name + '.html';

        this._super(config, parent);
      },

      /**
       * Executed when the dialog has been rendered
       */
      readyOnce: function() {
        this.element = d3.select(this.element);
      },

      ready: function() {
        var _this = this;
        this.placeholderEl = d3.select(this.placeholder);
        this.rootEl = d3.select(this.root.element);
        this.dragHandler = this.placeholderEl.select("[data-click='dragDialog']");
        this.dragHandler.html(iconDrag);
        this.pinIcon = this.placeholderEl.select("[data-click='pinDialog']");
        this.pinIcon.html(iconPin);
        this.dragContainerEl = d3.select('.vzb-tool');
        this.topPos = '';
        var profile = this.getLayoutProfile();

        var dg = dialogDrag(this.placeholderEl, this.dragContainerEl, 10);
        var dragBehavior = d3.behavior.drag()
          .on('dragstart', function D3dialogDragStart() {
            var topPos = _this.placeholderEl.node().offsetTop;
            _this.placeholderEl.style({'top': topPos + 'px', 'bottom': 'auto'});
            _this.trigger('dragstart');
            dg.dragStart(d3.event);
          })
          .on('drag', function D3dialogDrag() {
            _this.trigger('drag');
            dg.drag(d3.event);
          })
          .on('dragend', function D3dialogDrag() {
            _this.rightPos = _this.placeholderEl.style('right');
            _this.topPos = _this.placeholderEl.style('top');
            _this.trigger('dragend');
          });
        this.dragHandler.call(dragBehavior);

        this.dragHandler.classed("vzb-hidden", profile === 'small');
        this.pinIcon.classed("vzb-hidden", profile === 'small');
        this.resize();
      },

      resize: function() {
        if(this.placeholderEl && this.dragContainerEl) {
          var profile = this.getLayoutProfile();
          var chartWidth = parseInt(this.dragContainerEl.style('width'), 10);
          var dialogRight = parseInt(this.rightPos, 10);
          var chartHeight = parseInt(this.rootEl.style('height'), 10);
          var dialogTop = parseInt(this.topPos, 10);
          var dialogWidth = parseInt(this.placeholderEl.style('width'), 10);
          var dialogHeight = parseInt(this.placeholderEl.style('height'), 10);
          var dialogRightMargin = parseInt(this.placeholderEl.style('margin-right'), 10) || 0;
          if(isNumber(dialogRight) && dialogRight > chartWidth - dialogWidth - dialogRightMargin) {
            if(this.rightPos) {
              this.rightPos = (chartWidth - dialogWidth - dialogRightMargin) + 'px';
              if(this.isOpen) this.placeholderEl.style('right', this.rightPos);
            }
          }
          if(isNumber(dialogTop) && isNumber(dialogHeight) && dialogTop >= 0 && dialogTop > chartHeight - dialogHeight) {
            if(this.topPos) {
              this.topPos = ((chartHeight - dialogHeight) > 0 ? (chartHeight - dialogHeight) : 0)  + 'px';
              if(this.isOpen) this.placeholderEl.style('top', this.topPos);
            }
          }
          
          if(this.topPos && (this.getLayoutProfile() === 'large' && this.rootEl.classed("vzb-dialog-expand-true"))) {
              this.placeholderEl.style('bottom', 'auto');
          }

          if(profile === 'small') {
            this.rightPos = '';
            this.topPos = '';
            this.placeholderEl.attr('style', '');
          } else {
            //if(!this.isOpen) this.placeholderEl.style('right', '');
                
            if(this.rootEl.classed('vzb-landscape')) {
              // var contentHeight = parseInt(this.rootEl.style('height'));
              // var placeholderHeight = parseInt(this.placeholderEl.style('height'));
              // if (contentHeight < placeholderHeight) {
              //   this.topPos = (-contentHeight + 50) + 'px';
              //   this.rightPos = '';
              //   this.placeholderEl.style('right', this.rightPos);
              //   this.placeholderEl.style('bottom', 'auto');
              // } else {
              //   //this.topPos = '';
              //   this.placeholderEl.style('bottom', '');
              // }
            }
            //this.placeholderEl.style('top', this.topPos);
          }

          this.dragHandler.classed("vzb-hidden", profile === 'small');
          this.pinIcon.classed("vzb-hidden", profile === 'small');

        }
      },

      beforeOpen: function() {
        var _this = this;
        
        this.transitionEvents = ['webkitTransitionEnd', 'transitionend', 'msTransitionEnd', 'oTransitionEnd'];
        this.transitionEvents.forEach(function(event) {
          _this.placeholderEl.on(event, _this.transitionEnd.bind(_this, event));
        });

        this.placeholderEl.classed('notransition', true);
        
        this.placeholderEl.style({'top': '', 'bottom': ''}); // issues: 369 & 442
        
        if(this.topPos && this.getLayoutProfile() === 'large' && this.rootEl.classed("vzb-dialog-expand-true")) {
          var topPos = this.placeholderEl.node().offsetTop;
          this.placeholderEl.style({'top': topPos + 'px', 'bottom': 'auto'}); // issues: 369 & 442
        } else if(this.getLayoutProfile() !== 'small') {
          //if(this.rightPos) this.placeholderEl.style('right', this.rightPos);
        }

        this.placeholderEl.node().offsetTop;            
        this.placeholderEl.classed('notransition', false);

        if(this.getLayoutProfile() === 'small') {
          this.placeholderEl.style('top', ''); // issues: 369 & 442
        } else if(this.rootEl.classed('vzb-landscape')) { // need to recalculate popup position (Safari 8 bug)
          // var contentHeight = parseInt(this.rootEl.style('height'));
          // var placeholderHeight = parseInt(this.placeholderEl.style('height'));
          // if (contentHeight < placeholderHeight) {
          //   this.topPos = (-contentHeight + 50) + 'px';
          //   this.rightPos = '';
          //   this.placeholderEl.style('right', this.rightPos);
          //   this.placeholderEl.style('bottom', 'auto');
          // } else {
          //   this.topPos = '';
          //   this.placeholderEl.style('bottom', '');
          // }
          //this.placeholderEl.style('top', this.topPos);
        }
        
      },

      /**
       * User has clicked to open this dialog
       */
      open: function() {
        this.isOpen = true;
        if(this.getLayoutProfile() !== 'small') {
          if(this.topPos) {
            this.placeholderEl.style('top', this.topPos);
            this.placeholderEl.style('right', this.rightPos);
          }
        }
      },

      beforeClose: function() {
    //issues: 369 & 442
        if(this.rootEl.classed('vzb-portrait') && this.getLayoutProfile() === 'small') {
          this.placeholderEl.style('top', 'auto'); // issues: 369 & 442
        } 
        if(this.getLayoutProfile() === 'large' && this.rootEl.classed("vzb-dialog-expand-true")) {
          this.topPos0 = this.topPos ? (this.placeholderEl.node().parentNode.offsetHeight - this.placeholderEl.node().offsetHeight) + 'px' : '';
        }   
        this.placeholderEl.classed('notransition', false);
        this.placeholderEl.node().offsetHeight; // trigger a reflow (flushing the css changes)
      },

      /**
       * User has closed this dialog
       */
      close: function() {
    //issues: 369 & 442
        if(!(this.rootEl.classed('vzb-portrait') && this.getLayoutProfile() === 'small')) {
          this.placeholderEl.style('top', ''); // issues: 369 & 442
          this.placeholderEl.style('right', ''); // issues: 369 & 442
        }
        
        if(this.getLayoutProfile() === 'large' && this.rootEl.classed("vzb-dialog-expand-true")) {
          this.placeholderEl.style({'top' : this.topPos0, 'right' : ''});    
        }
        this.isOpen = false;
        this.trigger('close');
      },


      transitionEnd: function(eventName) {
        var _this = this;

        this.transitionEvents.forEach(function(event) {
          _this.placeholderEl.on(event, null);
        });
        if(this.isOpen) {
          this.placeholderEl.classed('notransition', true);
        }
      }

    });

    function dialogDrag(element, container, xOffset) {
      var posX, posY, divTop, divRight, marginRight, eWi, eHe, cWi, cHe, diffX, diffY;

      return {
        move: function(x, y) {
          element.style('right', x + 'px');
          element.style('top', y + 'px');
        },

        dragStart: function(evt) {
          if(!isTouchDevice()) {
            posX = evt.sourceEvent.clientX;
            posY = evt.sourceEvent.clientY;
          } else {
            var touchCoord = d3.touches(container.node());
            posX = touchCoord[0][0];
            posY = touchCoord[0][1];
          }
          divTop = parseInt(element.style('top')) || 0;
          divRight = parseInt(element.style('right')) || 0;
          marginRight = parseInt(element.style('margin-right')) || 0;
          eWi = parseInt(element.style('width'));
          eHe = parseInt(element.style('height'));
          cWi = parseInt(container.style('width')) - marginRight;
          cHe = parseInt(container.style('height'));
          diffX = posX + divRight;
          diffY = posY - divTop;
        },

        drag: function(evt) {
          if(!isTouchDevice()) {
            posX = evt.sourceEvent.clientX;
            posY = evt.sourceEvent.clientY;
          } else {
            var touchCoord = d3.touches(container.node());
            posX = touchCoord[0][0];
            posY = touchCoord[0][1];
          }
          var aX = -posX + diffX,
            aY = posY - diffY;
          if(aX < -xOffset) aX = -xOffset;
          if(aY < 0) aY = 0;
          if(aX + eWi > cWi) aX = cWi - eWi;
          if(aY + eHe > cHe) aY = cHe - eHe;

          this.move(aX, aY);
        }
      }
    }

    /*
     * stack dialog
     */

    var Stack = Dialog.extend({

        /**
         * Initializes the dialog component
         * @param config component configuration
         * @param context component context (parent)
         */
        init: function(config, parent) {
            this.name = 'stack';
            var _this = this;

            // in dialog, this.model_expects = ["state", "data"];

            this.components = [{
                component: DraggableList,
                placeholder: '.vzb-dialog-draggablelist',
                model: ["state.marker.group", "language"],
                groupID: "manualSorting",
                dataArrFn: _this.manualSorting.bind(_this),
                lang: 'region/'
            }];

            this.model_binds = {
                'change:state.marker.stack': function(evt) {
                    //console.log("stack change " + evt);
                    _this.updateView();
                },
                'change:state.marker.group': function(evt) {
                    //console.log("group change " + evt);
                    _this.updateView();
                }
            };

            this._super(config, parent);
        },

        resize: function() {
          if (this.getLayoutProfile() == 'small') {
            var height = this.root.element.offsetHeight;
            var titleHeight = this.element.select(".vzb-dialog-title").node().offsetHeight;
            var buttonsHeight = this.element.select(".vzb-dialog-buttons").node().offsetHeight;
            this.element.select(".vzb-dialog-content.vzb-dialog-scrollable").style('max-height', height - 90 - titleHeight - buttonsHeight + 'px');
          } else {
            this.element.select(".vzb-dialog-content").style('max-height', '');
          }
          this._super();
        },

        readyOnce: function() {
          this._super();

          var _this = this;
          this.group = this.model.state.marker.group;
          this.stack = this.model.state.marker.stack;

          this.howToStackEl = this.element.select('.vzb-howtostack').selectAll("input")
              .on("change", function() {
                  _this.setModel("stack", d3.select(this).node().value);
              })
          this.howToMergeEl = this.element.select('.vzb-howtomerge').selectAll("input")
              .on("change", function() {
                  _this.setModel("merge", d3.select(this).node().value);
              })

          this.updateView();
        },

        updateView: function() {
            var _this = this;

            this.howToStackEl
                .property('checked', function() {
                    return d3.select(this).node().value === _this.stack.which;
                });

            this.howToMergeEl
                .property('checked', function() {
                    if(d3.select(this).node().value === "none")  return !_this.group.merge && !_this.stack.merge;
                    if(d3.select(this).node().value === "grouped") return _this.group.merge;
                    if(d3.select(this).node().value === "stacked") return _this.stack.merge;
                })
                .attr('disabled', function(){
                    if(d3.select(this).node().value === "none")  return null; // always enabled
                    if(d3.select(this).node().value === "grouped") return _this.stack.which === "none" ? true : null;
                    if(d3.select(this).node().value === "stacked") return _this.stack.which === "all" ? null : true;
                });


        },

        manualSorting: function(value) {
            if(arguments.length === 0) return this.model.state.marker.group.manualSorting;
            this.model.state.marker.group.manualSorting = value;
        },

        setModel: function(what, value) {

            var obj = {stack: {}, group: {}};

            if(what === "merge") {
                switch (value){
                    case "none":
                        obj.group.merge = false;
                        obj.stack.merge = false;
                        break;
                    case "grouped":
                        obj.group.merge = true;
                        obj.stack.merge = false;
                        break;
                    case "stacked":
                        obj.group.merge = false;
                        obj.stack.merge = true;
                        break;
                }
            }
            if(what === "stack") {

                obj.stack.which = value;

                //validate use of stack hook
                if(value !== "all" && value !== "none"){
                    obj.stack.use = "property";
                } else {
                    obj.stack.use = "constant";
                }

                //validate possible merge values in group and stack hooks
                if(value === "none" && this.group.merge) obj.group.merge = false;
                if(value !== "all" && this.stack.merge) obj.stack.merge = false;
            }

            this.model.state.marker.set(obj);
        }
    });

    /*
     * Size dialog
     */

    var Speed = Dialog.extend({

    /**
     * Initializes the dialog component
     * @param config component configuration
     * @param context component context (parent)
     */
    init: function(config, parent) {
      this.name = 'speed';

      // in dialog, this.model_expects = ["state", "data"];

      this.components = [
      {
        component: SimpleSlider,
        placeholder: '.vzb-dialog-placeholder',
        model: ["state.time"],
        arg: "delay",
        properties: {min:1, max:5, step:0.1, scale: d3.scale.linear()
          .domain([1,2,3,4,5])
          .range([1200,900,450,200,75])
        }
      }
      ];

      this._super(config, parent);
    }
    });

    /*
     * Size dialog
     */

    var Size = Dialog.extend({

    /**
     * Initializes the dialog component
     * @param config component configuration
     * @param context component context (parent)
     */
    init: function(config, parent) {
      this.name = 'size';

      // in dialog, this.model_expects = ["state", "data"];

      this.components = [
      {
        component: BubbleSize,
        placeholder: '.vzb-dialog-bubblesize',
        model: ["state.marker.size"],
        ui: {
          show_button: false
        }
      },
      {
        component: IndPicker,
        placeholder: '.vzb-saxis-selector',
        model: ["state.marker", "language"],
        markerID: "size"
      }
      ];

      this._super(config, parent);
    }
    });

    /*!
     * VIZABI SHOW CONTROL
     * Reusable show dialog
     */

    var Show = Dialog.extend({

      init: function(config, parent) {
        this.name = 'show';
        var _this = this;

        this.model_binds = {
          "change:state.entities.show": function(evt) {
            _this.redraw();
          }
        }

        this._super(config, parent);
      },

      /**
       * Grab the list div
       */
      readyOnce: function() {
        this._super();
        this.list = this.element.select(".vzb-show-list");
        this.input_search = this.element.select("#vzb-show-search");
        this.deselect_all = this.element.select("#vzb-show-deselect");

        this.KEY = this.model.state.entities.getDimension();
        this.TIMEDIM = this.model.state.time.getDimension();

        var _this = this;
        this.input_search.on("input", function() {
          _this.showHideSearch();
        });

        this.deselect_all.on("click", function() {
          _this.deselectEntities();
        });


        //make sure it refreshes when all is reloaded
        this.root.on('ready', function() {
          _this.redraw();
        })
      },

      open: function() {
        this._super();

        this.input_search.node().value = "";
        this.showHideSearch();
      },

      ready: function() {
        this._super();
        this.redraw();
        preventAncestorScrolling(this.element.select('.vzb-dialog-scrollable'));

      },

      redraw: function(){

        var _this = this;
        this.translator = this.model.language.getTFunction();
          
        var values = this.model.state.marker_allpossible.getFrame();
        var data = keys(values.label)
            .map(function(d){
                var result = {};
                result[_this.KEY] = d;
                result["label"] = values.label[d];
                return result;
            });

        //sort data alphabetically
        data.sort(function(a, b) {
          return(a.label < b.label) ? -1 : 1;
        });

        this.list.html("");

        var items = this.list.selectAll(".vzb-show-item")
          .data(data)
          .enter()
          .append("div")
          .attr("class", "vzb-show-item vzb-dialog-checkbox")

        items.append("input")
          .attr("type", "checkbox")
          .attr("class", "vzb-show-item")
          .attr("id", function(d) {
            return "-show-" + d[_this.KEY];
          })
          .property("checked", function(d) {
            return _this.model.state.entities.isShown(d);
          })
          .on("change", function(d) {
            _this.model.state.entities.showEntity(d);
            _this.showHideDeselect();
          });

        items.append("label")
          .attr("for", function(d) {
            return "-show-" + d[_this.KEY];
          })
          .text(function(d) {
            return d.label;
          });

        this.input_search.attr("placeholder", this.translator("placeholder/search") + "...");

        this.showHideSearch();
        this.showHideDeselect();


      },

      showHideSearch: function() {

        var search = this.input_search.node().value || "";
        search = search.toLowerCase();

        this.list.selectAll(".vzb-show-item")
          .classed("vzb-hidden", function(d) {
            var lower = d.label.toLowerCase();
            return(lower.indexOf(search) === -1);
          });
      },

      showHideDeselect: function() {
        var show = this.model.state.entities.show[this.KEY];
        this.deselect_all.classed('vzb-hidden', !show || show[0]==="*");
      },

      deselectEntities: function() {
        this.model.state.entities.clearShow();
        this.showHideDeselect();
      },

      transitionEnd: function(event) {
        this._super(event);

        if(!isTouchDevice()) this.input_search.node().focus();
      }

    });

    /*
     * Size dialog
     */

    var Presentation = Dialog.extend({

    /**
     * Initializes the dialog component
     * @param config component configuration
     * @param context component context (parent)
     */
    init: function(config, parent) {
      this.name = 'presentation';

      // in dialog, this.model_expects = ["state", "data"];

      this.components = [
      {
        component: simplecheckbox,
        placeholder: '.vzb-presentationmode-switch',
        model: ["ui", "language"],
        checkbox: 'presentation'
      }];

      this._super(config, parent);
    }
    });

    /*
     * Size dialog
     */

    var Opacity = Dialog.extend({

    /**
     * Initializes the dialog component
     * @param config component configuration
     * @param context component context (parent)
     */
    init: function(config, parent) {
      this.name = 'opacity';

      // in dialog, this.model_expects = ["state", "data"];

      this.components = [
      {
        component: SimpleSlider,
        placeholder: '.vzb-dialog-bubbleopacity-regular',
        model: ["state.entities"],
        arg: "opacityRegular",
        properties: {step: 0.01}
      }, {
        component: SimpleSlider,
        placeholder: '.vzb-dialog-bubbleopacity-selectdim',
        model: ["state.entities"],
        arg: "opacitySelectDim",
        properties: {step: 0.01}
      }
      ];

      this._super(config, parent);
    }
    });

    /*!
     * VIZABI BUTTONLIST
     * Reusable buttonlist component
     */

    //default existing buttons
    var class_active = "vzb-active";
    var class_hidden = "vzb-hidden";
    var class_active_locked = "vzb-active-locked";
    var class_unavailable = "vzb-unavailable";
    var class_vzb_fullscreen = "vzb-force-fullscreen";
    var class_container_fullscreen = "vzb-container-fullscreen";

    var ButtonList = Component.extend({

      /**
       * Initializes the buttonlist
       * @param config component configuration
       * @param context component context (parent)
       */
      init: function(config, context) {

        //set properties
        var _this = this;
        this.name = this.name || 'gapminder-buttonlist';
    //    this.template = '<div class="vzb-buttonlist"></div>';

        this.model_expects = [{
          name: "state",
          type: "model"
        }, {
          name: "ui",
          type: "model"
        }, {
          name: "language",
          type: "language"
        }];

        this._available_buttons = {
          'find': {
            title: "buttons/find",
            icon: "search",
            required: false
          },
          'show': {
            title: "buttons/show",
            icon: "asterisk",
            required: false
          },
          'moreoptions': {
            title: "buttons/more_options",
            icon: "gear",
            required: true
          },
          'colors': {
            title: "buttons/colors",
            icon: "paintbrush",
            required: false
          },
          'size': {
            title: "buttons/size",
            icon: "circle",
            required: false
          },
          'fullscreen': {
            title: "buttons/expand",
            icon: "expand",
            func: this.toggleFullScreen.bind(this),
            required: true
          },
          'trails': {
            title: "buttons/trails",
            icon: "trails",
            func: this.toggleBubbleTrails.bind(this),
            required: false,
            statebind: "state.time.trails",
            statebindfunc: this.setBubbleTrails.bind(this)
          },
          'lock': {
            title: "buttons/lock",
            icon: "lock",
            func: this.toggleBubbleLock.bind(this),
            required: false,
            statebind: "state.time.lockNonSelected",
            statebindfunc: this.setBubbleLock.bind(this)
          },
          'presentation': {
            title: "buttons/presentation",
            icon: "presentation",
            func: this.togglePresentationMode.bind(this),
            required: false,
            statebind: "ui.presentation",
            statebindfunc: this.setPresentationMode.bind(this)
          },
          'about': {
            title: "buttons/about",
            icon: "about",
            required: false
          },
          'axes': {
            title: "buttons/axes",
            icon: "axes",
            required: false
          },
          'axesmc': {
            title: "buttons/axesmc",
            icon: "axes",
            required: false
          },
          'stack': {
            title: "buttons/stack",
            icon: "stack",
            required: false
          },
          '_default': {
            title: "Button",
            icon: "asterisk",
            required: false
          }
        };

        this._active_comp = false;

        this.model_binds = {
          "change:state.entities.select": function(evt) {
            if(!_this._readyOnce) return;

            if(_this.model.state.entities.select.length === 0) {
              _this.model.state.time.lockNonSelected = 0;
            }
            _this.setBubbleTrails();
            _this.setBubbleLock();
            _this._toggleButtons();


            //scroll button list to end if bottons appeared or disappeared
            // if(_this.entitiesSelected_1 !== (_this.model.state.entities.select.length > 0)) {
            //   _this.scrollToEnd();
            // }
            // _this.entitiesSelected_1 = _this.model.state.entities.select.length > 0;
          }      
        }
        
        Object.keys(this._available_buttons).forEach(function(buttonId) {
          var button = _this._available_buttons[buttonId];
          if(button.statebind) {
            _this.model_binds['change:' + button.statebind] = function(evt) {
              button.statebindfunc(buttonId, evt.source.value);
            }
          }
        });
        
        this.validatePopupButtons(config.ui.buttons, config.ui.dialogs.popup);

        this._super(config, context);

      },

      readyOnce: function() {

        var _this = this;
        
        this.root.findChildByName("gapminder-dialogs").on('close', function( evt, params) {
          _this.setButtonActive(params.id, false);
        });

        
        var button_expand = (this.model.ui.dialogs||{}).sidebar || [];

        this.element = d3.select(this.placeholder);

        this.element.selectAll("div").remove();

        // // if button_expand has been passed in with boolean param or array must check and covert to array
        // if (button_expand){
        //   this.model.ui.dialogs.sidebar = (button_expand === true) ? this.model.ui.buttons : button_expand;
        // }

        // if (button_expand && button_expand.length !== 0) {
        //     d3.select(this.root.element).classed("vzb-dialog-expand-true", true);
        // }
        
        var button_list = [].concat(this.model.ui.buttons);

        (button_expand||[]).forEach(function(button) {
          if (button_list.indexOf(button) === -1) {
            button_list.push(button);
          }
        });

        this.model.ui.buttons = button_list;

        //add buttons and render components
        this._addButtons();

        var buttons = this.element.selectAll(".vzb-buttonlist-btn");

        //clicking the button
        buttons.on('click', function() {

          d3.event.preventDefault();
          d3.event.stopPropagation();
          
          var id = d3.select(this).attr("data-btn");
          _this.proceedClick(id);
        });

        //store body overflow
        this._prev_body_overflow = document.body.style.overflow;

        this.setBubbleTrails();
        this.setBubbleLock();
        this.setPresentationMode();

        this._toggleButtons();

      },
      
      proceedClick: function(id) {
        var _this = this;
        var btn = _this.element.selectAll(".vzb-buttonlist-btn[data-btn='" + id + "']"),
          classes = btn.attr("class"),
          btn_config = _this._available_buttons[id];

        if(btn_config && btn_config.func) {
          btn_config.func(id);
        } else {
          var btn_active = classes.indexOf(class_active) === -1;

          btn.classed(class_active, btn_active);
          var evt = {};
          evt['id'] = id;
          evt['active'] = btn_active;
          _this.trigger('click', evt);
        }    
      },
      
      validatePopupButtons: function (buttons, popupDialogs) {
        var _this = this;
        var popupButtons = buttons.filter(function(d) {
          return (_this._available_buttons[d] && !_this._available_buttons[d].func); 
          });
        for(var i = 0, j = popupButtons.length; i < j; i++) {
           if(popupDialogs.indexOf(popupButtons[i]) == -1) {
               return error$1('Buttonlist: bad buttons config: "' + popupButtons[i] + '" is missing in popups list');
           }
        }
        return false; //all good
      },

      /*
       * reset buttons show state
       */
      _showAllButtons: function() {
        // show all existing buttons
        var _this = this;
        var buttons = this.element.selectAll(".vzb-buttonlist-btn");
        buttons.each(function(d,i) {
          var button = d3.select(this);
          button.style('display', '');
        });
      },

      /*
      * determine which buttons are shown on the buttonlist
      */
      _toggleButtons: function() {
        var _this = this;
        var parent = this.parent.element.node ? this.parent.element : d3.select(this.parent.element);

        //HERE
        var button_expand = (this.model.ui.dialogs||{}).sidebar || [];
        _this._showAllButtons();

        var buttons = this.element.selectAll(".vzb-buttonlist-btn");

        var container = this.element.node().getBoundingClientRect();

        var not_required = [];
        var required = [];

        var button_width = 80;
        var button_height = 80;
        var container_width = this.element.node().getBoundingClientRect().width;
        var container_height = this.element.node().getBoundingClientRect().height;
        var buttons_width = 0;
        var buttons_height = 0;

        buttons.each(function(d,i) {
          var button_data = d;
          var button = d3.select(this);
          var expandable = button_expand.indexOf(button_data.id) !== -1;
          var button_margin = {top: parseInt(button.style("margin-top")), right: parseInt(button.style("margin-right")), left: parseInt(button.style("margin-left")), bottom: parseInt(button.style("margin-bottom"))};
          button_width = button.node().getBoundingClientRect().width + button_margin.right + button_margin.left;
          button_height = button.node().getBoundingClientRect().height + button_margin.top + button_margin.bottom;

          if(!button.classed(class_hidden)) {
            if(!expandable || (_this.getLayoutProfile() !== 'large')){
              buttons_width += button_width;
              buttons_height += button_height;
              //sort buttons between required and not required buttons.
              // Not required buttons will only be shown if there is space available
              if(button_data.required){
                required.push(button);
              } else {
                not_required.push(button);
              }
            } else {
              button.style("display", "none");
            }
          }
        });
        var width_diff = buttons_width - container_width;
        var height_diff = buttons_height - container_height;
        var number_of_buttons = 1;

        //check if container is landscape or portrait
        // if portrait small or large with expand, use width
        if(parent.classed("vzb-large") && parent.classed("vzb-dialog-expand-true")
        || parent.classed("vzb-small") && parent.classed("vzb-portrait")) {
          //check if the width_diff is small. If it is, add to the container
          // width, to allow more buttons in a way that is still usable
          if(width_diff > 0 && width_diff <=10){
            container_width += width_diff;
          }
          number_of_buttons = Math.floor(container_width / button_width) - required.length;
          if(number_of_buttons < 0){
            number_of_buttons = 0;
          }
        // else, use height
        } else {
          //check if the width_diff is small. If it is, add to the container
          // width, to allow more buttons in a way that is still usable
          if(height_diff > 0 && height_diff <=10){
            container_height += height_diff;
          }
          number_of_buttons = Math.floor(container_height / button_height) - required.length;
          if(number_of_buttons < 0){
            number_of_buttons = 0;
          }
        }
        //change the display property of non required buttons, from right to
        // left
        not_required.reverse();
        var hiddenButtons = [];
        for (var i = 0, j = not_required.length - number_of_buttons; i < j ; i++) {
            not_required[i].style("display", "none");
            hiddenButtons.push(not_required[i].attr("data-btn"));
        }
        
        var evt = {};
        evt['hiddenButtons'] = hiddenButtons;
        _this.trigger('toggle', evt);

      },

      /*
       * adds buttons configuration to the components and template_data
       * @param {Array} button_list list of buttons to be added
       */
      _addButtons: function() {

        this._components_config = [];
        var button_list = this.model.ui.buttons||[];
        var details_btns = [];
        var button_expand = (this.model.ui.dialogs||{}).sidebar || [];
        if(!button_list.length) return;
        //add a component for each button
        for(var i = 0; i < button_list.length; i++) {

          var btn = button_list[i];
          var btn_config = this._available_buttons[btn];

          //add template data
          var d = (btn_config) ? btn : "_default";
          var details_btn = clone(this._available_buttons[d]);

          details_btn.id = btn;
          details_btn.icon = iconset[details_btn.icon];
          details_btns.push(details_btn);
        };

        var t = this.getTranslationFunction(true);

        this.element.selectAll('button').data(details_btns)
          .enter().append("button")
          .attr('class', function (d) {
            var cls = 'vzb-buttonlist-btn';
            if (button_expand.length > 0) {
              if (button_expand.indexOf(d.id) > -1) {
                cls += ' vzb-dialog-side-btn';
              }
            }

            return cls;
          })
          .attr('data-btn', function(d) {
            return d.id;
          })
          .html(function(btn) {
            return "<span class='vzb-buttonlist-btn-icon fa'>" +
              btn.icon + "</span><span class='vzb-buttonlist-btn-title'>" +
              t(btn.title) + "</span>";
          });

      },


      scrollToEnd: function() {
        var target = 0;
        var parent = d3.select(".vzb-tool");

        if(parent.classed("vzb-portrait") && parent.classed("vzb-small")) {
          if(this.model.state.entities.select.length > 0) target = this.element[0][0].scrollWidth
          this.element[0][0].scrollLeft = target;
        } else {
          if(this.model.state.entities.select.length > 0) target = this.element[0][0].scrollHeight
          this.element[0][0].scrollTop = target;
        }
      },


      /*
       * RESIZE:
       * Executed whenever the container is resized
       * Ideally, it contains only operations related to size
       */
      resize: function() {
        //TODO: what to do when resizing?

        //toggle presentaion off is switch to 'small' profile
        if(this.getLayoutProfile() === 'small' && this.model.ui.presentation) {
          this.togglePresentationMode();
        }

        this._toggleButtons();
      },

      setButtonActive: function(id, boolActive) {
        var btn = this.element.selectAll(".vzb-buttonlist-btn[data-btn='" + id + "']");

        btn.classed(class_active, boolActive);
      },

      toggleBubbleTrails: function() {
        this.model.state.time.trails = !this.model.state.time.trails;
        this.setBubbleTrails();
      },
      setBubbleTrails: function() {
        var id = "trails";
        var btn = this.element.selectAll(".vzb-buttonlist-btn[data-btn='" + id + "']");

        btn.classed(class_active_locked, this.model.state.time.trails);
        btn.classed(class_hidden, this.model.state.entities.select.length == 0);
      },
      toggleBubbleLock: function(id) {
        if(this.model.state.entities.select.length == 0) return;

        var locked = this.model.state.time.lockNonSelected;
        var time = this.model.state.time;
        locked = locked ? 0 : time.timeFormat(time.value);
        this.model.state.time.lockNonSelected = locked;

        this.setBubbleLock();
      },
      setBubbleLock: function() {
        var id = "lock";
        var btn = this.element.selectAll(".vzb-buttonlist-btn[data-btn='" + id + "']");
        var translator = this.model.language.getTFunction();

        var locked = this.model.state.time.lockNonSelected;

        btn.classed(class_unavailable, this.model.state.entities.select.length == 0);
        btn.classed(class_hidden, this.model.state.entities.select.length == 0);

        btn.classed(class_active_locked, locked)
        btn.select(".vzb-buttonlist-btn-title")
          .text(locked ? locked : translator("buttons/lock"));

        btn.select(".vzb-buttonlist-btn-icon")
          .html(iconset[locked ? "lock" : "unlock"]);
      },
      togglePresentationMode: function() {
        this.model.ui.presentation = !this.model.ui.presentation;
        this.setPresentationMode();
      },
      setPresentationMode: function() {
        var id = 'presentation';
        var translator = this.model.language.getTFunction();
        var btn = this.element.selectAll(".vzb-buttonlist-btn[data-btn='" + id + "']");

        btn.classed(class_active_locked, this.model.ui.presentation);
      },
      toggleFullScreen: function(id) {

        if(!window) return;

        var component = this;
        var pholder = component.placeholder;
        var pholder_found = false;
        var btn = this.element.selectAll(".vzb-buttonlist-btn[data-btn='" + id + "']");
        var fs = !this.model.ui.fullscreen;
        var body_overflow = (fs) ? "hidden" : this._prev_body_overflow;

        while(!(pholder_found = hasClass(pholder, 'vzb-placeholder'))) {
          component = component.parent;
          pholder = component.placeholder;
        }

        //TODO: figure out a way to avoid fullscreen resize delay in firefox
        if(fs) {
          launchIntoFullscreen(pholder);
          subscribeFullscreenChangeEvent.call(this, this.toggleFullScreen.bind(this, id));
        } else {
          exitFullscreen.call(this);
        }
        classed(pholder, class_vzb_fullscreen, fs);
        if (typeof container != 'undefined') {
          classed(container, class_container_fullscreen, fs);
        }

        this.model.ui.fullscreen = fs;
        var translator = this.model.language.getTFunction();
        btn.classed(class_active_locked, fs);

        btn.select(".vzb-buttonlist-btn-icon").html(iconset[fs ? "unexpand" : "expand"]);

        btn.select(".vzb-buttonlist-btn-title>span").text(
          translator("buttons/" + (fs ? "unexpand" : "expand"))
        )
        .attr("data-vzb-translate", "buttons/" + (fs ? "unexpand" : "expand"));

        //restore body overflow
        document.body.style.overflow = body_overflow;

        this.root.layout.resizeHandler();

        //force window resize event
        // utils.defer(function() {
        //   event = window.document.createEvent("HTMLEvents");
        //   event.initEvent("resize", true, true);
        //   event.eventName = "resize";
        //   window.dispatchEvent(event);
        // });
      }

    });

    function isFullscreen() {
      if(!window) return false;
      if(window.document.webkitIsFullScreen !== undefined)
        return window.document.webkitIsFullScreen;
      if(window.document.mozFullScreen !== undefined)
        return window.document.mozFullScreen;
      if(window.document.msFullscreenElement !== undefined)
        return window.document.msFullscreenElement;

      return false;
    }

    function exitHandler(emulateClickFunc) {
      if(!isFullscreen()) {
        emulateClickFunc();
      }
    }

    function subscribeFullscreenChangeEvent(exitFunc) {
      if(!window) return;
      var doc = window.document;

      this.exitFullscreenHandler = exitHandler.bind(this, exitFunc);
      doc.addEventListener('webkitfullscreenchange', this.exitFullscreenHandler, false);
      doc.addEventListener('mozfullscreenchange', this.exitFullscreenHandler, false);
      doc.addEventListener('fullscreenchange', this.exitFullscreenHandler, false);
      doc.addEventListener('MSFullscreenChange', this.exitFullscreenHandler, false);
    }

    function removeFullscreenChangeEvent() {
      var doc = window.document;

      doc.removeEventListener('webkitfullscreenchange', this.exitFullscreenHandler);
      doc.removeEventListener('mozfullscreenchange', this.exitFullscreenHandler);
      doc.removeEventListener('fullscreenchange', this.exitFullscreenHandler);
      doc.removeEventListener('MSFullscreenChange', this.exitFullscreenHandler);
    }

    function launchIntoFullscreen(elem) {
      if(elem.requestFullscreen) {
        elem.requestFullscreen();
      } else if(elem.msRequestFullscreen) {
        elem.msRequestFullscreen();
      } else if(elem.mozRequestFullScreen) {
        elem.mozRequestFullScreen();
      } else if(elem.webkitRequestFullscreen) {
        if (!(navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
        navigator.userAgent && !navigator.userAgent.match('CriOS'))) {
          elem.webkitRequestFullscreen();
        }

      }
    }

    function exitFullscreen() {
      removeFullscreenChangeEvent.call(this);

      if(document.exitFullscreen) {
        document.exitFullscreen();
      } else if(document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if(document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      }
    }

    /*!
     * VIZABI OPTIONSBUTTONLIST
     * Reusable optionsbuttonlist component
     */

    //default existing buttons
    var class_active$2 = "vzb-active";
    // var class_active_locked = "vzb-active-locked";
    // var class_expand_dialog = "vzb-dialog-side";
    // var class_hide_btn = "vzb-dialog-side-btn";
    // var class_unavailable = "vzb-unavailable";
    // var class_vzb_fullscreen = "vzb-force-fullscreen";
    // var class_container_fullscreen = "vzb-container-fullscreen";

    var BUTTON_IN_ROW = 4;

    var OptionsButtonList = ButtonList.extend({

      /**
       * Initializes the buttonlist
       * @param config component configuration
       * @param context component context (parent)
       */
      init: function(config, context) {

        //set properties
        var _this = this;
        this.name = 'gapminder-optionsbuttonlist';
        
        this._super(config, context);
      },
      
      readyOnce: function() {
        var _this = this;
        Object.keys(this._available_buttons).forEach(function(buttonId) {
          var button = _this._available_buttons[buttonId];
          button.required = !button.required;
        });
        
        this.buttonListComp = this.root.findChildByName("gapminder-buttonlist");
        
        this.buttonListComp.on("click", function(evt, button) {
          var btn = _this.element.selectAll(".vzb-buttonlist-btn[data-btn='" + button.id + "']");
          btn.classed(class_active$2, button.active);
        });
        
        this.buttonListComp.on("toggle", function(evt, params) {
          var btn = _this.element.selectAll(".vzb-buttonlist-btn");
          var visibleButton = 0;
          btn.each(function(d) {
            var button = d3.select(this);
            var isHidden = params.hiddenButtons.indexOf(d.id) == -1;
            button.style('display', isHidden ? 'none' : '');
            if(!isHidden) visibleButton++; 
          });
          
          var minHeight = '';
          
          if(visibleButton == 0) { minHeight = '0';}
          else if(visibleButton <= BUTTON_IN_ROW) { minHeight = '53px';}
          
          _this.element.style('min-height', minHeight);
          _this.element.offsetHeight;
          _this.element[0][0].scrollTop = 0;

        });

        this._super();
      },  
      
      proceedClick: function(id) {
        var _this = this;
        this.buttonListComp.proceedClick(id);
        var btn_data = this.element.selectAll(".vzb-buttonlist-btn[data-btn='" + id + "']").datum();
        if(btn_data.func) {
          delay(function() {
            _this.root.findChildByName("gapminder-dialogs").closeAllDialogs();
          }, 200);
        }
      },
       
      _toggleButtons: function() {
        //
      }
      
    });

    /*
     * More options dialog
     */

    var MoreOptions = Dialog.extend({

      /**
       * Initializes the dialog component
       * @param config component configuration
       * @param context component context (parent)
       */
      init: function(config, parent) {
        this.name = 'moreoptions';
        
        //specifying components
        this.components = [{
          component: OptionsButtonList,
          placeholder: '.vzb-dialog-options-buttonlist',
          model: ['state', 'ui', 'language']
        }];

        this._super(config, parent);
      },

      readyOnce: function() {
        var _this = this;
        this.element = d3.select(this.element);
        this.contentEl = this.element.select('.vzb-dialog-content');

        this.on('dragend', function() {
          _this._setMaxHeight();
        });
            
        var dialog_popup = (this.model.ui.dialogs||{}).popup || [];
        var dialog_moreoptions = (this.model.ui.dialogs||{}).moreoptions || [];
                
        // if dialog_moreoptions has been passed in with boolean param or array must check and covert to array
        if (dialog_moreoptions === true) {
          dialog_moreoptions = dialog_popup;
          (this.model.ui.dialogs||{}).moreoptions = dialog_moreoptions;
        }
        
        this._addDialogs(dialog_moreoptions);
        
        //accordion
        this.accordionEl = this.element.select('.vzb-accordion');
        if(this.accordionEl) {
          var titleEl = this.accordionEl.selectAll('.vzb-accordion-section')
            .select('.vzb-dialog-title>span:first-child')
          titleEl.on('click', function(d) {
            var element = _this.components[d.component].element;
            var sectionEl = _this.components[d.component].placeholderEl;
            var activeEl = _this.accordionEl.select('.vzb-accordion-active');
            if(activeEl) {
              activeEl.classed('vzb-accordion-active', false);
            }
            if(sectionEl.node() !== activeEl.node()) {
              sectionEl.classed('vzb-accordion-active', true);
            }
          })
        }
      },
      
      resize: function() {
        this._super();
        if(this.placeholderEl) {
          this._setMaxHeight();
        }
      },
      
      _setMaxHeight: function() {
        var totalHeight = this.root.element.offsetHeight;
        if(this.getLayoutProfile() !== 'small') {
          if(!this.topPos && (this.getLayoutProfile() === 'large' && this.rootEl.classed("vzb-dialog-expand-true"))) {
            var dialogBottom = parseInt(this.placeholderEl.style('bottom'), 10);
            totalHeight = totalHeight - dialogBottom;
          } else {
            var topPos = this.topPos ? parseInt(this.topPos, 10) : this.placeholderEl[0][0].offsetTop; 
            totalHeight = totalHeight - topPos;
          }
        } else {
          totalHeight = totalHeight - 50;
        }

        this.element.style('max-height', totalHeight + 'px');
      },
      
      _addDialogs: function(dialog_list) {
        this._components_config = [];
        var details_dlgs = [];
        if(!dialog_list.length) return;
        //add a component for each dialog
        for(var i = 0; i < dialog_list.length; i++) {

          //check moreoptions in dialog.moreoptions
          if(dialog_list[i] === "moreoptions") continue;
          
          var dlg = dialog_list[i];
          var dlg_config = deepClone(this.parent._available_dialogs[dlg]);

          //if it's a dialog, add component
          if(dlg_config && dlg_config.dialog) {
            var comps = this._components_config;

            //add corresponding component
            comps.push({
              component: dlg_config.dialog,
              placeholder: '.vzb-dialogs-dialog[data-dlg="' + dlg + '"]',
              model: ["state", "ui", "language"]
            });

            dlg_config.component = comps.length - 1;
          
            dlg_config.id = dlg;
            details_dlgs.push(dlg_config);
          }
        };

        this.contentEl.selectAll('div').data(details_dlgs)
          .enter().append("div")
          .attr('class', function (d) {
            var cls = 'vzb-dialogs-dialog vzb-moreoptions vzb-accordion-section';
            return cls;
          })
          .attr('data-dlg', function(d) {
            return d.id;
          });

        this.loadComponents();

        var _this = this;
        //render each subcomponent
        forEach(this.components, function(subcomp) {
          subcomp.render();
          _this.on('resize', function() {
            subcomp.trigger('resize');
          });
        });
      }
    });

    /*!
     * VIZABI FIND CONTROL
     * Reusable find dialog
     */

    var Find = Dialog.extend({

      init: function(config, parent) {
        this.name = 'find';
        var _this = this;

        this.components = [{
          component: SimpleSlider,
          placeholder: '.vzb-dialog-bubbleopacity',
          model: ["state.entities"],
          arg: "opacitySelectDim",
          properties: {step: 0.01}
        }];

        this.model_binds = {
          "change:state.entities.select": function(evt) {
            _this.ready();
          },
          "change:state.time.value": function(evt) {
            if(!_this.model.state.time.playing && !_this.model.state.time.dragging) {
              _this.ready();
            }
          },
          "change:language.strings": function() {
            _this.translator = _this.model.language.getTFunction();
            _this.input_search.attr("placeholder", _this.translator("placeholder/search") + "...");
          }
        }

        this._super(config, parent);
      },

      /**
       * Grab the list div
       */
      readyOnce: function() {
        this._super();

        this.list = this.element.select(".vzb-find-list");
        this.input_search = this.element.select("#vzb-find-search");
        this.deselect_all = this.element.select("#vzb-find-deselect");
        this.opacity_nonselected = this.element.select(".vzb-dialog-bubbleopacity");

        this.KEY = this.model.state.entities.getDimension();

        var _this = this;
        this.input_search.on("input", function() {
          _this.showHideSearch();
        });

        this.deselect_all.on("click", function() {
          _this.deselectEntities();
        });

        this.translator = this.model.language.getTFunction();
        this.input_search.attr("placeholder", this.translator("placeholder/search") + "...");

        //make sure it refreshes when all is reloaded
        var _this = this;
        this.root.on('ready', function() {
          _this.ready();
        })

      },

      open: function() {
        this._super();

        this.input_search.node().value = "";
        this.showHideSearch();
      },

      /**
       * Build the list everytime it updates
       */
      //TODO: split update in render and update methods
      ready: function() {
        this._super();

        var _this = this;
        var KEY = this.KEY;
        var TIMEDIM = this.model.state.time.getDimension();
        var selected = this.model.state.entities.getSelected();
        var marker = this.model.state.marker;
        var time = this.model.state.time.value;

        var values = marker.getFrame(time);

        var data = marker.getKeys().map(function(d) {
          var pointer = {};
          pointer[KEY] = d[KEY];
          pointer.name = values.label[d[KEY]];
          return pointer;
        }).filter(function(d) {
          var include = true;
          forEach(values, function(hook) {
            if(!hook[d[KEY]]) {
              include = false;
              return false;
            }
          });
          return include;
        })

        //sort data alphabetically
        data.sort(function(a, b) {
          return(a.name < b.name) ? -1 : 1;
        });

        this.list.html("");

        var items = this.list.selectAll(".vzb-find-item")
          .data(data)
          .enter()
          .append("div")
          .attr("class", "vzb-find-item vzb-dialog-checkbox")

        items.append("input")
          .attr("type", "checkbox")
          .attr("class", "vzb-find-item")
          .attr("id", function(d) {
            return "-find-" + d[KEY];
          })
          .property("checked", function(d) {
            return(selected.indexOf(d[KEY]) !== -1);
          })
          .on("change", function(d) {
            //clear highlight so it doesn't get in the way when selecting an entity
            if(!isTouchDevice()) _this.model.state.entities.clearHighlighted();        
            _this.model.state.entities.selectEntity(d);        
            //return to highlighted state
            if(!isTouchDevice()) _this.model.state.entities.highlightEntity(d); 
          });

        items.append("label")
          .attr("for", function(d) {
            return "-find-" + d[KEY];
          })
          .text(function(d) {
            return d.name;
          })
          .on("mouseover", function(d) {
            if(!isTouchDevice()) _this.model.state.entities.highlightEntity(d);
          })
          .on("mouseout", function(d) {
            if(!isTouchDevice()) _this.model.state.entities.clearHighlighted();
          });
        preventAncestorScrolling(_this.element.select('.vzb-dialog-scrollable'));

        this.showHideSearch();
        this.showHideDeselect();
      },
      resize: function() {
        if (this.getLayoutProfile() == 'small') {
          var height = this.root.element.offsetHeight;
          var titleHeight = this.element.select(".vzb-dialog-title").node().offsetHeight;
          var buttonsHeight = this.element.select(".vzb-dialog-buttons").node().offsetHeight;
          this.element.select(".vzb-dialog-content-fixed").style('max-height', height - 90 - titleHeight - buttonsHeight + 'px');
        } else {
          this.element.select(".vzb-dialog-content-fixed").style('max-height', '');
        }
        this._super();
      },
      showHideSearch: function() {
        var search = this.input_search.node().value || "";
        search = search.toLowerCase();

        this.list.selectAll(".vzb-find-item")
          .classed("vzb-hidden", function(d) {
            var lower = d.name.toLowerCase();
            return(lower.indexOf(search) === -1);
          });
      },

      showHideDeselect: function() {
        var someSelected = !!this.model.state.entities.select.length;
        this.deselect_all.classed('vzb-hidden', !someSelected);
        this.opacity_nonselected.classed('vzb-hidden', !someSelected);
      },

      deselectEntities: function() {
        this.model.state.entities.clearSelected();
      },

      transitionEnd: function(event) {
        this._super(event);

        if(!isTouchDevice()) this.input_search.node().focus();
      }

    });

    /*!
     * VIZABI COLOR DIALOG
     */

    var Colors = Dialog.extend({

      /**
       * Initializes the dialog component
       * @param config component configuration
       * @param context component context (parent)
       */
      init: function(config, parent) {
        this.name = 'colors';

        this.components = [{
          component: IndPicker,
          placeholder: '.vzb-caxis-selector',
          model: ["state.marker", "language"],
          markerID: "color"
        }, {
          component: ColorLegend,
          placeholder: '.vzb-clegend-container',
          model: ["state.marker.color", "state.entities", "language"]
        }];


        this._super(config, parent);
      }

    });

    /*
     * Axes dialog
     */


    var Axes$1 = Dialog.extend({

      /**
       * Initializes the dialog component
       * @param config component configuration
       * @param context component context (parent)
       */
      init: function(config, parent) {
        this.name = 'axesmc'; 
        var _this = this;

        this.model_binds = {
          'change:state.time.xLogStops': function() {
            _this.updateView();
          },
          'change:state.time.yMaxMethod': function() {
            _this.updateView();
          }
        };

        this.components = [{
          component: MinMaxInputs,
          placeholder: '.vzb-xlimits-container',
          model: ["state.marker", "language"],
          markerID: "axis_x",
          ui: {
            selectDomainMinMax: false,
            selectZoomedMinMax: true 
          }
        }]


        this._super(config, parent);
      },

      readyOnce: function() {
        var _this = this;
        this.element = d3.select(this.element);

        this.yMaxRadio = this.element.select('.vzb-yaxis-container').selectAll('input')
          .on("change", function() {
            _this.setModel("yMaxMethod", d3.select(this).node().value);
          })

        this.xLogStops = this.element.select('.vzb-xaxis-container').selectAll('input')
          .on("change", function() {
            _this.setModel("xLogStops", d3.select(this).node().value);
          })

        this.probeFieldEl = this.element.select(".vzb-probe-field")
          .on("change", function() {
            var result = parseFloat(this.value.replace(",", "."));
            if(!result || result <= _this.model.state.time.tailCutX) {
              this.value = _this.model.state.time.probeX;
              return;
            }
            this.value = result;
            _this.setModel("probeX", result);
          });

        this.updateView();

        this._super();
      },

      updateView: function() {
        var _this = this;

        this.yMaxRadio.property('checked', function() {
          return d3.select(this).node().value === _this.model.state.time.yMaxMethod;
        })
        this.xLogStops.property('checked', function() {
          return _this.model.state.time.xLogStops.indexOf(+d3.select(this).node().value) !== -1;
        })
        this.probeFieldEl.property("value", this.model.state.time.probeX);
      },

      setModel: function(what, value) {
        var result;

        if(what == "yMaxMethod") {
          result = value;
        }
        if(what == "xLogStops") {
          result = [];
          this.xLogStops.each(function() {
            if(d3.select(this).property('checked')) result.push(+d3.select(this).node().value);
          })
        }
        if(what == "probeX") {
          result = value;
        }

        this.model.state.time[what] = result;
      }
    });

    /*
     * Axes dialog
     */

    var Axes = Dialog.extend({

      /**
       * Initializes the dialog component
       * @param config component configuration
       * @param context component context (parent)
       */
      init: function(config, parent) {
        this.name = 'axes';
        var _this = this;

        this.components = [{
          component: IndPicker,
          placeholder: '.vzb-xaxis-selector',
          model: ["state.marker", "language"],
          markerID: "axis_x"
        },{
          component: MinMaxInputs,
          placeholder: '.vzb-xaxis-minmax',
          model: ["state.marker", "language"],
          markerID: "axis_x",
          ui: {
            selectDomainMinMax: false,
            selectZoomedMinMax: true
          }
        }, {
          component: IndPicker,
          placeholder: '.vzb-yaxis-selector',
          model: ["state.marker", "language"],
          markerID: "axis_y"
        }, {
          component: MinMaxInputs,
          placeholder: '.vzb-yaxis-minmax',
          model: ["state.marker", "language"],
          markerID: "axis_y",
          ui: {
            selectDomainMinMax: false,
            selectZoomedMinMax: true
          }
        }, {
          component: simplecheckbox,
          placeholder: '.vzb-axes-options',
          model: ["state", "language"],
          submodel: 'time',
          checkbox: 'adaptMinMaxZoom'
        }];

        this._super(config, parent);
      }
    });

    /*
     * Size dialog
     */

    var About = Dialog.extend({

    /**
     * Initializes the dialog component
     * @param config component configuration
     * @param context component context (parent)
     */
    init: function(config, parent) {
      this.name = 'about';

      this._super(config, parent);
    },
        
    readyOnce: function(){
      var version = globals.version;
      var updated = new Date(parseInt(globals.build));
        
      this.element = d3.select(this.element);
      this.element.select(".vzb-about-text0")
          .html("Vizabi, a project")
      this.element.select(".vzb-about-text1")
          .html("by <a href='http://gapminder.org'>Gapminder Foundation</a>")
      this.element.select(".vzb-about-version")
          .html("<a href='https://github.com/Gapminder/vizabi/releases/tag/v"+version+"'>Version: "+version+"</a>");  
      this.element.select(".vzb-about-updated")
          .html("Build: " + d3.time.format("%Y-%m-%d at %H:%M")(updated));    
      this.element.select(".vzb-about-text2")
          .html("Pre-alpha, don't expect too much!");
      this.element.select(".vzb-about-credits")
          .html("<a href='https://github.com/Gapminder/vizabi/graphs/contributors'>Contributors</a>");
    }
        
        
    });

    /*!
     * VIZABI DIALOGS
     * Reusable dialogs component
     */

    //default existing dialogs
    var class_active$1 = "vzb-active";
    var Dialogs = Component.extend({

      /**
       * Initializes the dialogs
       * @param config component configuration
       * @param context component context (parent)
       */
      init: function(config, context) {

        //set properties
        var _this = this;
        this.name = 'gapminder-dialogs';
        this._curr_dialog_index = 20;


        this.model_expects = [{
          name: "state",
          type: "model"
        }, {
          name: "ui",
          type: "model"
        }, {
          name: "language",
          type: "language"
        }];

        this._available_dialogs = {
          'find': {
            dialog: Find,
          },
          'show': {
            dialog: Show,
          },
          'moreoptions': {
            dialog: MoreOptions,
          },
          'colors': {
            dialog: Colors,
          },
          'size': {
            dialog: Size,
          },
          'axes': {
            dialog: Axes,
          },
          'axesmc': {
            dialog: Axes$1,
          },
          'stack': {
            dialog: Stack,
          },
          'speed': {
            dialog: Speed
          },
          'opacity': {
            dialog: Opacity
          },
          'presentation': {
            dialog: Presentation
          },
          'about': {
            dialog: About
          }
        };

        this._super(config, context);

      },

      readyOnce: function() {

        var _this = this;
        var dialog_popup = (this.model.ui.dialogs||{}).popup || [];
        var dialog_sidebar = (this.model.ui.dialogs||{}).sidebar || [];

        this.element = d3.select(this.placeholder);

        this.element.selectAll("div").remove();

        // if dialog_sidebar has been passed in with boolean param or array must check and covert to array
        if (dialog_sidebar === true) {
          dialog_sidebar = dialog_popup;
          (this.model.ui.dialogs||{}).sidebar = dialog_sidebar;
        }

        if (dialog_sidebar.length !== 0) {
            d3.select(this.root.element).classed("vzb-dialog-expand-true", true);
        }

        this._addDialogs(dialog_popup, dialog_sidebar);

        this.resize();

        if((this.model.ui.dialogs||{}).popup) {
          this.root.findChildByName("gapminder-buttonlist")
            .on("click", function(evt, button) {
              if(!_this._available_dialogs[button.id]) return;

              if(button.active) {
                _this.openDialog(button.id)
              } else {
                _this.closeDialog(button.id)
              }
            });

          var popupDialogs = this.element.selectAll(".vzb-top-dialog").filter(function(d) {return dialog_popup.indexOf(d.id) > -1});

          var close_buttons = popupDialogs.select(".vzb-top-dialog>.vzb-dialog-modal>.vzb-dialog-buttons>[data-click='closeDialog']");
          close_buttons.on('click', function(d, i) {
            _this.closeDialog(d.id);
          });

          var pinDialog = popupDialogs.select(".vzb-top-dialog>.vzb-dialog-modal>[data-click='pinDialog']");
          pinDialog.on('click', function(d, i) {
            _this.pinDialog(d.id);
          });

          this.root.element.addEventListener('click', function() {
            _this.closeAllDialogs();
          });

          d3.select(this.root.element).on("mousedown", function(e) {
            if(!this._active_comp) return; //don't do anything if nothing is open

            var target = d3.event.target;
            var closeDialog = true;
            while(target) {
              if(target.classList.contains("vzb-dialog-modal")) {
                closeDialog = false;
                break;
              }
              target = target.parentElement;
            }
            if(closeDialog) {
              _this.closeAllDialogs();
            }
          });
        }

        this.element.on('click', function() {
          d3.event.stopPropagation();
        });

      },

      resize: function() {
        var _this = this;
        var dialog_popup = (this.model.ui.dialogs||{}).popup || [];
        var dialog_sidebar = (this.model.ui.dialogs||{}).sidebar || [];
        var profile = this.getLayoutProfile();

        this.element.selectAll(".vzb-top-dialog").each(function(d) {
          var dialogEl = d3.select(this);
          var cls = dialogEl.attr('class').replace(' vzb-popup','').replace(' vzb-sidebar','');

          if (profile === 'large' && dialog_sidebar.indexOf(d.id) > -1) {
            cls += ' vzb-sidebar';
          } else if(dialog_popup.indexOf(d.id) > -1) {
            cls += ' vzb-popup';
          }

          dialogEl.attr('class', cls);
        });

      },

      /*
       * adds dialogs configuration to the components and template_data
       * @param {Array} dialog_list list of dialogs to be added
       */
      _addDialogs: function(dialog_popup, dialog_sidebar) {

        var profile = this.getLayoutProfile();
        var dialog_list = [];

        dialog_list = dialog_popup ? dialog_list.concat(dialog_popup) : dialog_list;
        dialog_list = dialog_sidebar ? dialog_list.concat(dialog_sidebar) : dialog_list;

        dialog_list = unique(dialog_list);

        this._components_config = [];
        var details_dlgs = [];
        if(!dialog_list.length) return;
        //add a component for each dialog
        for(var i = 0; i < dialog_list.length; i++) {

          var dlg = dialog_list[i];
          var dlg_config = this._available_dialogs[dlg];

          //if it's a dialog, add component
          if(dlg_config && dlg_config.dialog) {
            var comps = this._components_config;

            //add corresponding component
            comps.push({
              component: dlg_config.dialog,
              placeholder: '.vzb-dialogs-dialog[data-dlg="' + dlg + '"]',
              model: ["state", "ui", "language"]
            });

            dlg_config.component = comps.length - 1;
          }

          dlg_config.id = dlg;
          details_dlgs.push(dlg_config);
        };

        this.element.selectAll('div').data(details_dlgs)
          .enter().append("div")
          .attr('data-dlg', function(d) {
            return d.id;
          })
          .attr('class', 'vzb-top-dialog vzb-dialogs-dialog vzb-dialog-shadow');

        this.loadComponents();

        var _this = this;
        //render each subcomponent
        forEach(this.components, function(subcomp) {
          subcomp.render();
          _this.on('resize', function() {
            subcomp.trigger('resize');
          });
          subcomp.on('dragstart', function() {
            _this.bringForward(subcomp.name);
          });
          subcomp.on('close', function() {
            this.placeholderEl.each( function(d) {
              var evt = {};
              evt.id = d.id;
              _this.trigger('close', evt);
            });
          });
        });

      },

      bringForward: function(id) {
        var dialog = this.element.select(".vzb-popup.vzb-dialogs-dialog[data-dlg='" + id + "']");
        dialog.style('z-index', this._curr_dialog_index);
        this._curr_dialog_index += 10;
      },

      //TODO: make opening/closing a dialog via update and model
      /*
       * Activate a dialog
       * @param {String} id dialog id
       */
      openDialog: function(id) {
        //close pinned dialogs for small profile
        var forceClose = this.getLayoutProfile() === 'small' ? true : false;
        this.closeAllDialogs(forceClose);

        var dialog = this.element.selectAll(".vzb-popup.vzb-dialogs-dialog[data-dlg='" + id + "']");

        this._active_comp = this.components[this._available_dialogs[id].component];

        this._active_comp.beforeOpen();
        //add classes
        dialog.classed(class_active$1, true);

        this.bringForward(id);

        //call component function
        this._active_comp.open();
      },


      pinDialog: function(id) {
        var dialog = this.element.select(".vzb-popup.vzb-dialogs-dialog[data-dlg='" + id + "']");

        if(this._available_dialogs[id].ispin) {
          dialog.classed('pinned', false);
          this._available_dialogs[id].ispin = false;
        } else {
          dialog.classed('pinned', true);
          this._available_dialogs[id].ispin = true;
        }
      },


      /*
       * Closes a dialog
       * @param {String} id dialog id
       */
      closeDialog: function(id) {
        var dialog = this.element.selectAll(".vzb-popup.vzb-dialogs-dialog[data-dlg='" + id + "']");

        this._active_comp = this.components[this._available_dialogs[id].component];

        if(this._active_comp && !this._active_comp.isOpen) return;

        if(this._available_dialogs[id].ispin)
          this.pinDialog(id);

        if(this._active_comp) {
          this._active_comp.beforeClose();
        }
        //remove classes
        dialog.classed(class_active$1, false);

        //call component close function
        if(this._active_comp) {
          this._active_comp.close();
        }
        this._active_comp = false;

      },

      /*
       * Close all dialogs
       */
      closeAllDialogs: function(forceclose) {
        var _this = this;
        //remove classes
        var dialogClass = forceclose ? ".vzb-popup.vzb-dialogs-dialog.vzb-active" : ".vzb-popup.vzb-dialogs-dialog.vzb-active:not(.pinned)";
        var all_dialogs = this.element.selectAll(dialogClass);
          all_dialogs.each(function(d) {
            _this.closeDialog(d.id)
          });
      }

    });

    var hidden = true;

    var DataWarning = Component.extend({

      init: function(config, context) {
        var _this = this;

        this.name = 'gapminder-datawarning';

        this.model_expects = [{
          name: "language",
          type: "language"
        }];

        this.context = context;

        this.model_binds = {
          "change:language.strings": function(evt) {
            _this.ready();
          }
        }

        //contructor is the same as any component
        this._super(config, context);

        this.ui = extend({
          //...add properties here
        }, this.ui);

      },

      ready: function() {},

      readyOnce: function() {
        var _this = this;
        this.element = d3.select(this.placeholder);
        this.translator = this.model.language.getTFunction();

        this.element.selectAll("div").remove();

        this.element.append("div")
          .attr("class", "vzb-data-warning-background")
          .on("click", function() {
            _this.toggle(true)
          });

        var container = this.element.append("div")
          .attr("class", "vzb-data-warning-box");

        container.append("div")
          .html(iconClose)
          .on("click", function() {
            _this.toggle()
          })
          .select("svg")
          .attr("width", "0px")
          .attr("height", "0px")
          .attr("class", "vzb-data-warning-close");

        var icon = container.append("div")
          .attr("class", "vzb-data-warning-link")
          .html(iconWarn)

        icon.append("div")
          .text("Data doubts");

        if(this.parent.datawarning_content.title) {
          container.append("div")
            .attr("class", "vzb-data-warning-title")
            .html(this.parent.datawarning_content.title);
        }

        container.append("div")
          .attr("class", "vzb-data-warning-body vzb-dialog-scrollable")
          .html(this.parent.datawarning_content.body);

      },

      toggle: function(arg) {
        if(arg == null) arg = !hidden;
        hidden = arg;
        this.element.classed("vzb-hidden", hidden);

        var _this = this;
        this.parent.components.forEach(function(c) {
          c.element.classed("vzb-blur", c != _this && !hidden);
        })
      }

    });

    //d3.svg.colorPicker


    var instance = null;

    function colorPicker() {


      return function getInstance() {
        if (instance == null) {
          instance = d3_color_picker();
        }
        return instance;
      }();

      function d3_color_picker() {
        // tuning defaults
        var nCellsH = 15;
        // number of cells by hues (angular)
        var minH = 0;
        // which hue do we start from: 0 to 1 instead of 0 to 365
        var nCellsL = 4;
        // number of cells by lightness (radial)
        var minL = .5;
        // which lightness to start from: 0 to 1. Recommended .3...0.5
        var satConstant = .7;
        // constant saturation for color wheel: 0 to 1. Recommended .7...0.8
        var outerL_display = .4;
        // ecxeptional saturation of the outer circle. the one displayed 0 to 1
        var outerL_meaning = .3;
        // ecxeptional saturation of the outer circle. the one actually ment 0 to 1
        var firstAngleSat = 0;
        // exceptional saturation at first angular segment. Set 0 to have shades of grey
        var minRadius = 15;
        //radius of the central hole in color wheel: px
        var maxWidth = 280;
        var maxHeight = 323;
        var margin = {
          top: .1,
          bottom: .1,
          left: .1,
          right: .1
        };
        //margins in % of container's width and height
        var colorOld = '#000';
        var colorDef = '#000';
        // names of CSS classes
        var css = {
          INVISIBLE: 'vzb-invisible',
          COLOR_POINTER: 'vzb-colorpicker-pointer',
          COLOR_BUTTON: 'vzb-colorpicker-cell',
          COLOR_DEFAULT: 'vzb-colorpicker-default',
          COLOR_SAMPLE: 'vzb-colorpicker-sample',
          COLOR_PICKER: 'vzb-colorpicker-svg',
          COLOR_CIRCLE: 'vzb-colorpicker-circle',
          COLOR_SEGMENT: 'vzb-colorpicker-segment',
          COLOR_BACKGR: 'vzb-colorpicker-background'
        };
        var colorData = [];
        //here we store color data. formatted as follows:
        /*
         [
         [ // outer circle
         {display: "#123456", meaning: "#123456"}, // first angle
         ...
         {display: "#123456", meaning: "#123456"} // last angle, clockwise
         ],
         [ // next circle
         {display: "#123456", meaning: "#123456"}, // first angle
         ...
         {display: "#123456", meaning: "#123456"} // last angle, clockwise
         ],

         ...

         [ // inner circle
         {display: "#123456", meaning: "#123456"}, // first angle
         ...
         {display: "#123456", meaning: "#123456"} // last angle, clockwise
         ]
         ]
         */
        var arc = d3.svg.arc();
        var pie = d3.layout.pie().sort(null).value(function(d) {
          return 1;
        });
        var svg = null;
        var container = null;
        var colorPointer = null;
        var showColorPicker = false;
        var sampleRect = null;
        var sampleText = null;
        var background = null;
        var callback = function(value) {
          console.info('Color picker callback example. Setting color to ' + value);
        };

        function _generateColorData() {
          var result = [];
          // loop across circles
          for(var l = 0; l < nCellsL; l++) {
            var lightness = minL + (1 - minL) / nCellsL * l;
            // new circle of cells
            result.push([]);
            // loop across angles
            for(var h = 0; h <= nCellsH; h++) {
              var hue = minH + (1 - minH) / nCellsH * h;
              // new cell
              result[l].push({
                display: _hslToRgb(hue, h == 0 ? firstAngleSat : satConstant, l == 0 ? outerL_display : lightness),
                meaning: _hslToRgb(hue, h == 0 ? firstAngleSat : satConstant, l == 0 ? outerL_meaning : lightness)
              });
            }
          }
          return result;
        }

        function _hslToRgb(h, s, l) {
          var r, g, b;
          if(s == 0) {
            r = g = b = l; // achromatic
          } else {
            var _hue2rgb = function _hue2rgb(p, q, t) {
              if(t < 0)
                t += 1;
              if(t > 1)
                t -= 1;
              if(t < 1 / 6)
                return p + (q - p) * 6 * t;
              if(t < 1 / 2)
                return q;
              if(t < 2 / 3)
                return p + (q - p) * (2 / 3 - t) * 6;
              return p;
            };
            var q = l < .5 ? l * (1 + s) : l + s - l * s;
            var p = 2 * l - q;
            r = _hue2rgb(p, q, h + 1 / 3);
            g = _hue2rgb(p, q, h);
            b = _hue2rgb(p, q, h - 1 / 3);
          }
          return '#' + Math.round(r * 255).toString(16) + Math.round(g * 255).toString(16) + Math.round(b * 255).toString(
            16);
        }

        // this is init function. call it once after you are satisfied with parameters tuning
        // container should be a D3 selection that has a div where we want to render color picker
        // that div should have !=0 width and height in its style
        function colorPicker(container) {
          colorPicker.container = container;
          svg = container.select('.' + css.COLOR_PICKER);
          if(!svg.empty()) {
            return;
          }
          container.on('click', function() {
            colorPicker.show(false);
            d3.event.stopPropagation();
          });
          colorData = _generateColorData();

          svg = container.append('svg')
            .style('position', 'absolute')
            .style('top', '0')
            .style('left', '0')
            .style('width', '100%')
            .style('max-width', maxWidth)
            .style('height', '100%')
            .style('max-height', maxHeight)
            .style('z-index', 9999)
            .attr('class', css.COLOR_PICKER + " vzb-dialog-shadow")
            .classed(css.INVISIBLE, !showColorPicker);

          var width = parseInt(svg.style('width'));
          var height = parseInt(svg.style('height'));
          var maxRadius = width / 2 * (1 - margin.left - margin.right);
          background = svg.append('rect')
            .attr('width', width)
            .attr('height', height)
            .attr('class', css.COLOR_BUTTON +' '+ css.COLOR_BACKGR)
            .on('mouseover',
              function(d) {
                _cellHover(colorOld);
              });
          var circles = svg.append('g')
            .attr('transform', 'translate(' + (maxRadius + width * margin.left) +
            ',' + (maxRadius + height * margin.top) + ')');

          svg.append('rect')
            .attr('class', css.COLOR_SAMPLE)
            .attr('width', width / 2)
            .attr('height', height * margin.top / 2);

          sampleRect = svg.append('rect')
            .attr('class', css.COLOR_SAMPLE)
            .attr('width', width / 2)
            .attr('x', width / 2)
            .attr('height', height * margin.top / 2);

          svg.append('text')
            .attr('x', width * margin.left)
            .attr('y', height * margin.top / 2)
            .attr('dy', '0.5em')
            .attr('class', css.COLOR_SAMPLE)
            .style('text-anchor', 'start');

          sampleText = svg.append('text').attr('x', width * (1 - margin.right))
            .attr('y', height * margin.top / 2)
            .attr('dy', '0.5em')
            .attr('class', css.COLOR_SAMPLE)
            .style('text-anchor', 'end');

          svg.append('text')
            .attr('x', width * .1)
            .attr('y', height * (1 - margin.bottom))
            .attr('dy', '0.3em')
            .attr('class', "vzb-default-label")
            .style('text-anchor', 'start')
            .text('default');

          svg.append('circle')
            .attr('class', css.COLOR_DEFAULT + ' ' + css.COLOR_BUTTON)
            .attr('r', width * margin.left / 2)
            .attr('cx', width * margin.left * 1.5)
            .attr('cy', height * (1 - margin.bottom * 1.5))
            .on('mouseover',
              function() {
                d3.select(this).style('stroke', '#444');
                _cellHover(colorDef);
            })
            .on('mouseout', function() {
              d3.select(this).style('stroke', 'none');
            });

          circles.append('circle')
            .attr('r', minRadius - 1)
            .attr('fill', '#fff')
            .attr('class', css.COLOR_BUTTON)
            .on('mouseover',
              function() {
                d3.select(this).style('stroke', '#444');
                _cellHover('#fff');
            })
            .on('mouseout', function() {
              d3.select(this).style('stroke', 'none');
            });

          circles.selectAll('.' + css.COLOR_CIRCLE)
            .data(colorData).enter().append('g')
              .attr('class', css.COLOR_CIRCLE)
                .each(
                  function(circleData, index) {
                    arc.outerRadius(minRadius + (maxRadius - minRadius) / nCellsL *
                      (nCellsL - index)).innerRadius(minRadius +
                      (maxRadius - minRadius) / nCellsL * (nCellsL - index - 1));
                    var segment = d3.select(this).selectAll('.' + css.COLOR_SEGMENT)
                      .data(pie(circleData)).enter().append('g')
                        .attr('class', css.COLOR_SEGMENT);

                  segment.append('path')
                    .attr('class', css.COLOR_BUTTON)
                    .attr('d', arc)
                    .style('fill', function(d) {
                      return d.data.display;
                    })
                    .style('stroke', function(d) {
                      return d.data.display;
                    })
                    .on('mouseover', function(d) {
                      _cellHover(d.data.meaning, this);
                    })
                    .on('mouseout', function(d) {
                      _cellUnHover();
                    });
                });

          colorPointer = circles.append('path')
            .attr('class', css.COLOR_POINTER + ' ' + css.INVISIBLE);

          svg.selectAll('.' + css.COLOR_BUTTON)
            .on('click', function() {
              d3.event.stopPropagation();
              _this.show(false);
            });
          _doTheStyling(svg);
        }

        var _doTheStyling = function(svg) {
          //styling
          svg.select('.' + css.COLOR_BACKGR)
            .style('fill', 'white');

          svg.select('.' + css.COLOR_POINTER)
            .style('stroke-width', 2)
            .style('stroke', 'white')
            .style('pointer-events', 'none')
            .style('fill', 'none');

          svg.selectAll('.' + css.COLOR_BUTTON)
            .style('cursor', 'pointer');

          svg.selectAll('text')
            .style('dominant-baseline', 'hanging')
            .style('fill', '#D9D9D9')
            .style('font-size', '0.7em')
            .style('text-transform', 'uppercase');

          svg.selectAll('circle.' + css.COLOR_BUTTON)
            .style('stroke-width', 2);
        };

        var _this = colorPicker;
        var _cellHover = function(value, view) {
          // show color pointer if the view is set (a cell of colorwheel)
          if(view != null)
            colorPointer.classed(css.INVISIBLE, false)
              .attr('d', d3.select(view)
              .attr('d'));

          sampleRect.style('fill', value);
          sampleText.text(value);
          callback(value);
        };
        var _cellUnHover = function() {
          colorPointer.classed(css.INVISIBLE, true);
        };
        //Use this function to hide or show the color picker
        //true = show, false = hide, "toggle" or TOGGLE = toggle
        var TOGGLE = 'toggle';
        colorPicker.show = function(arg) {
          if(!arguments.length)
            return showColorPicker;
          if(svg == null)
            console.warn('Color picker is missing SVG element. Was init sequence performed?');
          showColorPicker = arg == TOGGLE ? !showColorPicker : arg;
          if (!showColorPicker) {
            callback = function() {};
          }
          svg.classed(css.INVISIBLE, !showColorPicker);
        };
        // getters and setters
        colorPicker.nCellsH = function(arg) {
          if(!arguments.length)
            return nCellsH;
          nCellsH = arg;
          return colorPicker;
        };
        colorPicker.minH = function(arg) {
          if(!arguments.length)
            return minH;
          minH = arg;
          return colorPicker;
        };
        colorPicker.nCellsL = function(arg) {
          if(!arguments.length)
            return nCellsL;
          nCellsL = arg;
          return colorPicker;
        };
        colorPicker.minL = function(arg) {
          if(!arguments.length)
            return minL;
          minL = arg;
          return colorPicker;
        };
        colorPicker.outerL_display = function(arg) {
          if(!arguments.length)
            return outerL_display;
          outerL_display = arg;
          return colorPicker;
        };
        colorPicker.outerL_meaning = function(arg) {
          if(!arguments.length)
            return outerL_meaning;
          outerL_meaning = arg;
          return colorPicker;
        };
        colorPicker.satConstant = function(arg) {
          if(!arguments.length)
            return satConstant;
          satConstant = arg;
          return colorPicker;
        };
        colorPicker.firstAngleSat = function(arg) {
          if(!arguments.length)
            return firstAngleSat;
          firstAngleSat = arg;
          return colorPicker;
        };
        colorPicker.minRadius = function(arg) {
          if(!arguments.length)
            return minRadius;
          minRadius = arg;
          return colorPicker;
        };
        colorPicker.margin = function(arg) {
          if(!arguments.length)
            return margin;
          margin = arg;
          return colorPicker;
        };
        colorPicker.callback = function(arg) {
          if(!arguments.length)
            return callback;
          callback = arg;
          return colorPicker;
        };
        colorPicker.colorDef = function(arg) {
          if(!arguments.length)
            return colorDef;
          if (typeof arg !== 'undefined') {
            colorDef = arg;
          }
          if(svg == null)
            console.warn('Color picker is missing SVG element. Was init sequence performed?');
          svg.select('.' + css.COLOR_DEFAULT).style('fill', colorDef);
          return colorPicker;
        };
        /**
         * @param {ClientRect} screen parent element
         * @param {int[]} arg [x,y] of color picker position
         */
        colorPicker.fitToScreen = function(arg) {
          var screen = colorPicker.container.node().getBoundingClientRect();
          var xPos, yPos;

          var width = parseInt(svg.style('width'));
          var height = parseInt(svg.style('height'));

          if (!arg) {
            xPos = screen.width - parseInt(svg.style('right')) - width;
            yPos = parseInt(svg.style('top'));
          } else {
            xPos = arg[0] - screen.left;
            yPos = arg[1] - screen.top;
          }

          var styles = {left: ''};
          if (screen.width * 0.8 <= width) {
            styles.right = (screen.width - width) * 0.5;
          } else if (xPos + width > screen.width) {
            styles.right = Math.min(screen.width * 0.1, 20);
          } else {
            styles.right = screen.width - xPos - width;
          }
          if (screen.height * 0.8 <= height) {
            styles.top = (screen.height - height) * 0.5;
          } else if (yPos + height * 1.2 > screen.height) {
            styles.top = screen.height * 0.9 - height;
          } else {
            styles.top = yPos;
          }

          svg.style(styles);
          return colorPicker;
        };
        colorPicker.colorOld = function(arg) {
          if(!arguments.length)
            return colorOld;
          colorOld = arg;
          if(svg == null)
            console.warn('Color picker is missing SVG element. Was init sequence performed?');
          svg.select('rect.' + css.COLOR_SAMPLE).style('fill', colorOld);
          svg.select('text.' + css.COLOR_SAMPLE).text(colorOld);
          return colorPicker;
        };

        colorPicker.resize = function(arg) {

          if(!arguments.length)
            return resize;
          if (typeof arg !== 'undefined') {
            var svg = arg;
            var width = parseInt(svg.style('width'));
            var height = parseInt(svg.style('height'));
            var selectedColor = svg.select('.'+css.COLOR_DEFAULT);
            var defaultLabel = svg.select('.vzb-default-label');
            selectedColor.attr('cx', width * margin.left * 1.5)
                         .attr('cy', height * (1 - margin.bottom * 1.5));
            defaultLabel.attr('x', width * .1)
                        .attr('y', height * (1 - margin.bottom));
          }
          colorPicker.fitToScreen();

          return colorPicker;
        };
        return colorPicker;
      };
    };

    //d3.svg.worldMap

    function worldMap() {
      return function d3_world_map() {
        var world =
          '<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 584.5 364.5"><path id="europe" d="M556.7,26.9l-35.5-7.3l-3.5,1.4l-49.9-5.2l-2.7,2l-45.8-4.1l-1.3-1.9l-15.3-2.2l-0.2,0.1h-0.1l-0.2,0.2l-6,0.6 l-0.5,0.5L372.4,17l-1.7,1.7l-5.8-3.1h-1.7l-1.5,3.7l1.8,2.5l-0.4,0.2l-10.1-1.5l-6.8,1.9l-5.3-0.6l-7.2,2.6l-4.2-1h-0.1l-3.1,3.2 l-0.9,0.2l-2.6,2.2l-2.3,0.8l-1.6,2h-1.7l-5.1-5.1l-1-0.2l-0.1-0.5l1.3-0.9l8.4,1.6l0.5-0.1l2.4-1.8l-0.8-0.9l-20.2-5.5l-16.9,3.4 L268,37l0.8,6.1l3.2,1.7l4-1l1.5,0.9l2.6,5.5h0.8l0.7,1.2l0.8,0.2l7.9-9.7l-2.9-5.4l8.5-8.9h0.5l1.3,1.7l-2.7,6.6l0.8,2.8l11.9,2.4 l-4,1.8l-3.5-0.3l-1.5,1.2l1,1.6l-0.1,2.2l-0.9-0.6H297l-1.8,1.2l-0.5,3.9l-2.3,2.2h-4.3l-4.2,1.9l-6.8-0.7l-0.6-0.4l2.5-1.7 l0.5-1.2l-0.9-1.7l-0.2-0.1l-2.3,0.5l-0.2-0.1l-0.2-3.4l-0.4-0.1l-2.6,3.9l1.3,3.7l-1.4,1.7L269,57l-18.9,13.1l0.1,1l1.7,1.6 l0.8,0.3l1.3,2.2l0.3,3.6l-3.1,4.5l-9.7-0.9l-1.3,1.5L239,97.9l0.4,1.1l5.1,3.1l0.2,0.8l1.6-0.2l0.1-0.2h0.1v-0.1l7.9-4.5l10-14.3 l10-2.8l1.2,0.5l11,11.5l0.2,2.3l-2,1.8l-1.9-0.4l-1.8,0.5l3.8,3.9l1.1-0.7l3.7-5.6l0.2-0.5l-0.9-1.9l0.2-0.4l2.3,0.3l0.8-1 l-1.7-0.9l-8.7-7.6l-0.5-4.5l1.4,0.2l10.4,8l3.4,9l1,0.5l0.5,0.6v1.5l4.5,6.1v0.4l0.7,1.1l3.7,1.3l1.4-1.6l-3.8-2.3l-0.1-1.7 l2.2-2.6l-6.3-6.3l5.6-2.2L306,90l5.8,8l4.2-0.6l2.7,0.9l1,4.7l0.7-0.1l1.8-2l-1.3-1.7l0.2-0.9h4.3l0.3,2.7l15.2-4.7l0.4-5.1 l1.5-0.9l2.5,2l3.9-2.1l1.4,1.5l0.3-3.9l-3.1-5.3l-1.3-8.6l2.9-2.5l-0.6-2.7l-3.8-3.8l4.5-6.9l6.8,1.6l1.1-3.1l9.2,3.8l13.3-3.1 l-1.8-6.7l0.2-1l8.7,7.4l22.2,7.4l4.3-2.7l1.5-4.5l1-0.5l0.2,0.2l6,0.4l1,1.2l1.7,0.8l0.5-0.3l7.5,2.9l7.5-4.2l1.5,1.8l22.1,0.8 l0.7-1.8l23.5-1.4l7,9.2l9.6-1.2l3.4,15.2l1,1.1l-0.2,0.2l1.7,1.7l0.5,0.1l1.8-2.2l1.6-5.3L508,56.7l-2.9-2.2l-5.5,0.3l-2.6-2.5 l1.8-7.8l0.5-0.3l0.2-0.9l3.4-1.7l14.2,0.6l1.3-4.8l1.6-1.2l0.4-0.1l4.3,1.2l0.1-0.1l0.2,0.1l3.1-2.5l1.7,0.9l-1,12l6.9,15.9l3.1-3 l0.1-0.3l2.3,1.1l0.8-2.2l-1.1-8.7l-4.8-5.8l0.1-2.6l0.8-1.5l4.5-2.2l2.2,0.2l4-3.7l2.1-0.3l1.1-1.7l-5.2-2.5l-0.5-1.7l2.9-1.7 l8.2,2.2l0.9-0.2l0.8-1.2L556.7,26.9 M331,87l-11.6-3.1l-8.9,2.9l-0.2-0.1l-0.5-1.9l2.9-7l2.9-2.5h1.7l2.1,1.1l2.3-1.7l1.8-3.4 l1.8-0.6l2.1,0.6l-0.8,3.9l7.7,7.3L331,87 M252.8,18.2l-5.8,5.6l-3.7,1.1l-1.1,4.3l-2.2,1.7l-0.2,1.2l0.9,1.7l7.8,1.2l-2.4,2.9 l-4.6,1.7l-5.9-2.9l2-1.8l1.9-0.8l-2.5-2.1l-11.4,1.7l-4.7,3.1l-8,1.7L203,49l-3.4,0.3l-3.7-2.8l-1.3-10.6l5.2-4.5l1.1-2l-1.9-3.3 l-0.5-0.3v-0.6l-0.5-0.3v-0.6l-0.6-0.3l-1.1-1.4l-3.1-1.4h-5.5l-4-1.7l71.2-3.4L252.8,18.2 M258.9,60.7l0.7,1.2l-10.5,1.5l3.4-1.5 l-0.1-1.5l-2.7-0.9l4.2-4.9l-2.7-2.7l-5.9,7.4l-4.4,0.8l1.1-2.7l-0.2-2.7l8.5-4.8l0.3-3.8l1-1.3l1.3,0.4l0.2,1.1l1.3,0.3l-0.8,3.2 l3.3,2.4l1.7,5.1l2.6,0.9L258.9,60.7"/><path id="africa" d="M322.7,114.7l-1-1.8l-6.5,2.3l-16-4.8l-2.3,1.7l-1.8,4.5l-16.9-8.6l-0.2-0.6l-0.3-5.5l-2-2.8l-29,4.4l-0.2-0.4 l-1.7,0.2l-0.1,1.1l-6.7,7l-0.5,1.9l-0.6,0.7l-0.3,3.3l-15.3,23.7l0.6,13.2l-1.4,3l1.1,7.6l12.1,17.9l6,2.8l7.1-1.9l4.5,0.8 l13.7-3.3l3.9,4.5h3.5l1.6,1.4l1.8,3.6l-1.1,10.7l9.2,27.4l-4,14.6l8.5,30.7l1.1,1.1v0.7h0.5l3.5,12.5l2,1.7l11.9-0.6l15-18.2v-3.9 l5.1-4.5l1.1-4.2l-1.1-5.9l10.5-12.2l0.6-0.3l1.6-3.7l-3.4-24l25-43.3l-13.1,1.1l-1.8-1.1l-24.7-48.6l0.9-0.4l0.6-1L322.7,114.7  M360.1,233.2l2.3,1.7l-8.6,30.5l-4.3-0.6l-2-7.6l2.8-14.6l6.4-4.4l2.8-4.9L360.1,233.2z"/><path id="americas" d="M134.8,152l-11.4,1.8l-3.1-1.7l5.3-1.3l-0.7-1.1l-3.3-1.4h-0.1l-8.1-0.9l-0.3-0.3l-0.3-1.5l-6.2-3.6l-3.4,0.8 l-1.6,1.3l-1.2-0.5l-0.7-1.7l3.8-1.6l9.1,0.7l9.5,5.3l0,0l3.3,1.8l1.7-0.5l6.6,2.8L134.8,152 M183.7,25.4l-0.5-1.5l-2.6-2.2 l-2.1-0.6l-2.9-2.2l-18.2-2.2l-5.1,3.7l2,4.3l-6,2.2l1-1.7l-4.6-1.9l-0.5-1.7l-1.1-1.2l-2.9,0.5l-2.1,4.2l-5.8,2.5l-15.5-2.2 l10.5-1.7l-1.3-4l-11.6-0.4l-3.2-1.5L96,20.7h5.8l4,1.9l-1.7,1l0.8,1l7.2,2.3l-78.9-5.3l-10,3.6l-0.4,4.4L18,31.1l1,1.8l1.7,1.2 l-5.5,4.5l-0.4,5.6L13.8,46l1.8,1.8l-4.4,6.2L22,43.7l1.8-0.5l1.3-1.2l13.4,4l4,4.2l-1.3,14l1.6,2.6l-3.3,1.3L39.4,70l2.7,2.6 L28.6,96.9l1.6,11.2l4.8,5.6l-0.2,3.4l2.5,6.1l-0.5,5l6.6,11.9L38,121.5l1.7-4l3.4,6.1l0.3,2.2l7.1,13.1l1.1,9.2l11.1,8.7l1.6,0.3 l1.3,0.9l5.5,1.2l3.4-0.9l5.5,4.2l0.3,0.5l0.8,0.3l2.1,1.9l5.5,0.5l0.2,0.6l0.8,0.3l4.8,8.9l2.3,1.5l0.2,0.5l7.1,3.4l1.6-1.7 l-5.1-2.2l-1.3-15.6l-6.3-2.2l-3.7,0.3v-4.6l3.7-8.9l-5.2-0.9l-0.5,0.3L83,151l-6.3,2.2l-4-2.8l-3.2-8.9l3.2-11.8l0.5-0.3l0.2-1.2 l2.6-3.1l8.5-3.6l6.3,1.8l4.5-3.1l9.2,1.1l2.5,3.1l1.5,7.8l1.3,1.8l2.1-4.5l-1.1-5l1.6-7l13.7-12.3l0.2-3.7l0.8-1.7l0.9-0.2l0.7,0.5 l0.6-1.9l15-8.8l2.2-3.9l11.9-5.1l-2.2,3.6l11.4-3.8l-5.2-1.7l-1.8-2.8l1.6-4.2l-0.8-0.9h-4.2l0.8-1.5l19.5-3.2l1.6,2.8l-4.5,4.2 l6,1.7l5.3-2.2l-6.3-7.6l4.5-6.1l-1.1-0.6l-0.2-0.5h-3.2l-3.7-13.4l-7.7,3.1l-1.8-1.9l0.2-3.9l-2.3-2.5l-3.4-1.5l-6.6,1.9l-2.1,4.2 l-1.1,0.6l-1.3,2.2l-0.3,3.4l-10,9.5l-0.8,2.8l-1.8,1.9l-2.1,0.3l-1.8-2.5l1.1-4.8l-11.9-6.1l-3.1-5.1l15-12l1.3,0.3l5.1-1.2 l1.1-1.2l0.4-1.2l3.4-0.3l-1.7,4.8L147,34l4.6,0.7l-2.2-2.9l-2.1-1.2l8.2-2.8l0.3-0.6l2-1.7l0.7,0.1l8.1-4.2l7.4,5.3l0.2,1.5l-6,1.5 l-1.8,2.2l3.7,5.3l3.4,1.2l2.3-2.2l2.9-1.2L179,33l-0.2-1.9l7.7-1.7L183.7,25.4 M119.7,74.5l0.8,3.1l1.7,1.8l3.3-0.2l5.4,4.7 l2.7,0.2l-0.5,1.7l-4.7-0.4l0.2-1.2l-2.6-0.9l-2,0.6l-2.6,3.4l3.1,1.7l-3.2,2.3l-2.6-1.2l0.1-9.3l-9.6,9.9L108,88l4.5-7l4.3-2 l-5.1-2.1l-4.8,0.5l0.2-1.7l1.3-1.2l8.7-2.2L119.7,74.5 M205.9,223.1l-1.3,3.1H204l-7.1,11.2l-1.9,18.2l-3.1,6.1h-0.5v0.6l-0.8,0.3 l-1.1,1.2l-2.7-0.3l-9.4,6.7l-7.7,21.6l-3.9,3.3l-5.1-1.1l2.1,3.3l0.5,5.3l-7.9,3.3l-1.4,1.5l-0.5,3.6l-1.1,0.6l-1.1-0.3l-1.8,0.9 l1.8,6.1l-1.8,5.6l3.4,6.1l-2,5.9l0.5,3.1l11.1,8.2l-0.2,0.5l-9.3-0.6l-4.3-5.1l-4.7-1.7l-8.6-17.1l0.5-1.7l-6-12.3l-4.5-56.7 l-12.4-10.2l-4.2-8.1l-0.8-0.6l-9.8-21.5l1.1-2.2l-0.3-2.6l-0.5-0.8l7.9-15.3l0.3-5.6l-1-2.8l1-3.9l1.8-0.3l9.7-8.2l2.1,0.3l0.8,5.1 l2.7-5.1l1.3-0.3l4.2,2.8h0.9l0.2,0.6l14.8,3.9l1.6,1.4l0.3,0.6l7.9,6.7l7.7,0.9l4.3,4l2,6.3l-1,4.6l4.4,1.4l1.1,2.2l5.2-1.1 l2.1,1.1l2.6,4l2.9-0.9l9,1.9l8.6,5.8L205.9,223.1"/><path id="asia" d="M322.9,118.9l22.8,42.5l13.5-5.9l16.8-19l-7.3-6.5l-0.7-3.4h-0.1l-5.7,5.2l-0.9,0.1l-3.2-4.4l-0.4-0.2l-0.7,1.7 l-1.2-0.4l-4.1-11.4l0.2-0.5l1.9-1.2l5.1,6.8l6.2,2.7l0.8-0.2l1.1-1.1l1.6,0.4l2.9,2.6l0.4,0.8l16.4,0.8l6.9,6.5l0.4,0.1l1.4-0.3 l0.3,0.1l-1.7,2.5l2.9,2.8h0.7l3.3-3.3l0.5,0.3l9.2,32.1l4,3.7l1.3-1.3h0.2l1.7,1.3l1.4,6.6l1.6,0.9l1.7-2.9l-2.3-7.3l-0.1,0.3v-0.2 l-1.7,0.6l-1.3-1.1l1.2-14.3l14.3-17.6l5.9-1.7l0.3,0.1l3.1,4.5l0.8,0.2l0.9,1.5l0.8,0.3l4.7,10.3l0.2,0.1l2-0.6l5.4,10.1l-0.3,10.5 l2.8,3.7l0,0l4.2,10.8l1.8,1.7l-1.1,2.4l-0.8-0.6l-1.9-4l-1.7-1.4l-0.3-0.9l-5.5-3.5l-2.4-0.3l-0.2,1.2l19.8,28.5l2.6-3.6l-5.7-11.2 l0.9-4l0.7-0.2l0.2-2.3l-9.3-18.6l-0.3-8.9l1.4-1.5l6.7,7.8l1.4,0.3l1.1-0.6l0.1,0.1l-0.2,3.4l0.6,0.5l0.5,0.2l7.4-7.9l-2-10.4 l-6.9-9.5l4.9-6l0.8,0.2l0.8,0.5l1.7,3.9l2.9-4.7l10.1-3.6l5.1-8.1l1.6-9.9l-2.5-2l1.1-1.7l-7.5-11.5l3.5-4.7l-6.1-0.9l-3.5-3.7 l4.1-4.3l0.8-0.1l1.4,0.9l0.6,2.9l2.8-1.3l3.9,1.4l0.9,3.2l2.3,0.5l5,9h0.4l2.3-2.4h0.3l1-1.5l-1.7-3.8l-5.8-5.9l2.1-4v-3.6l2.6-2.4 l0.5,0.1l0.2-0.1l-3.5-15.2l-0.2,0.1v-0.1l-9.3,1.2l-7.3-9.3L464,58.8l-0.8,1.9L441.2,60l-1.5-1.8l-0.2,0.1l0,0l-7.3,4.1l-7.5-3 l-0.5,0.3l-1.8-0.8l-0.9-1.2l-0.3,0.1l-0.1-0.1l-5.7-0.4l-0.3-0.2l0,0l0,0l-1,0.5l-1.5,4.5l-4.2,2.7l-16.8-4.4L377.5,50l0,0l-0.2,1 l1.8,6.7l-13.3,3l-9.2-3.8l-1.1,3.1l-6.7-1.6l-0.1,0.1h-0.2l-4.4,6.8l3.8,3.8l0.6,2.7l0,0l0,0L352,71l2.6,2.2V74l-2.3,1.9l-0.8,1.6 l1.6,3.9l0.9,0.3l1,1.1l2.6,0.9l1.7,1.7l-0.2,1.1l-1.5,2.8l2.1,3.7v4.5l-1.3,1.4l-3.8-0.9l-4.7-5.1v-0.6l-1.4-1.4l-3.9,2.1l-2.4-2.1 l-1.6,0.9l-0.3,5.1l-15.2,4.7l-1.7,9.8l-2.5,1.7L322.9,118.9 M531.1,99.3l-1,2l-4,1.7l-2.4,3l-3.3-2.5l-6.4,0.2l-0.2-0.7l8.9-4.2 l3.7-4.9l-0.6-3.3l-3.2-5.1l-0.7-0.4v-5.1l1.4-2.6l1.7,0.3l0.6,0.7h0.8l1.1,0.8l1.3,0.3l0.6,1.9l-1.7,2l-2.6-1.2L531.1,99.3  M500.5,130.3l1.9-0.9l-0.8,6.3l-1.6-0.3L500.5,130.3 M515.9,180.5l-1.7,0.4l-2.2-3.3l-3.6-2.2l4.3-2.5l0.9-3.1l-0.3-4.1l-4.6-2.1 l-2,0.5l-5.1,8.5l-2.4,0.3l-0.2-3.4l0.8-0.7l4.2-9.3l-1.8-3.7l1.4-9.3l2.4,1.8l1.6,3.6l-0.5,4.8l8,6.4l0.1-0.1l3.1,11.2L515.9,180.5 L515.9,180.5L515.9,180.5 M497.7,179.5l2.6,0.9l1.1,1.9l-1.8,5.1l0.8,7l-6,10.9l-9.2-1.7l-2.9-10.9L497.7,179.5L497.7,179.5  M509,194.8l-1.8,0.1L509,194.8 M515,193.9l-1.7,2.2l-2.4-0.2l-1.9-1.1l-3.3,1.3l-0.3,1.9l1.2,1.4l2.1-0.3l0.9-0.7l1.1,0.1l0.3,1.2 l-1.9,2.6l0.7,5.6l-2.3-2l-1-2l-1.5,1l0.9,5.2l-3.1-0.4l0.2-2.8l-1.4-2.5l2.9-10.5l3.2-1.6l3.8,1.2l3.4-1.1L515,193.9 M530.7,198.1 l2.5,0.5l0.4,0.4l2,5.3l2.1-2.2l4.2-1.7l14.5,11.5l2.4,0.5l4-2.6l-1.2,4.7l-3.5,1.4l-0.5,1.4l0.1,1.3l4.4,6.5l-4.4-1.5l-5.2-7.5 l-5.6,4.4l-5.6-2l-1.2-1.5l1.3-1.5l-1.9-2.4l-0.3-0.8l-8.5-5l-0.9-4.7l-3.4-3.1l2.4-1.4H530.7 M476.6,212.1l19.1,5l3.1-0.8l4.4,1.4 l3.3-0.9l12.4,2.1l-0.1,0.6l-8.2,4v-1.9l-35.4-5.6l-1.5-1.8l2.5-1.9H476.6 M569.4,280.1l-19,14.6l-0.7-1.1l2.2-4.6l5.1-3l7.4-9.7 l0.9-4.3l4.8,5.1L569.4,280.1 M554.3,267.3l-11.1,18.2l-5.7,3.1l-4.8,7.7l-2.5,0.5l-0.6-1.9l0.5-3.4l2.8-2.9l-6.6-0.8l-1.6-1.4 l-1.7-8.4l-0.9-0.9l-3.1,1.1l-5.2-3.9l-32.3,7.3l-2.3-1.9l2.3-4.5l0.6-21.9l1.8-2.5l13.9-6.4l4.3-4.8l0.3-0.9l10-9.2l4.2,1.9l5.5-7 l4.2-1.4l4.9,2l-1.1,5l2.8,4.8l4.5,2.8l3.2-4.5l2.5-11.7l4.6,10.8v7.6l7.7,18.5L554.3,267.3L554.3,267.3L554.3,267.3L554.3,267.3 L554.3,267.3L554.3,267.3"/></svg>';

        function worldMap(container) {
          container.selectAll('path').remove();
          container.html(world);
          container.selectAll('path').datum(function() {
            return {
              'geo.name': d3.select(this).attr('id'),
              'geo.region': d3.select(this).attr('id')
            };
          });
        }

        return worldMap;
      }();
    };

    /*!
     * VIZABI BUBBLE COLOR LEGEND COMPONENT
     */

    var ColorLegend = Component.extend({

      init: function(config, context) {
        var _this = this;
        this.template = '<div class="vzb-cl-outer"></div>';
        this.name = 'colorlegend';

        this.model_expects = [{
          name: "color",
          type: "color"
        }, {
          name: "entities",
          type: "entities"
        }, {
          name: "language",
          type: "language"
        }];

        this.needsUpdate = false;
        this.which_1 = false;
        this.scaleType_1 = false;

        this.model_binds = {
          "change:color": function(evt) {
            _this.updateView();
          },
          "change:language.strings": function(evt) {
            _this.updateView();
          },
          "ready": function(evt) {
            if(!_this._readyOnce) return;
            _this.updateView();
          }
        }

        //contructor is the same as any component
        this._super(config, context);
      },


      readyOnce: function() {
        var _this = this;
        this.element = d3.select(this.element);
        this.listColorsEl = this.element.append("div").attr("class", "vzb-cl-holder").append("div").attr("class",
          "vzb-cl-colorlist");
        this.rainbowEl = this.listColorsEl.append("div").attr("class", "vzb-cl-rainbow");
        this.worldmapEl = this.listColorsEl.append("div").attr("class", "vzb-cl-worldmap");

        this.colorPicker = colorPicker();
        //this.element.call(this.colorPicker);
        d3.select(this.root.element).call(this.colorPicker);
        this.worldMap = worldMap();
        this.worldmapEl.call(this.worldMap);

        this.updateView();
      },


      updateView: function() {
        var _this = this;
        this.translator = this.model.language.getTFunction();
        var KEY = this.model.entities.getDimension();


        var palette = this.model.color.getPalette();
        var paletteDefault = this.model.color.getDefaultPalette();

        this.listColorsEl.selectAll(".vzb-cl-option").remove();

        var colors = this.listColorsEl
          .selectAll(".vzb-cl-option")
          .data(keys(palette), function(d) {
            return d
          });

        colors.enter().append("div").attr("class", "vzb-cl-option")
          .each(function() {
            d3.select(this).append("div").attr("class", "vzb-cl-color-sample");
            d3.select(this).append("div").attr("class", "vzb-cl-color-legend");
          })
          .on("mouseover", function() {
            //disable interaction if so stated in metadata
            if(!_this.model.color.isUserSelectable()) return;

            var sample = d3.select(this).select(".vzb-cl-color-sample");
            sample.style("border-width", "5px");
            sample.style("background-color", "transparent");

          })
          .on("mouseout", function(d) {
            //disable interaction if so stated in metadata
            if(!_this.model.color.isUserSelectable()) return;

            var sample = d3.select(this).select(".vzb-cl-color-sample");
            sample.style("border-width", "0px");
            sample.style("background-color", _this.model.color.palette[d]);
          })
          .on("click", function(d) {
            //disable interaction if so stated in metadata
            if(!_this.model.color.isUserSelectable()) return;
            _this.colorPicker
              .colorOld(palette[d])
              .colorDef(paletteDefault[d])
              .callback(function(value) {

                _this.model.color.setColor(value, d)
              })
              .fitToScreen([d3.event.pageX, d3.event.pageY])
              .show(true);
          });

        if(this.model.color.use == "indicator") {
          var gradientHeight;
          var colorOptions = this.listColorsEl.selectAll('.vzb-cl-option');
          if(colorOptions && colorOptions[0]) {
            var firstOptionSize = colorOptions[0][0].getBoundingClientRect();
            var lastOptionSize = colorOptions[0][colorOptions[0].length - 1].getBoundingClientRect();
            gradientHeight = lastOptionSize.bottom - firstOptionSize.top;
          }
          if(!isFinite(gradientHeight))
            gradientHeight = keys(palette).length * 25 + 5;
          this.rainbowEl.classed("vzb-hidden", false)
            .style("height", gradientHeight + 2 + "px")
            .style("background", "linear-gradient(" + values(palette).join(", ") + ")");
        } else {
          this.rainbowEl.classed("vzb-hidden", true);
        }

        //TODO: is it okay that "geo.region" is hardcoded?
        if(this.model.color.which == "geo.region") {
          var regions = this.worldmapEl.classed("vzb-hidden", false)
            .select("svg").selectAll("path");
          regions.each(function() {
              var view = d3.select(this);
              var color = palette[view.attr("id")];
              view.style("fill", isArray(color)? color[0] : color);
            })
            .style("opacity", .8)
            .on("mouseover", function() {
              var view = d3.select(this);
              var region = view.attr("id");
              regions.style("opacity", .5);
              view.style("opacity", 1);

              var filtered = _this.model.color.getNestedItems([KEY]);
              var highlight = values(filtered)
                //returns a function over time. pick the last time-value
                .map(function(d) {
                  return d[d.length - 1]
                })
                //filter so that only countries of the correct region remain
                .filter(function(f) {
                  return f["geo.region"] == region
                })
                //fish out the "key" field, leave the rest behind
                .map(function(d) {
                  return clone(d, [KEY])
                });

              _this.model.entities.setHighlight(highlight);
            })
            .on("mouseout", function() {
              regions.style("opacity", .8);
              _this.model.entities.clearHighlighted();
            })
            .on("click", function(d) {
              //disable interaction if so stated in metadata
              if(!_this.model.color.isUserSelectable()) return;
              var view = d3.select(this);
              var region = view.attr("id")

              _this.colorPicker
                .colorOld(palette[region])
                .colorDef(paletteDefault[region])
                .callback(function(value) {
                  _this.model.color.setColor(value, region)
                })
                .show(true);
            })
          colors.classed("vzb-hidden", true);
        } else {
          this.worldmapEl.classed("vzb-hidden", true);
          //if using a discrete palette that is not supplied from metadata but from defaults
          if(this.model.color.use === "property" && (!this.model.color.getMetadata().color || this.model.color.getMetadata().color.palette)) {
            colors.classed("vzb-cl-compact", true);
          } else {
            colors.classed("vzb-cl-compact", false);
          }
        }

        colors.each(function(d, index) {
          d3.select(this).select(".vzb-cl-color-sample")
            .style("background-color", palette[d])
            .style("border", "1px solid " + palette[d]);

          if(_this.model.color.use == "indicator") {
            var domain = _this.model.color.getScale().domain();
            d3.select(this).select(".vzb-cl-color-legend")
              .text(_this.model.color.tickFormatter(domain[index]))
          } else {

            d3.select(this).select(".vzb-cl-color-legend")
              .text(_this.translator("color/" + d));
          }
        });
      },

      resize: function() {
        this.colorPicker.resize(d3.select('.vzb-colorpicker-svg'));
      }

    });

    /*!
     * VIZABI BUBBLE SIZE slider
     * Reusable bubble size slider
     */

    var OPTIONS = {
      DOMAIN_MIN: 0,
      DOMAIN_MAX: 1,
      TEXT_PARAMS: { TOP: 18, LEFT: 10, MAX_WIDTH: 42, MAX_HEIGHT: 16 },
      BAR_WIDTH: 6,
      THUMB_RADIUS: 10,
      THUMB_STROKE_WIDTH: 4,
      INTRO_DURATION: 250
    }

    var profiles = {
        "small": {
          minRadius: 0.5,
          maxRadius: 40
        },
        "medium": {
          minRadius: 1,
          maxRadius: 55
        },
        "large": {
          minRadius: 1,
          maxRadius: 70
        }
    };


    var BubbleSize = Component.extend({

      /**
       * Initializes the timeslider.
       * Executed once before any template is rendered.
       * @param config The options passed to the component
       * @param context The component's parent
       */
      init: function (config, context) {

        this.name = 'bubblesize';

        this.template = this.template || "bubblesize.html";

        this.model_expects = [{
          name: "size",
          type: "size"
        }];

        var _this = this;
        this.model_binds = {
          'change:size.domainMin': changeMinMaxHandler,
          'change:size.domainMax': changeMinMaxHandler,
          'ready': readyHandler
        };

        function changeMinMaxHandler(evt, path) {
          var size = [
              _this.model.size.domainMin,
              _this.model.size.domainMax
          ];
          _this._updateArcs(size);
          _this._updateLabels(size);
          _this.sliderEl.call(_this.brush.extent(size));
          if(size[0] == size[1]){
            _this.sliderEl.selectAll(".resize")
              .style("display", "block");
          }
        }
        function readyHandler(evt) {
            _this.sizeScaleMinMax = _this.model.size.getScale().domain();
            _this._setLabelsText();
        }

        this._setModel = throttle(this._setModel, 50);
        //contructor is the same as any component
        this._super(config, context);
      },

      /**
       * Executes after the template is loaded and rendered.
       * Ideally, it contains HTML instantiations related to template
       * At this point, this.element and this.placeholder are available as a d3 object
       */
      readyOnce: function () {
        var values = [this.model.size.domainMin, this.model.size.domainMax],
          _this = this;
        this.element = d3.select(this.element);
        this.sliderSvg = this.element.select(".vzb-bs-svg");
        this.sliderWrap = this.sliderSvg.select(".vzb-bs-slider-wrap");
        this.sliderEl = this.sliderWrap.select(".vzb-bs-slider");

        var
          textMargin = { v: OPTIONS.TEXT_PARAMS.TOP, h: OPTIONS.TEXT_PARAMS.LEFT },
          textMaxWidth = OPTIONS.TEXT_PARAMS.MAX_WIDTH,
          textMaxHeight = OPTIONS.TEXT_PARAMS.MAX_HEIGHT,
          barWidth = OPTIONS.BAR_WIDTH,
          thumbRadius = OPTIONS.THUMB_RADIUS,
          thumbStrokeWidth = OPTIONS.THUMB_STROKE_WIDTH,
          padding = {
            top: thumbStrokeWidth,
            left: textMargin.h + textMaxWidth,
            right: textMargin.h + textMaxWidth,
            bottom: barWidth + textMaxHeight
          }

        this.padding = padding;

        var minMaxBubbleRadius = this.getMinMaxBubbleRadius();

        this.xScale = d3.scale.linear()
          .domain([OPTIONS.DOMAIN_MIN, OPTIONS.DOMAIN_MAX])
          .range([minMaxBubbleRadius.min * 2, minMaxBubbleRadius.max * 2])
          .clamp(true)

        this.brush = d3.svg.brush()
          .x(this.xScale)
          .extent([OPTIONS.DOMAIN_MIN, OPTIONS.DOMAIN_MIN])
          .on("brush", function () {
            _this._setFromExtent(false, false); // non persistent change
          })
          .on("brushend", function () {
             _this.sliderEl.selectAll(".resize")
              .style("display", null);

            _this._setFromExtent(true); // force a persistent change
          });

        this.sliderEl
          .call(_this.brush);

        //For return to round thumbs
        //var thumbArc = d3.svg.arc()
        //  .outerRadius(thumbRadius)
        //  .startAngle(0)
        //  .endAngle(2 * Math.PI)

        this.sliderThumbs = this.sliderEl.selectAll(".resize").sort(d3.descending)
          .classed("vzb-bs-slider-thumb", true)

        this.sliderThumbs.append("g")
          .attr("class", "vzb-bs-slider-thumb-badge")
          .append("path")
          .attr("d", "M0 " + (barWidth * .5) + "l" + (-thumbRadius) + " " + (thumbRadius * 1.5) + "h" + (thumbRadius * 2) + "Z")

          //For return to circles
          //.attr("d", "M0 0 l" + (thumbRadius * 2) + " " + (-thumbRadius) + "v" + (thumbRadius * 2) + "Z")

          //For return to round thumbs
          //.attr("d", thumbArc)

        this.sliderThumbs.append("path")
          .attr("class", "vzb-bs-slider-thumb-arc")
        this.sliderEl.selectAll("text").data([0,0]).enter()
          .append("text")
          .attr("class", "vzb-bs-slider-thumb-label")
          .attr("dy", "0.35em")
          .attr("text-anchor", function(d, i) {
            return i ? "start" : "end"})
          .attr("dominant-baseline", function(d, i) {
            return i ? "text-after-edge" : "text-before-edge"})

        this.sliderLabelsEl = this.sliderEl.selectAll("text.vzb-bs-slider-thumb-label");

        this.sliderEl.selectAll("rect")
          .attr("height", barWidth)
          .attr("rx", barWidth * 0.25)
          .attr("ry", barWidth * 0.25)
          .attr("transform", "translate(0," + (-barWidth * 0.5) + ")")
        this.sliderEl.select(".extent")
          .classed("vzb-bs-slider-extent", true)

        //For return to circles
        // var circleLabelTransform = function(d, i) {
        //    var dX = i ? textMargin.h + _this.xScale(d) : -textMargin.h,
        //        dY = -textMargin.v;
        //    return "translate(" + (dX) + "," + (dY) + ")";
        // }

        this.on("resize", function() {
          //console.log("EVENT: resize");

          _this.xScale.range([_this.getMinMaxBubbleRadius().min * 2, _this.getMinMaxBubbleRadius().max * 2]);
          _this._updateSize();

          _this.sliderEl
            .call(_this.brush.extent(_this.brush.extent()))
            .call(_this.brush.event);

        });

        this._updateSize();
        this.sliderEl
          .call(this.brush.extent(values))
          .call(this.brush.event);

        _this.sizeScaleMinMax = _this.model.size.getScale().domain();

        if(_this.sizeScaleMinMax) {
          _this._setLabelsText();
        }
      },

      getMinMaxBubbleRadius: function() {
        return { min: profiles[this.getLayoutProfile()].minRadius, max: profiles[this.getLayoutProfile()].maxRadius};
      },

      /*
       * RESIZE:
       * Executed whenever the container is resized
       */
      _updateSize: function() {
        this.sliderSvg
          .attr("height", this.getMinMaxBubbleRadius().max + this.padding.top + this.padding.bottom)
          .attr("width", this.getMinMaxBubbleRadius().max * 2 + this.padding.left + this.padding.right)
        this.sliderWrap
          .attr("transform", "translate(" + this.padding.left + "," + (this.getMinMaxBubbleRadius().max + this.padding.top) + ")")
      },

      _updateArcs: function(s) {
        var _this = this;
        var valueArc = d3.svg.arc()
          .outerRadius(function (d) { return _this.xScale(d) * 0.5 })
          .innerRadius(function (d) { return _this.xScale(d) * 0.5 })
          .startAngle(-Math.PI * 0.5)
          .endAngle(Math.PI * 0.5);

        this.sliderThumbs.select('.vzb-bs-slider-thumb-arc').data(s)
          .attr("d", valueArc)
          .attr("transform", function (d) {return "translate(" + (-_this.xScale(d) * 0.5) + ",0)"; })
      },

      _updateLabels: function(s) {
        var _this = this;
        var arcLabelTransform = function(d, i) {
          var textMargin = { v: OPTIONS.TEXT_PARAMS.TOP, h: OPTIONS.TEXT_PARAMS.LEFT },
              dX = textMargin.h * (i ? .5 : -1.0) + _this.xScale(d),
              dY = i ? -textMargin.v : 0;
           return "translate(" + (dX) + "," + (dY) + ")";
        }
        this.sliderLabelsEl.data(s)
          .attr("transform", arcLabelTransform);
      },

      _setLabelsText: function() {
          var _this = this;
          _this.sliderLabelsEl
            .data([_this.model.size.tickFormatter(_this.sizeScaleMinMax[0]),_this.model.size.tickFormatter(_this.sizeScaleMinMax[1])])
            .text(function (d) { return d; });
      },

      /**
       * Prepares setting of the current model with the values from extent.
       * @param {boolean} force force firing the change event
       * @param {boolean} persistent sets the persistency of the change event
       */
      _setFromExtent: function(force, persistent) {
        var s = this.brush.extent();
        this._updateArcs(s);
        this._updateLabels(s);
        this._setModel(s, force, persistent);
      },

      /**
       * Sets the current value in model. avoid updating more than once in framerate
       * @param {number} value
       * @param {boolean} force force firing the change event
       * @param {boolean} persistent sets the persistency of the change event
       */
      _setModel: function (value, force, persistent) {
        var _this = this;
        _this.model.size.getModelObject('domainMin').set(value[0], force, persistent);
        _this.model.size.getModelObject('domainMax').set(value[1], force, persistent);
      }

    });

    var _index$1 = {
    bubblesize : BubbleSize,
    buttonlist : ButtonList,
    colorlegend : ColorLegend,
    datawarning : DataWarning,
    _dialog : Dialog,
    dialogs : Dialogs,
    draggablelist : DraggableList,
    indicatorpicker : IndPicker,
    minmaxinputs : MinMaxInputs,
    simplecheckbox : simplecheckbox,
    simpleslider : SimpleSlider,
    timeslider : TimeSlider,
    treemenu : TreeMenu,
    };

    var components$1 = {
    	bubblesize: BubbleSize,
    	buttonlist: ButtonList,
    	colorlegend: ColorLegend,
    	datawarning: DataWarning,
    	_dialog: Dialog,
    	dialogs: Dialogs,
    	draggablelist: DraggableList,
    	indicatorpicker: IndPicker,
    	minmaxinputs: MinMaxInputs,
    	simplecheckbox: simplecheckbox,
    	simpleslider: SimpleSlider,
    	timeslider: TimeSlider,
    	treemenu: TreeMenu,
    	default: _index$1
    };

    //BAR CHART TOOL
    var BarChart = Tool.extend('BarChart', {

      /**
       * Initializes the tool (Bar Chart Tool).
       * Executed once before any template is rendered.
       * @param {Object} placeholder Placeholder element for the tool
       * @param {Object} external_model Model as given by the external page
       */
      init: function(placeholder, external_model) {

        this.name = "barchart";

        //specifying components
        this.components = [{
          component: BarComponent,
          placeholder: '.vzb-tool-viz',
          model: ["state.time", "state.entities", "state.marker", "language"] //pass models to component
        }, {
          component: TimeSlider,
          placeholder: '.vzb-tool-timeslider',
          model: ["state.time"]
        }, {
          component: Dialogs,
          placeholder: '.vzb-tool-dialogs',
          model: ['state', 'ui', 'language']
        }, {
          component: ButtonList,
          placeholder: '.vzb-tool-buttonlist',
          model: ['state', 'ui', 'language']
        }, {
          component: TreeMenu,
          placeholder: '.vzb-tool-treemenu',
          model: ['state.marker', 'language']
        }];

        //constructor is the same as any tool
        this._super(placeholder, external_model);
      },

      default_model: {
        state: {
          time: {},
          entities: {
            dim: "geo",
            show: {
              _defs_: {
                "geo": ["*"],
                "geo.cat": ["region"]
              }
            }
          },
          marker: {
            space: ["entities", "time"],
            label: {
              use: "property",
              which: "geo.name"
            },
            axis_y: {
              use: "indicator",
              which: "lex"
            },
            axis_x: {
              use: "property",
              which: "geo.name"
            },
            color: {
              use: "property",
              which: "geo.region"
            }
          }
        },
        data: {
          reader: "csv",
          path: "data/waffles/basic-indicators.csv"
        }
      }
    });

    var DynamicBackground = Class.extend({

      init: function(context, conditions) {
        this.context = context;
        this.width = 0;
        this.height = 0;
        this.topOffset = 0;
        this.leftOffset = 0;
        this.bottomOffset = 0;
        this.rightOffset = 0;
        this.fontSize = 0;
        this.fontWidth = 0;
        this.fontHeight = 0;
        this.xAlign = 'center';
        this.yAlign = 'center';
        this.symbols = [];
        if (conditions) {
          this.setConditions(conditions);
        }
      },

      setConditions: function(conditions) {
        if (conditions.rightOffset && !isNaN(parseFloat(conditions.rightOffset)) && isFinite(conditions.rightOffset)) {
          this.rifgtOffset = conditions.rightOffset;
        }
        if (conditions.leftOffset && !isNaN(parseFloat(conditions.leftOffset)) && isFinite(conditions.leftOffset)) {
          this.leftOffset = conditions.leftOffset;
        }
        if (conditions.topOffset && !isNaN(parseFloat(conditions.topOffset)) && isFinite(conditions.topOffset)) {
          this.topOffset = conditions.topOffset;
        }
        if (conditions.bottomOffset && !isNaN(parseFloat(conditions.bottomOffset)) && isFinite(conditions.bottomOffset)) {
          this.bottomOffset = conditions.bottomOffset;
        }
        if (conditions.xAlign) {
          this.xAlign = conditions.xAlign;
        }
        if (conditions.yAlign) {
          this.yAlign = conditions.yAlign;
        }
        return this;
      },

      resize: function(width, height, fontSize, topOffset, leftOffset) {
        this.width = width;
        this.height = height;
        this.fontSize = fontSize;
        if (topOffset) {
          this.topOffset = topOffset;
        }
        if (leftOffset) {
          this.leftOffset = leftOffset;
        }

        var sample = this.context.append("text").text("0").style("font-size", this.fontSize + "px");
        this.fontWidth = sample[0][0].getBBox().width;
        this.fontHeight = this.fontSize*0.72;

        d3.select(sample[0][0]).remove();
        this.__resizeText();
      },

      setText: function(text, resize) {
        var _this = this;
        var newSymbols = text.split('');
        if (newSymbols.length != this.symbols.length) {
          resize = true;
        }
        this.symbols = text.split('');

        this.context.selectAll("text")
          .data(this.symbols).exit().remove();
        this.context.selectAll("text")
          .data(this.symbols)
          .enter()
          .append("text")
          .text(function(d){return d;});

        this.context.selectAll("text").each(function (d, i) {
            d3.select(this).text(d);
        });
        if (resize) {
          return this.__resizeText();
        } else {
          return this;
        }

      },

      __resizeText: function() {
        var _this = this;
        this.context.attr("transform", "translate(" + this.__getLeftOffset() + "," + this.__getTopOffset() + ")");
        this.context.selectAll("text").each(function(d, i) {
            d3.select(this)
              .attr("x", _this.fontWidth * i)
              .style("font-size", _this.fontSize + 'px')
              .style("text-anchor", "middle");
          });
        return this;
      },
      __getLeftOffset: function() {
        switch (this.xAlign) {
          case 'right':
            return this.width - this.fontWidth * this.symbols.length + this.fontWidth/2;
            break;
          case 'left':
            return this.fontWidth/2;
            break;
          default :
            return this.fontWidth/2 + (this.width - this.fontWidth * this.symbols.length)/2;
        }
      },
      __getTopOffset: function() {
        //console.log(this.topOffset);
        switch (this.yAlign) {
          case 'top':
            return this.fontHeight + this.topOffset;
            break;
          case 'bottom':
            return this.height - this.bottomOffset;
            break;
          default :
            return this.fontHeight + (this.height - this.fontHeight)/2 + this.topOffset;
        }
      }

    });

    /*!
     * VIZABI POP BY AGE Component
     */


    //POP BY AGE CHART COMPONENT
    var BarRankChart$1 = Component.extend({

      /**
       * Initializes the component (Bar Chart).
       * Executed once before any template is rendered.
       * @param {Object} config The config passed to the component
       * @param {Object} context The component's parent
       */
      init: function(config, context) {

        this.name = 'barrankchart-component';
        this.template = 'barrank.html';

        //define expected models for this component
        this.model_expects = [{
          name: "time",
          type: "time"
        }, {
          name: "entities",
          type: "entities"
        }, {
          name: "marker",
          type: "model"
        }, {
          name: "language",
          type: "language"
        }, {
          name: "ui",
          type: "model"
        }];

        var _this = this;
        this.model_binds = {
          "change:time.value": function(evt) {
            _this.onTimeChange();
          },
          "change:entities.select": function(evt) {
            _this.selectBars();
          },
          "change:marker.axis_x.scaleType": function(evt) {
            _this.draw();
          },
          'change:marker.color.palette': function() {
            //console.log("EVENT change:marker:color:palette");
            //_this.drawColors();
          },
        };

        //contructor is the same as any component
        this._super(config, context);

        // set up the scales
        this.xScale = null;
        this.cScale = d3.scale.category10();

        // set up the axes
        this.xAxis = axisSmart();
      },

      onTimeChange: function() {
        //this.year.setText(this.model.time.timeFormat(this.model.time.value));

        this.loadData();
        this.draw();
      },

      /**
       * DOM and model are ready
       */
      readyOnce: function() {
        this.element = d3.select(this.element);

        // reference elements
        //this.graph = this.element.select('.vzb-br-graph');
        //this.yearEl = this.element.select('.vzb-br-year');
        //this.year = new DynamicBackground(this.yearEl);
        this.header = this.element.select('.vzb-br-header');
        this.barViewport = this.element.select('.barsviewport');
        this.barSvg = this.element.select('.vzb-br-bars-svg');
        this.barContainer = this.element.select('.vzb-br-bars');

        // set up formatters
        this.xAxis.tickFormat(this.model.marker.axis_x.tickFormatter);

        this.ready();

        this.selectBars();

      },

      readyRuns: 0,

      /*
       * Both model and DOM are ready
       */
      ready: function() {

        // hack: second run is right after readyOnce (in which ready() is also called)
        // then it's not necessary to run ready()
        // (without hack it's impossible to run things in readyOnce áfter ready has ran)
        if (++this.readyRuns == 2) return;

        this.loadData();
        this.draw();
      },

      resize: function() {
        this.draw();
      },

      loadData: function() {

        // get data, for the active year. Nest them using the entity of the graph
        this.values = this.model.marker.getFrame(this.model.time.value);

        // sort the data (also sets this.total)
        this.sortedEntities = this.sortByIndicator(this.values.axis_x);

        // change header titles for new data
        var translator = this.model.language.getTFunction();
        this.header.select('.vzb-br-title')
          .text(translator("indicator/" + this.model.marker.axis_x.which) + ' ' + this.model.time.timeFormat(this.model.time.value))
        this.header.select('.vzb-br-total')
          .text('Σ = ' + this.model.marker.axis_x.tickFormatter(this.total))

        // new scales and axes
        this.xScale = this.model.marker.axis_x.getScale(false);
        this.cScale = this.model.marker.color.getScale();

      },

      draw: function() {
        this.drawAxes();
        this.drawData();
      },

      /*
      * draw the chart/stage
      */
      drawAxes: function() {

        // these should go in some style-config
        this.barHeight = 20; 
        var margin = {top: 60, bottom: 40, left: 90, right: 20}; // need right margin for scroll bar

        // draw the stage - copied from popbyage, should figure out what it exactly does and what is necessary.
        this.height = parseInt(this.element.style("height"), 10) - margin.top - margin.bottom;
        this.width = parseInt(this.element.style("width"), 10) - margin.left - margin.right;

        this.barContainer.attr('transform', 'translate(' + margin.left + ', 0)');
        this.barViewport.style('height', this.height + 'px');

        // header
        this.header
          .attr('height', margin.top)
          .select('.vzb-br-title')
            .attr('dominant-baseline', 'middle')
            .attr('y', margin.top/2)
            .attr('x', margin.left);
        this.header
          .select('.vzb-br-total')
            .attr('text-anchor', 'end')
            .attr('dominant-baseline', 'middle')
            .attr('y', margin.top/2)
            .attr('x', this.width + margin.left);


        // although axes are not drawn, need the xScale for bar width
        if(this.model.marker.axis_x.scaleType !== "ordinal") {
          this.xScale.range([0, this.width]);
        } else {
          this.xScale.rangePoints([0, this.width]).range();
        }

        // redraw the limits
        var limits = this.model.marker.axis_x.getLimits(this.model.marker.axis_x.which);
        this.xScale = this.xScale.domain([limits.min, limits.max]);

      },

      drawData: function() {

        var _this = this;
        var bar_margin = 2; // should go in some config
        var duration = (this.model.time.playing) ? this.model.time.delayAnimations : 0;

        // apply the current data to the bars (including ordering)
        var updatedBars = this.barContainer
          .selectAll('.vzb-br-bar')
          .data(this.sortedEntities, getDataKey)
          .order();

        // update the shown bars for new data-set
        this.createAndDeleteBars(updatedBars);

       
        this.barContainer
          .selectAll('.vzb-br-bar') 
          .data(this.sortedEntities, getDataKey)
          .order()
          .each(function (d, i) {

            var bar = d3.select(this);
            var barWidth = _this.xScale(d.value);
            var xValue = _this.model.marker.axis_x.tickFormatter(d.value);
            
            // save the current index in the bar datum
            d.index = i;

            // set width of the bars
            bar.selectAll('rect')
              .transition().duration(duration).ease("linear")
              .attr("width", (barWidth > 0) ? barWidth : 0)

            // set positions of the bar-values
            bar.selectAll('.vzb-br-value')
              .text(xValue)

            // set title (tooltip)
            bar.selectAll('title')
              .text(_this.values.label[d.entity] + ' (' + xValue + ')');

          })
          .transition().duration(duration).ease("linear")
          .attr("transform", function(d, i) {
            return 'translate(0, '+ getBarPosition(d,i) + ')'
          })
          .call(endAll, function() {
            // when all the transitions have ended

            // set the height of the svg so it resizes according to its children
            var height = _this.barContainer.node().getBoundingClientRect().height
            _this.barSvg.attr('height', height + "px");

            // move along with a selection if playing
            if (_this.model.time.playing) {
              var follow = _this.barContainer.select('.vzb-selected');
              if (!follow.empty()) {
                var d = follow.datum();
                var yPos = getBarPosition(d, d.index);

                var currentTop = _this.barViewport.node().scrollTop;
                var currentBottom = currentTop + _this.height;

                var scrollTo = false;
                if (yPos < currentTop)
                  scrollTo = yPos;
                if ((yPos + _this.barHeight) > currentBottom)
                  scrollTo = yPos + _this.barHeight - _this.height;

                if (scrollTo)
                  _this.barViewport.transition().duration(duration)
                    .tween('scrollfor' + d.entity, scrollTopTween(scrollTo));

              }

            }

            function scrollTopTween(scrollTop) {
              return function() {
                var i = d3.interpolateNumber(this.scrollTop, scrollTop);
                return function(t) { this.scrollTop = i(t); };
              };
            }

          });


        // helper functions
        function getBarPosition(d, i) {
            return (_this.barHeight+bar_margin)*i;
        }
        function getDataKey(d) {          
          return d.entity;  
        } 
        // http://stackoverflow.com/questions/10692100/invoke-a-callback-at-the-end-of-a-transition
        function endAll(transition, callback) { 
          if (transition.size() === 0) { callback() }
          var n = 0; 
          transition 
              .each(function() { ++n; }) 
              .each("end", function() { if (!--n) callback.apply(this, arguments); }); 
        } 

      },

      createAndDeleteBars: function(updatedBars) {

        var _this = this;

        // remove groups for entities that are gone
        updatedBars.exit().remove();

        // make the groups for the entities which were not drawn yet (.data.enter() does this)
        var newGroups = updatedBars.enter().append("g")
            .attr("class", 'vzb-br-bar')
            .attr("id", function(d) {
              return "vzb-br-bar-" + d.entity;
            })
            .on("mousemove", function(bar) { _this.setHover(bar, true)  })
            .on("mouseout",  function(bar) { _this.setHover(bar, false) })
            .on("click", function(d) {

              forEach(_this.model.marker.space, function(entity) {
                if (_this.model[entity].getDimension() !== 'time')
                  _this.model[entity].selectEntity(d); // this will trigger a change in the model, which the tool listens to
              });

            });

        // draw new bars per group
        newGroups.append('rect')
            .attr("x", 0)
            .attr("rx", this.barHeight/4)
            .attr("ry", this.barHeight/4)
            .attr("stroke", "white")
            .attr("stroke-opacity", 0)
            .attr("stroke-width", 2)
            .attr("height", this.barHeight)
            .style("fill", function(d) {
              var color = _this.cScale(_this.values.color[d.entity]);
              return d3.rgb(color);
            });

        // draw new labels per group
        newGroups.append('text')
            .attr("class", "vzb-br-label") 
            .attr("x", -5)
            .attr("y", this.barHeight/2)
            .attr("text-anchor", "end")
            .attr("dominant-baseline", "middle")
            .text(function(d, i) {
              var label = _this.values.label[d.entity];
              return label.length < 12 ? label : label.substring(0, 9) + '...';
            })
            .style("fill", function(d) {
              var color = _this.cScale(_this.values.color[d.entity]);
              return d3.rgb(color).darker(2);
            })
            .append('title'); // watch out: might be overwritten if changing the labeltext later on

        // draw new values on each bar
        newGroups.append('text')
            .attr("class", "vzb-br-value") 
            .attr("x", 5)
            .attr("y", this.barHeight/2)
            .attr("dominant-baseline", "middle")
            .style("fill", function(d) {
              var color = _this.cScale(_this.values.color[d.entity]);
              return d3.rgb(color).darker(2);
            });
      },

      drawColors: function() {
        var _this = this;

        this.barContainer.selectAll('.vzb-br-bar>rect')
          .style("fill", getColor);
        this.barContainer.selectAll('.vzb-br-bar>text')
          .style("fill", getDarkerColor);

        function getColor(d) {
          var color = _this.cScale(_this.values.color[d.entity]);
          return d3.rgb(color);
        }
        function getDarkerColor(d) {
          return getColor(d).darker(2);
        }
      },


      /**
      * DATA HELPER FUNCTIONS
      */  

      sortByIndicator: function(values) {

        var _this = this;
        var data_array = [];
        this.total = 0; // setting this.total for efficiency at the same time

        // first put the data in an array (objects aren't sortable)
        forEach(values, function(indicator_value, entity) {
          var row = { entity: entity, value: indicator_value };
          row[_this.model.entities.dim] = entity;
          data_array.push(row);

          // setting this.total for efficiency at the same time
          _this.total += indicator_value; 
        });
        data_array.sort(function(a, b) {
          // if a is bigger, a comes first, i.e. descending sort
          return b.value - a.value;
        });  
        return data_array;  
      },

      /**
      * UI METHODS
      */

      /**
       * setting hover
       */
      setHover: function(bar, hover) {
        this.barContainer.classed('vzb-dimmed', hover);
        this.barContainer.select("#vzb-br-bar-" + bar.entity).classed('vzb-hovered', hover);
      },

      /**
       * Select Entities
       */
      selectBars: function() {
        var _this = this;
        var entityDim = this.model.entities.dim;
        var selected = this.model.entities.select;

        // unselect all bars
        this.barContainer.classed('vzb-dimmed-selected', false);
        this.barContainer.selectAll('.vzb-br-bar.vzb-selected').classed('vzb-selected', false);

        // select the selected ones
        if(selected.length) {
          this.barContainer.classed('vzb-dimmed-selected', true);
          forEach(selected, function(selectedBar) {
            _this.barContainer.select("#vzb-br-bar-" + selectedBar[entityDim]).classed('vzb-selected', true);
          });
        }

      },

    });

    var BarRankChart = Tool.extend('BarRankChart', {

      //Run when the tool is created
      init: function(placeholder, external_model) {

        this.name = "barrankchart";

        this.components = [{
          component: BarRankChart$1, 
          placeholder: '.vzb-tool-viz', 
          model: ["state.time", "state.entities", "state.marker", "language", "ui"] 
        }, {
          component: TimeSlider,
          placeholder: '.vzb-tool-timeslider',
          model: ["state.time"]
        }, {
          component: Dialogs,
          placeholder: '.vzb-tool-dialogs',
          model: ['state', 'ui', 'language']
        }, {
          component: ButtonList,
          placeholder: '.vzb-tool-buttonlist',
          model: ['state', 'ui', 'language']
        }, {
          component: TreeMenu,
          placeholder: '.vzb-tool-treemenu',
          model: ['state.marker', 'language']
        }];

        //constructor is the same as any tool
        this._super(placeholder, external_model);
      }

    });

    // topojson
    var topojson = (function() {
      var topojson = {
        version: "1.6.19",
        mesh: function(topology) {
          return object(topology, meshArcs.apply(this, arguments));
        },
        meshArcs: meshArcs,
        merge: function(topology) {
          return object(topology, mergeArcs.apply(this, arguments));
        },
        mergeArcs: mergeArcs,
        feature: featureOrCollection,
        neighbors: neighbors,
        presimplify: presimplify
      };

      function stitchArcs(topology, arcs) {
        var stitchedArcs = {},
          fragmentByStart = {},
          fragmentByEnd = {},
          fragments = [],
          emptyIndex = -1;

        // Stitch empty arcs first, since they may be subsumed by other arcs.
        arcs.forEach(function(i, j) {
          var arc = topology.arcs[i < 0 ? ~i : i],
            t;
          if(arc.length < 3 && !arc[1][0] && !arc[1][1]) {
            t = arcs[++emptyIndex], arcs[emptyIndex] = i, arcs[j] = t;
          }
        });

        arcs.forEach(function(i) {
          var e = ends(i),
            start = e[0],
            end = e[1],
            f, g;

          if(f = fragmentByEnd[start]) {
            delete fragmentByEnd[f.end];
            f.push(i);
            f.end = end;
            if(g = fragmentByStart[end]) {
              delete fragmentByStart[g.start];
              var fg = g === f ? f : f.concat(g);
              fragmentByStart[fg.start = f.start] = fragmentByEnd[fg.end = g.end] = fg;
            } else {
              fragmentByStart[f.start] = fragmentByEnd[f.end] = f;
            }
          } else if(f = fragmentByStart[end]) {
            delete fragmentByStart[f.start];
            f.unshift(i);
            f.start = start;
            if(g = fragmentByEnd[start]) {
              delete fragmentByEnd[g.end];
              var gf = g === f ? f : g.concat(f);
              fragmentByStart[gf.start = g.start] = fragmentByEnd[gf.end = f.end] = gf;
            } else {
              fragmentByStart[f.start] = fragmentByEnd[f.end] = f;
            }
          } else {
            f = [i];
            fragmentByStart[f.start = start] = fragmentByEnd[f.end = end] = f;
          }
        });

        function ends(i) {
          var arc = topology.arcs[i < 0 ? ~i : i],
            p0 = arc[0],
            p1;
          if(topology.transform) p1 = [0, 0], arc.forEach(function(dp) {
            p1[0] += dp[0], p1[1] += dp[1];
          });
          else p1 = arc[arc.length - 1];
          return i < 0 ? [p1, p0] : [p0, p1];
        }

        function flush(fragmentByEnd, fragmentByStart) {
          for(var k in fragmentByEnd) {
            var f = fragmentByEnd[k];
            delete fragmentByStart[f.start];
            delete f.start;
            delete f.end;
            f.forEach(function(i) {
              stitchedArcs[i < 0 ? ~i : i] = 1;
            });
            fragments.push(f);
          }
        }

        flush(fragmentByEnd, fragmentByStart);
        flush(fragmentByStart, fragmentByEnd);
        arcs.forEach(function(i) {
          if(!stitchedArcs[i < 0 ? ~i : i]) fragments.push([i]);
        });

        return fragments;
      }

      function meshArcs(topology, o, filter) {
        var arcs = [];

        function arc(i) {
          var j = i < 0 ? ~i : i;
          (geomsByArc[j] || (geomsByArc[j] = [])).push({
            i: i,
            g: geom
          });
        }

        function line(arcs) {
          arcs.forEach(arc);
        }

        function polygon(arcs) {
          arcs.forEach(line);
        }

        function geometry(o) {
          if(o.type === "GeometryCollection") o.geometries.forEach(geometry);
          else if(o.type in geometryType) geom = o, geometryType[o.type](o.arcs);
        }

        if(arguments.length > 1) {
          var geomsByArc = [],
            geom;

          var geometryType = {
            LineString: line,
            MultiLineString: polygon,
            Polygon: polygon,
            MultiPolygon: function(arcs) {
              arcs.forEach(polygon);
            }
          };

          geometry(o);

          geomsByArc.forEach(arguments.length < 3 ? function(geoms) {
            arcs.push(geoms[0].i);
          } : function(geoms) {
            if(filter(geoms[0].g, geoms[geoms.length - 1].g)) arcs.push(geoms[0].i);
          });
        } else {
          for(var i = 0, n = topology.arcs.length; i < n; ++i) arcs.push(i);
        }

        return {
          type: "MultiLineString",
          arcs: stitchArcs(topology, arcs)
        };
      }

      function mergeArcs(topology, objects) {
        var polygonsByArc = {},
          polygons = [],
          components = [];

        objects.forEach(function(o) {
          if(o.type === "Polygon") register(o.arcs);
          else if(o.type === "MultiPolygon") o.arcs.forEach(register);
        });

        function register(polygon) {
          polygon.forEach(function(ring) {
            ring.forEach(function(arc) {
              (polygonsByArc[arc = arc < 0 ? ~arc : arc] || (polygonsByArc[arc] = [])).push(polygon);
            });
          });
          polygons.push(polygon);
        }

        function exterior(ring) {
          return cartesianRingArea(object(topology, {
            type: "Polygon",
            arcs: [ring]
          }).coordinates[0]) > 0; // TODO allow spherical?
        }

        polygons.forEach(function(polygon) {
          if(!polygon._) {
            var component = [],
              neighbors = [polygon];
            polygon._ = 1;
            components.push(component);
            while(polygon = neighbors.pop()) {
              component.push(polygon);
              polygon.forEach(function(ring) {
                ring.forEach(function(arc) {
                  polygonsByArc[arc < 0 ? ~arc : arc].forEach(function(polygon) {
                    if(!polygon._) {
                      polygon._ = 1;
                      neighbors.push(polygon);
                    }
                  });
                });
              });
            }
          }
        });

        polygons.forEach(function(polygon) {
          delete polygon._;
        });

        return {
          type: "MultiPolygon",
          arcs: components.map(function(polygons) {
            var arcs = [];

            // Extract the exterior (unique) arcs.
            polygons.forEach(function(polygon) {
              polygon.forEach(function(ring) {
                ring.forEach(function(arc) {
                  if(polygonsByArc[arc < 0 ? ~arc : arc].length < 2) {
                    arcs.push(arc);
                  }
                });
              });
            });

            // Stitch the arcs into one or more rings.
            arcs = stitchArcs(topology, arcs);

            // If more than one ring is returned,
            // at most one of these rings can be the exterior;
            // this exterior ring has the same winding order
            // as any exterior ring in the original polygons.
            if((n = arcs.length) > 1) {
              var sgn = exterior(polygons[0][0]);
              for(var i = 0, t; i < n; ++i) {
                if(sgn === exterior(arcs[i])) {
                  t = arcs[0], arcs[0] = arcs[i], arcs[i] = t;
                  break;
                }
              }
            }

            return arcs;
          })
        };
      }

      function featureOrCollection(topology, o) {
        return o.type === "GeometryCollection" ? {
          type: "FeatureCollection",
          features: o.geometries.map(function(o) {
            return feature(topology, o);
          })
        } : feature(topology, o);
      }

      function feature(topology, o) {
        var f = {
          type: "Feature",
          id: o.id,
          properties: o.properties || {},
          geometry: object(topology, o)
        };
        if(o.id == null) delete f.id;
        return f;
      }

      function object(topology, o) {
        var absolute = transformAbsolute(topology.transform),
          arcs = topology.arcs;

        function arc(i, points) {
          if(points.length) points.pop();
          for(var a = arcs[i < 0 ? ~i : i], k = 0, n = a.length, p; k < n; ++k) {
            points.push(p = a[k].slice());
            absolute(p, k);
          }
          if(i < 0) reverse(points, n);
        }

        function point(p) {
          p = p.slice();
          absolute(p, 0);
          return p;
        }

        function line(arcs) {
          var points = [];
          for(var i = 0, n = arcs.length; i < n; ++i) arc(arcs[i], points);
          if(points.length < 2) points.push(points[0].slice());
          return points;
        }

        function ring(arcs) {
          var points = line(arcs);
          while(points.length < 4) points.push(points[0].slice());
          return points;
        }

        function polygon(arcs) {
          return arcs.map(ring);
        }

        function geometry(o) {
          var t = o.type;
          return t === "GeometryCollection" ? {
            type: t,
            geometries: o.geometries.map(geometry)
          } : t in geometryType ? {
            type: t,
            coordinates: geometryType[t](o)
          } : null;
        }

        var geometryType = {
          Point: function(o) {
            return point(o.coordinates);
          },
          MultiPoint: function(o) {
            return o.coordinates.map(point);
          },
          LineString: function(o) {
            return line(o.arcs);
          },
          MultiLineString: function(o) {
            return o.arcs.map(line);
          },
          Polygon: function(o) {
            return polygon(o.arcs);
          },
          MultiPolygon: function(o) {
            return o.arcs.map(polygon);
          }
        };

        return geometry(o);
      }

      function reverse(array, n) {
        var t, j = array.length,
          i = j - n;
        while(i < --j) t = array[i], array[i++] = array[j], array[j] = t;
      }

      function bisect(a, x) {
        var lo = 0,
          hi = a.length;
        while(lo < hi) {
          var mid = lo + hi >>> 1;
          if(a[mid] < x) lo = mid + 1;
          else hi = mid;
        }
        return lo;
      }

      function neighbors(objects) {
        var indexesByArc = {}, // arc index -> array of object indexes
          neighbors = objects.map(function() {
            return [];
          });

        function line(arcs, i) {
          arcs.forEach(function(a) {
            if(a < 0) a = ~a;
            var o = indexesByArc[a];
            if(o) o.push(i);
            else indexesByArc[a] = [i];
          });
        }

        function polygon(arcs, i) {
          arcs.forEach(function(arc) {
            line(arc, i);
          });
        }

        function geometry(o, i) {
          if(o.type === "GeometryCollection") o.geometries.forEach(function(o) {
            geometry(o, i);
          });
          else if(o.type in geometryType) geometryType[o.type](o.arcs, i);
        }

        var geometryType = {
          LineString: line,
          MultiLineString: polygon,
          Polygon: polygon,
          MultiPolygon: function(arcs, i) {
            arcs.forEach(function(arc) {
              polygon(arc, i);
            });
          }
        };

        objects.forEach(geometry);

        for(var i in indexesByArc) {
          for(var indexes = indexesByArc[i], m = indexes.length, j = 0; j < m; ++j) {
            for(var k = j + 1; k < m; ++k) {
              var ij = indexes[j],
                ik = indexes[k],
                n;
              if((n = neighbors[ij])[i = bisect(n, ik)] !== ik) n.splice(i, 0, ik);
              if((n = neighbors[ik])[i = bisect(n, ij)] !== ij) n.splice(i, 0, ij);
            }
          }
        }

        return neighbors;
      }

      function presimplify(topology, triangleArea) {
        var absolute = transformAbsolute(topology.transform),
          relative = transformRelative(topology.transform),
          heap = minAreaHeap();

        if(!triangleArea) triangleArea = cartesianTriangleArea;

        topology.arcs.forEach(function(arc) {
          var triangles = [],
            maxArea = 0,
            triangle;

          // To store each point鈥檚 effective area, we create a new array rather than
          // extending the passed-in point to workaround a Chrome/V8 bug (getting
          // stuck in smi mode). For midpoints, the initial effective area of
          // Infinity will be computed in the next step.
          for(var i = 0, n = arc.length, p; i < n; ++i) {
            p = arc[i];
            absolute(arc[i] = [p[0], p[1], Infinity], i);
          }

          for(var i = 1, n = arc.length - 1; i < n; ++i) {
            triangle = arc.slice(i - 1, i + 2);
            triangle[1][2] = triangleArea(triangle);
            triangles.push(triangle);
            heap.push(triangle);
          }

          for(var i = 0, n = triangles.length; i < n; ++i) {
            triangle = triangles[i];
            triangle.previous = triangles[i - 1];
            triangle.next = triangles[i + 1];
          }

          while(triangle = heap.pop()) {
            var previous = triangle.previous,
              next = triangle.next;

            // If the area of the current point is less than that of the previous point
            // to be eliminated, use the latter's area instead. This ensures that the
            // current point cannot be eliminated without eliminating previously-
            // eliminated points.
            if(triangle[1][2] < maxArea) triangle[1][2] = maxArea;
            else maxArea = triangle[1][2];

            if(previous) {
              previous.next = next;
              previous[2] = triangle[2];
              update(previous);
            }

            if(next) {
              next.previous = previous;
              next[0] = triangle[0];
              update(next);
            }
          }

          arc.forEach(relative);
        });

        function update(triangle) {
          heap.remove(triangle);
          triangle[1][2] = triangleArea(triangle);
          heap.push(triangle);
        }

        return topology;
      };

      function cartesianRingArea(ring) {
        var i = -1,
          n = ring.length,
          a,
          b = ring[n - 1],
          area = 0;

        while(++i < n) {
          a = b;
          b = ring[i];
          area += a[0] * b[1] - a[1] * b[0];
        }

        return area * .5;
      }

      function cartesianTriangleArea(triangle) {
        var a = triangle[0],
          b = triangle[1],
          c = triangle[2];
        return Math.abs((a[0] - c[0]) * (b[1] - a[1]) - (a[0] - b[0]) * (c[1] - a[1]));
      }

      function compareArea(a, b) {
        return a[1][2] - b[1][2];
      }

      function minAreaHeap() {
        var heap = {},
          array = [],
          size = 0;

        heap.push = function(object) {
          up(array[object._ = size] = object, size++);
          return size;
        };

        heap.pop = function() {
          if(size <= 0) return;
          var removed = array[0],
            object;
          if(--size > 0) object = array[size], down(array[object._ = 0] = object, 0);
          return removed;
        };

        heap.remove = function(removed) {
          var i = removed._,
            object;
          if(array[i] !== removed) return; // invalid request
          if(i !== --size) object = array[size], (compareArea(object, removed) < 0 ? up : down)(array[object._ = i] =
            object, i);
          return i;
        };

        function up(object, i) {
          while(i > 0) {
            var j = ((i + 1) >> 1) - 1,
              parent = array[j];
            if(compareArea(object, parent) >= 0) break;
            array[parent._ = i] = parent;
            array[object._ = i = j] = object;
          }
        }

        function down(object, i) {
          while(true) {
            var r = (i + 1) << 1,
              l = r - 1,
              j = i,
              child = array[j];
            if(l < size && compareArea(array[l], child) < 0) child = array[j = l];
            if(r < size && compareArea(array[r], child) < 0) child = array[j = r];
            if(j === i) break;
            array[child._ = i] = child;
            array[object._ = i = j] = object;
          }
        }

        return heap;
      }

      function transformAbsolute(transform) {
        if(!transform) return noop;
        var x0,
          y0,
          kx = transform.scale[0],
          ky = transform.scale[1],
          dx = transform.translate[0],
          dy = transform.translate[1];
        return function(point, i) {
          if(!i) x0 = y0 = 0;
          point[0] = (x0 += point[0]) * kx + dx;
          point[1] = (y0 += point[1]) * ky + dy;
        };
      }

      function transformRelative(transform) {
        if(!transform) return noop;
        var x0,
          y0,
          kx = transform.scale[0],
          ky = transform.scale[1],
          dx = transform.translate[0],
          dy = transform.translate[1];
        return function(point, i) {
          if(!i) x0 = y0 = 0;
          var x1 = (point[0] - dx) / kx | 0,
            y1 = (point[1] - dy) / ky | 0;
          point[0] = x1 - x0;
          point[1] = y1 - y0;
          x0 = x1;
          y0 = y1;
        };
      }

      function noop() {}

      return topojson;
    }());

    function d3_geo_projection() {
      d3.geo.project = function(object, projection) {
        var stream = projection.stream;
        if (!stream) throw new Error("not yet supported");
        return (object && d3_geo_projectObjectType.hasOwnProperty(object.type) ? d3_geo_projectObjectType[object.type] : d3_geo_projectGeometry)(object, stream);
      };
      function d3_geo_projectFeature(object, stream) {
        return {
          type: "Feature",
          id: object.id,
          properties: object.properties,
          geometry: d3_geo_projectGeometry(object.geometry, stream)
        };
      }
      function d3_geo_projectGeometry(geometry, stream) {
        if (!geometry) return null;
        if (geometry.type === "GeometryCollection") return {
          type: "GeometryCollection",
          geometries: object.geometries.map(function(geometry) {
            return d3_geo_projectGeometry(geometry, stream);
          })
        };
        if (!d3_geo_projectGeometryType.hasOwnProperty(geometry.type)) return null;
        var sink = d3_geo_projectGeometryType[geometry.type];
        d3.geo.stream(geometry, stream(sink));
        return sink.result();
      }
      var d3_geo_projectObjectType = {
        Feature: d3_geo_projectFeature,
        FeatureCollection: function(object, stream) {
          return {
            type: "FeatureCollection",
            features: object.features.map(function(feature) {
              return d3_geo_projectFeature(feature, stream);
            })
          };
        }
      };
      var d3_geo_projectPoints = [], d3_geo_projectLines = [];
      var d3_geo_projectPoint = {
        point: function(x, y) {
          d3_geo_projectPoints.push([ x, y ]);
        },
        result: function() {
          var result = !d3_geo_projectPoints.length ? null : d3_geo_projectPoints.length < 2 ? {
            type: "Point",
            coordinates: d3_geo_projectPoints[0]
          } : {
            type: "MultiPoint",
            coordinates: d3_geo_projectPoints
          };
          d3_geo_projectPoints = [];
          return result;
        }
      };
      var d3_geo_projectLine = {
        lineStart: d3_geo_projectNoop,
        point: function(x, y) {
          d3_geo_projectPoints.push([ x, y ]);
        },
        lineEnd: function() {
          if (d3_geo_projectPoints.length) d3_geo_projectLines.push(d3_geo_projectPoints), 
          d3_geo_projectPoints = [];
        },
        result: function() {
          var result = !d3_geo_projectLines.length ? null : d3_geo_projectLines.length < 2 ? {
            type: "LineString",
            coordinates: d3_geo_projectLines[0]
          } : {
            type: "MultiLineString",
            coordinates: d3_geo_projectLines
          };
          d3_geo_projectLines = [];
          return result;
        }
      };
      var d3_geo_projectPolygon = {
        polygonStart: d3_geo_projectNoop,
        lineStart: d3_geo_projectNoop,
        point: function(x, y) {
          d3_geo_projectPoints.push([ x, y ]);
        },
        lineEnd: function() {
          var n = d3_geo_projectPoints.length;
          if (n) {
            do d3_geo_projectPoints.push(d3_geo_projectPoints[0].slice()); while (++n < 4);
            d3_geo_projectLines.push(d3_geo_projectPoints), d3_geo_projectPoints = [];
          }
        },
        polygonEnd: d3_geo_projectNoop,
        result: function() {
          if (!d3_geo_projectLines.length) return null;
          var polygons = [], holes = [];
          d3_geo_projectLines.forEach(function(ring) {
            if (d3_geo_projectClockwise(ring)) polygons.push([ ring ]); else holes.push(ring);
          });
          holes.forEach(function(hole) {
            var point = hole[0];
            polygons.some(function(polygon) {
              if (d3_geo_projectContains(polygon[0], point)) {
                polygon.push(hole);
                return true;
              }
            }) || polygons.push([ hole ]);
          });
          d3_geo_projectLines = [];
          return !polygons.length ? null : polygons.length > 1 ? {
            type: "MultiPolygon",
            coordinates: polygons
          } : {
            type: "Polygon",
            coordinates: polygons[0]
          };
        }
      };
      var d3_geo_projectGeometryType = {
        Point: d3_geo_projectPoint,
        MultiPoint: d3_geo_projectPoint,
        LineString: d3_geo_projectLine,
        MultiLineString: d3_geo_projectLine,
        Polygon: d3_geo_projectPolygon,
        MultiPolygon: d3_geo_projectPolygon,
        Sphere: d3_geo_projectPolygon
      };
      function d3_geo_projectNoop() {}
      function d3_geo_projectClockwise(ring) {
        if ((n = ring.length) < 4) return false;
        var i = 0, n, area = ring[n - 1][1] * ring[0][0] - ring[n - 1][0] * ring[0][1];
        while (++i < n) area += ring[i - 1][1] * ring[i][0] - ring[i - 1][0] * ring[i][1];
        return area <= 0;
      }
      function d3_geo_projectContains(ring, point) {
        var x = point[0], y = point[1], contains = false;
        for (var i = 0, n = ring.length, j = n - 1; i < n; j = i++) {
          var pi = ring[i], xi = pi[0], yi = pi[1], pj = ring[j], xj = pj[0], yj = pj[1];
          if (yi > y ^ yj > y && x < (xj - xi) * (y - yi) / (yj - yi) + xi) contains = !contains;
        }
        return contains;
      }
      var ε = 1e-6, ε2 = ε * ε, π = Math.PI, halfπ = π / 2, sqrtπ = Math.sqrt(π), radians = π / 180, degrees = 180 / π;
      function sinci(x) {
        return x ? x / Math.sin(x) : 1;
      }
      function sgn(x) {
        return x > 0 ? 1 : x < 0 ? -1 : 0;
      }
      function asin(x) {
        return x > 1 ? halfπ : x < -1 ? -halfπ : Math.asin(x);
      }
      function acos(x) {
        return x > 1 ? 0 : x < -1 ? π : Math.acos(x);
      }
      function asqrt(x) {
        return x > 0 ? Math.sqrt(x) : 0;
      }
      var projection = d3.geo.projection, projectionMutator = d3.geo.projectionMutator;
      d3.geo.interrupt = function(project) {
        var lobes = [ [ [ [ -π, 0 ], [ 0, halfπ ], [ π, 0 ] ] ], [ [ [ -π, 0 ], [ 0, -halfπ ], [ π, 0 ] ] ] ];
        var bounds;
        function forward(λ, φ) {
          var sign = φ < 0 ? -1 : +1, hemilobes = lobes[+(φ < 0)];
          for (var i = 0, n = hemilobes.length - 1; i < n && λ > hemilobes[i][2][0]; ++i) ;
          var coordinates = project(λ - hemilobes[i][1][0], φ);
          coordinates[0] += project(hemilobes[i][1][0], sign * φ > sign * hemilobes[i][0][1] ? hemilobes[i][0][1] : φ)[0];
          return coordinates;
        }
        function reset() {
          bounds = lobes.map(function(hemilobes) {
            return hemilobes.map(function(lobe) {
              var x0 = project(lobe[0][0], lobe[0][1])[0], x1 = project(lobe[2][0], lobe[2][1])[0], y0 = project(lobe[1][0], lobe[0][1])[1], y1 = project(lobe[1][0], lobe[1][1])[1], t;
              if (y0 > y1) t = y0, y0 = y1, y1 = t;
              return [ [ x0, y0 ], [ x1, y1 ] ];
            });
          });
        }
        if (project.invert) forward.invert = function(x, y) {
          var hemibounds = bounds[+(y < 0)], hemilobes = lobes[+(y < 0)];
          for (var i = 0, n = hemibounds.length; i < n; ++i) {
            var b = hemibounds[i];
            if (b[0][0] <= x && x < b[1][0] && b[0][1] <= y && y < b[1][1]) {
              var coordinates = project.invert(x - project(hemilobes[i][1][0], 0)[0], y);
              coordinates[0] += hemilobes[i][1][0];
              return pointEqual(forward(coordinates[0], coordinates[1]), [ x, y ]) ? coordinates : null;
            }
          }
        };
        var projection = d3.geo.projection(forward), stream_ = projection.stream;
        projection.stream = function(stream) {
          var rotate = projection.rotate(), rotateStream = stream_(stream), sphereStream = (projection.rotate([ 0, 0 ]), 
          stream_(stream));
          projection.rotate(rotate);
          rotateStream.sphere = function() {
            d3.geo.stream(sphere(), sphereStream);
          };
          return rotateStream;
        };
        projection.lobes = function(_) {
          if (!arguments.length) return lobes.map(function(lobes) {
            return lobes.map(function(lobe) {
              return [ [ lobe[0][0] * 180 / π, lobe[0][1] * 180 / π ], [ lobe[1][0] * 180 / π, lobe[1][1] * 180 / π ], [ lobe[2][0] * 180 / π, lobe[2][1] * 180 / π ] ];
            });
          });
          lobes = _.map(function(lobes) {
            return lobes.map(function(lobe) {
              return [ [ lobe[0][0] * π / 180, lobe[0][1] * π / 180 ], [ lobe[1][0] * π / 180, lobe[1][1] * π / 180 ], [ lobe[2][0] * π / 180, lobe[2][1] * π / 180 ] ];
            });
          });
          reset();
          return projection;
        };
        function sphere() {
          var ε = 1e-6, coordinates = [];
          for (var i = 0, n = lobes[0].length; i < n; ++i) {
            var lobe = lobes[0][i], λ0 = lobe[0][0] * 180 / π, φ0 = lobe[0][1] * 180 / π, φ1 = lobe[1][1] * 180 / π, λ2 = lobe[2][0] * 180 / π, φ2 = lobe[2][1] * 180 / π;
            coordinates.push(resample([ [ λ0 + ε, φ0 + ε ], [ λ0 + ε, φ1 - ε ], [ λ2 - ε, φ1 - ε ], [ λ2 - ε, φ2 + ε ] ], 30));
          }
          for (var i = lobes[1].length - 1; i >= 0; --i) {
            var lobe = lobes[1][i], λ0 = lobe[0][0] * 180 / π, φ0 = lobe[0][1] * 180 / π, φ1 = lobe[1][1] * 180 / π, λ2 = lobe[2][0] * 180 / π, φ2 = lobe[2][1] * 180 / π;
            coordinates.push(resample([ [ λ2 - ε, φ2 - ε ], [ λ2 - ε, φ1 + ε ], [ λ0 + ε, φ1 + ε ], [ λ0 + ε, φ0 - ε ] ], 30));
          }
          return {
            type: "Polygon",
            coordinates: [ d3.merge(coordinates) ]
          };
        }
        function resample(coordinates, m) {
          var i = -1, n = coordinates.length, p0 = coordinates[0], p1, dx, dy, resampled = [];
          while (++i < n) {
            p1 = coordinates[i];
            dx = (p1[0] - p0[0]) / m;
            dy = (p1[1] - p0[1]) / m;
            for (var j = 0; j < m; ++j) resampled.push([ p0[0] + j * dx, p0[1] + j * dy ]);
            p0 = p1;
          }
          resampled.push(p1);
          return resampled;
        }
        function pointEqual(a, b) {
          return Math.abs(a[0] - b[0]) < ε && Math.abs(a[1] - b[1]) < ε;
        }
        return projection;
      };
      function airy(β) {
        var tanβ_2 = Math.tan(.5 * β), B = 2 * Math.log(Math.cos(.5 * β)) / (tanβ_2 * tanβ_2);
        function forward(λ, φ) {
          var cosλ = Math.cos(λ), cosφ = Math.cos(φ), sinφ = Math.sin(φ), cosz = cosφ * cosλ, K = -((1 - cosz ? Math.log(.5 * (1 + cosz)) / (1 - cosz) : -.5) + B / (1 + cosz));
          return [ K * cosφ * Math.sin(λ), K * sinφ ];
        }
        forward.invert = function(x, y) {
          var ρ = Math.sqrt(x * x + y * y), z = β * -.5, i = 50, δ;
          if (!ρ) return [ 0, 0 ];
          do {
            var z_2 = .5 * z, cosz_2 = Math.cos(z_2), sinz_2 = Math.sin(z_2), tanz_2 = Math.tan(z_2), lnsecz_2 = Math.log(1 / cosz_2);
            z -= δ = (2 / tanz_2 * lnsecz_2 - B * tanz_2 - ρ) / (-lnsecz_2 / (sinz_2 * sinz_2) + 1 - B / (2 * cosz_2 * cosz_2));
          } while (Math.abs(δ) > ε && --i > 0);
          var sinz = Math.sin(z);
          return [ Math.atan2(x * sinz, ρ * Math.cos(z)), asin(y * sinz / ρ) ];
        };
        return forward;
      }
      function airyProjection() {
        var β = halfπ, m = projectionMutator(airy), p = m(β);
        p.radius = function(_) {
          if (!arguments.length) return β / π * 180;
          return m(β = _ * π / 180);
        };
        return p;
      }
      (d3.geo.airy = airyProjection).raw = airy;
      function aitoff(λ, φ) {
        var cosφ = Math.cos(φ), sinciα = sinci(acos(cosφ * Math.cos(λ /= 2)));
        return [ 2 * cosφ * Math.sin(λ) * sinciα, Math.sin(φ) * sinciα ];
      }
      aitoff.invert = function(x, y) {
        if (x * x + 4 * y * y > π * π + ε) return;
        var λ = x, φ = y, i = 25;
        do {
          var sinλ = Math.sin(λ), sinλ_2 = Math.sin(λ / 2), cosλ_2 = Math.cos(λ / 2), sinφ = Math.sin(φ), cosφ = Math.cos(φ), sin_2φ = Math.sin(2 * φ), sin2φ = sinφ * sinφ, cos2φ = cosφ * cosφ, sin2λ_2 = sinλ_2 * sinλ_2, C = 1 - cos2φ * cosλ_2 * cosλ_2, E = C ? acos(cosφ * cosλ_2) * Math.sqrt(F = 1 / C) : F = 0, F, fx = 2 * E * cosφ * sinλ_2 - x, fy = E * sinφ - y, δxδλ = F * (cos2φ * sin2λ_2 + E * cosφ * cosλ_2 * sin2φ), δxδφ = F * (.5 * sinλ * sin_2φ - E * 2 * sinφ * sinλ_2), δyδλ = F * .25 * (sin_2φ * sinλ_2 - E * sinφ * cos2φ * sinλ), δyδφ = F * (sin2φ * cosλ_2 + E * sin2λ_2 * cosφ), denominator = δxδφ * δyδλ - δyδφ * δxδλ;
          if (!denominator) break;
          var δλ = (fy * δxδφ - fx * δyδφ) / denominator, δφ = (fx * δyδλ - fy * δxδλ) / denominator;
          λ -= δλ, φ -= δφ;
        } while ((Math.abs(δλ) > ε || Math.abs(δφ) > ε) && --i > 0);
        return [ λ, φ ];
      };
      (d3.geo.aitoff = function() {
        return projection(aitoff);
      }).raw = aitoff;
      function armadillo(φ0) {
        var sinφ0 = Math.sin(φ0), cosφ0 = Math.cos(φ0), sφ0 = φ0 > 0 ? 1 : -1, tanφ0 = Math.tan(sφ0 * φ0), k = (1 + sinφ0 - cosφ0) / 2;
        function forward(λ, φ) {
          var cosφ = Math.cos(φ), cosλ = Math.cos(λ /= 2);
          return [ (1 + cosφ) * Math.sin(λ), (sφ0 * φ > -Math.atan2(cosλ, tanφ0) - .001 ? 0 : -sφ0 * 10) + k + Math.sin(φ) * cosφ0 - (1 + cosφ) * sinφ0 * cosλ ];
        }
        forward.invert = function(x, y) {
          var λ = 0, φ = 0, i = 50;
          do {
            var cosλ = Math.cos(λ), sinλ = Math.sin(λ), cosφ = Math.cos(φ), sinφ = Math.sin(φ), A = 1 + cosφ, fx = A * sinλ - x, fy = k + sinφ * cosφ0 - A * sinφ0 * cosλ - y, δxδλ = .5 * A * cosλ, δxδφ = -sinλ * sinφ, δyδλ = .5 * sinφ0 * A * sinλ, δyδφ = cosφ0 * cosφ + sinφ0 * cosλ * sinφ, denominator = δxδφ * δyδλ - δyδφ * δxδλ, δλ = .5 * (fy * δxδφ - fx * δyδφ) / denominator, δφ = (fx * δyδλ - fy * δxδλ) / denominator;
            λ -= δλ, φ -= δφ;
          } while ((Math.abs(δλ) > ε || Math.abs(δφ) > ε) && --i > 0);
          return sφ0 * φ > -Math.atan2(Math.cos(λ), tanφ0) - .001 ? [ λ * 2, φ ] : null;
        };
        return forward;
      }
      function armadilloProjection() {
        var φ0 = π / 9, sφ0 = φ0 > 0 ? 1 : -1, tanφ0 = Math.tan(sφ0 * φ0), m = projectionMutator(armadillo), p = m(φ0), stream_ = p.stream;
        p.parallel = function(_) {
          if (!arguments.length) return φ0 / π * 180;
          tanφ0 = Math.tan((sφ0 = (φ0 = _ * π / 180) > 0 ? 1 : -1) * φ0);
          return m(φ0);
        };
        p.stream = function(stream) {
          var rotate = p.rotate(), rotateStream = stream_(stream), sphereStream = (p.rotate([ 0, 0 ]), 
          stream_(stream));
          p.rotate(rotate);
          rotateStream.sphere = function() {
            sphereStream.polygonStart(), sphereStream.lineStart();
            for (var λ = sφ0 * -180; sφ0 * λ < 180; λ += sφ0 * 90) sphereStream.point(λ, sφ0 * 90);
            while (sφ0 * (λ -= φ0) >= -180) {
              sphereStream.point(λ, sφ0 * -Math.atan2(Math.cos(λ * radians / 2), tanφ0) * degrees);
            }
            sphereStream.lineEnd(), sphereStream.polygonEnd();
          };
          return rotateStream;
        };
        return p;
      }
      (d3.geo.armadillo = armadilloProjection).raw = armadillo;
      function tanh(x) {
        x = Math.exp(2 * x);
        return (x - 1) / (x + 1);
      }
      function sinh(x) {
        return .5 * (Math.exp(x) - Math.exp(-x));
      }
      function cosh(x) {
        return .5 * (Math.exp(x) + Math.exp(-x));
      }
      function arsinh(x) {
        return Math.log(x + asqrt(x * x + 1));
      }
      function arcosh(x) {
        return Math.log(x + asqrt(x * x - 1));
      }
      function august(λ, φ) {
        var tanφ = Math.tan(φ / 2), k = asqrt(1 - tanφ * tanφ), c = 1 + k * Math.cos(λ /= 2), x = Math.sin(λ) * k / c, y = tanφ / c, x2 = x * x, y2 = y * y;
        return [ 4 / 3 * x * (3 + x2 - 3 * y2), 4 / 3 * y * (3 + 3 * x2 - y2) ];
      }
      august.invert = function(x, y) {
        x *= 3 / 8, y *= 3 / 8;
        if (!x && Math.abs(y) > 1) return null;
        var x2 = x * x, y2 = y * y, s = 1 + x2 + y2, sin3η = Math.sqrt(.5 * (s - Math.sqrt(s * s - 4 * y * y))), η = asin(sin3η) / 3, ξ = sin3η ? arcosh(Math.abs(y / sin3η)) / 3 : arsinh(Math.abs(x)) / 3, cosη = Math.cos(η), coshξ = cosh(ξ), d = coshξ * coshξ - cosη * cosη;
        return [ sgn(x) * 2 * Math.atan2(sinh(ξ) * cosη, .25 - d), sgn(y) * 2 * Math.atan2(coshξ * Math.sin(η), .25 + d) ];
      };
      (d3.geo.august = function() {
        return projection(august);
      }).raw = august;
      var bakerφ = Math.log(1 + Math.SQRT2);
      function baker(λ, φ) {
        var φ0 = Math.abs(φ);
        return φ0 < π / 4 ? [ λ, Math.log(Math.tan(π / 4 + φ / 2)) ] : [ λ * Math.cos(φ0) * (2 * Math.SQRT2 - 1 / Math.sin(φ0)), sgn(φ) * (2 * Math.SQRT2 * (φ0 - π / 4) - Math.log(Math.tan(φ0 / 2))) ];
      }
      baker.invert = function(x, y) {
        if ((y0 = Math.abs(y)) < bakerφ) return [ x, 2 * Math.atan(Math.exp(y)) - halfπ ];
        var sqrt8 = Math.sqrt(8), φ = π / 4, i = 25, δ, y0;
        do {
          var cosφ_2 = Math.cos(φ / 2), tanφ_2 = Math.tan(φ / 2);
          φ -= δ = (sqrt8 * (φ - π / 4) - Math.log(tanφ_2) - y0) / (sqrt8 - .5 * cosφ_2 * cosφ_2 / tanφ_2);
        } while (Math.abs(δ) > ε2 && --i > 0);
        return [ x / (Math.cos(φ) * (sqrt8 - 1 / Math.sin(φ))), sgn(y) * φ ];
      };
      (d3.geo.baker = function() {
        return projection(baker);
      }).raw = baker;
      var berghausAzimuthalEquidistant = d3.geo.azimuthalEquidistant.raw;
      function berghaus(n) {
        var k = 2 * π / n;
        function forward(λ, φ) {
          var p = berghausAzimuthalEquidistant(λ, φ);
          if (Math.abs(λ) > halfπ) {
            var θ = Math.atan2(p[1], p[0]), r = Math.sqrt(p[0] * p[0] + p[1] * p[1]), θ0 = k * Math.round((θ - halfπ) / k) + halfπ, α = Math.atan2(Math.sin(θ -= θ0), 2 - Math.cos(θ));
            θ = θ0 + asin(π / r * Math.sin(α)) - α;
            p[0] = r * Math.cos(θ);
            p[1] = r * Math.sin(θ);
          }
          return p;
        }
        forward.invert = function(x, y) {
          var r = Math.sqrt(x * x + y * y);
          if (r > halfπ) {
            var θ = Math.atan2(y, x), θ0 = k * Math.round((θ - halfπ) / k) + halfπ, s = θ > θ0 ? -1 : 1, A = r * Math.cos(θ0 - θ), cotα = 1 / Math.tan(s * Math.acos((A - π) / Math.sqrt(π * (π - 2 * A) + r * r)));
            θ = θ0 + 2 * Math.atan((cotα + s * Math.sqrt(cotα * cotα - 3)) / 3);
            x = r * Math.cos(θ), y = r * Math.sin(θ);
          }
          return berghausAzimuthalEquidistant.invert(x, y);
        };
        return forward;
      }
      function berghausProjection() {
        var n = 5, m = projectionMutator(berghaus), p = m(n), stream_ = p.stream, ε = .01, cr = -Math.cos(ε * radians), sr = Math.sin(ε * radians);
        p.lobes = function(_) {
          if (!arguments.length) return n;
          return m(n = +_);
        };
        p.stream = function(stream) {
          var rotate = p.rotate(), rotateStream = stream_(stream), sphereStream = (p.rotate([ 0, 0 ]), 
          stream_(stream));
          p.rotate(rotate);
          rotateStream.sphere = function() {
            sphereStream.polygonStart(), sphereStream.lineStart();
            for (var i = 0, δ = 360 / n, δ0 = 2 * π / n, φ = 90 - 180 / n, φ0 = halfπ; i < n; ++i, 
            φ -= δ, φ0 -= δ0) {
              sphereStream.point(Math.atan2(sr * Math.cos(φ0), cr) * degrees, asin(sr * Math.sin(φ0)) * degrees);
              if (φ < -90) {
                sphereStream.point(-90, -180 - φ - ε);
                sphereStream.point(-90, -180 - φ + ε);
              } else {
                sphereStream.point(90, φ + ε);
                sphereStream.point(90, φ - ε);
              }
            }
            sphereStream.lineEnd(), sphereStream.polygonEnd();
          };
          return rotateStream;
        };
        return p;
      }
      (d3.geo.berghaus = berghausProjection).raw = berghaus;
      function mollweideBromleyθ(Cp) {
        return function(θ) {
          var Cpsinθ = Cp * Math.sin(θ), i = 30, δ;
          do θ -= δ = (θ + Math.sin(θ) - Cpsinθ) / (1 + Math.cos(θ)); while (Math.abs(δ) > ε && --i > 0);
          return θ / 2;
        };
      }
      function mollweideBromley(Cx, Cy, Cp) {
        var θ = mollweideBromleyθ(Cp);
        function forward(λ, φ) {
          return [ Cx * λ * Math.cos(φ = θ(φ)), Cy * Math.sin(φ) ];
        }
        forward.invert = function(x, y) {
          var θ = asin(y / Cy);
          return [ x / (Cx * Math.cos(θ)), asin((2 * θ + Math.sin(2 * θ)) / Cp) ];
        };
        return forward;
      }
      var mollweideθ = mollweideBromleyθ(π), mollweide = mollweideBromley(Math.SQRT2 / halfπ, Math.SQRT2, π);
      (d3.geo.mollweide = function() {
        return projection(mollweide);
      }).raw = mollweide;
      function boggs(λ, φ) {
        var k = 2.00276, θ = mollweideθ(φ);
        return [ k * λ / (1 / Math.cos(φ) + 1.11072 / Math.cos(θ)), (φ + Math.SQRT2 * Math.sin(θ)) / k ];
      }
      boggs.invert = function(x, y) {
        var k = 2.00276, ky = k * y, θ = y < 0 ? -π / 4 : π / 4, i = 25, δ, φ;
        do {
          φ = ky - Math.SQRT2 * Math.sin(θ);
          θ -= δ = (Math.sin(2 * θ) + 2 * θ - π * Math.sin(φ)) / (2 * Math.cos(2 * θ) + 2 + π * Math.cos(φ) * Math.SQRT2 * Math.cos(θ));
        } while (Math.abs(δ) > ε && --i > 0);
        φ = ky - Math.SQRT2 * Math.sin(θ);
        return [ x * (1 / Math.cos(φ) + 1.11072 / Math.cos(θ)) / k, φ ];
      };
      (d3.geo.boggs = function() {
        return projection(boggs);
      }).raw = boggs;
      function parallel1Projection(projectAt) {
        var φ0 = 0, m = projectionMutator(projectAt), p = m(φ0);
        p.parallel = function(_) {
          if (!arguments.length) return φ0 / π * 180;
          return m(φ0 = _ * π / 180);
        };
        return p;
      }
      function sinusoidal(λ, φ) {
        return [ λ * Math.cos(φ), φ ];
      }
      sinusoidal.invert = function(x, y) {
        return [ x / Math.cos(y), y ];
      };
      (d3.geo.sinusoidal = function() {
        return projection(sinusoidal);
      }).raw = sinusoidal;
      function bonne(φ0) {
        if (!φ0) return sinusoidal;
        var cotφ0 = 1 / Math.tan(φ0);
        function forward(λ, φ) {
          var ρ = cotφ0 + φ0 - φ, E = ρ ? λ * Math.cos(φ) / ρ : ρ;
          return [ ρ * Math.sin(E), cotφ0 - ρ * Math.cos(E) ];
        }
        forward.invert = function(x, y) {
          var ρ = Math.sqrt(x * x + (y = cotφ0 - y) * y), φ = cotφ0 + φ0 - ρ;
          return [ ρ / Math.cos(φ) * Math.atan2(x, y), φ ];
        };
        return forward;
      }
      (d3.geo.bonne = function() {
        return parallel1Projection(bonne).parallel(45);
      }).raw = bonne;
      var bromley = mollweideBromley(1, 4 / π, π);
      (d3.geo.bromley = function() {
        return projection(bromley);
      }).raw = bromley;
      function chamberlin(points) {
        points = points.map(function(p) {
          return [ p[0], p[1], Math.sin(p[1]), Math.cos(p[1]) ];
        });
        for (var a = points[2], b, i = 0; i < 3; ++i, a = b) {
          b = points[i];
          a.v = chamberlinDistanceAzimuth(b[1] - a[1], a[3], a[2], b[3], b[2], b[0] - a[0]);
          a.point = [ 0, 0 ];
        }
        var β0 = chamberlinAngle(points[0].v[0], points[2].v[0], points[1].v[0]), β1 = chamberlinAngle(points[0].v[0], points[1].v[0], points[2].v[0]), β2 = π - β0;
        points[2].point[1] = 0;
        points[0].point[0] = -(points[1].point[0] = .5 * points[0].v[0]);
        var mean = [ points[2].point[0] = points[0].point[0] + points[2].v[0] * Math.cos(β0), 2 * (points[0].point[1] = points[1].point[1] = points[2].v[0] * Math.sin(β0)) ];
        function forward(λ, φ) {
          var sinφ = Math.sin(φ), cosφ = Math.cos(φ), v = new Array(3);
          for (var i = 0; i < 3; ++i) {
            var p = points[i];
            v[i] = chamberlinDistanceAzimuth(φ - p[1], p[3], p[2], cosφ, sinφ, λ - p[0]);
            if (!v[i][0]) return p.point;
            v[i][1] = chamberlinLongitude(v[i][1] - p.v[1]);
          }
          var point = mean.slice();
          for (var i = 0; i < 3; ++i) {
            var j = i == 2 ? 0 : i + 1;
            var a = chamberlinAngle(points[i].v[0], v[i][0], v[j][0]);
            if (v[i][1] < 0) a = -a;
            if (!i) {
              point[0] += v[i][0] * Math.cos(a);
              point[1] -= v[i][0] * Math.sin(a);
            } else if (i == 1) {
              a = β1 - a;
              point[0] -= v[i][0] * Math.cos(a);
              point[1] -= v[i][0] * Math.sin(a);
            } else {
              a = β2 - a;
              point[0] += v[i][0] * Math.cos(a);
              point[1] += v[i][0] * Math.sin(a);
            }
          }
          point[0] /= 3, point[1] /= 3;
          return point;
        }
        return forward;
      }
      function chamberlinProjection() {
        var points = [ [ 0, 0 ], [ 0, 0 ], [ 0, 0 ] ], m = projectionMutator(chamberlin), p = m(points), rotate = p.rotate;
        delete p.rotate;
        p.points = function(_) {
          if (!arguments.length) return points;
          points = _;
          var origin = d3.geo.centroid({
            type: "MultiPoint",
            coordinates: points
          }), r = [ -origin[0], -origin[1] ];
          rotate.call(p, r);
          return m(points.map(d3.geo.rotation(r)).map(chamberlinRadians));
        };
        return p.points([ [ -150, 55 ], [ -35, 55 ], [ -92.5, 10 ] ]);
      }
      function chamberlinDistanceAzimuth(dφ, c1, s1, c2, s2, dλ) {
        var cosdλ = Math.cos(dλ), r;
        if (Math.abs(dφ) > 1 || Math.abs(dλ) > 1) {
          r = acos(s1 * s2 + c1 * c2 * cosdλ);
        } else {
          var sindφ = Math.sin(.5 * dφ), sindλ = Math.sin(.5 * dλ);
          r = 2 * asin(Math.sqrt(sindφ * sindφ + c1 * c2 * sindλ * sindλ));
        }
        if (Math.abs(r) > ε) {
          return [ r, Math.atan2(c2 * Math.sin(dλ), c1 * s2 - s1 * c2 * cosdλ) ];
        }
        return [ 0, 0 ];
      }
      function chamberlinAngle(b, c, a) {
        return acos(.5 * (b * b + c * c - a * a) / (b * c));
      }
      function chamberlinLongitude(λ) {
        return λ - 2 * π * Math.floor((λ + π) / (2 * π));
      }
      function chamberlinRadians(point) {
        return [ point[0] * radians, point[1] * radians ];
      }
      (d3.geo.chamberlin = chamberlinProjection).raw = chamberlin;
      function collignon(λ, φ) {
        var α = asqrt(1 - Math.sin(φ));
        return [ 2 / sqrtπ * λ * α, sqrtπ * (1 - α) ];
      }
      collignon.invert = function(x, y) {
        var λ = (λ = y / sqrtπ - 1) * λ;
        return [ λ > 0 ? x * Math.sqrt(π / λ) / 2 : 0, asin(1 - λ) ];
      };
      (d3.geo.collignon = function() {
        return projection(collignon);
      }).raw = collignon;
      function craig(φ0) {
        var tanφ0 = Math.tan(φ0);
        function forward(λ, φ) {
          return [ λ, (λ ? λ / Math.sin(λ) : 1) * (Math.sin(φ) * Math.cos(λ) - tanφ0 * Math.cos(φ)) ];
        }
        forward.invert = tanφ0 ? function(x, y) {
          if (x) y *= Math.sin(x) / x;
          var cosλ = Math.cos(x);
          return [ x, 2 * Math.atan2(Math.sqrt(cosλ * cosλ + tanφ0 * tanφ0 - y * y) - cosλ, tanφ0 - y) ];
        } : function(x, y) {
          return [ x, asin(x ? y * Math.tan(x) / x : y) ];
        };
        return forward;
      }
      (d3.geo.craig = function() {
        return parallel1Projection(craig);
      }).raw = craig;
      function craster(λ, φ) {
        var sqrt3 = Math.sqrt(3);
        return [ sqrt3 * λ * (2 * Math.cos(2 * φ / 3) - 1) / sqrtπ, sqrt3 * sqrtπ * Math.sin(φ / 3) ];
      }
      craster.invert = function(x, y) {
        var sqrt3 = Math.sqrt(3), φ = 3 * asin(y / (sqrt3 * sqrtπ));
        return [ sqrtπ * x / (sqrt3 * (2 * Math.cos(2 * φ / 3) - 1)), φ ];
      };
      (d3.geo.craster = function() {
        return projection(craster);
      }).raw = craster;
      function cylindricalEqualArea(φ0) {
        var cosφ0 = Math.cos(φ0);
        function forward(λ, φ) {
          return [ λ * cosφ0, Math.sin(φ) / cosφ0 ];
        }
        forward.invert = function(x, y) {
          return [ x / cosφ0, asin(y * cosφ0) ];
        };
        return forward;
      }
      (d3.geo.cylindricalEqualArea = function() {
        return parallel1Projection(cylindricalEqualArea);
      }).raw = cylindricalEqualArea;
      function cylindricalStereographic(φ0) {
        var cosφ0 = Math.cos(φ0);
        function forward(λ, φ) {
          return [ λ * cosφ0, (1 + cosφ0) * Math.tan(φ * .5) ];
        }
        forward.invert = function(x, y) {
          return [ x / cosφ0, Math.atan(y / (1 + cosφ0)) * 2 ];
        };
        return forward;
      }
      (d3.geo.cylindricalStereographic = function() {
        return parallel1Projection(cylindricalStereographic);
      }).raw = cylindricalStereographic;
      function eckert1(λ, φ) {
        var α = Math.sqrt(8 / (3 * π));
        return [ α * λ * (1 - Math.abs(φ) / π), α * φ ];
      }
      eckert1.invert = function(x, y) {
        var α = Math.sqrt(8 / (3 * π)), φ = y / α;
        return [ x / (α * (1 - Math.abs(φ) / π)), φ ];
      };
      (d3.geo.eckert1 = function() {
        return projection(eckert1);
      }).raw = eckert1;
      function eckert2(λ, φ) {
        var α = Math.sqrt(4 - 3 * Math.sin(Math.abs(φ)));
        return [ 2 / Math.sqrt(6 * π) * λ * α, sgn(φ) * Math.sqrt(2 * π / 3) * (2 - α) ];
      }
      eckert2.invert = function(x, y) {
        var α = 2 - Math.abs(y) / Math.sqrt(2 * π / 3);
        return [ x * Math.sqrt(6 * π) / (2 * α), sgn(y) * asin((4 - α * α) / 3) ];
      };
      (d3.geo.eckert2 = function() {
        return projection(eckert2);
      }).raw = eckert2;
      function eckert3(λ, φ) {
        var k = Math.sqrt(π * (4 + π));
        return [ 2 / k * λ * (1 + Math.sqrt(1 - 4 * φ * φ / (π * π))), 4 / k * φ ];
      }
      eckert3.invert = function(x, y) {
        var k = Math.sqrt(π * (4 + π)) / 2;
        return [ x * k / (1 + asqrt(1 - y * y * (4 + π) / (4 * π))), y * k / 2 ];
      };
      (d3.geo.eckert3 = function() {
        return projection(eckert3);
      }).raw = eckert3;
      function eckert4(λ, φ) {
        var k = (2 + halfπ) * Math.sin(φ);
        φ /= 2;
        for (var i = 0, δ = Infinity; i < 10 && Math.abs(δ) > ε; i++) {
          var cosφ = Math.cos(φ);
          φ -= δ = (φ + Math.sin(φ) * (cosφ + 2) - k) / (2 * cosφ * (1 + cosφ));
        }
        return [ 2 / Math.sqrt(π * (4 + π)) * λ * (1 + Math.cos(φ)), 2 * Math.sqrt(π / (4 + π)) * Math.sin(φ) ];
      }
      eckert4.invert = function(x, y) {
        var A = .5 * y * Math.sqrt((4 + π) / π), k = asin(A), c = Math.cos(k);
        return [ x / (2 / Math.sqrt(π * (4 + π)) * (1 + c)), asin((k + A * (c + 2)) / (2 + halfπ)) ];
      };
      (d3.geo.eckert4 = function() {
        return projection(eckert4);
      }).raw = eckert4;
      function eckert5(λ, φ) {
        return [ λ * (1 + Math.cos(φ)) / Math.sqrt(2 + π), 2 * φ / Math.sqrt(2 + π) ];
      }
      eckert5.invert = function(x, y) {
        var k = Math.sqrt(2 + π), φ = y * k / 2;
        return [ k * x / (1 + Math.cos(φ)), φ ];
      };
      (d3.geo.eckert5 = function() {
        return projection(eckert5);
      }).raw = eckert5;
      function eckert6(λ, φ) {
        var k = (1 + halfπ) * Math.sin(φ);
        for (var i = 0, δ = Infinity; i < 10 && Math.abs(δ) > ε; i++) {
          φ -= δ = (φ + Math.sin(φ) - k) / (1 + Math.cos(φ));
        }
        k = Math.sqrt(2 + π);
        return [ λ * (1 + Math.cos(φ)) / k, 2 * φ / k ];
      }
      eckert6.invert = function(x, y) {
        var j = 1 + halfπ, k = Math.sqrt(j / 2);
        return [ x * 2 * k / (1 + Math.cos(y *= k)), asin((y + Math.sin(y)) / j) ];
      };
      (d3.geo.eckert6 = function() {
        return projection(eckert6);
      }).raw = eckert6;
      function eisenlohr(λ, φ) {
        var s0 = Math.sin(λ /= 2), c0 = Math.cos(λ), k = Math.sqrt(Math.cos(φ)), c1 = Math.cos(φ /= 2), t = Math.sin(φ) / (c1 + Math.SQRT2 * c0 * k), c = Math.sqrt(2 / (1 + t * t)), v = Math.sqrt((Math.SQRT2 * c1 + (c0 + s0) * k) / (Math.SQRT2 * c1 + (c0 - s0) * k));
        return [ eisenlohrK * (c * (v - 1 / v) - 2 * Math.log(v)), eisenlohrK * (c * t * (v + 1 / v) - 2 * Math.atan(t)) ];
      }
      eisenlohr.invert = function(x, y) {
        var p = d3.geo.august.raw.invert(x / 1.2, y * 1.065);
        if (!p) return null;
        var λ = p[0], φ = p[1], i = 20;
        x /= eisenlohrK, y /= eisenlohrK;
        do {
          var _0 = λ / 2, _1 = φ / 2, s0 = Math.sin(_0), c0 = Math.cos(_0), s1 = Math.sin(_1), c1 = Math.cos(_1), cos1 = Math.cos(φ), k = Math.sqrt(cos1), t = s1 / (c1 + Math.SQRT2 * c0 * k), t2 = t * t, c = Math.sqrt(2 / (1 + t2)), v0 = Math.SQRT2 * c1 + (c0 + s0) * k, v1 = Math.SQRT2 * c1 + (c0 - s0) * k, v2 = v0 / v1, v = Math.sqrt(v2), vm1v = v - 1 / v, vp1v = v + 1 / v, fx = c * vm1v - 2 * Math.log(v) - x, fy = c * t * vp1v - 2 * Math.atan(t) - y, δtδλ = s1 && Math.SQRT1_2 * k * s0 * t2 / s1, δtδφ = (Math.SQRT2 * c0 * c1 + k) / (2 * (c1 + Math.SQRT2 * c0 * k) * (c1 + Math.SQRT2 * c0 * k) * k), δcδt = -.5 * t * c * c * c, δcδλ = δcδt * δtδλ, δcδφ = δcδt * δtδφ, A = (A = 2 * c1 + Math.SQRT2 * k * (c0 - s0)) * A * v, δvδλ = (Math.SQRT2 * c0 * c1 * k + cos1) / A, δvδφ = -(Math.SQRT2 * s0 * s1) / (k * A), δxδλ = vm1v * δcδλ - 2 * δvδλ / v + c * (δvδλ + δvδλ / v2), δxδφ = vm1v * δcδφ - 2 * δvδφ / v + c * (δvδφ + δvδφ / v2), δyδλ = t * vp1v * δcδλ - 2 * δtδλ / (1 + t2) + c * vp1v * δtδλ + c * t * (δvδλ - δvδλ / v2), δyδφ = t * vp1v * δcδφ - 2 * δtδφ / (1 + t2) + c * vp1v * δtδφ + c * t * (δvδφ - δvδφ / v2), denominator = δxδφ * δyδλ - δyδφ * δxδλ;
          if (!denominator) break;
          var δλ = (fy * δxδφ - fx * δyδφ) / denominator, δφ = (fx * δyδλ - fy * δxδλ) / denominator;
          λ -= δλ;
          φ = Math.max(-halfπ, Math.min(halfπ, φ - δφ));
        } while ((Math.abs(δλ) > ε || Math.abs(δφ) > ε) && --i > 0);
        return Math.abs(Math.abs(φ) - halfπ) < ε ? [ 0, φ ] : i && [ λ, φ ];
      };
      var eisenlohrK = 3 + 2 * Math.SQRT2;
      (d3.geo.eisenlohr = function() {
        return projection(eisenlohr);
      }).raw = eisenlohr;
      function fahey(λ, φ) {
        var t = Math.tan(φ / 2);
        return [ λ * faheyK * asqrt(1 - t * t), (1 + faheyK) * t ];
      }
      fahey.invert = function(x, y) {
        var t = y / (1 + faheyK);
        return [ x ? x / (faheyK * asqrt(1 - t * t)) : 0, 2 * Math.atan(t) ];
      };
      var faheyK = Math.cos(35 * radians);
      (d3.geo.fahey = function() {
        return projection(fahey);
      }).raw = fahey;
      function foucaut(λ, φ) {
        var k = φ / 2, cosk = Math.cos(k);
        return [ 2 * λ / sqrtπ * Math.cos(φ) * cosk * cosk, sqrtπ * Math.tan(k) ];
      }
      foucaut.invert = function(x, y) {
        var k = Math.atan(y / sqrtπ), cosk = Math.cos(k), φ = 2 * k;
        return [ x * sqrtπ * .5 / (Math.cos(φ) * cosk * cosk), φ ];
      };
      (d3.geo.foucaut = function() {
        return projection(foucaut);
      }).raw = foucaut;
      d3.geo.gilbert = function(projection) {
        var e = d3.geo.equirectangular().scale(degrees).translate([ 0, 0 ]);
        function gilbert(coordinates) {
          return projection([ coordinates[0] * .5, asin(Math.tan(coordinates[1] * .5 * radians)) * degrees ]);
        }
        if (projection.invert) gilbert.invert = function(coordinates) {
          coordinates = projection.invert(coordinates);
          coordinates[0] *= 2;
          coordinates[1] = 2 * Math.atan(Math.sin(coordinates[1] * radians)) * degrees;
          return coordinates;
        };
        gilbert.stream = function(stream) {
          stream = projection.stream(stream);
          var s = e.stream({
            point: function(λ, φ) {
              stream.point(λ * .5, asin(Math.tan(-φ * .5 * radians)) * degrees);
            },
            lineStart: function() {
              stream.lineStart();
            },
            lineEnd: function() {
              stream.lineEnd();
            },
            polygonStart: function() {
              stream.polygonStart();
            },
            polygonEnd: function() {
              stream.polygonEnd();
            }
          });
          s.sphere = function() {
            stream.sphere();
          };
          s.valid = false;
          return s;
        };
        return gilbert;
      };
      var gingeryAzimuthalEquidistant = d3.geo.azimuthalEquidistant.raw;
      function gingery(ρ, n) {
        var k = 2 * π / n, ρ2 = ρ * ρ;
        function forward(λ, φ) {
          var p = gingeryAzimuthalEquidistant(λ, φ), x = p[0], y = p[1], r2 = x * x + y * y;
          if (r2 > ρ2) {
            var r = Math.sqrt(r2), θ = Math.atan2(y, x), θ0 = k * Math.round(θ / k), α = θ - θ0, ρcosα = ρ * Math.cos(α), k_ = (ρ * Math.sin(α) - α * Math.sin(ρcosα)) / (halfπ - ρcosα), s_ = arcLength_(α, k_), e = (π - ρ) / gingeryIntegrate(s_, ρcosα, π);
            x = r;
            var i = 50, δ;
            do {
              x -= δ = (ρ + gingeryIntegrate(s_, ρcosα, x) * e - r) / (s_(x) * e);
            } while (Math.abs(δ) > ε && --i > 0);
            y = α * Math.sin(x);
            if (x < halfπ) y -= k_ * (x - halfπ);
            var s = Math.sin(θ0), c = Math.cos(θ0);
            p[0] = x * c - y * s;
            p[1] = x * s + y * c;
          }
          return p;
        }
        forward.invert = function(x, y) {
          var r2 = x * x + y * y;
          if (r2 > ρ2) {
            var r = Math.sqrt(r2), θ = Math.atan2(y, x), θ0 = k * Math.round(θ / k), dθ = θ - θ0, x = r * Math.cos(dθ);
            y = r * Math.sin(dθ);
            var x_halfπ = x - halfπ, sinx = Math.sin(x), α = y / sinx, δ = x < halfπ ? Infinity : 0, i = 10;
            while (true) {
              var ρsinα = ρ * Math.sin(α), ρcosα = ρ * Math.cos(α), sinρcosα = Math.sin(ρcosα), halfπ_ρcosα = halfπ - ρcosα, k_ = (ρsinα - α * sinρcosα) / halfπ_ρcosα, s_ = arcLength_(α, k_);
              if (Math.abs(δ) < ε2 || !--i) break;
              α -= δ = (α * sinx - k_ * x_halfπ - y) / (sinx - x_halfπ * 2 * (halfπ_ρcosα * (ρcosα + α * ρsinα * Math.cos(ρcosα) - sinρcosα) - ρsinα * (ρsinα - α * sinρcosα)) / (halfπ_ρcosα * halfπ_ρcosα));
            }
            r = ρ + gingeryIntegrate(s_, ρcosα, x) * (π - ρ) / gingeryIntegrate(s_, ρcosα, π);
            θ = θ0 + α;
            x = r * Math.cos(θ);
            y = r * Math.sin(θ);
          }
          return gingeryAzimuthalEquidistant.invert(x, y);
        };
        return forward;
      }
      function arcLength_(α, k) {
        return function(x) {
          var y_ = α * Math.cos(x);
          if (x < halfπ) y_ -= k;
          return Math.sqrt(1 + y_ * y_);
        };
      }
      function gingeryProjection() {
        var n = 6, ρ = 30 * radians, cρ = Math.cos(ρ), sρ = Math.sin(ρ), m = projectionMutator(gingery), p = m(ρ, n), stream_ = p.stream, ε = .01, cr = -Math.cos(ε * radians), sr = Math.sin(ε * radians);
        p.radius = function(_) {
          if (!arguments.length) return ρ * degrees;
          cρ = Math.cos(ρ = _ * radians);
          sρ = Math.sin(ρ);
          return m(ρ, n);
        };
        p.lobes = function(_) {
          if (!arguments.length) return n;
          return m(ρ, n = +_);
        };
        p.stream = function(stream) {
          var rotate = p.rotate(), rotateStream = stream_(stream), sphereStream = (p.rotate([ 0, 0 ]), 
          stream_(stream));
          p.rotate(rotate);
          rotateStream.sphere = function() {
            sphereStream.polygonStart(), sphereStream.lineStart();
            for (var i = 0, δ = 2 * π / n, φ = 0; i < n; ++i, φ -= δ) {
              sphereStream.point(Math.atan2(sr * Math.cos(φ), cr) * degrees, Math.asin(sr * Math.sin(φ)) * degrees);
              sphereStream.point(Math.atan2(sρ * Math.cos(φ - δ / 2), cρ) * degrees, Math.asin(sρ * Math.sin(φ - δ / 2)) * degrees);
            }
            sphereStream.lineEnd(), sphereStream.polygonEnd();
          };
          return rotateStream;
        };
        return p;
      }
      function gingeryIntegrate(f, a, b) {
        var n = 50, h = (b - a) / n, s = f(a) + f(b);
        for (var i = 1, x = a; i < n; ++i) s += 2 * f(x += h);
        return s * .5 * h;
      }
      (d3.geo.gingery = gingeryProjection).raw = gingery;
      function ginzburgPolyconic(a, b, c, d, e, f, g, h) {
        if (arguments.length < 8) h = 0;
        function forward(λ, φ) {
          if (!φ) return [ a * λ / π, 0 ];
          var φ2 = φ * φ, xB = a + φ2 * (b + φ2 * (c + φ2 * d)), yB = φ * (e - 1 + φ2 * (f - h + φ2 * g)), m = (xB * xB + yB * yB) / (2 * yB), α = λ * Math.asin(xB / m) / π;
          return [ m * Math.sin(α), φ * (1 + φ2 * h) + m * (1 - Math.cos(α)) ];
        }
        forward.invert = function(x, y) {
          var λ = π * x / a, φ = y, δλ, δφ, i = 50;
          do {
            var φ2 = φ * φ, xB = a + φ2 * (b + φ2 * (c + φ2 * d)), yB = φ * (e - 1 + φ2 * (f - h + φ2 * g)), p = xB * xB + yB * yB, q = 2 * yB, m = p / q, m2 = m * m, dαdλ = Math.asin(xB / m) / π, α = λ * dαdλ;
            xB2 = xB * xB, dxBdφ = (2 * b + φ2 * (4 * c + φ2 * 6 * d)) * φ, dyBdφ = e + φ2 * (3 * f + φ2 * 5 * g), 
            dpdφ = 2 * (xB * dxBdφ + yB * (dyBdφ - 1)), dqdφ = 2 * (dyBdφ - 1), dmdφ = (dpdφ * q - p * dqdφ) / (q * q), 
            cosα = Math.cos(α), sinα = Math.sin(α), mcosα = m * cosα, msinα = m * sinα, dαdφ = λ / π * (1 / asqrt(1 - xB2 / m2)) * (dxBdφ * m - xB * dmdφ) / m2, 
            fx = msinα - x, fy = φ * (1 + φ2 * h) + m - mcosα - y, δxδφ = dmdφ * sinα + mcosα * dαdφ, 
            δxδλ = mcosα * dαdλ, δyδφ = 1 + dmdφ - (dmdφ * cosα - msinα * dαdφ), δyδλ = msinα * dαdλ, 
            denominator = δxδφ * δyδλ - δyδφ * δxδλ;
            if (!denominator) break;
            λ -= δλ = (fy * δxδφ - fx * δyδφ) / denominator;
            φ -= δφ = (fx * δyδλ - fy * δxδλ) / denominator;
          } while ((Math.abs(δλ) > ε || Math.abs(δφ) > ε) && --i > 0);
          return [ λ, φ ];
        };
        return forward;
      }
      var ginzburg4 = ginzburgPolyconic(2.8284, -1.6988, .75432, -.18071, 1.76003, -.38914, .042555);
      (d3.geo.ginzburg4 = function() {
        return projection(ginzburg4);
      }).raw = ginzburg4;
      var ginzburg5 = ginzburgPolyconic(2.583819, -.835827, .170354, -.038094, 1.543313, -.411435, .082742);
      (d3.geo.ginzburg5 = function() {
        return projection(ginzburg5);
      }).raw = ginzburg5;
      var ginzburg6 = ginzburgPolyconic(5 / 6 * π, -.62636, -.0344, 0, 1.3493, -.05524, 0, .045);
      (d3.geo.ginzburg6 = function() {
        return projection(ginzburg6);
      }).raw = ginzburg6;
      function ginzburg8(λ, φ) {
        var λ2 = λ * λ, φ2 = φ * φ;
        return [ λ * (1 - .162388 * φ2) * (.87 - 952426e-9 * λ2 * λ2), φ * (1 + φ2 / 12) ];
      }
      ginzburg8.invert = function(x, y) {
        var λ = x, φ = y, i = 50, δ;
        do {
          var φ2 = φ * φ;
          φ -= δ = (φ * (1 + φ2 / 12) - y) / (1 + φ2 / 4);
        } while (Math.abs(δ) > ε && --i > 0);
        i = 50;
        x /= 1 - .162388 * φ2;
        do {
          var λ4 = (λ4 = λ * λ) * λ4;
          λ -= δ = (λ * (.87 - 952426e-9 * λ4) - x) / (.87 - .00476213 * λ4);
        } while (Math.abs(δ) > ε && --i > 0);
        return [ λ, φ ];
      };
      (d3.geo.ginzburg8 = function() {
        return projection(ginzburg8);
      }).raw = ginzburg8;
      var ginzburg9 = ginzburgPolyconic(2.6516, -.76534, .19123, -.047094, 1.36289, -.13965, .031762);
      (d3.geo.ginzburg9 = function() {
        return projection(ginzburg9);
      }).raw = ginzburg9;
      function quincuncialProjection(projectHemisphere) {
        var dx = projectHemisphere(halfπ, 0)[0] - projectHemisphere(-halfπ, 0)[0];
        function projection() {
          var quincuncial = false, m = projectionMutator(projectAt), p = m(quincuncial);
          p.quincuncial = function(_) {
            if (!arguments.length) return quincuncial;
            return m(quincuncial = !!_);
          };
          return p;
        }
        function projectAt(quincuncial) {
          var forward = quincuncial ? function(λ, φ) {
            var t = Math.abs(λ) < halfπ, p = projectHemisphere(t ? λ : λ > 0 ? λ - π : λ + π, φ);
            var x = (p[0] - p[1]) * Math.SQRT1_2, y = (p[0] + p[1]) * Math.SQRT1_2;
            if (t) return [ x, y ];
            var d = dx * Math.SQRT1_2, s = x > 0 ^ y > 0 ? -1 : 1;
            return [ s * x - sgn(y) * d, s * y - sgn(x) * d ];
          } : function(λ, φ) {
            var s = λ > 0 ? -.5 : .5, point = projectHemisphere(λ + s * π, φ);
            point[0] -= s * dx;
            return point;
          };
          if (projectHemisphere.invert) forward.invert = quincuncial ? function(x0, y0) {
            var x = (x0 + y0) * Math.SQRT1_2, y = (y0 - x0) * Math.SQRT1_2, t = Math.abs(x) < .5 * dx && Math.abs(y) < .5 * dx;
            if (!t) {
              var d = dx * Math.SQRT1_2, s = x > 0 ^ y > 0 ? -1 : 1, x1 = -s * (x0 + (y > 0 ? 1 : -1) * d), y1 = -s * (y0 + (x > 0 ? 1 : -1) * d);
              x = (-x1 - y1) * Math.SQRT1_2;
              y = (x1 - y1) * Math.SQRT1_2;
            }
            var p = projectHemisphere.invert(x, y);
            if (!t) p[0] += x > 0 ? π : -π;
            return p;
          } : function(x, y) {
            var s = x > 0 ? -.5 : .5, location = projectHemisphere.invert(x + s * dx, y), λ = location[0] - s * π;
            if (λ < -π) λ += 2 * π; else if (λ > π) λ -= 2 * π;
            location[0] = λ;
            return location;
          };
          return forward;
        }
        projection.raw = projectAt;
        return projection;
      }
      function gringorten(λ, φ) {
        var sλ = sgn(λ), sφ = sgn(φ), cosφ = Math.cos(φ), x = Math.cos(λ) * cosφ, y = Math.sin(λ) * cosφ, z = Math.sin(sφ * φ);
        λ = Math.abs(Math.atan2(y, z));
        φ = asin(x);
        if (Math.abs(λ - halfπ) > ε) λ %= halfπ;
        var point = gringortenHexadecant(λ > π / 4 ? halfπ - λ : λ, φ);
        if (λ > π / 4) z = point[0], point[0] = -point[1], point[1] = -z;
        return point[0] *= sλ, point[1] *= -sφ, point;
      }
      gringorten.invert = function(x, y) {
        var sx = sgn(x), sy = sgn(y), x0 = -sx * x, y0 = -sy * y, t = y0 / x0 < 1, p = gringortenHexadecantInvert(t ? y0 : x0, t ? x0 : y0), λ = p[0], φ = p[1];
        if (t) λ = -halfπ - λ;
        var cosφ = Math.cos(φ), x = Math.cos(λ) * cosφ, y = Math.sin(λ) * cosφ, z = Math.sin(φ);
        return [ sx * (Math.atan2(y, -z) + π), sy * asin(x) ];
      };
      function gringortenHexadecant(λ, φ) {
        if (φ === halfπ) return [ 0, 0 ];
        var sinφ = Math.sin(φ), r = sinφ * sinφ, r2 = r * r, j = 1 + r2, k = 1 + 3 * r2, q = 1 - r2, z = asin(1 / Math.sqrt(j)), v = q + r * j * z, p2 = (1 - sinφ) / v, p = Math.sqrt(p2), a2 = p2 * j, a = Math.sqrt(a2), h = p * q;
        if (λ === 0) return [ 0, -(h + r * a) ];
        var cosφ = Math.cos(φ), secφ = 1 / cosφ, drdφ = 2 * sinφ * cosφ, dvdφ = (-3 * r + z * k) * drdφ, dp2dφ = (-v * cosφ - (1 - sinφ) * dvdφ) / (v * v), dpdφ = .5 * dp2dφ / p, dhdφ = q * dpdφ - 2 * r * p * drdφ, dra2dφ = r * j * dp2dφ + p2 * k * drdφ, μ = -secφ * drdφ, ν = -secφ * dra2dφ, ζ = -2 * secφ * dhdφ, Λ = 4 * λ / π;
        if (λ > .222 * π || φ < π / 4 && λ > .175 * π) {
          var x = (h + r * asqrt(a2 * (1 + r2) - h * h)) / (1 + r2);
          if (λ > π / 4) return [ x, x ];
          var x1 = x, x0 = .5 * x, i = 50;
          x = .5 * (x0 + x1);
          do {
            var g = Math.sqrt(a2 - x * x), f = x * (ζ + μ * g) + ν * asin(x / a) - Λ;
            if (!f) break;
            if (f < 0) x0 = x; else x1 = x;
            x = .5 * (x0 + x1);
          } while (Math.abs(x1 - x0) > ε && --i > 0);
        } else {
          var x = ε, i = 25, δ;
          do {
            var x2 = x * x, g = asqrt(a2 - x2), ζμg = ζ + μ * g, f = x * ζμg + ν * asin(x / a) - Λ, df = ζμg + (ν - μ * x2) / g;
            x -= δ = g ? f / df : 0;
          } while (Math.abs(δ) > ε && --i > 0);
        }
        return [ x, -h - r * asqrt(a2 - x * x) ];
      }
      function gringortenHexadecantInvert(x, y) {
        var x0 = 0, x1 = 1, r = .5, i = 50;
        while (true) {
          var r2 = r * r, sinφ = Math.sqrt(r), z = Math.asin(1 / Math.sqrt(1 + r2)), v = 1 - r2 + r * (1 + r2) * z, p2 = (1 - sinφ) / v, p = Math.sqrt(p2), a2 = p2 * (1 + r2), h = p * (1 - r2), g2 = a2 - x * x, g = Math.sqrt(g2), y0 = y + h + r * g;
          if (Math.abs(x1 - x0) < ε2 || --i === 0 || y0 === 0) break;
          if (y0 > 0) x0 = r; else x1 = r;
          r = .5 * (x0 + x1);
        }
        if (!i) return null;
        var φ = Math.asin(sinφ), cosφ = Math.cos(φ), secφ = 1 / cosφ, drdφ = 2 * sinφ * cosφ, dvdφ = (-3 * r + z * (1 + 3 * r2)) * drdφ, dp2dφ = (-v * cosφ - (1 - sinφ) * dvdφ) / (v * v), dpdφ = .5 * dp2dφ / p, dhdφ = (1 - r2) * dpdφ - 2 * r * p * drdφ, ζ = -2 * secφ * dhdφ, μ = -secφ * drdφ, ν = -secφ * (r * (1 + r2) * dp2dφ + p2 * (1 + 3 * r2) * drdφ);
        return [ π / 4 * (x * (ζ + μ * g) + ν * Math.asin(x / Math.sqrt(a2))), φ ];
      }
      d3.geo.gringorten = quincuncialProjection(gringorten);
      function ellipticJi(u, v, m) {
        if (!u) {
          var b = ellipticJ(v, 1 - m);
          return [ [ 0, b[0] / b[1] ], [ 1 / b[1], 0 ], [ b[2] / b[1], 0 ] ];
        }
        var a = ellipticJ(u, m);
        if (!v) return [ [ a[0], 0 ], [ a[1], 0 ], [ a[2], 0 ] ];
        var b = ellipticJ(v, 1 - m), denominator = b[1] * b[1] + m * a[0] * a[0] * b[0] * b[0];
        return [ [ a[0] * b[2] / denominator, a[1] * a[2] * b[0] * b[1] / denominator ], [ a[1] * b[1] / denominator, -a[0] * a[2] * b[0] * b[2] / denominator ], [ a[2] * b[1] * b[2] / denominator, -m * a[0] * a[1] * b[0] / denominator ] ];
      }
      function ellipticJ(u, m) {
        var ai, b, φ, t, twon;
        if (m < ε) {
          t = Math.sin(u);
          b = Math.cos(u);
          ai = .25 * m * (u - t * b);
          return [ t - ai * b, b + ai * t, 1 - .5 * m * t * t, u - ai ];
        }
        if (m >= 1 - ε) {
          ai = .25 * (1 - m);
          b = cosh(u);
          t = tanh(u);
          φ = 1 / b;
          twon = b * sinh(u);
          return [ t + ai * (twon - u) / (b * b), φ - ai * t * φ * (twon - u), φ + ai * t * φ * (twon + u), 2 * Math.atan(Math.exp(u)) - halfπ + ai * (twon - u) / b ];
        }
        var a = [ 1, 0, 0, 0, 0, 0, 0, 0, 0 ], c = [ Math.sqrt(m), 0, 0, 0, 0, 0, 0, 0, 0 ], i = 0;
        b = Math.sqrt(1 - m);
        twon = 1;
        while (Math.abs(c[i] / a[i]) > ε && i < 8) {
          ai = a[i++];
          c[i] = .5 * (ai - b);
          a[i] = .5 * (ai + b);
          b = asqrt(ai * b);
          twon *= 2;
        }
        φ = twon * a[i] * u;
        do {
          t = c[i] * Math.sin(b = φ) / a[i];
          φ = .5 * (asin(t) + φ);
        } while (--i);
        return [ Math.sin(φ), t = Math.cos(φ), t / Math.cos(φ - b), φ ];
      }
      function ellipticFi(φ, ψ, m) {
        var r = Math.abs(φ), i = Math.abs(ψ), sinhψ = sinh(i);
        if (r) {
          var cscφ = 1 / Math.sin(r), cotφ2 = 1 / (Math.tan(r) * Math.tan(r)), b = -(cotφ2 + m * sinhψ * sinhψ * cscφ * cscφ - 1 + m), c = (m - 1) * cotφ2, cotλ2 = .5 * (-b + Math.sqrt(b * b - 4 * c));
          return [ ellipticF(Math.atan(1 / Math.sqrt(cotλ2)), m) * sgn(φ), ellipticF(Math.atan(asqrt((cotλ2 / cotφ2 - 1) / m)), 1 - m) * sgn(ψ) ];
        }
        return [ 0, ellipticF(Math.atan(sinhψ), 1 - m) * sgn(ψ) ];
      }
      function ellipticF(φ, m) {
        if (!m) return φ;
        if (m === 1) return Math.log(Math.tan(φ / 2 + π / 4));
        var a = 1, b = Math.sqrt(1 - m), c = Math.sqrt(m);
        for (var i = 0; Math.abs(c) > ε; i++) {
          if (φ % π) {
            var dφ = Math.atan(b * Math.tan(φ) / a);
            if (dφ < 0) dφ += π;
            φ += dφ + ~~(φ / π) * π;
          } else φ += φ;
          c = (a + b) / 2;
          b = Math.sqrt(a * b);
          c = ((a = c) - b) / 2;
        }
        return φ / (Math.pow(2, i) * a);
      }
      function guyou(λ, φ) {
        var k_ = (Math.SQRT2 - 1) / (Math.SQRT2 + 1), k = Math.sqrt(1 - k_ * k_), K = ellipticF(halfπ, k * k), f = -1;
        var ψ = Math.log(Math.tan(π / 4 + Math.abs(φ) / 2)), r = Math.exp(f * ψ) / Math.sqrt(k_), at = guyouComplexAtan(r * Math.cos(f * λ), r * Math.sin(f * λ)), t = ellipticFi(at[0], at[1], k * k);
        return [ -t[1], (φ >= 0 ? 1 : -1) * (.5 * K - t[0]) ];
      }
      function guyouComplexAtan(x, y) {
        var x2 = x * x, y_1 = y + 1, t = 1 - x2 - y * y;
        return [ .5 * ((x >= 0 ? halfπ : -halfπ) - Math.atan2(t, 2 * x)), -.25 * Math.log(t * t + 4 * x2) + .5 * Math.log(y_1 * y_1 + x2) ];
      }
      function guyouComplexDivide(a, b) {
        var denominator = b[0] * b[0] + b[1] * b[1];
        return [ (a[0] * b[0] + a[1] * b[1]) / denominator, (a[1] * b[0] - a[0] * b[1]) / denominator ];
      }
      guyou.invert = function(x, y) {
        var k_ = (Math.SQRT2 - 1) / (Math.SQRT2 + 1), k = Math.sqrt(1 - k_ * k_), K = ellipticF(halfπ, k * k), f = -1;
        var j = ellipticJi(.5 * K - y, -x, k * k), tn = guyouComplexDivide(j[0], j[1]), λ = Math.atan2(tn[1], tn[0]) / f;
        return [ λ, 2 * Math.atan(Math.exp(.5 / f * Math.log(k_ * tn[0] * tn[0] + k_ * tn[1] * tn[1]))) - halfπ ];
      };
      d3.geo.guyou = quincuncialProjection(guyou);
      function hammerRetroazimuthal(φ0) {
        var sinφ0 = Math.sin(φ0), cosφ0 = Math.cos(φ0), rotate = hammerRetroazimuthalRotation(φ0);
        rotate.invert = hammerRetroazimuthalRotation(-φ0);
        function forward(λ, φ) {
          var p = rotate(λ, φ);
          λ = p[0], φ = p[1];
          var sinφ = Math.sin(φ), cosφ = Math.cos(φ), cosλ = Math.cos(λ), z = acos(sinφ0 * sinφ + cosφ0 * cosφ * cosλ), sinz = Math.sin(z), K = Math.abs(sinz) > ε ? z / sinz : 1;
          return [ K * cosφ0 * Math.sin(λ), (Math.abs(λ) > halfπ ? K : -K) * (sinφ0 * cosφ - cosφ0 * sinφ * cosλ) ];
        }
        forward.invert = function(x, y) {
          var ρ = Math.sqrt(x * x + y * y), sinz = -Math.sin(ρ), cosz = Math.cos(ρ), a = ρ * cosz, b = -y * sinz, c = ρ * sinφ0, d = asqrt(a * a + b * b - c * c), φ = Math.atan2(a * c + b * d, b * c - a * d), λ = (ρ > halfπ ? -1 : 1) * Math.atan2(x * sinz, ρ * Math.cos(φ) * cosz + y * Math.sin(φ) * sinz);
          return rotate.invert(λ, φ);
        };
        return forward;
      }
      function hammerRetroazimuthalRotation(φ0) {
        var sinφ0 = Math.sin(φ0), cosφ0 = Math.cos(φ0);
        return function(λ, φ) {
          var cosφ = Math.cos(φ), x = Math.cos(λ) * cosφ, y = Math.sin(λ) * cosφ, z = Math.sin(φ);
          return [ Math.atan2(y, x * cosφ0 - z * sinφ0), asin(z * cosφ0 + x * sinφ0) ];
        };
      }
      function hammerRetroazimuthalProjection() {
        var φ0 = 0, m = projectionMutator(hammerRetroazimuthal), p = m(φ0), rotate_ = p.rotate, stream_ = p.stream, circle = d3.geo.circle();
        p.parallel = function(_) {
          if (!arguments.length) return φ0 / π * 180;
          var r = p.rotate();
          return m(φ0 = _ * π / 180).rotate(r);
        };
        p.rotate = function(_) {
          if (!arguments.length) return _ = rotate_.call(p), _[1] += φ0 / π * 180, _;
          rotate_.call(p, [ _[0], _[1] - φ0 / π * 180 ]);
          circle.origin([ -_[0], -_[1] ]);
          return p;
        };
        p.stream = function(stream) {
          stream = stream_(stream);
          stream.sphere = function() {
            stream.polygonStart();
            var ε = .01, ring = circle.angle(90 - ε)().coordinates[0], n = ring.length - 1, i = -1, p;
            stream.lineStart();
            while (++i < n) stream.point((p = ring[i])[0], p[1]);
            stream.lineEnd();
            ring = circle.angle(90 + ε)().coordinates[0];
            n = ring.length - 1;
            stream.lineStart();
            while (--i >= 0) stream.point((p = ring[i])[0], p[1]);
            stream.lineEnd();
            stream.polygonEnd();
          };
          return stream;
        };
        return p;
      }
      (d3.geo.hammerRetroazimuthal = hammerRetroazimuthalProjection).raw = hammerRetroazimuthal;
      var hammerAzimuthalEqualArea = d3.geo.azimuthalEqualArea.raw;
      function hammer(A, B) {
        if (arguments.length < 2) B = A;
        if (B === 1) return hammerAzimuthalEqualArea;
        if (B === Infinity) return hammerQuarticAuthalic;
        function forward(λ, φ) {
          var coordinates = hammerAzimuthalEqualArea(λ / B, φ);
          coordinates[0] *= A;
          return coordinates;
        }
        forward.invert = function(x, y) {
          var coordinates = hammerAzimuthalEqualArea.invert(x / A, y);
          coordinates[0] *= B;
          return coordinates;
        };
        return forward;
      }
      function hammerProjection() {
        var B = 2, m = projectionMutator(hammer), p = m(B);
        p.coefficient = function(_) {
          if (!arguments.length) return B;
          return m(B = +_);
        };
        return p;
      }
      function hammerQuarticAuthalic(λ, φ) {
        return [ λ * Math.cos(φ) / Math.cos(φ /= 2), 2 * Math.sin(φ) ];
      }
      hammerQuarticAuthalic.invert = function(x, y) {
        var φ = 2 * asin(y / 2);
        return [ x * Math.cos(φ / 2) / Math.cos(φ), φ ];
      };
      (d3.geo.hammer = hammerProjection).raw = hammer;
      function hatano(λ, φ) {
        var c = Math.sin(φ) * (φ < 0 ? 2.43763 : 2.67595);
        for (var i = 0, δ; i < 20; i++) {
          φ -= δ = (φ + Math.sin(φ) - c) / (1 + Math.cos(φ));
          if (Math.abs(δ) < ε) break;
        }
        return [ .85 * λ * Math.cos(φ *= .5), Math.sin(φ) * (φ < 0 ? 1.93052 : 1.75859) ];
      }
      hatano.invert = function(x, y) {
        var θ = Math.abs(θ = y * (y < 0 ? .5179951515653813 : .5686373742600607)) > 1 - ε ? θ > 0 ? halfπ : -halfπ : asin(θ);
        return [ 1.1764705882352942 * x / Math.cos(θ), Math.abs(θ = ((θ += θ) + Math.sin(θ)) * (y < 0 ? .4102345310814193 : .3736990601468637)) > 1 - ε ? θ > 0 ? halfπ : -halfπ : asin(θ) ];
      };
      (d3.geo.hatano = function() {
        return projection(hatano);
      }).raw = hatano;
      var healpixParallel = 41 + 48 / 36 + 37 / 3600;
      function healpix(h) {
        var lambert = d3.geo.cylindricalEqualArea.raw(0), φ0 = healpixParallel * π / 180, dx0 = 2 * π, dx1 = d3.geo.collignon.raw(π, φ0)[0] - d3.geo.collignon.raw(-π, φ0)[0], y0 = lambert(0, φ0)[1], y1 = d3.geo.collignon.raw(0, φ0)[1], dy1 = d3.geo.collignon.raw(0, halfπ)[1] - y1, k = 2 * π / h;
        function forward(λ, φ) {
          var point, φ2 = Math.abs(φ);
          if (φ2 > φ0) {
            var i = Math.min(h - 1, Math.max(0, Math.floor((λ + π) / k)));
            λ += π * (h - 1) / h - i * k;
            point = d3.geo.collignon.raw(λ, φ2);
            point[0] = point[0] * dx0 / dx1 - dx0 * (h - 1) / (2 * h) + i * dx0 / h;
            point[1] = y0 + (point[1] - y1) * 4 * dy1 / dx0;
            if (φ < 0) point[1] = -point[1];
          } else {
            point = lambert(λ, φ);
          }
          point[0] /= 2;
          return point;
        }
        forward.invert = function(x, y) {
          x *= 2;
          var y2 = Math.abs(y);
          if (y2 > y0) {
            var i = Math.min(h - 1, Math.max(0, Math.floor((x + π) / k)));
            x = (x + π * (h - 1) / h - i * k) * dx1 / dx0;
            var point = d3.geo.collignon.raw.invert(x, .25 * (y2 - y0) * dx0 / dy1 + y1);
            point[0] -= π * (h - 1) / h - i * k;
            if (y < 0) point[1] = -point[1];
            return point;
          }
          return lambert.invert(x, y);
        };
        return forward;
      }
      function healpixProjection() {
        var n = 2, m = projectionMutator(healpix), p = m(n), stream_ = p.stream;
        p.lobes = function(_) {
          if (!arguments.length) return n;
          return m(n = +_);
        };
        p.stream = function(stream) {
          var rotate = p.rotate(), rotateStream = stream_(stream), sphereStream = (p.rotate([ 0, 0 ]), 
          stream_(stream));
          p.rotate(rotate);
          rotateStream.sphere = function() {
            d3.geo.stream(sphere(), sphereStream);
          };
          return rotateStream;
        };
        function sphere() {
          var step = 180 / n;
          return {
            type: "Polygon",
            coordinates: [ d3.range(-180, 180 + step / 2, step).map(function(x, i) {
              return [ x, i & 1 ? 90 - 1e-6 : healpixParallel ];
            }).concat(d3.range(180, -180 - step / 2, -step).map(function(x, i) {
              return [ x, i & 1 ? -90 + 1e-6 : -healpixParallel ];
            })) ]
          };
        }
        return p;
      }
      (d3.geo.healpix = healpixProjection).raw = healpix;
      function hill(K) {
        var L = 1 + K, sinβ = Math.sin(1 / L), β = asin(sinβ), A = 2 * Math.sqrt(π / (B = π + 4 * β * L)), B, ρ0 = .5 * A * (L + Math.sqrt(K * (2 + K))), K2 = K * K, L2 = L * L;
        function forward(λ, φ) {
          var t = 1 - Math.sin(φ), ρ, ω;
          if (t && t < 2) {
            var θ = halfπ - φ, i = 25, δ;
            do {
              var sinθ = Math.sin(θ), cosθ = Math.cos(θ), β_β1 = β + Math.atan2(sinθ, L - cosθ), C = 1 + L2 - 2 * L * cosθ;
              θ -= δ = (θ - K2 * β - L * sinθ + C * β_β1 - .5 * t * B) / (2 * L * sinθ * β_β1);
            } while (Math.abs(δ) > ε2 && --i > 0);
            ρ = A * Math.sqrt(C);
            ω = λ * β_β1 / π;
          } else {
            ρ = A * (K + t);
            ω = λ * β / π;
          }
          return [ ρ * Math.sin(ω), ρ0 - ρ * Math.cos(ω) ];
        }
        forward.invert = function(x, y) {
          var ρ2 = x * x + (y -= ρ0) * y, cosθ = (1 + L2 - ρ2 / (A * A)) / (2 * L), θ = acos(cosθ), sinθ = Math.sin(θ), β_β1 = β + Math.atan2(sinθ, L - cosθ);
          return [ asin(x / Math.sqrt(ρ2)) * π / β_β1, asin(1 - 2 * (θ - K2 * β - L * sinθ + (1 + L2 - 2 * L * cosθ) * β_β1) / B) ];
        };
        return forward;
      }
      function hillProjection() {
        var K = 1, m = projectionMutator(hill), p = m(K);
        p.ratio = function(_) {
          if (!arguments.length) return K;
          return m(K = +_);
        };
        return p;
      }
      (d3.geo.hill = hillProjection).raw = hill;
      var sinuMollweideφ = .7109889596207567, sinuMollweideY = .0528035274542;
      function sinuMollweide(λ, φ) {
        return φ > -sinuMollweideφ ? (λ = mollweide(λ, φ), λ[1] += sinuMollweideY, λ) : sinusoidal(λ, φ);
      }
      sinuMollweide.invert = function(x, y) {
        return y > -sinuMollweideφ ? mollweide.invert(x, y - sinuMollweideY) : sinusoidal.invert(x, y);
      };
      (d3.geo.sinuMollweide = function() {
        return projection(sinuMollweide).rotate([ -20, -55 ]);
      }).raw = sinuMollweide;
      function homolosine(λ, φ) {
        return Math.abs(φ) > sinuMollweideφ ? (λ = mollweide(λ, φ), λ[1] -= φ > 0 ? sinuMollweideY : -sinuMollweideY, 
        λ) : sinusoidal(λ, φ);
      }
      homolosine.invert = function(x, y) {
        return Math.abs(y) > sinuMollweideφ ? mollweide.invert(x, y + (y > 0 ? sinuMollweideY : -sinuMollweideY)) : sinusoidal.invert(x, y);
      };
      (d3.geo.homolosine = function() {
        return projection(homolosine);
      }).raw = homolosine;
      function kavrayskiy7(λ, φ) {
        return [ 3 * λ / (2 * π) * Math.sqrt(π * π / 3 - φ * φ), φ ];
      }
      kavrayskiy7.invert = function(x, y) {
        return [ 2 / 3 * π * x / Math.sqrt(π * π / 3 - y * y), y ];
      };
      (d3.geo.kavrayskiy7 = function() {
        return projection(kavrayskiy7);
      }).raw = kavrayskiy7;
      function lagrange(n) {
        function forward(λ, φ) {
          if (Math.abs(Math.abs(φ) - halfπ) < ε) return [ 0, φ < 0 ? -2 : 2 ];
          var sinφ = Math.sin(φ), v = Math.pow((1 + sinφ) / (1 - sinφ), n / 2), c = .5 * (v + 1 / v) + Math.cos(λ *= n);
          return [ 2 * Math.sin(λ) / c, (v - 1 / v) / c ];
        }
        forward.invert = function(x, y) {
          var y0 = Math.abs(y);
          if (Math.abs(y0 - 2) < ε) return x ? null : [ 0, sgn(y) * halfπ ];
          if (y0 > 2) return null;
          x /= 2, y /= 2;
          var x2 = x * x, y2 = y * y, t = 2 * y / (1 + x2 + y2);
          t = Math.pow((1 + t) / (1 - t), 1 / n);
          return [ Math.atan2(2 * x, 1 - x2 - y2) / n, asin((t - 1) / (t + 1)) ];
        };
        return forward;
      }
      function lagrangeProjection() {
        var n = .5, m = projectionMutator(lagrange), p = m(n);
        p.spacing = function(_) {
          if (!arguments.length) return n;
          return m(n = +_);
        };
        return p;
      }
      (d3.geo.lagrange = lagrangeProjection).raw = lagrange;
      function larrivee(λ, φ) {
        return [ λ * (1 + Math.sqrt(Math.cos(φ))) / 2, φ / (Math.cos(φ / 2) * Math.cos(λ / 6)) ];
      }
      larrivee.invert = function(x, y) {
        var x0 = Math.abs(x), y0 = Math.abs(y), π_sqrt2 = π / Math.SQRT2, λ = ε, φ = halfπ;
        if (y0 < π_sqrt2) φ *= y0 / π_sqrt2; else λ += 6 * acos(π_sqrt2 / y0);
        for (var i = 0; i < 25; i++) {
          var sinφ = Math.sin(φ), sqrtcosφ = asqrt(Math.cos(φ)), sinφ_2 = Math.sin(φ / 2), cosφ_2 = Math.cos(φ / 2), sinλ_6 = Math.sin(λ / 6), cosλ_6 = Math.cos(λ / 6), f0 = .5 * λ * (1 + sqrtcosφ) - x0, f1 = φ / (cosφ_2 * cosλ_6) - y0, df0dφ = sqrtcosφ ? -.25 * λ * sinφ / sqrtcosφ : 0, df0dλ = .5 * (1 + sqrtcosφ), df1dφ = (1 + .5 * φ * sinφ_2 / cosφ_2) / (cosφ_2 * cosλ_6), df1dλ = φ / cosφ_2 * (sinλ_6 / 6) / (cosλ_6 * cosλ_6), denom = df0dφ * df1dλ - df1dφ * df0dλ, dφ = (f0 * df1dλ - f1 * df0dλ) / denom, dλ = (f1 * df0dφ - f0 * df1dφ) / denom;
          φ -= dφ;
          λ -= dλ;
          if (Math.abs(dφ) < ε && Math.abs(dλ) < ε) break;
        }
        return [ x < 0 ? -λ : λ, y < 0 ? -φ : φ ];
      };
      (d3.geo.larrivee = function() {
        return projection(larrivee);
      }).raw = larrivee;
      function laskowski(λ, φ) {
        var λ2 = λ * λ, φ2 = φ * φ;
        return [ λ * (.975534 + φ2 * (-.119161 + λ2 * -.0143059 + φ2 * -.0547009)), φ * (1.00384 + λ2 * (.0802894 + φ2 * -.02855 + λ2 * 199025e-9) + φ2 * (.0998909 + φ2 * -.0491032)) ];
      }
      laskowski.invert = function(x, y) {
        var λ = sgn(x) * π, φ = y / 2, i = 50;
        do {
          var λ2 = λ * λ, φ2 = φ * φ, λφ = λ * φ, fx = λ * (.975534 + φ2 * (-.119161 + λ2 * -.0143059 + φ2 * -.0547009)) - x, fy = φ * (1.00384 + λ2 * (.0802894 + φ2 * -.02855 + λ2 * 199025e-9) + φ2 * (.0998909 + φ2 * -.0491032)) - y, δxδλ = .975534 - φ2 * (.119161 + 3 * λ2 * .0143059 + φ2 * .0547009), δxδφ = -λφ * (2 * .119161 + 4 * .0547009 * φ2 + 2 * .0143059 * λ2), δyδλ = λφ * (2 * .0802894 + 4 * 199025e-9 * λ2 + 2 * -.02855 * φ2), δyδφ = 1.00384 + λ2 * (.0802894 + 199025e-9 * λ2) + φ2 * (3 * (.0998909 - .02855 * λ2) - 5 * .0491032 * φ2), denominator = δxδφ * δyδλ - δyδφ * δxδλ, δλ = (fy * δxδφ - fx * δyδφ) / denominator, δφ = (fx * δyδλ - fy * δxδλ) / denominator;
          λ -= δλ, φ -= δφ;
        } while ((Math.abs(δλ) > ε || Math.abs(δφ) > ε) && --i > 0);
        return i && [ λ, φ ];
      };
      (d3.geo.laskowski = function() {
        return projection(laskowski);
      }).raw = laskowski;
      function littrow(λ, φ) {
        return [ Math.sin(λ) / Math.cos(φ), Math.tan(φ) * Math.cos(λ) ];
      }
      littrow.invert = function(x, y) {
        var x2 = x * x, y2 = y * y, y2_1 = y2 + 1, cosφ = x ? Math.SQRT1_2 * Math.sqrt((y2_1 - Math.sqrt(x2 * x2 + 2 * x2 * (y2 - 1) + y2_1 * y2_1)) / x2 + 1) : 1 / Math.sqrt(y2_1);
        return [ asin(x * cosφ), sgn(y) * acos(cosφ) ];
      };
      (d3.geo.littrow = function() {
        return projection(littrow);
      }).raw = littrow;
      function loximuthal(φ0) {
        var cosφ0 = Math.cos(φ0), tanφ0 = Math.tan(π / 4 + φ0 / 2);
        function forward(λ, φ) {
          var y = φ - φ0, x = Math.abs(y) < ε ? λ * cosφ0 : Math.abs(x = π / 4 + φ / 2) < ε || Math.abs(Math.abs(x) - halfπ) < ε ? 0 : λ * y / Math.log(Math.tan(x) / tanφ0);
          return [ x, y ];
        }
        forward.invert = function(x, y) {
          var λ, φ = y + φ0;
          return [ Math.abs(y) < ε ? x / cosφ0 : Math.abs(λ = π / 4 + φ / 2) < ε || Math.abs(Math.abs(λ) - halfπ) < ε ? 0 : x * Math.log(Math.tan(λ) / tanφ0) / y, φ ];
        };
        return forward;
      }
      (d3.geo.loximuthal = function() {
        return parallel1Projection(loximuthal).parallel(40);
      }).raw = loximuthal;
      function miller(λ, φ) {
        return [ λ, 1.25 * Math.log(Math.tan(π / 4 + .4 * φ)) ];
      }
      miller.invert = function(x, y) {
        return [ x, 2.5 * Math.atan(Math.exp(.8 * y)) - .625 * π ];
      };
      (d3.geo.miller = function() {
        return projection(miller);
      }).raw = miller;
      function modifiedStereographic(C) {
        var m = C.length - 1;
        function forward(λ, φ) {
          var cosφ = Math.cos(φ), k = 2 / (1 + cosφ * Math.cos(λ)), zr = k * cosφ * Math.sin(λ), zi = k * Math.sin(φ), i = m, w = C[i], ar = w[0], ai = w[1], t;
          while (--i >= 0) {
            w = C[i];
            ar = w[0] + zr * (t = ar) - zi * ai;
            ai = w[1] + zr * ai + zi * t;
          }
          ar = zr * (t = ar) - zi * ai;
          ai = zr * ai + zi * t;
          return [ ar, ai ];
        }
        forward.invert = function(x, y) {
          var i = 20, zr = x, zi = y;
          do {
            var j = m, w = C[j], ar = w[0], ai = w[1], br = 0, bi = 0, t;
            while (--j >= 0) {
              w = C[j];
              br = ar + zr * (t = br) - zi * bi;
              bi = ai + zr * bi + zi * t;
              ar = w[0] + zr * (t = ar) - zi * ai;
              ai = w[1] + zr * ai + zi * t;
            }
            br = ar + zr * (t = br) - zi * bi;
            bi = ai + zr * bi + zi * t;
            ar = zr * (t = ar) - zi * ai - x;
            ai = zr * ai + zi * t - y;
            var denominator = br * br + bi * bi, δr, δi;
            zr -= δr = (ar * br + ai * bi) / denominator;
            zi -= δi = (ai * br - ar * bi) / denominator;
          } while (Math.abs(δr) + Math.abs(δi) > ε * ε && --i > 0);
          if (i) {
            var ρ = Math.sqrt(zr * zr + zi * zi), c = 2 * Math.atan(ρ * .5), sinc = Math.sin(c);
            return [ Math.atan2(zr * sinc, ρ * Math.cos(c)), ρ ? asin(zi * sinc / ρ) : 0 ];
          }
        };
        return forward;
      }
      var modifiedStereographicCoefficients = {
        alaska: [ [ .9972523, 0 ], [ .0052513, -.0041175 ], [ .0074606, .0048125 ], [ -.0153783, -.1968253 ], [ .0636871, -.1408027 ], [ .3660976, -.2937382 ] ],
        gs48: [ [ .98879, 0 ], [ 0, 0 ], [ -.050909, 0 ], [ 0, 0 ], [ .075528, 0 ] ],
        gs50: [ [ .984299, 0 ], [ .0211642, .0037608 ], [ -.1036018, -.0575102 ], [ -.0329095, -.0320119 ], [ .0499471, .1223335 ], [ .026046, .0899805 ], [ 7388e-7, -.1435792 ], [ .0075848, -.1334108 ], [ -.0216473, .0776645 ], [ -.0225161, .0853673 ] ],
        miller: [ [ .9245, 0 ], [ 0, 0 ], [ .01943, 0 ] ],
        lee: [ [ .721316, 0 ], [ 0, 0 ], [ -.00881625, -.00617325 ] ]
      };
      function modifiedStereographicProjection() {
        var coefficients = modifiedStereographicCoefficients.miller, m = projectionMutator(modifiedStereographic), p = m(coefficients);
        p.coefficients = function(_) {
          if (!arguments.length) return coefficients;
          return m(coefficients = typeof _ === "string" ? modifiedStereographicCoefficients[_] : _);
        };
        return p;
      }
      (d3.geo.modifiedStereographic = modifiedStereographicProjection).raw = modifiedStereographic;
      function mtFlatPolarParabolic(λ, φ) {
        var sqrt6 = Math.sqrt(6), sqrt7 = Math.sqrt(7), θ = Math.asin(7 * Math.sin(φ) / (3 * sqrt6));
        return [ sqrt6 * λ * (2 * Math.cos(2 * θ / 3) - 1) / sqrt7, 9 * Math.sin(θ / 3) / sqrt7 ];
      }
      mtFlatPolarParabolic.invert = function(x, y) {
        var sqrt6 = Math.sqrt(6), sqrt7 = Math.sqrt(7), θ = 3 * asin(y * sqrt7 / 9);
        return [ x * sqrt7 / (sqrt6 * (2 * Math.cos(2 * θ / 3) - 1)), asin(Math.sin(θ) * 3 * sqrt6 / 7) ];
      };
      (d3.geo.mtFlatPolarParabolic = function() {
        return projection(mtFlatPolarParabolic);
      }).raw = mtFlatPolarParabolic;
      function mtFlatPolarQuartic(λ, φ) {
        var k = (1 + Math.SQRT1_2) * Math.sin(φ), θ = φ;
        for (var i = 0, δ; i < 25; i++) {
          θ -= δ = (Math.sin(θ / 2) + Math.sin(θ) - k) / (.5 * Math.cos(θ / 2) + Math.cos(θ));
          if (Math.abs(δ) < ε) break;
        }
        return [ λ * (1 + 2 * Math.cos(θ) / Math.cos(θ / 2)) / (3 * Math.SQRT2), 2 * Math.sqrt(3) * Math.sin(θ / 2) / Math.sqrt(2 + Math.SQRT2) ];
      }
      mtFlatPolarQuartic.invert = function(x, y) {
        var sinθ_2 = y * Math.sqrt(2 + Math.SQRT2) / (2 * Math.sqrt(3)), θ = 2 * asin(sinθ_2);
        return [ 3 * Math.SQRT2 * x / (1 + 2 * Math.cos(θ) / Math.cos(θ / 2)), asin((sinθ_2 + Math.sin(θ)) / (1 + Math.SQRT1_2)) ];
      };
      (d3.geo.mtFlatPolarQuartic = function() {
        return projection(mtFlatPolarQuartic);
      }).raw = mtFlatPolarQuartic;
      function mtFlatPolarSinusoidal(λ, φ) {
        var A = Math.sqrt(6 / (4 + π)), k = (1 + π / 4) * Math.sin(φ), θ = φ / 2;
        for (var i = 0, δ; i < 25; i++) {
          θ -= δ = (θ / 2 + Math.sin(θ) - k) / (.5 + Math.cos(θ));
          if (Math.abs(δ) < ε) break;
        }
        return [ A * (.5 + Math.cos(θ)) * λ / 1.5, A * θ ];
      }
      mtFlatPolarSinusoidal.invert = function(x, y) {
        var A = Math.sqrt(6 / (4 + π)), θ = y / A;
        if (Math.abs(Math.abs(θ) - halfπ) < ε) θ = θ < 0 ? -halfπ : halfπ;
        return [ 1.5 * x / (A * (.5 + Math.cos(θ))), asin((θ / 2 + Math.sin(θ)) / (1 + π / 4)) ];
      };
      (d3.geo.mtFlatPolarSinusoidal = function() {
        return projection(mtFlatPolarSinusoidal);
      }).raw = mtFlatPolarSinusoidal;
      function naturalEarth(λ, φ) {
        var φ2 = φ * φ, φ4 = φ2 * φ2;
        return [ λ * (.8707 - .131979 * φ2 + φ4 * (-.013791 + φ4 * (.003971 * φ2 - .001529 * φ4))), φ * (1.007226 + φ2 * (.015085 + φ4 * (-.044475 + .028874 * φ2 - .005916 * φ4))) ];
      }
      naturalEarth.invert = function(x, y) {
        var φ = y, i = 25, δ;
        do {
          var φ2 = φ * φ, φ4 = φ2 * φ2;
          φ -= δ = (φ * (1.007226 + φ2 * (.015085 + φ4 * (-.044475 + .028874 * φ2 - .005916 * φ4))) - y) / (1.007226 + φ2 * (.015085 * 3 + φ4 * (-.044475 * 7 + .028874 * 9 * φ2 - .005916 * 11 * φ4)));
        } while (Math.abs(δ) > ε && --i > 0);
        return [ x / (.8707 + (φ2 = φ * φ) * (-.131979 + φ2 * (-.013791 + φ2 * φ2 * φ2 * (.003971 - .001529 * φ2)))), φ ];
      };
      (d3.geo.naturalEarth = function() {
        return projection(naturalEarth);
      }).raw = naturalEarth;
      function nellHammer(λ, φ) {
        return [ λ * (1 + Math.cos(φ)) / 2, 2 * (φ - Math.tan(φ / 2)) ];
      }
      nellHammer.invert = function(x, y) {
        var p = y / 2;
        for (var i = 0, δ = Infinity; i < 10 && Math.abs(δ) > ε; i++) {
          var c = Math.cos(y / 2);
          y -= δ = (y - Math.tan(y / 2) - p) / (1 - .5 / (c * c));
        }
        return [ 2 * x / (1 + Math.cos(y)), y ];
      };
      (d3.geo.nellHammer = function() {
        return projection(nellHammer);
      }).raw = nellHammer;
      var pattersonK1 = 1.0148, pattersonK2 = .23185, pattersonK3 = -.14499, pattersonK4 = .02406, pattersonC1 = pattersonK1, pattersonC2 = 5 * pattersonK2, pattersonC3 = 7 * pattersonK3, pattersonC4 = 9 * pattersonK4, pattersonYmax = 1.790857183;
      function patterson(λ, φ) {
        var φ2 = φ * φ;
        return [ λ, φ * (pattersonK1 + φ2 * φ2 * (pattersonK2 + φ2 * (pattersonK3 + pattersonK4 * φ2))) ];
      }
      patterson.invert = function(x, y) {
        if (y > pattersonYmax) y = pattersonYmax; else if (y < -pattersonYmax) y = -pattersonYmax;
        var yc = y, δ;
        do {
          var y2 = yc * yc;
          yc -= δ = (yc * (pattersonK1 + y2 * y2 * (pattersonK2 + y2 * (pattersonK3 + pattersonK4 * y2))) - y) / (pattersonC1 + y2 * y2 * (pattersonC2 + y2 * (pattersonC3 + pattersonC4 * y2)));
        } while (Math.abs(δ) > ε);
        return [ x, yc ];
      };
      (d3.geo.patterson = function() {
        return projection(patterson);
      }).raw = patterson;
      var peirceQuincuncialProjection = quincuncialProjection(guyou);
      (d3.geo.peirceQuincuncial = function() {
        return peirceQuincuncialProjection().quincuncial(true).rotate([ -90, -90, 45 ]).clipAngle(180 - 1e-6);
      }).raw = peirceQuincuncialProjection.raw;
      function polyconic(λ, φ) {
        if (Math.abs(φ) < ε) return [ λ, 0 ];
        var tanφ = Math.tan(φ), k = λ * Math.sin(φ);
        return [ Math.sin(k) / tanφ, φ + (1 - Math.cos(k)) / tanφ ];
      }
      polyconic.invert = function(x, y) {
        if (Math.abs(y) < ε) return [ x, 0 ];
        var k = x * x + y * y, φ = y * .5, i = 10, δ;
        do {
          var tanφ = Math.tan(φ), secφ = 1 / Math.cos(φ), j = k - 2 * y * φ + φ * φ;
          φ -= δ = (tanφ * j + 2 * (φ - y)) / (2 + j * secφ * secφ + 2 * (φ - y) * tanφ);
        } while (Math.abs(δ) > ε && --i > 0);
        tanφ = Math.tan(φ);
        return [ (Math.abs(y) < Math.abs(φ + 1 / tanφ) ? asin(x * tanφ) : sgn(x) * (acos(Math.abs(x * tanφ)) + halfπ)) / Math.sin(φ), φ ];
      };
      (d3.geo.polyconic = function() {
        return projection(polyconic);
      }).raw = polyconic;
      function rectangularPolyconic(φ0) {
        var sinφ0 = Math.sin(φ0);
        function forward(λ, φ) {
          var A = sinφ0 ? Math.tan(λ * sinφ0 / 2) / sinφ0 : λ / 2;
          if (!φ) return [ 2 * A, -φ0 ];
          var E = 2 * Math.atan(A * Math.sin(φ)), cotφ = 1 / Math.tan(φ);
          return [ Math.sin(E) * cotφ, φ + (1 - Math.cos(E)) * cotφ - φ0 ];
        }
        forward.invert = function(x, y) {
          if (Math.abs(y += φ0) < ε) return [ sinφ0 ? 2 * Math.atan(sinφ0 * x / 2) / sinφ0 : x, 0 ];
          var k = x * x + y * y, φ = 0, i = 10, δ;
          do {
            var tanφ = Math.tan(φ), secφ = 1 / Math.cos(φ), j = k - 2 * y * φ + φ * φ;
            φ -= δ = (tanφ * j + 2 * (φ - y)) / (2 + j * secφ * secφ + 2 * (φ - y) * tanφ);
          } while (Math.abs(δ) > ε && --i > 0);
          var E = x * (tanφ = Math.tan(φ)), A = Math.tan(Math.abs(y) < Math.abs(φ + 1 / tanφ) ? asin(E) * .5 : acos(E) * .5 + π / 4) / Math.sin(φ);
          return [ sinφ0 ? 2 * Math.atan(sinφ0 * A) / sinφ0 : 2 * A, φ ];
        };
        return forward;
      }
      (d3.geo.rectangularPolyconic = function() {
        return parallel1Projection(rectangularPolyconic);
      }).raw = rectangularPolyconic;
      var robinsonConstants = [ [ .9986, -.062 ], [ 1, 0 ], [ .9986, .062 ], [ .9954, .124 ], [ .99, .186 ], [ .9822, .248 ], [ .973, .31 ], [ .96, .372 ], [ .9427, .434 ], [ .9216, .4958 ], [ .8962, .5571 ], [ .8679, .6176 ], [ .835, .6769 ], [ .7986, .7346 ], [ .7597, .7903 ], [ .7186, .8435 ], [ .6732, .8936 ], [ .6213, .9394 ], [ .5722, .9761 ], [ .5322, 1 ] ];
      robinsonConstants.forEach(function(d) {
        d[1] *= 1.0144;
      });
      function robinson(λ, φ) {
        var i = Math.min(18, Math.abs(φ) * 36 / π), i0 = Math.floor(i), di = i - i0, ax = (k = robinsonConstants[i0])[0], ay = k[1], bx = (k = robinsonConstants[++i0])[0], by = k[1], cx = (k = robinsonConstants[Math.min(19, ++i0)])[0], cy = k[1], k;
        return [ λ * (bx + di * (cx - ax) / 2 + di * di * (cx - 2 * bx + ax) / 2), (φ > 0 ? halfπ : -halfπ) * (by + di * (cy - ay) / 2 + di * di * (cy - 2 * by + ay) / 2) ];
      }
      robinson.invert = function(x, y) {
        var yy = y / halfπ, φ = yy * 90, i = Math.min(18, Math.abs(φ / 5)), i0 = Math.max(0, Math.floor(i));
        do {
          var ay = robinsonConstants[i0][1], by = robinsonConstants[i0 + 1][1], cy = robinsonConstants[Math.min(19, i0 + 2)][1], u = cy - ay, v = cy - 2 * by + ay, t = 2 * (Math.abs(yy) - by) / u, c = v / u, di = t * (1 - c * t * (1 - 2 * c * t));
          if (di >= 0 || i0 === 1) {
            φ = (y >= 0 ? 5 : -5) * (di + i);
            var j = 50, δ;
            do {
              i = Math.min(18, Math.abs(φ) / 5);
              i0 = Math.floor(i);
              di = i - i0;
              ay = robinsonConstants[i0][1];
              by = robinsonConstants[i0 + 1][1];
              cy = robinsonConstants[Math.min(19, i0 + 2)][1];
              φ -= (δ = (y >= 0 ? halfπ : -halfπ) * (by + di * (cy - ay) / 2 + di * di * (cy - 2 * by + ay) / 2) - y) * degrees;
            } while (Math.abs(δ) > ε2 && --j > 0);
            break;
          }
        } while (--i0 >= 0);
        var ax = robinsonConstants[i0][0], bx = robinsonConstants[i0 + 1][0], cx = robinsonConstants[Math.min(19, i0 + 2)][0];
        return [ x / (bx + di * (cx - ax) / 2 + di * di * (cx - 2 * bx + ax) / 2), φ * radians ];
      };
      (d3.geo.robinson = function() {
        return projection(robinson);
      }).raw = robinson;
      function satelliteVertical(P) {
        function forward(λ, φ) {
          var cosφ = Math.cos(φ), k = (P - 1) / (P - cosφ * Math.cos(λ));
          return [ k * cosφ * Math.sin(λ), k * Math.sin(φ) ];
        }
        forward.invert = function(x, y) {
          var ρ2 = x * x + y * y, ρ = Math.sqrt(ρ2), sinc = (P - Math.sqrt(1 - ρ2 * (P + 1) / (P - 1))) / ((P - 1) / ρ + ρ / (P - 1));
          return [ Math.atan2(x * sinc, ρ * Math.sqrt(1 - sinc * sinc)), ρ ? asin(y * sinc / ρ) : 0 ];
        };
        return forward;
      }
      function satellite(P, ω) {
        var vertical = satelliteVertical(P);
        if (!ω) return vertical;
        var cosω = Math.cos(ω), sinω = Math.sin(ω);
        function forward(λ, φ) {
          var coordinates = vertical(λ, φ), y = coordinates[1], A = y * sinω / (P - 1) + cosω;
          return [ coordinates[0] * cosω / A, y / A ];
        }
        forward.invert = function(x, y) {
          var k = (P - 1) / (P - 1 - y * sinω);
          return vertical.invert(k * x, k * y * cosω);
        };
        return forward;
      }
      function satelliteProjection() {
        var P = 1.4, ω = 0, m = projectionMutator(satellite), p = m(P, ω);
        p.distance = function(_) {
          if (!arguments.length) return P;
          return m(P = +_, ω);
        };
        p.tilt = function(_) {
          if (!arguments.length) return ω * 180 / π;
          return m(P, ω = _ * π / 180);
        };
        return p;
      }
      (d3.geo.satellite = satelliteProjection).raw = satellite;
      function times(λ, φ) {
        var t = Math.tan(φ / 2), s = Math.sin(π / 4 * t);
        return [ λ * (.74482 - .34588 * s * s), 1.70711 * t ];
      }
      times.invert = function(x, y) {
        var t = y / 1.70711, s = Math.sin(π / 4 * t);
        return [ x / (.74482 - .34588 * s * s), 2 * Math.atan(t) ];
      };
      (d3.geo.times = function() {
        return projection(times);
      }).raw = times;
      function twoPointEquidistant(z0) {
        if (!z0) return d3.geo.azimuthalEquidistant.raw;
        var λa = -z0 / 2, λb = -λa, z02 = z0 * z0, tanλ0 = Math.tan(λb), S = .5 / Math.sin(λb);
        function forward(λ, φ) {
          var za = acos(Math.cos(φ) * Math.cos(λ - λa)), zb = acos(Math.cos(φ) * Math.cos(λ - λb)), ys = φ < 0 ? -1 : 1;
          za *= za, zb *= zb;
          return [ (za - zb) / (2 * z0), ys * asqrt(4 * z02 * zb - (z02 - za + zb) * (z02 - za + zb)) / (2 * z0) ];
        }
        forward.invert = function(x, y) {
          var y2 = y * y, cosza = Math.cos(Math.sqrt(y2 + (t = x + λa) * t)), coszb = Math.cos(Math.sqrt(y2 + (t = x + λb) * t)), t, d;
          return [ Math.atan2(d = cosza - coszb, t = (cosza + coszb) * tanλ0), (y < 0 ? -1 : 1) * acos(Math.sqrt(t * t + d * d) * S) ];
        };
        return forward;
      }
      function twoPointEquidistantProjection() {
        var points = [ [ 0, 0 ], [ 0, 0 ] ], m = projectionMutator(twoPointEquidistant), p = m(0), rotate = p.rotate;
        delete p.rotate;
        p.points = function(_) {
          if (!arguments.length) return points;
          points = _;
          var interpolate = d3.geo.interpolate(_[0], _[1]), origin = interpolate(.5), p = d3.geo.rotation([ -origin[0], -origin[1] ])(_[0]), b = interpolate.distance * .5, γ = -asin(Math.sin(p[1] * radians) / Math.sin(b));
          if (p[0] > 0) γ = π - γ;
          rotate.call(p, [ -origin[0], -origin[1], -γ * degrees ]);
          return m(b * 2);
        };
        return p;
      }
      (d3.geo.twoPointEquidistant = twoPointEquidistantProjection).raw = twoPointEquidistant;
      function twoPointAzimuthal(d) {
        var cosd = Math.cos(d);
        function forward(λ, φ) {
          var coordinates = d3.geo.gnomonic.raw(λ, φ);
          coordinates[0] *= cosd;
          return coordinates;
        }
        forward.invert = function(x, y) {
          return d3.geo.gnomonic.raw.invert(x / cosd, y);
        };
        return forward;
      }
      function twoPointAzimuthalProjection() {
        var points = [ [ 0, 0 ], [ 0, 0 ] ], m = projectionMutator(twoPointAzimuthal), p = m(0), rotate = p.rotate;
        delete p.rotate;
        p.points = function(_) {
          if (!arguments.length) return points;
          points = _;
          var interpolate = d3.geo.interpolate(_[0], _[1]), origin = interpolate(.5), p = d3.geo.rotation([ -origin[0], -origin[1] ])(_[0]), b = interpolate.distance * .5, γ = -asin(Math.sin(p[1] * radians) / Math.sin(b));
          if (p[0] > 0) γ = π - γ;
          rotate.call(p, [ -origin[0], -origin[1], -γ * degrees ]);
          return m(b);
        };
        return p;
      }
      (d3.geo.twoPointAzimuthal = twoPointAzimuthalProjection).raw = twoPointAzimuthal;
      function vanDerGrinten(λ, φ) {
        if (Math.abs(φ) < ε) return [ λ, 0 ];
        var sinθ = Math.abs(φ / halfπ), θ = asin(sinθ);
        if (Math.abs(λ) < ε || Math.abs(Math.abs(φ) - halfπ) < ε) return [ 0, sgn(φ) * π * Math.tan(θ / 2) ];
        var cosθ = Math.cos(θ), A = Math.abs(π / λ - λ / π) / 2, A2 = A * A, G = cosθ / (sinθ + cosθ - 1), P = G * (2 / sinθ - 1), P2 = P * P, P2_A2 = P2 + A2, G_P2 = G - P2, Q = A2 + G;
        return [ sgn(λ) * π * (A * G_P2 + Math.sqrt(A2 * G_P2 * G_P2 - P2_A2 * (G * G - P2))) / P2_A2, sgn(φ) * π * (P * Q - A * Math.sqrt((A2 + 1) * P2_A2 - Q * Q)) / P2_A2 ];
      }
      vanDerGrinten.invert = function(x, y) {
        if (Math.abs(y) < ε) return [ x, 0 ];
        if (Math.abs(x) < ε) return [ 0, halfπ * Math.sin(2 * Math.atan(y / π)) ];
        var x2 = (x /= π) * x, y2 = (y /= π) * y, x2_y2 = x2 + y2, z = x2_y2 * x2_y2, c1 = -Math.abs(y) * (1 + x2_y2), c2 = c1 - 2 * y2 + x2, c3 = -2 * c1 + 1 + 2 * y2 + z, d = y2 / c3 + (2 * c2 * c2 * c2 / (c3 * c3 * c3) - 9 * c1 * c2 / (c3 * c3)) / 27, a1 = (c1 - c2 * c2 / (3 * c3)) / c3, m1 = 2 * Math.sqrt(-a1 / 3), θ1 = acos(3 * d / (a1 * m1)) / 3;
        return [ π * (x2_y2 - 1 + Math.sqrt(1 + 2 * (x2 - y2) + z)) / (2 * x), sgn(y) * π * (-m1 * Math.cos(θ1 + π / 3) - c2 / (3 * c3)) ];
      };
      (d3.geo.vanDerGrinten = function() {
        return projection(vanDerGrinten);
      }).raw = vanDerGrinten;
      function vanDerGrinten2(λ, φ) {
        if (Math.abs(φ) < ε) return [ λ, 0 ];
        var sinθ = Math.abs(φ / halfπ), θ = asin(sinθ);
        if (Math.abs(λ) < ε || Math.abs(Math.abs(φ) - halfπ) < ε) return [ 0, sgn(φ) * π * Math.tan(θ / 2) ];
        var cosθ = Math.cos(θ), A = Math.abs(π / λ - λ / π) / 2, A2 = A * A, x1 = cosθ * (Math.sqrt(1 + A2) - A * cosθ) / (1 + A2 * sinθ * sinθ);
        return [ sgn(λ) * π * x1, sgn(φ) * π * asqrt(1 - x1 * (2 * A + x1)) ];
      }
      vanDerGrinten2.invert = function(x, y) {
        if (!x) return [ 0, halfπ * Math.sin(2 * Math.atan(y / π)) ];
        var x1 = Math.abs(x / π), A = (1 - x1 * x1 - (y /= π) * y) / (2 * x1), A2 = A * A, B = Math.sqrt(A2 + 1);
        return [ sgn(x) * π * (B - A), sgn(y) * halfπ * Math.sin(2 * Math.atan2(Math.sqrt((1 - 2 * A * x1) * (A + B) - x1), Math.sqrt(B + A + x1))) ];
      };
      (d3.geo.vanDerGrinten2 = function() {
        return projection(vanDerGrinten2);
      }).raw = vanDerGrinten2;
      function vanDerGrinten3(λ, φ) {
        if (Math.abs(φ) < ε) return [ λ, 0 ];
        var sinθ = φ / halfπ, θ = asin(sinθ);
        if (Math.abs(λ) < ε || Math.abs(Math.abs(φ) - halfπ) < ε) return [ 0, π * Math.tan(θ / 2) ];
        var A = (π / λ - λ / π) / 2, y1 = sinθ / (1 + Math.cos(θ));
        return [ π * (sgn(λ) * asqrt(A * A + 1 - y1 * y1) - A), π * y1 ];
      }
      vanDerGrinten3.invert = function(x, y) {
        if (!y) return [ x, 0 ];
        var y1 = y / π, A = (π * π * (1 - y1 * y1) - x * x) / (2 * π * x);
        return [ x ? π * (sgn(x) * Math.sqrt(A * A + 1) - A) : 0, halfπ * Math.sin(2 * Math.atan(y1)) ];
      };
      (d3.geo.vanDerGrinten3 = function() {
        return projection(vanDerGrinten3);
      }).raw = vanDerGrinten3;
      function vanDerGrinten4(λ, φ) {
        if (!φ) return [ λ, 0 ];
        var φ0 = Math.abs(φ);
        if (!λ || φ0 === halfπ) return [ 0, φ ];
        var B = φ0 / halfπ, B2 = B * B, C = (8 * B - B2 * (B2 + 2) - 5) / (2 * B2 * (B - 1)), C2 = C * C, BC = B * C, B_C2 = B2 + C2 + 2 * BC, B_3C = B + 3 * C, λ0 = λ / halfπ, λ1 = λ0 + 1 / λ0, D = sgn(Math.abs(λ) - halfπ) * Math.sqrt(λ1 * λ1 - 4), D2 = D * D, F = B_C2 * (B2 + C2 * D2 - 1) + (1 - B2) * (B2 * (B_3C * B_3C + 4 * C2) + 12 * BC * C2 + 4 * C2 * C2), x1 = (D * (B_C2 + C2 - 1) + 2 * asqrt(F)) / (4 * B_C2 + D2);
        return [ sgn(λ) * halfπ * x1, sgn(φ) * halfπ * asqrt(1 + D * Math.abs(x1) - x1 * x1) ];
      }
      vanDerGrinten4.invert = function(x, y) {
        if (!x || !y) return [ x, y ];
        y /= π;
        var x1 = sgn(x) * x / halfπ, D = (x1 * x1 - 1 + 4 * y * y) / Math.abs(x1), D2 = D * D, B = 2 * y, i = 50;
        do {
          var B2 = B * B, C = (8 * B - B2 * (B2 + 2) - 5) / (2 * B2 * (B - 1)), C_ = (3 * B - B2 * B - 10) / (2 * B2 * B), C2 = C * C, BC = B * C, B_C = B + C, B_C2 = B_C * B_C, B_3C = B + 3 * C, F = B_C2 * (B2 + C2 * D2 - 1) + (1 - B2) * (B2 * (B_3C * B_3C + 4 * C2) + C2 * (12 * BC + 4 * C2)), F_ = -2 * B_C * (4 * BC * C2 + (1 - 4 * B2 + 3 * B2 * B2) * (1 + C_) + C2 * (-6 + 14 * B2 - D2 + (-8 + 8 * B2 - 2 * D2) * C_) + BC * (-8 + 12 * B2 + (-10 + 10 * B2 - D2) * C_)), sqrtF = Math.sqrt(F), f = D * (B_C2 + C2 - 1) + 2 * sqrtF - x1 * (4 * B_C2 + D2), f_ = D * (2 * C * C_ + 2 * B_C * (1 + C_)) + F_ / sqrtF - 8 * B_C * (D * (-1 + C2 + B_C2) + 2 * sqrtF) * (1 + C_) / (D2 + 4 * B_C2);
          B -= δ = f / f_;
        } while (δ > ε && --i > 0);
        return [ sgn(x) * (Math.sqrt(D * D + 4) + D) * π / 4, halfπ * B ];
      };
      (d3.geo.vanDerGrinten4 = function() {
        return projection(vanDerGrinten4);
      }).raw = vanDerGrinten4;
      var wagner4 = function() {
        var A = 4 * π + 3 * Math.sqrt(3), B = 2 * Math.sqrt(2 * π * Math.sqrt(3) / A);
        return mollweideBromley(B * Math.sqrt(3) / π, B, A / 6);
      }();
      (d3.geo.wagner4 = function() {
        return projection(wagner4);
      }).raw = wagner4;
      function wagner6(λ, φ) {
        return [ λ * Math.sqrt(1 - 3 * φ * φ / (π * π)), φ ];
      }
      wagner6.invert = function(x, y) {
        return [ x / Math.sqrt(1 - 3 * y * y / (π * π)), y ];
      };
      (d3.geo.wagner6 = function() {
        return projection(wagner6);
      }).raw = wagner6;
      function wagner7(λ, φ) {
        var s = .90631 * Math.sin(φ), c0 = Math.sqrt(1 - s * s), c1 = Math.sqrt(2 / (1 + c0 * Math.cos(λ /= 3)));
        return [ 2.66723 * c0 * c1 * Math.sin(λ), 1.24104 * s * c1 ];
      }
      wagner7.invert = function(x, y) {
        var t1 = x / 2.66723, t2 = y / 1.24104, p = Math.sqrt(t1 * t1 + t2 * t2), c = 2 * asin(p / 2);
        return [ 3 * Math.atan2(x * Math.tan(c), 2.66723 * p), p && asin(y * Math.sin(c) / (1.24104 * .90631 * p)) ];
      };
      (d3.geo.wagner7 = function() {
        return projection(wagner7);
      }).raw = wagner7;
      function wiechel(λ, φ) {
        var cosφ = Math.cos(φ), sinφ = Math.cos(λ) * cosφ, sin1_φ = 1 - sinφ, cosλ = Math.cos(λ = Math.atan2(Math.sin(λ) * cosφ, -Math.sin(φ))), sinλ = Math.sin(λ);
        cosφ = asqrt(1 - sinφ * sinφ);
        return [ sinλ * cosφ - cosλ * sin1_φ, -cosλ * cosφ - sinλ * sin1_φ ];
      }
      wiechel.invert = function(x, y) {
        var w = -.5 * (x * x + y * y), k = Math.sqrt(-w * (2 + w)), b = y * w + x * k, a = x * w - y * k, D = Math.sqrt(a * a + b * b);
        return [ Math.atan2(k * b, D * (1 + w)), D ? -asin(k * a / D) : 0 ];
      };
      (d3.geo.wiechel = function() {
        return projection(wiechel);
      }).raw = wiechel;
      function winkel3(λ, φ) {
        var coordinates = aitoff(λ, φ);
        return [ (coordinates[0] + λ / halfπ) / 2, (coordinates[1] + φ) / 2 ];
      }
      winkel3.invert = function(x, y) {
        var λ = x, φ = y, i = 25;
        do {
          var cosφ = Math.cos(φ), sinφ = Math.sin(φ), sin_2φ = Math.sin(2 * φ), sin2φ = sinφ * sinφ, cos2φ = cosφ * cosφ, sinλ = Math.sin(λ), cosλ_2 = Math.cos(λ / 2), sinλ_2 = Math.sin(λ / 2), sin2λ_2 = sinλ_2 * sinλ_2, C = 1 - cos2φ * cosλ_2 * cosλ_2, E = C ? acos(cosφ * cosλ_2) * Math.sqrt(F = 1 / C) : F = 0, F, fx = .5 * (2 * E * cosφ * sinλ_2 + λ / halfπ) - x, fy = .5 * (E * sinφ + φ) - y, δxδλ = .5 * F * (cos2φ * sin2λ_2 + E * cosφ * cosλ_2 * sin2φ) + .5 / halfπ, δxδφ = F * (sinλ * sin_2φ / 4 - E * sinφ * sinλ_2), δyδλ = .125 * F * (sin_2φ * sinλ_2 - E * sinφ * cos2φ * sinλ), δyδφ = .5 * F * (sin2φ * cosλ_2 + E * sin2λ_2 * cosφ) + .5, denominator = δxδφ * δyδλ - δyδφ * δxδλ, δλ = (fy * δxδφ - fx * δyδφ) / denominator, δφ = (fx * δyδλ - fy * δxδλ) / denominator;
          λ -= δλ, φ -= δφ;
        } while ((Math.abs(δλ) > ε || Math.abs(δφ) > ε) && --i > 0);
        return [ λ, φ ];
      };
      (d3.geo.winkel3 = function() {
        return projection(winkel3);
      }).raw = winkel3;
    };

    //import Selectlist from './bubblemap-selectlist';

    //BUBBLE MAP CHART COMPONENT
    var BubbleMapComponent = Component.extend({
      /**
       * Initializes the component (Bubble Map Chart).
       * Executed once before any template is rendered.
       * @param {Object} config The config passed to the component
       * @param {Object} context The component's parent
       */
      init: function (config, context) {
        this.name = 'bubblemap';
        this.template = 'bubblemap.html';
        this.bubblesDrawing = null;


        //http://stackoverflow.com/questions/11381673/detecting-a-mobile-browser
        var mobileAndTabletcheck = function() {
          var check = false;
          (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
          return check;
        };
        this.isMobile = mobileAndTabletcheck();

        //define expected models for this component
        this.model_expects = [{
          name: "time",
          type: "time"
        }, {
          name: "entities",
          type: "entities"
        }, {
          name: "marker",
          type: "model"
        }, {
          name: "language",
          type: "language"
        }, {
          name: "ui",
          type: "model"
        }];

        var _this = this;
        this.model_binds = {
          "change:time.value": function (evt) {
            _this.updateTime();
            _this.updateDoubtOpacity();
            _this.redrawDataPoints(null, false);
          },
          "change:entities.highlight": function (evt) {
            if (!_this._readyOnce) return;
            _this.highlightEntities();
            _this.updateOpacity();
          },
          "change:marker": function(evt, path) {
            // bubble size change is processed separately
            if(!_this._readyOnce) return;
              
            if(path.indexOf("scaleType") > -1) _this.ready();
          },
          'change:marker.size': function(evt, path) {
            //console.log("EVENT change:marker:size:max");
            if(!_this._readyOnce) return;
            if(path.indexOf("domainMin") > -1 || path.indexOf("domainMax") > -1) {
              _this.updateMarkerSizeLimits();
              _this.redrawDataPoints(null, false);
              return;
            }
          },
          "change:marker.color.palette": function (evt, path) {
              if (!_this._readyOnce) return;
              _this.redrawDataPoints(null, false);
          },
          "change:entities.select": function (evt) {
              if (!_this._readyOnce) return;
              _this.selectEntities();
              _this.redrawDataPoints(null, false);
              _this.updateOpacity();
              _this.updateDoubtOpacity();
              
          },
          "change:entities.opacitySelectDim": function (evt) {
              _this.updateOpacity();
          },
          "change:entities.opacityRegular": function (evt) {
              _this.updateOpacity();
          },
        };

        //this._selectlist = new Selectlist(this);

        //contructor is the same as any component
        this._super(config, context);

        this.sScale = null;
        this.cScale = d3.scale.category10();


        this.cached = {};
        // default UI settings
        this.ui = extend({
          labels: {}
        }, this.ui["vzb-tool-" + this.name]);

        this.ui.labels = extend({
          autoResolveCollisions: false,
          dragging: true
        }, this.ui.labels);

        this.labelDragger = d3.behavior.drag()
          .on("dragstart", function(d, i) {
            d3.event.sourceEvent.stopPropagation();
            var KEY = _this.KEY;
            _this.druging = d[KEY];
          })
          .on("drag", function(d, i) {

            var KEY = _this.KEY;
            if(!_this.ui.labels.dragging) return;
            //_this.cached[d[KEY]].scaledS0 = 0; // to extend line when radius shorten
            var cache = _this.cached[d[KEY]];
            cache.labelFixed = true;

            cache.labelX_ += d3.event.dx / _this.width;
            cache.labelY_ += d3.event.dy / _this.height;

            var resolvedX = (cache.labelX0 + cache.labelX_) * _this.width;
            var resolvedY = (cache.labelY0 + cache.labelY_) * _this.height;

            var resolvedX0 = cache.labelX0 * _this.width;
            var resolvedY0 = cache.labelY0 * _this.height;

            var lineGroup = _this.entityLines.filter(function(f) {
              return f[KEY] == d[KEY];
            });

            _this._repositionLabels(d, i, this, resolvedX, resolvedY, resolvedX0, resolvedY0, 0, lineGroup);
          })
          .on("dragend", function(d, i) {
            var KEY = _this.KEY;
            _this.druging = null;
            _this.model.entities.setLabelOffset(d, [
              _this.cached[d[KEY]].labelX_,
              _this.cached[d[KEY]].labelY_
            ]);
          });


        this.defaultWidth = 960;
        this.defaultHeight = 500;
        this.boundBox = [[0.06, 0], [1.0, 0.85]]; // two points to set box bound on 960 * 500 image;

        d3_geo_projection();
      },


      afterPreload: function(){
        if(!this.world) warn("bubble map afterPreload: missing country shapes " + this.world);
          
        // http://bl.ocks.org/mbostock/d4021aa4dccfd65edffd patterson
        // http://bl.ocks.org/mbostock/3710566 robinson
        // map background
        var defaultWidth = this.defaultWidth;
        var defaultHeight = this.defaultHeight;
        var world = this.world;
        var projection = this.projection = d3.geo.robinson()
            .scale(150)
            .translate([defaultWidth / 2, defaultHeight / 2])
            .precision(.1);

        var path = this.bgPath = d3.geo.path()
            .projection(projection);

        var graticule = d3.geo.graticule();

        var svg = this.mapGraph = d3.select(this.element).select(".vzb-bmc-map-graph")
            .attr("width", defaultWidth)
            .attr("height", defaultHeight);
        svg.html('');

        /* // no latlng line
        svg.append("defs").append("path")
            .datum({type: "Sphere"})
            .attr("id", "sphere")
            .attr("d", path);

        svg.append("use")
            .attr("class", "stroke")
            .attr("xlink:href", "#sphere");

        svg.append("use")
            .attr("class", "fill")
            .attr("xlink:href", "#sphere");

        svg.append("path")
            .datum(graticule)
            .attr("class", "graticule")
            .attr("d", path);
        */

        svg.insert("path", ".graticule")
            .datum(topojson.feature(world, world.objects.land))
            .attr("class", "land")
            .attr("d", path);

        svg.insert("path", ".graticule")
            .datum(topojson.mesh(world, world.objects.countries, function(a, b) { return a !== b; }))
            .attr("class", "boundary")
            .attr("d", path);
      },

      /**
       * DOM is ready
       */
      readyOnce: function () {

        this.element = d3.select(this.element);

        this.graph = this.element.select('.vzb-bmc-graph');
        this.mapSvg = this.element.select('.vzb-bmc-map-background');

        this.bubbleContainerCrop = this.graph.select('.vzb-bmc-bubbles-crop');
        this.bubbleContainer = this.graph.select('.vzb-bmc-bubbles');
        this.labelListContainer = this.graph.select('.vzb-bmc-bubble-labels');
        this.labelsContainer = this.graph.select('.vzb-bmc-labels');
        this.linesContainer = this.graph.select('.vzb-bmc-lines');
        this.dataWarningEl = this.graph.select(".vzb-data-warning");

        this.yTitleEl = this.graph.select(".vzb-bmc-axis-y-title");
        this.cTitleEl = this.graph.select(".vzb-bmc-axis-c-title");
        this.infoEl = this.graph.select(".vzb-bmc-axis-info");

        this.entityBubbles = null;
        this.entityLabels = null;
        this.tooltip = this.element.select('.vzb-bmc-tooltip');
        this.entityLines = null;

        // year background
        this.yearEl = this.graph.select('.vzb-bmc-year');
        this.year = new DynamicBackground(this.yearEl);
        this.year.setConditions({xAlign: 'left', yAlign: 'bottom', bottomOffset: 5});

        var _this = this;
        this.on("resize", function () {
          
          _this.updateSize();
          _this.updateMarkerSizeLimits();
          _this.redrawDataPoints();
          //_this._selectlist.redraw();
          
        });

        this.KEY = this.model.entities.getDimension();
        this.TIMEDIM = this.model.time.getDimension();
          
          
        this.updateUIStrings();

        this.wScale = d3.scale.linear()
            .domain(this.parent.datawarning_content.doubtDomain)
            .range(this.parent.datawarning_content.doubtRange);
          
          

      },

      /*
       * Both model and DOM are ready
       */
      ready: function () {
        this.updateUIStrings();
        this.updateIndicators();
        this.updateSize();
        this.updateMarkerSizeLimits();
        this.updateEntities();
        this.updateTime();
        this.redrawDataPoints();
        this.highlightEntities();
        this.selectEntities();
    //    this._selectlist.redraw();
        this.updateDoubtOpacity();
        this.updateOpacity();
      },

      updateUIStrings: function () {
          var _this = this;

          this.translator = this.model.language.getTFunction();    
          var sizeMetadata = this.model.marker.size.getMetadata();

          this.strings = {
              title: {
                S: this.translator("indicator/" + _this.model.marker.size.which),
                C: this.translator("indicator/" + _this.model.marker.color.which)
              }
          };

          this.yTitleEl.select("text")
              .text(this.translator("buttons/size") + ": " + this.strings.title.S)
              .on("click", function() {
                _this.parent
                  .findChildByName("gapminder-treemenu")
                  .markerID("size")
                  .alignX("left")
                  .alignY("top")
                  .updateView()
                  .toggle();
              });

          this.cTitleEl.select("text")
              .text(this.translator("buttons/color") + ": " + this.strings.title.C)
              .on("click", function() {
                _this.parent
                  .findChildByName("gapminder-treemenu")
                  .markerID("color")
                  .alignX("left")
                  .alignY("top")
                  .updateView()
                  .toggle();
              });

          setIcon(this.dataWarningEl, iconWarn).select("svg").attr("width", "0px").attr("height", "0px");
          this.dataWarningEl.append("text")
              .attr("text-anchor", "end")
              .text(this.translator("hints/dataWarning"));

          this.infoEl
              .html(iconQuestion)
              .select("svg").attr("width", "0px").attr("height", "0px");

          //TODO: move away from UI strings, maybe to ready or ready once
          this.infoEl.on("click", function () {
              window.open(sizeMetadata.sourceLink, "_blank").focus();
          })

          this.dataWarningEl
              .on("click", function () {
                  _this.parent.findChildByName("gapminder-datawarning").toggle();
              })
              .on("mouseover", function () {
                  _this.updateDoubtOpacity(1);
              })
              .on("mouseout", function () {
                  _this.updateDoubtOpacity();
              })
      },

      // show size number on title when hovered on a bubble
      updateTitleNumbers: function(){
          var _this = this;

          var mobile; // if is mobile device and only one bubble is selected, update the ytitle for the bubble
          if (_this.isMobile && _this.model.entities.select && _this.model.entities.select.length === 1) {
            mobile = _this.model.entities.select[0];
          }

          if(_this.hovered || mobile) {
            var hovered = _this.hovered || mobile;
            var formatterS = _this.model.marker.size.tickFormatter;
            var formatterC = _this.model.marker.color.tickFormatter;

            _this.yTitleEl.select("text")
              .text(_this.translator("buttons/size") + ": " +
                    formatterS(_this.values.size[hovered[_this.KEY]]) + " " +
                    _this.translator("unit/" + _this.model.marker.size.which));

            _this.cTitleEl.select("text")
              .text(_this.translator("buttons/color") + ": " +
                    formatterC(_this.values.color[hovered[_this.KEY]]) + " " +
                    _this.translator("unit/" + _this.model.marker.color.which));

            this.infoEl.classed("vzb-hidden", true);
          }else{
            this.yTitleEl.select("text")
                .text(this.translator("buttons/size") + ": " + this.strings.title.S);
            this.cTitleEl.select("text")
                .text(this.translator("buttons/color") + ": " + this.strings.title.C);

            this.infoEl.classed("vzb-hidden", false);
          }
      },

      updateDoubtOpacity: function (opacity) {
          if (opacity == null) opacity = this.wScale(+this.time.getUTCFullYear().toString());
          if (this.someSelected) opacity = 1;
          this.dataWarningEl.style("opacity", opacity);
      },

      updateOpacity: function () {
          var _this = this;
          /*
          this.entityBubbles.classed("vzb-selected", function (d) {
              return _this.model.entities.isSelected(d);
          });
          */

          var OPACITY_HIGHLT = 1.0;
          var OPACITY_HIGHLT_DIM = .3;
          var OPACITY_SELECT = 1.0;
          var OPACITY_REGULAR = this.model.entities.opacityRegular;
          var OPACITY_SELECT_DIM = this.model.entities.opacitySelectDim;

          this.entityBubbles.style("opacity", function (d) {

              if (_this.someHighlighted) {
                  //highlight or non-highlight
                  if (_this.model.entities.isHighlighted(d)) return OPACITY_HIGHLT;
              }

              if (_this.someSelected) {
                  //selected or non-selected
                  return _this.model.entities.isSelected(d) ? OPACITY_SELECT : OPACITY_SELECT_DIM;
              }

              if (_this.someHighlighted) return OPACITY_HIGHLT_DIM;

              return OPACITY_REGULAR;

          });

          this.entityBubbles.classed("vzb-selected", function (d) {
              return _this.model.entities.isSelected(d)
          });

          var someSelectedAndOpacityZero = _this.someSelected && _this.model.entities.opacitySelectDim < .01;

          // when pointer events need update...
          if (someSelectedAndOpacityZero !== this.someSelectedAndOpacityZero_1) {
              this.entityBubbles.style("pointer-events", function (d) {
                  return (!someSelectedAndOpacityZero || _this.model.entities.isSelected(d)) ?
                      "visible" : "none";
              });
          }

          this.someSelectedAndOpacityZero_1 = _this.someSelected && _this.model.entities.opacitySelectDim < .01;
      },

      /**
       * Changes labels for indicators
       */
      updateIndicators: function () {
        this.sScale = this.model.marker.size.getScale();
        this.cScale = this.model.marker.color.getScale();
      },

      /**
       * Updates entities
       */
      updateEntities: function () {

        var _this = this;
        var KEY = this.KEY;
        var TIMEDIM = this.TIMEDIM;

        var getKeys = function(prefix) {
          prefix = prefix || "";
          return this.model.marker.getKeys()
            .map(function(d) {
              var pointer = {};
              pointer[KEY] = d[KEY];
              pointer[TIMEDIM] = endTime;
              pointer.sortValue = _this.values.size[d[KEY]]||0;
              pointer[KEY] = prefix + d[KEY];
              return pointer;
            })
            .sort(function(a, b) {
              return b.sortValue - a.sortValue;
            })
        };
          
        // get array of GEOs, sorted by the size hook
        // that makes larger bubbles go behind the smaller ones
        var endTime = this.model.time.end;
        _this.values = this.model.marker.getFrame(endTime);
        this.model.entities.setVisible(getKeys.call(this));


        // TODO: add to csv
        //Africa 9.1021° N, 18.2812°E
        //Europe 53.0000° N, 9.0000° E
        //Asia 49.8380° N, 105.8203° E
        //north American 48.1667° N and longitude 100.1667° W
        /*
        var pos = {
          "afr": {lat: 9.1, lng: 18.3},
          "eur": {lat: 53.0, lng: 9.0},
          "asi": {lat: 49.8, lng: 105.8},
          "ame": {lat: 48.2, lng: -100.2},
        };
        */


        this.entityBubbles = this.bubbleContainer.selectAll('.vzb-bmc-bubble')
          .data(this.model.entities.getVisible(), function(d) { return d[KEY]; })
          .order();

        //exit selection
        this.entityBubbles.exit().remove();

        //enter selection -- init circles
        this.entityBubbles.enter().append("circle")
          .attr("class", "vzb-bmc-bubble")
          .on("mouseover", function (d, i) {
              if (isTouchDevice()) return;
              _this._interact()._mouseover(d, i);
          })
          .on("mouseout", function (d, i) {
              if (isTouchDevice()) return;
              _this._interact()._mouseout(d, i);
          })
          .on("click", function (d, i) {
              if (isTouchDevice()) return;
              _this._interact()._click(d, i);
              _this.highlightEntities();
          })
          .onTap(function (d, i) {
              _this._interact()._click(d, i);
              d3.event.stopPropagation();
          })
          .onLongTap(function (d, i) {
          })

      },
        
      redrawDataPoints: function(duration, reposition){
        var _this = this;  
        if(!duration) duration = this.duration;
        if(!reposition) reposition = true;
          
        this.entityBubbles.each(function(d, index){
          var view = d3.select(this);

          var valueX = _this.values.lng[d[_this.KEY]]||0;
          var valueY = _this.values.lat[d[_this.KEY]]||0;
          var valueS = _this.values.size[d[_this.KEY]]||0;
          var valueC = _this.values.color[d[_this.KEY]];
          var valueL = _this.values.label[d[_this.KEY]];

          d.hidden_1 = d.hidden;
          d.hidden = !valueS || !valueX || !valueY;

          if(d.hidden !== d.hidden_1) view.classed("vzb-hidden", d.hidden);
            
          if(!d.hidden){
              
              d.r = areaToRadius(_this.sScale(valueS));
              d.label = valueL;
              
              view.classed("vzb-hidden", false)
                  .attr("fill", valueC?_this.cScale(valueC):"transparent")
              
              if(reposition){
                  d.cLoc = _this.skew(_this.projection([valueX, valueY]));
                  
                  view.attr("cx", d.cLoc[0])
                      .attr("cy", d.cLoc[1]);
              }

              if(duration){
                  view.transition().duration(duration).ease("linear")
                      .attr("r", d.r);
              }else{
                  view.interrupt()
                      .attr("r", d.r);
              }

              _this._updateLabel(d, index, d.cLoc[0], d.cLoc[1], d.r, d.label, duration);
            }

          });
      },

      /*
       * UPDATE TIME:
       * Ideally should only update when time or data changes
       */
      updateTime: function() {
        var _this = this;

        this.time_1 = this.time == null ? this.model.time.value : this.time;
        this.time = this.model.time.value;
        this.duration = this.model.time.playing && (this.time - this.time_1 > 0) ? this.model.time.delayAnimations : 0;
        this.year.setText(this.model.time.timeFormat(this.time));
        this.values = this.model.marker.getFrame(this.time);

        //possibly update the exact value in size title
        this.updateTitleNumbers();
      },

        
      fitSizeOfTitles: function(){
          
        //reset font sizes first to make the measurement consistent
        var yTitleText = this.yTitleEl.select("text")
          .style("font-size", null);
        var cTitleText = this.cTitleEl.select("text")
          .style("font-size", null);    

        var yTitleText = this.yTitleEl.select("text");
        var cTitleText = this.cTitleEl.select("text");

        var yTitleBB = yTitleText.node().getBBox();
        var cTitleBB = cTitleText.classed('vzb-hidden') ? yTitleBB : cTitleText.node().getBBox();

        var font = 
            Math.max(parseInt(yTitleText.style("font-size")), parseInt(cTitleText.style("font-size"))) 
            * this.width / Math.max(yTitleBB.width, cTitleBB.width);
          
        if(Math.max(yTitleBB.width, cTitleBB.width) > this.width) {
          yTitleText.style("font-size", font + "px");
          cTitleText.style("font-size", font + "px");
        } else {
          
          // Else - reset the font size to default so it won't get stuck
          yTitleText.style("font-size", null);
          cTitleText.style("font-size", null);
        }
          
      },
        
        
      /**
       * Executes everytime the container or vizabi is resized
       * Ideally,it contains only operations related to size
       */
      updateSize: function () {

        var _this = this;
        var margin, infoElHeight;

        var profiles = {
          small: {
            margin: { top: 10, right: 10, left: 10, bottom: 0 },
            infoElHeight: 16,
            minRadius: 0.5,
            maxRadius: 30
          },
          medium: {
            margin: { top: 20, right: 20, left: 20, bottom: 30 },
            infoElHeight: 20,
            minRadius: 1,
            maxRadius: 55
          },
          large: {
            margin: { top: 30, right: 30, left: 30, bottom: 35 },
            infoElHeight: 22,
            minRadius: 1,
            maxRadius: 70
          }
        };

        var presentationProfileChanges = {
          medium: {
            infoElHeight: 26
          },
          large: {
            infoElHeight: 32
          }
        };

        this.activeProfile = this.getActiveProfile(profiles, presentationProfileChanges);
        margin = this.activeProfile.margin;
        infoElHeight = this.activeProfile.infoElHeight;

        //stage
        var height = this.height = parseInt(this.element.style("height"), 10) - margin.top - margin.bottom;
        var width = this.width = parseInt(this.element.style("width"), 10) - margin.left - margin.right;
        var boundBox = this.boundBox;
        var viewBox = [ boundBox[0][0] * this.defaultWidth,
                        boundBox[0][1] * this.defaultHeight,
                        Math.abs(boundBox[1][0] - boundBox[0][0]) * this.defaultWidth,
                        Math.abs(boundBox[1][1] - boundBox[0][1]) * this.defaultHeight];

        this.graph
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        this.year.resize(this.width, this.height,
          Math.min(this.width/2.5, Math.max(this.height / 4, this.width / 4)) / 2.5);

        this.mapSvg
          .attr('width', width)
          .attr('height', height)
          .attr('viewBox', viewBox.join(' '))
          .attr('preserveAspectRatio', 'none')
          .style("transform", "translate3d(" + margin.left + "px," + margin.top + "px,0)");

        //update scales to the new range
        //this.updateMarkerSizeLimits();
        //this.sScale.range([0, this.height / 4]);

        var skew = this.skew = (function () {
          var vb = viewBox;
          var w = width;
          var h = height;
          var vbCenter = [vb[0] + vb[2] / 2, vb[1] + vb[3] / 2];
          var vbWidth = vb[2] || 0.001;
          var vbHeight = vb[3] || 0.001;
          //input pixel loc after projection, return pixel loc after skew;
          return function (points) {
            var x = (points[0] - vbCenter[0]) / vbWidth * width + width / 2;
            var y = (points[1] - vbCenter[1]) / vbHeight * height + height / 2;
            return [x, y];
          }
        }());


        this.yTitleEl
            .style("font-size", infoElHeight)
            .attr("transform", "translate(0," + margin.top + ")")

        var yTitleBB = this.yTitleEl.select("text").node().getBBox();

        this.cTitleEl.select("text")
            .attr("transform", "translate(" + 0 + "," + (margin.top + yTitleBB.height) + ")")
            .classed("vzb-hidden", this.model.marker.color.which.indexOf("geo") != -1);

        var warnBB = this.dataWarningEl.select("text").node().getBBox();
        this.dataWarningEl.select("svg")
            .attr("width", warnBB.height * 0.75)
            .attr("height", warnBB.height * 0.75)
            .attr("x", -warnBB.width - warnBB.height * 1.2)
            .attr("y", -warnBB.height * 0.65)

        this.dataWarningEl
            .attr("transform", "translate(" + (this.width) + "," + (this.height - warnBB.height * 0.5) + ")")
            .select("text");

        if(this.infoEl.select('svg').node()) {
            var titleBBox = this.yTitleEl.node().getBBox();
            var translate = d3.transform(this.yTitleEl.attr('transform')).translate;

            this.infoEl.select('svg')
                .attr("width", infoElHeight)
                .attr("height", infoElHeight)
            this.infoEl.attr('transform', 'translate('
                + (titleBBox.x + translate[0] + titleBBox.width + infoElHeight * .4) + ','
                + (translate[1] - infoElHeight * 0.8) + ')');
        }
      },

      updateMarkerSizeLimits: function() {
        var _this = this;
        var minRadius = this.activeProfile.minRadius;
        var maxRadius = this.activeProfile.maxRadius;

        this.minRadius = Math.max(maxRadius * this.model.marker.size.domainMin, minRadius);
        this.maxRadius = Math.max(maxRadius * this.model.marker.size.domainMax, minRadius);

        if(this.model.marker.size.scaleType !== "ordinal") {
          this.sScale.range([radiusToArea(_this.minRadius), radiusToArea(_this.maxRadius)]);
        } else {
          this.sScale.rangePoints([radiusToArea(_this.minRadius), radiusToArea(_this.maxRadius)], 0).range();
        }

      },

      _interact: function () {
          var _this = this;

          return {
              _mouseover: function (d, i) {
                  if (_this.model.time.dragging) return;

                  _this.model.entities.highlightEntity(d);

                  _this.hovered = d;
                  //put the exact value in the size title
                  _this.updateTitleNumbers();
                  _this.fitSizeOfTitles();

                  if (_this.model.entities.isSelected(d)) { // if selected, not show hover tooltip
                    _this._setTooltip();
                  } else {
                    //position tooltip
                    _this._setTooltip(d);
                  }
              },
              _mouseout: function (d, i) {
                  if (_this.model.time.dragging) return;
                  _this._setTooltip();
                  _this.hovered = null;
                  _this.updateTitleNumbers();
                  _this.fitSizeOfTitles();
                  _this.model.entities.clearHighlighted();
              },
              _click: function (d, i) {
                  _this.model.entities.selectEntity(d);
              }
          };

      },


      highlightEntities: function () {
          var _this = this;
          this.someHighlighted = (this.model.entities.highlight.length > 0);


    //      if (!this.selectList || !this.someSelected) return;
    //      this.selectList.classed("vzb-highlight", function (d) {
    //          return _this.model.entities.isHighlighted(d);
    //      });
    //      this.selectList.each(function (d, i) {
    //        d3.select(this).selectAll(".vzb-bmc-label-x")
    //          .classed("vzb-invisible", function(n) {
    //            return !_this.model.entities.isHighlighted(d);
    //          });
    //
    //      });

      },

      _updateLabel: function(d, index, valueX, valueY, scaledS, valueL, duration) {
        var _this = this;
        var KEY = this.KEY;
        if(d[KEY] == _this.druging)
          return;

        if(_this.cached[d[KEY]] == null) _this.cached[d[KEY]] = {};

        var cached = _this.cached[d[KEY]];
        if(duration == null) duration = _this.duration;

        // only for selected entities
        if(_this.model.entities.isSelected(d) && _this.entityLabels != null) {

          var select = find(_this.model.entities.select, function(f) {
            return f[KEY] == d[KEY]
          });

          var lineGroup = _this.entityLines.filter(function(f) {
            return f[KEY] == d[KEY];
          });
          // reposition label
          _this.entityLabels.filter(function(f) {
              return f[KEY] == d[KEY]
            })
            .each(function(groupData) {

              cached.valueX = valueX;
              cached.valueY = valueY;

              var limitedX, limitedY, limitedX0, limitedY0;
              if(cached.scaledS0 == null || cached.labelX0 == null || cached.labelX0 == null) { //initialize label once
                cached.scaledS0 = scaledS;
                cached.labelX0 = valueX / _this.width;
                cached.labelY0 = valueY / _this.height;
              }
              var labelGroup = d3.select(this);

              var text = labelGroup.selectAll(".vzb-bmc-label-content")
                .text(valueL);

              lineGroup.select("line").style("stroke-dasharray", "0 " + scaledS + " 100%");

              var rect = labelGroup.select("rect");

              var contentBBox = text[0][0].getBBox();
              if(!cached.contentBBox || cached.contentBBox.width != contentBBox.width) {
                cached.contentBBox = contentBBox;

                var labelCloseGroup = labelGroup.select(".vzb-bmc-label-x")
                  .attr("x", /*contentBBox.height * .0 + */ 4)
                  .attr("y", contentBBox.height * -1);

                labelCloseGroup.select("circle")
                  .attr("cx", /*contentBBox.height * .0 + */ 4)
                  .attr("cy", contentBBox.height * -1)
                  .attr("r", contentBBox.height * .5);

                labelCloseGroup.select("svg")
                  .attr("x", -contentBBox.height * .5 + 4)
                  .attr("y", contentBBox.height * -1.5)
                  .attr("width", contentBBox.height)
                  .attr("height", contentBBox.height)

                rect.attr("width", contentBBox.width + 8)
                  .attr("height", contentBBox.height * 1.2)
                  .attr("x", -contentBBox.width - 4)
                  .attr("y", -contentBBox.height * .85)
                  .attr("rx", contentBBox.height * .2)
                  .attr("ry", contentBBox.height * .2);
              }

              limitedX0 = cached.labelX0 * _this.width;
              limitedY0 = cached.labelY0 * _this.height;

              cached.labelX_ = select.labelOffset[0] || (-cached.scaledS0 * .75 - 5) / _this.width;
              cached.labelY_ = select.labelOffset[1] || (-cached.scaledS0 * .75 - 11) / _this.height;

              limitedX = limitedX0 + cached.labelX_ * _this.width;
              if(limitedX - cached.contentBBox.width <= 0) { //check left
                cached.labelX_ = (cached.scaledS0 * .75 + cached.contentBBox.width + 10) / _this.width;
                limitedX = limitedX0 + cached.labelX_ * _this.width;
              } else if(limitedX + 15 > _this.width) { //check right
                cached.labelX_ = (_this.width - 15 - limitedX0) / _this.width;
                limitedX = limitedX0 + cached.labelX_ * _this.width;
              }
              limitedY = limitedY0 + cached.labelY_ * _this.height;
              if(limitedY - cached.contentBBox.height <= 0) { // check top 
                cached.labelY_ = (cached.scaledS0 * .75 + cached.contentBBox.height) / _this.height;
                limitedY = limitedY0 + cached.labelY_ * _this.height;
              } else if(limitedY + 10 > _this.height) { //check bottom
                cached.labelY_ = (_this.height - 10 - limitedY0) / _this.height;
                limitedY = limitedY0 + cached.labelY_ * _this.height;
              }

              _this._repositionLabels(d, index, this, limitedX, limitedY, limitedX0, limitedY0, duration, lineGroup);

            })
        } else {
          //for non-selected bubbles
          //make sure there is no cached data
          if(_this.cached[d[KEY]] != null) {
            _this.cached[d[KEY]] = void 0;
          }
        }
      },

      _repositionLabels: function(d, index, context, resolvedX, resolvedY, resolvedX0, resolvedY0, duration, lineGroup) {
        var _this = this;
        var cache = this.cached[d[this.KEY]];

        var labelGroup = d3.select(context);

        var width = parseInt(labelGroup.select("rect").attr("width"));
        var height = parseInt(labelGroup.select("rect").attr("height"));

        var labelX0 = cache.labelX0 * _this.width;
        var labelY0 = cache.labelY0 * _this.height;

        if(resolvedX - width <= 0) { //check left
          cache.labelX_ = (width - labelX0) / this.width;
          resolvedX = labelX0 + cache.labelX_ * this.width;
        } else if(resolvedX + 20 > this.width) { //check right
          cache.labelX_ = (this.width - 20 - labelX0) / this.width;
          resolvedX = labelX0 + cache.labelX_ * this.width;
        }
        if(resolvedY - (height + 30) <= 0) { // check top // not conflict with color label, 25
          cache.labelY_ = (height + 30 - labelY0) / this.height;
          resolvedY = labelY0 + cache.labelY_ * this.height;
        } else if(resolvedY + 13 > this.height) { //check bottom
          cache.labelY_ = (this.height - 13 - labelY0) / this.height;
          resolvedY = cache.labelY0 + cache.labelY_ * this.height;
        }

        if(duration) {
          labelGroup
            .transition().duration(duration).ease("linear")
            .attr("transform", "translate(" + resolvedX + "," + resolvedY + ")");
          lineGroup.transition().duration(duration).ease("linear")
            .attr("transform", "translate(" + resolvedX + "," + resolvedY + ")");
        } else {
          labelGroup
              .interrupt()
              .attr("transform", "translate(" + resolvedX + "," + resolvedY + ")");
          lineGroup
              .interrupt()
              .attr("transform", "translate(" + resolvedX + "," + resolvedY + ")");
        }

        var diffX1 = resolvedX0 - resolvedX;
        var diffY1 = resolvedY0 - resolvedY;
        var diffX2 = 0;
        var diffY2 = 0;

        var angle = Math.atan2(diffX1 + width / 2, diffY1 + height / 2) * 180 / Math.PI;
        // middle bottom
        if(Math.abs(angle) <= 45) {
          diffX2 = width / 2;
          diffY2 = 0
        }
        // right middle
        if(angle > 45 && angle < 135) {
          diffX2 = 0;
          diffY2 = height / 4;
        }
        // middle top
        if(angle < -45 && angle > -135) {
          diffX2 = width - 4;
          diffY2 = height / 4;
        }
        // left middle
        if(Math.abs(angle) >= 135) {
          diffX2 = width / 2;
          diffY2 = height / 2
        }

        lineGroup.selectAll("line")
          .attr("x1", diffX1)
          .attr("y1", diffY1)
          .attr("x2", -diffX2)
          .attr("y2", -diffY2);

      },

      selectEntities: function () {
          var _this = this;
          var KEY = this.KEY;
          this.someSelected = (this.model.entities.select.length > 0);

    //      this._selectlist.rebuild();

          this.entityLabels = this.labelsContainer.selectAll('.vzb-bmc-entity')
            .data(_this.model.entities.select, function(d) {
              //console.log(_this.model.entities.select);
              return(d[KEY]);
            });
          this.entityLines = this.linesContainer.selectAll('.vzb-bmc-entity')
            .data(_this.model.entities.select, function(d) {
              return(d[KEY]);
            });

          this.entityLabels.exit()
            .remove();
          this.entityLines.exit()
            .remove();

          this.entityLines
            .enter().append('g')
            .attr("class", "vzb-bmc-entity")
            .each(function(d, index) {
              d3.select(this).append("line").attr("class", "vzb-bmc-label-line");
            });

          this.entityLabels
            .enter().append("g")
            .attr("class", "vzb-bmc-entity")
            .call(_this.labelDragger)
            .each(function(d, index) {
              var view = d3.select(this);

              view.append("rect");

              view.append("text").attr("class", "vzb-bmc-label-content vzb-label-shadow");

              view.append("text").attr("class", "vzb-bmc-label-content");

              var cross = view.append("g").attr("class", "vzb-bmc-label-x vzb-transparent");
              setIcon(cross, iconClose);

              cross.insert("circle", "svg");

              cross.select("svg")
                .attr("class", "vzb-bmc-label-x-icon")
                .attr("width", "0px")
                .attr("height", "0px");

              cross.on("click", function() {
                _this.model.entities.clearHighlighted();
                //default prevented is needed to distinguish click from drag
                if(d3.event.defaultPrevented) return;
                _this.model.entities.selectEntity(d);
              });
            })
            .on("mouseover", function(d) {
              if(isTouchDevice()) return;
              _this.model.entities.highlightEntity(d);
              d3.select(this).selectAll(".vzb-bmc-label-x")
                .classed("vzb-transparent", false);
            })
            .on("mouseout", function(d) {
              if(isTouchDevice()) return;
              _this.model.entities.clearHighlighted();
              d3.select(this).selectAll(".vzb-bmc-label-x")
                .classed("vzb-transparent", true);
            })
            .on("click", function(d) {
              if (!isTouchDevice()) return;
              var cross = d3.select(this).selectAll(".vzb-bmc-label-x");
              cross.classed("vzb-transparent", !cross.classed("vzb-transparent"));
            })
          

            // hide recent hover tooltip
            if (!_this.hovered || _this.model.entities.isSelected(_this.hovered)) {
              _this._setTooltip();
            }

      },

      _setTooltip: function (d) {
        var _this = this;
        if (d) {
          var tooltipText = d.label;
          var x = d.cLoc[0];
          var y = d.cLoc[1];
          var offset = d.r;
          var mouse = d3.mouse(this.graph.node()).map(function(d) {
            return parseInt(d)
          });
          var xPos, yPos, xSign = -1,
            ySign = -1,
            xOffset = 0,
            yOffset = 0;

          if(offset) {
            xOffset = offset * .71; // .71 - sin and cos for 315
            yOffset = offset * .71;
          }
          //position tooltip
          this.tooltip.classed("vzb-hidden", false)
            //.attr("style", "left:" + (mouse[0] + 50) + "px;top:" + (mouse[1] + 50) + "px")
            .selectAll("text")
            .text(tooltipText);

          var contentBBox = this.tooltip.select('text')[0][0].getBBox();
          if(x - xOffset - contentBBox.width < 0) {
            xSign = 1;
            x += contentBBox.width + 5; // corrective to the block Radius and text padding
          } else {
            x -= 5; // corrective to the block Radius and text padding
          }
          if(y - yOffset - contentBBox.height < 0) {
            ySign = 1;
            y += contentBBox.height;
          } else {
            y -= 11; // corrective to the block Radius and text padding
          }
          if(offset) {
            xPos = x + xOffset * xSign;
            yPos = y + yOffset * ySign; // 5 and 11 - corrective to the block Radius and text padding
          } else {
            xPos = x + xOffset * xSign; // .71 - sin and cos for 315
            yPos = y + yOffset * ySign; // 5 and 11 - corrective to the block Radius and text padding
          }
          this.tooltip.attr("transform", "translate(" + (xPos ? xPos : mouse[0]) + "," + (yPos ? yPos : mouse[1]) +
            ")")

          this.tooltip.select('rect').attr("width", contentBBox.width + 8)
            .attr("height", contentBBox.height * 1.2)
            .attr("x", -contentBBox.width - 4)
            .attr("y", -contentBBox.height * .85)
            .attr("rx", contentBBox.height * .2)
            .attr("ry", contentBBox.height * .2);


        } else {

          this.tooltip.classed("vzb-hidden", true);
        }
      }

    });

    //BAR CHART TOOL
    var BubbleMap = Tool.extend('BubbleMap', {


      /**
       * Initializes the tool (Bar Chart Tool).
       * Executed once before any template is rendered.
       * @param {Object} placeholder Placeholder element for the tool
       * @param {Object} external_model Model as given by the external page
       */
      init: function(placeholder, external_model) {

        this.name = "bubblemap";

        //specifying components
        this.components = [{
          component: BubbleMapComponent,
          placeholder: '.vzb-tool-viz',
          model: ["state.time", "state.entities", "state.marker", "language", "ui"] //pass models to component
        }, {
          component: TimeSlider,
          placeholder: '.vzb-tool-timeslider',
          model: ["state.time"]
        }, {
          component: Dialogs,
          placeholder: '.vzb-tool-dialogs',
          model: ['state', 'ui', 'language']
        }, {
          component: ButtonList,
          placeholder: '.vzb-tool-buttonlist',
          model: ['state', 'ui', 'language']
        }, {
          component: TreeMenu,
          placeholder: '.vzb-tool-treemenu',
          model: ['state.marker', 'language']
        }, {
          component: DataWarning,
          placeholder: '.vzb-tool-datawarning',
          model: ['language']
        }
        ];

        //constructor is the same as any tool
        this._super(placeholder, external_model);
      },

      default_model: {
        state: {
          time: {},
          entities: {
            dim: "geo",
            show: {
              _defs_: {
                "geo": ["*"],
                "geo.cat": ["region"]
              }
            }
          },
          marker: {
            space: ["entities", "time"],
            label: {
              use: "property",
              which: "geo.name"
            },
            axis_y: {
              use: "indicator",
              which: "lex"
            },
            axis_x: {
              use: "property",
              which: "geo.name"
            },
            color: {
              use: "property",
              which: "geo.region"
            }
          }
        },
        data: {
          reader: "csv",
          path: "data/waffles/basic-indicators.csv"
        },
        ui: {
          presentation: true
        }
      }
    });

    var prefix = "";
    var deleteClasses = [];
    var SVGHEADER = '<?xml version="1.0" encoding="utf-8"?>';

    var Exporter = Class.extend({

      init: function(context) {
        this.context = context;
        this.shapes = [];
        this.groups = [];
        this.counter = 0;
        this.name = "";
        this.label = "";
      },

      reset: function() {
        this.container.remove();
        this.context.element.selectAll(".vzb-export-redball").remove();
        this.context.element.selectAll(".vzb-export-counter").remove();
        this.counter = 0;
      },

      prefix: function(arg) {
        if(!arguments.length) return prefix;
        prefix = arg;
        return this;
      },
      deleteClasses: function(arg) {
        if(!arguments.length) return deleteClasses;
        deleteClasses = arg;
        return this;
      },

      open: function(element, name) {
        var _this = this;

        //reset if some exports exists on opening
        if(this.svg) this.reset();

        if(!element) element = this.context.element;
        if(!name) name = this.context.name;
        this.name = name;

        var width = parseInt(element.style("width"), 10);
        var height = parseInt(element.style("height"), 10);

        this.container = element.append("div").attr("class", "vzb-svg-export");
        this.svg = this.container.node().appendChild(element.select("svg").node().cloneNode(true));
        this.svg = d3.select(this.svg);
        this.svg
          .attr("viewBox", "0 0 " + width + " " + height)
          .attr("version", "1.1")
          .attr("param1", "http://www.w3.org/2000/svg")
          .attr("param2", "http://www.w3.org/1999/xlink")
          .attr("x", "0px")
          .attr("y", "0px")
          .attr("style", "enable-background:new " + "0 0 " + width + " " + height)
          .attr("xml:space", "preserve");

        this.redBall = element.append("div")
          .attr("class", "vzb-export-redball")
          .style("position", "absolute")
          .style("top", "20px")
          .style("right", "20px")
          .style("width", "20px")
          .style("height", "20px")
          .style("background", "red")
          .style("color", "white")
          .style("text-align", "center")
          .style("border-radius", "10px")
          .style("font-size", "14px")
          .style("line-height", "20px")
          .style("opacity", .8)
          .style("cursor", "pointer")
          .on("mouseover", function() {
            d3.select(this).style("opacity", 1).text("▼");
            _this.counterEl.text("Download");
          })
          .on("mouseout", function() {
            d3.select(this).style("opacity", .8).text("");
            _this.counterEl.text(_this.label);
          })
          .on("click", function() {
            _this.close()
          });

        this.counterEl = element.append("div")
          .attr("class", "vzb-export-counter")
          .style("position", "absolute")
          .style("top", "20px")
          .style("right", "45px")
          .style("color", "red")
          .style("opacity", .8)
          .style("line-height", "20px")
          .style("font-size", "14px")
          .style("text-align", "center")



        this.root = this.svg.select("." + prefix + "graph");

        this.root.selectAll("g, text, svg, line, rect")
          .filter(function() {
            var view = d3.select(this);
            var result = false;
            deleteClasses.forEach(function(one) {
              result = result || view.classed(one);
            })
            return result;
          })
          .remove();

        this.svg.selectAll(".tick line")
          .attr("fill", "none")
          .attr("stroke", "#999");
        this.svg.selectAll("." + prefix + "axis-x path")
          .attr("fill", "none")
          .attr("stroke", "#999");
        this.svg.selectAll("." + prefix + "axis-y path")
          .attr("fill", "none")
          .attr("stroke", "#999");
      },

      write: function(me) {
        var groupBy = "time";

        if(!this.root) this.open();

        //avoid writing the same thing again
        if(this.shapes.indexOf(me.id + "_" + me.time) > -1) return;

        this.shapes.push(me.id + "_" + me.time);


        // check if need to create a new group and do so
        if(this.groups.indexOf(me[groupBy]) == -1) {
          this.root.append("g").attr("id", "g_" + me[groupBy]);
          this.groups.push(me[groupBy]);
        }

        // put a marker into the group
        if(me.opacity == null) me.opacity = .5;
        if(me.fill == null) me.fill = "#ff80dd";

        var marker = this.root.select("#g_" + me[groupBy])
          .append(me.type)
          .attr("id", me.id + "_" + me.time)
          .style("fill", me.fill)
          .style("opacity", me.opacity);

        switch(me.type) {
          case "path":
            marker
              .attr("d", me.d);
            break;

          case "circle":
            marker
              .attr("cx", me.cx)
              .attr("cy", me.cy)
              .attr("r", me.r);
            break;
        }

        this.counter++;
        this.redBall.style("opacity", this.counter % 10 / 12 + .2);
        this.label = me.type + " shapes: " + this.counter;
        this.counterEl.text(this.label);
      },

      close: function() {

        var result = SVGHEADER + " " + this.container.node().innerHTML
          .replace("param1", "xmlns")
          .replace("param2", "xmlns:xlink")
          //round all numbers in SVG code 
          .replace(/\d+(\.\d+)/g, function(x) {
            return Math.round(+x * 100) / 100 + ""
          });


        if(result.length / 1024 / 1024 > 2) {

          alert("The file size is " + Math.round(result.length / 1024) +
            "kB, which is too large to download. Will try to print it in the console instead...")
          console.log(result);

        } else {

          var link = document.createElement('a');
          link.download = this.name + " " + this.counter + " shapes" + ".svg";
          link.href = 'data:,' + result;
          link.click();
        }
      }



    });

    var MCMath = Class.extend({

            init: function (context) {
                this.context = context;
                
                this.xScaleFactor = 1;
                this.xScaleShift = 0;
            },
                    
            rescale: function (x) {
                return Math.exp(this.xScaleFactor * Math.log(x) + this.xScaleShift);
            },
            unscale: function (x) {
                return Math.exp((Math.log(x) - this.xScaleShift) / this.xScaleFactor);
            },

            generateMesh: function (length, scaleType, domain) {
                // span a uniform mesh across the entire X scale
                // if the scale is log, the mesh would be exponentially distorted to look uniform
                
                var rangeFrom = scaleType === "linear" ? domain[0] 
                    : Math.log(this.unscale(domain[0]));
                
                var rangeTo = scaleType === "linear" ? domain[1] 
                    : Math.log(this.unscale(domain[1]));
                
                var rangeStep = (rangeTo - rangeFrom) / length;
                
                var mesh = d3.range(rangeFrom, rangeTo, rangeStep).concat(rangeTo);

                if (scaleType !== "linear") {
                    mesh = mesh.map(function (dX) { return Math.exp(dX); });
                } else {
                    mesh = mesh.filter(function (dX) { return dX > 0; });
                }

                return mesh;
            },
            
            gdpToMu: function(gdp, sigma, xScaleFactor, xScaleShift){
                // converting gdp per capita per day into MU for lognormal distribution
                // see https://en.wikipedia.org/wiki/Log-normal_distribution
                return Math.log(gdp/365) - sigma*sigma/2;
            },
            
            giniToSigma: function (gini) {
                // The ginis are turned into std deviation. 
                // Mattias uses this formula in Excel: stddev = NORMSINV( ((gini/100)+1)/2 )*2^0.5
                return this.normsinv( ( (gini / 100) + 1 ) / 2 ) * Math.pow(2,0.5);
            },
                                 
            // this function returns PDF values for a specified distribution
            pdf: {
                normal: function(x, mu, sigma){
                    return Math.exp(
                        - .5 * Math.log(2 * Math.PI)
                        - Math.log(sigma)
                        - Math.pow(x - mu, 2) / (2 * sigma * sigma)
                        );
                },
                lognormal: function(x, mu, sigma){
                    return Math.exp(
                        - .5 * Math.log(2 * Math.PI) //should not be different for the two scales- (scaleType=="linear"?Math.log(x):0)
                        - Math.log(sigma)
                        - Math.pow(Math.log(x) - mu, 2) / (2 * sigma * sigma)
                    );
                }
            },

            
            normsinv: function (p) {
                //
                // Lower tail quantile for standard normal distribution function.
                //
                // This function returns an approximation of the inverse cumulative
                // standard normal distribution function.  I.e., given P, it returns
                // an approximation to the X satisfying P = Pr{Z <= X} where Z is a
                // random variable from the standard normal distribution.
                //
                // The algorithm uses a minimax approximation by rational functions
                // and the result has a relative error whose absolute value is less
                // than 1.15e-9.
                //
                // Author:      Peter John Acklam
                // (Javascript version by Alankar Misra @ Digital Sutras (alankar@digitalsutras.com))
                // Time-stamp:  2003-05-05 05:15:14
                // E-mail:      pjacklam@online.no
                // WWW URL:     http://home.online.no/~pjacklam
                
                // Taken from http://home.online.no/~pjacklam/notes/invnorm/index.html
                // adapted from Java code
                
                // An algorithm with a relative error less than 1.15*10-9 in the entire region.

                // Coefficients in rational approximations
                var a = [-3.969683028665376e+01, 2.209460984245205e+02, -2.759285104469687e+02, 1.383577518672690e+02, -3.066479806614716e+01, 2.506628277459239e+00];
                var b = [-5.447609879822406e+01, 1.615858368580409e+02, -1.556989798598866e+02, 6.680131188771972e+01, -1.328068155288572e+01];
                var c = [-7.784894002430293e-03, -3.223964580411365e-01, -2.400758277161838e+00, -2.549732539343734e+00, 4.374664141464968e+00, 2.938163982698783e+00];
                var d = [7.784695709041462e-03, 3.224671290700398e-01, 2.445134137142996e+00, 3.754408661907416e+00];

                // Define break-points.
                var plow = .02425;
                var phigh = 1 - plow;

                // Rational approximation for lower region:
                if (p < plow) {
                    var q = Math.sqrt(-2 * Math.log(p));
                    return (((((c[0] * q + c[1]) * q + c[2]) * q + c[3]) * q + c[4]) * q + c[5]) /
                        ((((d[0] * q + d[1]) * q + d[2]) * q + d[3]) * q + 1);
                }

                // Rational approximation for upper region:
                if (phigh < p) {
                    var q = Math.sqrt(-2 * Math.log(1 - p));
                    return -(((((c[0] * q + c[1]) * q + c[2]) * q + c[3]) * q + c[4]) * q + c[5]) /
                        ((((d[0] * q + d[1]) * q + d[2]) * q + d[3]) * q + 1);
                }

                // Rational approximation for central region:
                var q = p - .5;
                var r = q * q;
                return (((((a[0] * r + a[1]) * r + a[2]) * r + a[3]) * r + a[4]) * r + a[5]) * q /
                    (((((b[0] * r + b[1]) * r + b[2]) * r + b[3]) * r + b[4]) * r + 1);

            }



        });

    var MCSelectList = Class.extend({

      init: function (context) {
        this.context = context;

      },

      rebuild: function (data) {
        var _this = this.context;

        var listData = _this.mountainPointers
          .concat(_this.groupedPointers)
          .concat(_this.stackedPointers)
          .filter(function (f) {
            return _this.model.entities.isSelected(f);
          }).sort(function (a, b) {
            if (a.sortValue && b.sortValue) {
              if(a.sortValue[1] === b.sortValue[1]) {
                return d3.descending(a.sortValue[0], b.sortValue[0]);
              }
              return d3.descending(a.sortValue[1], b.sortValue[1]);
            } else {
              if (a.aggrLevel != b.aggrLevel) {
                return d3.descending(a.aggrLevel, b.aggrLevel);
              } else if (a.aggrLevel && b.aggrLevel) {
                return d3.descending(a.yMax, b.yMax);
              } else {
                return 0;
              }
            }
          });
        _this.selectList = _this.mountainLabelContainer.selectAll("g.vzb-mc-label")
          .data(unique(listData, function (d) {
            return d.KEY()
          }));
        _this.selectList.exit().remove();
        _this.selectList.enter().append("g")
          .attr("class", "vzb-mc-label")
          .each(function (d, i) {
            var label = d3.select(this);
            label.append("circle").attr('class', 'vzb-mc-label-legend');
            label.append("text").attr("class", "vzb-mc-label-shadow vzb-mc-label-text");
            label.append("text").attr("class", "vzb-mc-label-text");
            label.append("g").attr("class", "vzb-mc-label-x vzb-label-shadow vzb-invisible")
              .on("click", function (d, i) {
                if (isTouchDevice()) return;
                d3.event.stopPropagation();
                _this.model.entities.clearHighlighted();
                _this.model.entities.selectEntity(d);
                d3.event.stopPropagation();
              })
              .onTap(function (d, i) {
                d3.select("#" + d.geo + "-label").remove();
                _this.model.entities.clearHighlighted();
                _this.model.entities.selectEntity(d);
              });
            var labelCloseGroup = label.select("g.vzb-mc-label-x")
            if (!isTouchDevice()){
              setIcon(labelCloseGroup, iconClose)
                .select("svg")
                .attr("class", "vzb-mc-label-x-icon")
                .attr("width", "0px")
                .attr("height", "0px");

              labelCloseGroup.insert("circle", "svg");

            } else {
              labelCloseGroup.append("rect");
              labelCloseGroup.append("text")
                .attr("class", "vzb-mc-label-x-text")
                .text("Deselect");
            }
          })
          .on("mousemove", function (d, i) {
            if (isTouchDevice()) return;
            _this.model.entities.highlightEntity(d);
          })
          .on("mouseout", function (d, i) {
            if (isTouchDevice()) return;
            _this.model.entities.clearHighlighted();

          })
          .on("click", function (d, i) {
            if (isTouchDevice()) return;
            _this.model.entities.clearHighlighted();
            _this.model.entities.selectEntity(d);
          });
      },

      redraw: function () {
        var _this = this.context;
        if (!_this.selectList || !_this.someSelected) return;

        var sample = _this.mountainLabelContainer.append("g").attr("class", "vzb-mc-label").append("text").text("0");
        var fontHeight = sample[0][0].getBBox().height*1.2;
        var fontSizeToFontHeight = parseFloat(sample.style("font-size")) / fontHeight;
        d3.select(sample[0][0].parentNode).remove();
        var formatter = _this.model.marker.axis_y.tickFormatter;

        var titleHeight = _this.yTitleEl.select("text").node().getBBox().height || 0;

        var maxFontHeight = (_this.height - titleHeight * 3) / (_this.selectList.data().length + 2);
        if(fontHeight > maxFontHeight) fontHeight = maxFontHeight;

        var currentAggrLevel = "null";
        var aggrLevelSpacing = 0;

        _this.selectList
          .attr("transform", function (d, i) {
            if(d.aggrLevel != currentAggrLevel) aggrLevelSpacing += fontHeight;
            var spacing = fontHeight * i + titleHeight * 1.5 + aggrLevelSpacing;
            currentAggrLevel = d.aggrLevel;
            return "translate(0," + spacing + ")";
          })
          .each(function (d, i) {

            var view = d3.select(this).attr("id", d.geo + '-label');
            var name = d.key ? _this.translator("region/" + d.key) : _this.values.label[d.KEY()];
            var number = _this.values.axis_y[d.KEY()];

            var string = name + ": " + formatter(number) + (i === 0 ? " "+ _this.translator("mount/people") : "");

            var text = view.selectAll(".vzb-mc-label-text")
              .attr("x", fontHeight)
              .attr("y", fontHeight)
              .text(string)
              .style("font-size", fontHeight === maxFontHeight ? (fontHeight * fontSizeToFontHeight + "px") : null);

            var contentBBox = text[0][0].getBBox();

            var closeGroup = view.select(".vzb-mc-label-x");

            if (isTouchDevice()) {
              var closeTextBBox = closeGroup.select("text").node().getBBox();
              closeGroup
                .classed("vzb-revert-color", true)
                .select(".vzb-mc-label-x-text")
                .classed("vzb-revert-color", true)
                .attr("x", contentBBox.width + contentBBox.height * 1.12 + closeTextBBox.width * .5)
                .attr("y", contentBBox.height * .55);

              closeGroup.select("rect")
                .attr("width", closeTextBBox.width + contentBBox.height * .6)
                .attr("height", contentBBox.height)
                .attr("x", contentBBox.width + contentBBox.height * .9)
                .attr("y", 0)
                .attr("rx", contentBBox.height * .25)
                .attr("ry", contentBBox.height * .25);
            } else {
              closeGroup
                .attr("x", contentBBox.width + contentBBox.height * 1.1)
                .attr("y", contentBBox.height / 3);

              closeGroup.select("circle")
                .attr("r", contentBBox.height * .4)
                .attr("cx", contentBBox.width + contentBBox.height * 1.1)
                .attr("cy", contentBBox.height / 3);

              closeGroup.select("svg")
                .attr("x", contentBBox.width + contentBBox.height * (1.1 - .4))
                .attr("y", contentBBox.height * (1 / 3 - .4))
                .attr("width", contentBBox.height * .8)
                .attr("height", contentBBox.height * .8);
            }

            view.select(".vzb-mc-label-legend")
              .attr("r", fontHeight / 3)
              .attr("cx", fontHeight * .4)
              .attr("cy", fontHeight / 1.5)
              .style("fill", _this.cScale(_this.values.color[d.KEY()]));

            view.onTap(function (d, i) {
              d3.event.stopPropagation();
              _this.model.entities.highlightEntity(d);
              setTimeout(function() {
                _this.model.entities.unhighlightEntity(d);
              }, 2000)
            });
          });
      }
    });

    var MCProbe = Class.extend({

            init: function (context) {
                this.context = context;
                
            },
                    
            redraw: function (options) {
                var _this = this.context;
                if (!options) options = {};

                if (!options.level) options.level = _this.model.time.probeX;

                _this.probeEl.classed("vzb-hidden", !options.level);
                if (!options.level) return;

                _this.xAxisEl.call(_this.xAxis.highlightValue(options.full ? options.level : "none"));

                var sumValue = 0;
                var totalArea = 0;
                var leftArea = 0;

                var _computeAreas = function (d) {
                    sumValue += _this.values.axis_y[d.KEY()];
                    _this.cached[d.KEY()].forEach(function (d) {
                        totalArea += d.y;
                        if (_this._math.rescale(d.x) < options.level) leftArea += d.y;
                    })
                };

                if (_this.model.marker.stack.which === "all") {
                    _this.stackedPointers.forEach(_computeAreas);
                } else if (_this.model.marker.stack.which === "none") {
                    _this.mountainPointers.forEach(_computeAreas);
                } else {
                    _this.groupedPointers.forEach(_computeAreas);
                }

                var formatter1 = d3.format(".3r");
                var formatter2 = _this.model.marker.axis_y.tickFormatter;
                _this.heightOfLabels = _this.heightOfLabels || (.66 * this.height);

                _this.probeTextEl.each(function (d, i) {
                    if (i !== 8) return;
                    var view = d3.select(this);

                    if (!options.full && _this.model.time.probeX == _this.model.time.tailFatX) {

                        view.text(_this.translator("mount/extremepoverty"))
                            .classed("vzb-hidden", false)
                            .attr("x", -_this.height)
                            .attr("y", _this.xScale(options.level))
                            .attr("dy", "-1em")
                            .attr("dx", "0.5em")
                            .attr("transform", "rotate(-90)");

                        _this.heightOfLabels = _this.height - view.node().getBBox().width - view.node().getBBox().height * 1.75;
                    }else{
                        view.classed("vzb-hidden", true);
                    }
                });


                _this.probeTextEl.each(function (d, i) {
                    if (i === 8) return;
                    var view = d3.select(this);

                    var string;
                    if (i === 0 || i === 4) string = formatter1(leftArea / totalArea * 100) + "%";
                    if (i === 1 || i === 5) string = formatter1(100 - leftArea / totalArea * 100) + "%";
                    if (i === 2 || i === 6) string = formatter2(sumValue * leftArea / totalArea);
                    if (i === 3 || i === 7) string = formatter2(sumValue * (1 - leftArea / totalArea)) + " " + _this.translator("mount/people");

                    view.text(string)
                        .classed("vzb-hidden", !options.full && i !== 0 && i !== 4)
                        .attr("x", _this.xScale(options.level) + ([0, 4, 2, 6].indexOf(i) > -1 ? -5 : +5))
                        .attr("y", _this.heightOfLabels)
                        .attr("dy", [0, 1, 4, 5].indexOf(i) > -1 ? 0 : "1.5em");
                });


                _this.probeLineEl
                    .attr("x1", _this.xScale(options.level))
                    .attr("x2", _this.xScale(options.level))
                    .attr("y1", _this.height + 6)
                    .attr("y2", 0);


            },



        });

    var THICKNESS_THRESHOLD = 0.001;

    //MOUNTAIN CHART COMPONENT
    var MountainChartComponent = Component.extend({

        /**
         * Initialize the component
         * Executed once before any template is rendered.
         * @param {Object} config The config passed to the component
         * @param {Object} context The component's parent
         */
        init: function (config, context) {

            var _this = this;
            this.name = "mountainchart";
            this.template = "mountainchart.html";

            //define expected models for this component
            this.model_expects = [
                { name: "time", type: "time" },
                { name: "entities", type: "entities" },
                { name: "marker", type: "model" },
                { name: "language", type: "language" }
            ];

            //attach event listeners to the model items
            this.model_binds = {
                "change:time.value": function (evt) {
                    //console.log("MONT: " + evt);
                    _this.updateTime();
                    _this.redrawDataPoints();
                    _this._selectlist.redraw();
                    _this._probe.redraw();
                    _this.updateDoubtOpacity();
                },
                "change:time.playing": function (evt) {
                    // this listener is a patch for fixing #1228. time.js doesn't produce the last event
                    // with playing == false when paused softly
                    if(!_this.model.time.playing){
                        _this.redrawDataPoints();
                    }
                },
                "change:time.xScaleFactor": function () {
                    _this.ready();
                },
                "change:time.xScaleShift": function () {
                    _this.ready();
                },
                "change:time.tailCutX": function () {
                    _this.ready();
                },
                "change:time.tailFade": function () {
                    _this.ready();
                },
                "change:time.probeX": function () {
                    _this.ready();
                },
                "change:time.xPoints": function () {
                    _this.ready();
                },
                "change:time.xLogStops": function () {
                    _this.updateSize();
                },
                "change:time.yMaxMethod": function () {
                    _this._adjustMaxY({ force: true });
                    _this.redrawDataPoints();
                },
                "change:time.record": function (evt) {
                    if (_this.model.time.record) {
                        _this._export.open(this.element, this.name);
                    } else {
                        _this._export.reset();
                    }
                },
                "change:entities.highlight": function (evt) {
                    if (!_this._readyOnce) return;
                    _this.highlightEntities();
                    _this.updateOpacity();
                },
                "change:entities.select": function (evt) {
                    if (!_this._readyOnce) return;
                    _this.selectEntities();
                    _this._selectlist.redraw();
                    _this.updateOpacity();
                    _this.updateDoubtOpacity();
                    _this.redrawDataPoints();
                },
                "change:entities.opacitySelectDim": function (evt) {
                    _this.updateOpacity();
                },
                "change:entities.opacityRegular": function (evt) {
                    _this.updateOpacity();
                },
                "change:marker": function (evt, path) {
                    if (!_this._readyOnce) return;
                    if(path.indexOf("scaleType") > -1) {
                        _this.ready();
                        return;
                    }
                    if (path.indexOf("zoomedMin") > -1 || path.indexOf("zoomedMax") > -1) {
                        _this.zoomToMaxMin();
                        _this.redrawDataPoints();
                        _this._probe.redraw();
                        return;
                    }
                },
                "change:marker.group": function (evt, path) {
                    if (!_this._readyOnce) return;
                    if (path.indexOf("group.merge") > -1) return;
                    _this.ready();
                },
                "change:marker.group.merge": function (evt) {
                    if (!_this._readyOnce) return;
                    _this.updateTime();
                    _this.redrawDataPoints();
                },
                "change:marker.stack": function (evt) {
                    if (!_this._readyOnce) return;
                    _this.ready();
                },
                "change:marker.color.palette": function (evt) {
                    if (!_this._readyOnce) return;
                    _this.redrawDataPointsOnlyColors();
                    _this._selectlist.redraw();
                },
            };

            this._super(config, context);

            this._math = new MCMath(this);
            this._export = new Exporter(this);
            this._export
                .prefix("vzb-mc-")
                .deleteClasses(["vzb-mc-mountains-mergestacked", "vzb-mc-mountains-mergegrouped", "vzb-mc-mountains", "vzb-mc-year", "vzb-mc-mountains-labels", "vzb-mc-axis-labels"]);
            this._probe = new MCProbe(this);
            this._selectlist = new MCSelectList(this);

            // define path generator
            this.area = d3.svg.area()
                .interpolate("basis")
                .x(function (d) {
                    return _this.xScale(_this._math.rescale(d.x));
                })
                .y0(function (d) {
                    return _this.yScale(d.y0);
                })
                .y1(function (d) {
                    return _this.yScale(d.y0 + d.y);
                });

            //define d3 stack layout
            this.stack = d3.layout.stack()
                .order("reverse")
                .values(function (d) {
                    return _this.cached[d.KEY()];
                })
                .out(function out(d, y0, y) {
                    d.y0 = y0;
                });

            // init internal variables
            this.xScale = null;
            this.yScale = null;
            this.cScale = null;

            this.xAxis = axisSmart();


            this.rangeRatio = 1;
            this.rangeShift = 0;
            this.cached = {};
            this.mesh = [];
            this.yMax = 0;
        },

        domReady: function () {
            var _this = this;

            // reference elements
            this.element = d3.select(this.element);
            this.graph = this.element.select(".vzb-mc-graph");
            this.xAxisEl = this.graph.select(".vzb-mc-axis-x");
            this.xTitleEl = this.graph.select(".vzb-mc-axis-x-title");
            this.yTitleEl = this.graph.select(".vzb-mc-axis-y-title");
            this.infoEl = this.graph.select(".vzb-mc-axis-info");
            this.dataWarningEl = this.graph.select(".vzb-data-warning");

            this.yearEl = this.graph.select(".vzb-mc-year");
            this.year = new DynamicBackground(this.yearEl);

            this.mountainMergeStackedContainer = this.graph.select(".vzb-mc-mountains-mergestacked");
            this.mountainMergeGroupedContainer = this.graph.select(".vzb-mc-mountains-mergegrouped");
            this.mountainAtomicContainer = this.graph.select(".vzb-mc-mountains");
            this.mountainLabelContainer = this.graph.select(".vzb-mc-mountains-labels");
            this.tooltip = this.element.select(".vzb-mc-tooltip");
            this.eventAreaEl = this.element.select(".vzb-mc-eventarea");
            this.probeEl = this.element.select(".vzb-mc-probe");
            this.probeLineEl = this.probeEl.select("line");
            this.probeTextEl = this.probeEl.selectAll("text");

            this.element
                .onTap(function (d, i) {
                    _this._interact()._mouseout(d, i);
                });
        },

        afterPreload: function () {
            var _this = this;

            var yearNow = _this.model.time.value.getUTCFullYear();
            var yearEnd = _this.model.time.end.getUTCFullYear();

            this._math.xScaleFactor = this.model.time.xScaleFactor;
            this._math.xScaleShift = this.model.time.xScaleShift;

            if (!this.precomputedShapes || !this.precomputedShapes[yearNow] || !this.precomputedShapes[yearEnd]) return;

            var yMax = this.precomputedShapes[this.model.time.yMaxMethod == "immediate" ? yearNow : yearEnd].yMax;
            var shape = this.precomputedShapes[yearNow].shape;

            if (!yMax || !shape || shape.length === 0) return;

            this.xScale = d3.scale.log().domain([this.model.marker.axis_x.domainMin, this.model.marker.axis_x.domainMax]);
            this.yScale = d3.scale.linear().domain([0, +yMax]);

            _this.updateSize(shape.length);
            _this.zoomToMaxMin();

            shape = shape.map(function (m, i) {return {x: _this.mesh[i], y0: 0, y: +m};})

            this.mountainAtomicContainer.selectAll(".vzb-mc-prerender")
                .data([0])
                .enter().append("path")
                .attr("class", "vzb-mc-prerender")
                .style("fill", "pink")
                .style("opacity", 0)
                .attr("d", _this.area(shape))
                .transition().duration(1000).ease("linear")
                .style("opacity", 1);
        },

        readyOnce: function () {

            this.eventAreaEl
                .on("mousemove", function () {
                    if (_this.model.time.dragging) return;
                    _this._probe.redraw({
                        level: _this.xScale.invert(d3.mouse(this)[0]),
                        full: true
                    });
                })
                .on("mouseout", function () {
                    if (_this.model.time.dragging) return;
                    _this._probe.redraw();
                });

            var _this = this;
            this.on("resize", function () {
                //console.log("acting on resize");
                _this.updateSize();
                _this.updateTime(); // respawn is needed
                _this.redrawDataPoints();
                _this._selectlist.redraw();
                _this._probe.redraw();
            });

            this.KEY = this.model.entities.getDimension();
            this.TIMEDIM = this.model.time.getDimension();

            this.mountainAtomicContainer.select(".vzb-mc-prerender").remove();

            this.wScale = d3.scale.linear()
                .domain(this.parent.datawarning_content.doubtDomain)
                .range(this.parent.datawarning_content.doubtRange);
        },

        ready: function () {
            //console.log("ready")
            
            this._math.xScaleFactor = this.model.time.xScaleFactor;
            this._math.xScaleShift = this.model.time.xScaleShift;

            this.updateUIStrings();
            this.updateIndicators();
            this.updateEntities();
            this.updateSize();
            this.zoomToMaxMin();
            this._spawnMasks();
            this.updateTime();
            this._adjustMaxY({force: true});
            this.redrawDataPoints();
            this.redrawDataPointsOnlyColors();
            this.highlightEntities();
            this.selectEntities();
            this._selectlist.redraw();
            this.updateOpacity();
            this.updateDoubtOpacity();
            this._probe.redraw();
        },

        updateSize: function (meshLength) {

            var margin, infoElHeight;
            var padding = 2;

            var profiles = {
              small: {
                margin: { top: 10, right: 10, left: 10, bottom: 18 },
                infoElHeight: 16
              },
              medium: {
                margin: { top: 20, right: 20, left: 20, bottom: 30 },
                infoElHeight: 20
              },
              large: {
                margin: { top: 30, right: 30, left: 30, bottom: 35 },
                infoElHeight: 22
              }
            };

            var presentationProfileChanges = {
              medium: {
                margin: { top: 20, right: 20, left: 20, bottom: 50 },
                infoElHeight: 26
              },
              large: {
                margin: { top: 30, right: 30, left: 30, bottom: 50 },
                infoElHeight: 32
              }
            };

            this.activeProfile = this.getActiveProfile(profiles, presentationProfileChanges);
            margin = this.activeProfile.margin;
            infoElHeight = this.activeProfile.infoElHeight;

            //mesure width and height
            this.height = parseInt(this.element.style("height"), 10) - margin.top - margin.bottom;
            this.width = parseInt(this.element.style("width"), 10) - margin.left - margin.right;

            //graph group is shifted according to margins (while svg element is at 100 by 100%)
            this.graph.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            var yearLabelOptions = {
                topOffset: this.getLayoutProfile()==="large"? margin.top * 2 : 0,
                xAlign: this.getLayoutProfile()==="large"? 'right' : 'center',
                yAlign: this.getLayoutProfile()==="large"? 'top' : 'center',
            };

            var yearLabelFontSize = this.getLayoutProfile()==="large"? this.width / 6 : Math.max(this.height / 4, this.width / 4);

            //year is centered and resized
            this.year
                .setConditions(yearLabelOptions)
                .resize(this.width, this.height, yearLabelFontSize);

            //update scales to the new range
            this.yScale.range([this.height, 0]);
            this.xScale.range([this.rangeShift, this.width * this.rangeRatio + this.rangeShift]);


            //need to know scale type of X to move on
            var scaleType = this._readyOnce ? this.model.marker.axis_x.scaleType : "log";

            //axis is updated
            this.xAxis.scale(this.xScale)
                .orient("bottom")
                .tickSize(6, 0)
                .tickSizeMinor(3, 0)
                .labelerOptions({
                    scaleType: scaleType,
                    toolMargin: margin,
                    pivotingLimit: margin.bottom * 1.5,
                    method: this.xAxis.METHOD_REPEATING,
                    stops: this._readyOnce ? this.model.time.xLogStops : [1]
                });


            this.xAxisEl
                .attr("transform", "translate(0," + this.height + ")")
                .call(this.xAxis);

            this.xTitleEl.select("text")
                .attr("transform", "translate(" + this.width + "," + this.height + ")")
                .attr("dy", "-0.36em");

            this.yTitleEl
              .style("font-size", infoElHeight)
              .attr("transform", "translate(0," + margin.top + ")")


            var warnBB = this.dataWarningEl.select("text").node().getBBox();
            this.dataWarningEl.select("svg")
                .attr("width", warnBB.height)
                .attr("height", warnBB.height)
                .attr("x", warnBB.height * .1)
                .attr("y", -warnBB.height * 1.0 + 1)

            this.dataWarningEl
                .attr("transform", "translate(" + (0) + "," + (margin.top + warnBB.height * 1.5) + ")")
                .select("text")
                .attr("dx", warnBB.height * 1.5);

            if(this.infoEl.select('svg').node()) {
                var titleBBox = this.yTitleEl.node().getBBox();
                var translate = d3.transform(this.yTitleEl.attr('transform')).translate;

                this.infoEl.select('svg')
                    .attr("width", infoElHeight)
                    .attr("height", infoElHeight)
                this.infoEl.attr('transform', 'translate('
                    + (titleBBox.x + translate[0] + titleBBox.width + infoElHeight * .4) + ','
                    + (translate[1]-infoElHeight * .8) + ')');
            }

            this.eventAreaEl
                .attr("y", this.height)
                .attr("width", this.width)
                .attr("height", margin.bottom);

            if (!meshLength) meshLength = this.model.time.xPoints;
            this.mesh = this._math.generateMesh(meshLength, scaleType, this.xScale.domain());
        },


        zoomToMaxMin: function(){
            var _this = this;

            if(this.model.marker.axis_x.zoomedMin==null || this.model.marker.axis_x.zoomedMax==null) return;

            var x1 = this.xScale(this.model.marker.axis_x.zoomedMin);
            var x2 = this.xScale(this.model.marker.axis_x.zoomedMax);

            this.rangeRatio = this.width / (x2 - x1) * this.rangeRatio;
            this.rangeShift = (this.rangeShift - x1) / (x2 - x1) * this.width;

            this.xScale.range([this.rangeShift, this.width*this.rangeRatio + this.rangeShift]);

            this.xAxisEl.call(this.xAxis);
        },


        updateUIStrings: function () {
            var _this = this;

            this.translator = this.model.language.getTFunction();
            var xMetadata = this.model.marker.axis_x.getMetadata();


            this.xTitleEl.select("text")
                .text(this.translator("unit/mountainchart_hardcoded_income_per_day"));

            this.yTitleEl.select("text")
                .text(this.translator("mount/title"));

            setIcon(this.dataWarningEl, iconWarn).select("svg").attr("width", "0px").attr("height", "0px");
            this.dataWarningEl.append("text")
                .text(this.translator("hints/dataWarning"));

            setIcon(this.infoEl, iconQuestion).select("svg").attr("width", "0px").attr("height", "0px");

            //TODO: move away from UI strings, maybe to ready or ready once
            this.infoEl.on("click", function () {
                window.open(xMetadata.sourceLink, "_blank").focus();
            })
            this.dataWarningEl
                .on("click", function () {
                    _this.parent.findChildByName("gapminder-datawarning").toggle();
                })
                .on("mouseover", function () {
                    _this.updateDoubtOpacity(1);
                })
                .on("mouseout", function () {
                    _this.updateDoubtOpacity();
                })
        },

        updateDoubtOpacity: function (opacity) {
            if (opacity == null) opacity = this.wScale(+this.time.getUTCFullYear().toString());
            if (this.someSelected) opacity = 1;
            this.dataWarningEl.style("opacity", opacity);
        },

        updateIndicators: function () {
            var _this = this;

            //fetch scales, or rebuild scales if there are none, then fetch
            this.yScale = this.model.marker.axis_y.getScale();
            this.xScale = this.model.marker.axis_x.getScale();
            this.cScale = this.model.marker.color.getScale();

            this.xAxis.tickFormat(_this.model.marker.axis_x.tickFormatter);
        },

        updateEntities: function () {
            var _this = this;

            this.values = this.model.marker.getFrame(this.model.time.end);

            // construct pointers
            this.mountainPointers = this.model.marker.getKeys()
                .filter(function(d) { return 1
                    && _this.values.axis_y[d[_this.KEY]]
                    && _this.values.axis_y[d[_this.KEY]]
                    && _this.values.size[d[_this.KEY]];
                })
                .map(function(d) {
                    var pointer = {};
                    pointer[_this.KEY] = d[_this.KEY];
                    pointer.KEY = function () {
                        return this[_this.KEY];
                    };
                    pointer.sortValue = [_this.values.axis_y[pointer.KEY()]||0, 0];
                    pointer.aggrLevel = 0;
                    return pointer;
                });


            //TODO: optimise this!
            this.groupedPointers = d3.nest()
                .key(function (d) {
                    return _this.model.marker.stack.use === "property" ? _this.values.stack[d.KEY()] : _this.values.group[d.KEY()];
                })
                .sortValues(function (a, b) {
                    return b.sortValue[0] - a.sortValue[0];
                })
                .entries(this.mountainPointers);


            var groupManualSort = this.model.marker.group.manualSorting;
            this.groupedPointers.forEach(function (group) {
                var groupSortValue = d3.sum(group.values.map(function (m) {
                    return m.sortValue[0];
                }));

                if (groupManualSort && groupManualSort.length > 1) groupSortValue = groupManualSort.length-1 - groupManualSort.indexOf(group.key);

                group.values.forEach(function (d) {
                    d.sortValue[1] = groupSortValue;
                });

                group[_this.model.entities.getDimension()] = group.key; // hack to get highlihgt and selection work
                group.KEY = function () {
                    return this.key;
                };
                group.aggrLevel = 1;
            });

            var sortGroupKeys = {};
            _this.groupedPointers.map(function (m) {
                sortGroupKeys[m.key] = m.values[0].sortValue[1];
            });


            // update the stacked pointers
            if (_this.model.marker.stack.which === "none") {
                this.stackedPointers = [];
                this.mountainPointers.sort(function (a, b) {
                    return b.sortValue[0] - a.sortValue[0];
                });

            } else {
                this.stackedPointers = d3.nest()
                    .key(function (d) {
                        return _this.values.stack[d.KEY()];
                    })
                    .key(function (d) {
                        return _this.values.group[d.KEY()];
                    })
                    .sortKeys(function (a, b) {
                        return sortGroupKeys[b] - sortGroupKeys[a];
                    })
                    .sortValues(function (a, b) {
                        return b.sortValue[0] - a.sortValue[0];
                    })
                    .entries(this.mountainPointers);

                this.mountainPointers.sort(function (a, b) {
                    return b.sortValue[1] - a.sortValue[1];
                });


                this.stackedPointers.forEach(function (stack) {
                    stack.KEY = function () {
                        return this.key;
                    };
                    stack[_this.model.entities.getDimension()] = stack.key; // hack to get highlihgt and selection work
                    stack.aggrLevel = 2;
                });
            }

            //console.log(JSON.stringify(this.mountainPointers.map(function(m){return m.geo})))
            //console.log(this.stackedPointers)


            //bind the data to DOM elements
            this.mountainsMergeStacked = this.mountainAtomicContainer.selectAll(".vzb-mc-mountain.vzb-mc-aggrlevel2")
                .data(this.stackedPointers);
            this.mountainsMergeGrouped = this.mountainAtomicContainer.selectAll(".vzb-mc-mountain.vzb-mc-aggrlevel1")
                .data(this.groupedPointers);
            this.mountainsAtomic = this.mountainAtomicContainer.selectAll(".vzb-mc-mountain.vzb-mc-aggrlevel0")
                .data(this.mountainPointers);

            //exit selection -- remove shapes
            this.mountainsMergeStacked.exit().remove();
            this.mountainsMergeGrouped.exit().remove();
            this.mountainsAtomic.exit().remove();

            //enter selection -- add shapes
            this.mountainsMergeStacked.enter().append("path")
                .attr("class", "vzb-mc-mountain vzb-mc-aggrlevel2");
            this.mountainsMergeGrouped.enter().append("path")
                .attr("class", "vzb-mc-mountain vzb-mc-aggrlevel1");
            this.mountainsAtomic.enter().append("path")
                .attr("class", "vzb-mc-mountain vzb-mc-aggrlevel0");

            //add interaction
            this.mountains = this.mountainAtomicContainer.selectAll(".vzb-mc-mountain");

            this.mountains
                .on("mousemove", function (d, i) {
                    if (isTouchDevice()) return;
                    _this._interact()._mousemove(d, i);
                })
                .on("mouseout", function (d, i) {
                    if (isTouchDevice()) return;
                    _this._interact()._mouseout(d, i);
                })
                .on("click", function (d, i) {
                    if (isTouchDevice()) return;
                    _this._interact()._click(d, i);
                    _this.highlightEntities();
                })
                .onTap(function (d, i) {
                    _this._interact()._click(d, i);
                    d3.event.stopPropagation();
                })
                .onLongTap(function (d, i) {
                })
        },

        _interact: function () {
            var _this = this;

            return {
                _mousemove: function (d, i) {
                    if (_this.model.time.dragging || _this.model.time.playing) return;

                    _this.model.entities.highlightEntity(d);

                    var mouse = d3.mouse(_this.graph.node()).map(function (d) {
                        return parseInt(d);
                    });

                    //position tooltip
                    _this._setTooltip(d.key ? _this.translator("region/" + d.key) : _this.values.label[d.KEY()]);

                },
                _mouseout: function (d, i) {
                    if (_this.model.time.dragging || _this.model.time.playing) return;

                    _this._setTooltip("");
                    _this.model.entities.clearHighlighted();
                },
                _click: function (d, i) {
                    if (_this.model.time.dragging || _this.model.time.playing) return;
                    
                    _this.model.entities.selectEntity(d);
                }
            };

        },

        highlightEntities: function () {
            var _this = this;
            this.someHighlighted = (this.model.entities.highlight.length > 0);

            if (!this.selectList || !this.someSelected) return;
            this.selectList.classed("vzb-highlight", function (d) {
                return _this.model.entities.isHighlighted(d);
            });
            this.selectList.each(function (d, i) {
              d3.select(this).selectAll(".vzb-mc-label-x")
                .classed("vzb-invisible", function(n) {
                  return !_this.model.entities.isHighlighted(d);
                });

            });

        },

        selectEntities: function () {
            var _this = this;
            this.someSelected = (this.model.entities.select.length > 0);

            this._selectlist.rebuild();
        },

        _sumLeafPointersByMarker: function (branch, marker) {
            var _this = this;
            if (!branch.key) return _this.values[marker][branch.KEY()];
            return d3.sum(branch.values.map(function (m) {
                return _this._sumLeafPointersByMarker(m, marker);
            }));
        },

        updateOpacity: function () {
            var _this = this;
            //if(!duration)duration = 0;

            var OPACITY_HIGHLT = 1.0;
            var OPACITY_HIGHLT_DIM = .3;
            var OPACITY_SELECT = 1.0;
            var OPACITY_REGULAR = this.model.entities.opacityRegular;
            var OPACITY_SELECT_DIM = this.model.entities.opacitySelectDim;

            this.mountains.style("opacity", function (d) {

                if (_this.someHighlighted) {
                    //highlight or non-highlight
                    if (_this.model.entities.isHighlighted(d)) return OPACITY_HIGHLT;
                }

                if (_this.someSelected) {
                    //selected or non-selected
                    return _this.model.entities.isSelected(d) ? OPACITY_SELECT : OPACITY_SELECT_DIM;
                }

                if (_this.someHighlighted) return OPACITY_HIGHLT_DIM;

                return OPACITY_REGULAR;

            });

            this.mountains.classed("vzb-selected", function (d) {
                return _this.model.entities.isSelected(d)
            });

            var someSelectedAndOpacityZero = _this.someSelected && _this.model.entities.opacitySelectDim < .01;

            // when pointer events need update...
            if (someSelectedAndOpacityZero !== this.someSelectedAndOpacityZero_1) {
                this.mountainsAtomic.style("pointer-events", function (d) {
                    return (!someSelectedAndOpacityZero || _this.model.entities.isSelected(d)) ?
                        "visible" : "none";
                });
            }

            this.someSelectedAndOpacityZero_1 = _this.someSelected && _this.model.entities.opacitySelectDim < .01;
        },

        updateTime: function (time) {
            var _this = this;

            this.time = this.model.time.value;
            if (time == null) time = this.time;

            this.year.setText(time.getUTCFullYear().toString());

            this.values = this.model.marker.getFrame(time);
            this.yMax = 0;


            //spawn the original mountains
            this.mountainPointers.forEach(function (d, i) {
                var vertices = _this._spawn(_this.values, d);
                _this.cached[d.KEY()] = vertices;
                d.hidden = vertices.length === 0;
            });


            //recalculate stacking
            if (_this.model.marker.stack.which !== "none") {
                this.stackedPointers.forEach(function (group) {
                    var toStack = [];
                    group.values.forEach(function (subgroup) {
                        toStack = toStack.concat(subgroup.values.filter(function (f) {
                            return !f.hidden;
                        }));
                    });
                    _this.stack(toStack);
                });
            }

            this.mountainPointers.forEach(function (d) {
                d.yMax = d3.max(_this.cached[d.KEY()].map(function (m) {
                    return m.y0 + m.y;
                }));
                if (_this.yMax < d.yMax) _this.yMax = d.yMax;
            });

            var mergeGrouped = _this.model.marker.group.merge;
            var mergeStacked = _this.model.marker.stack.merge;
            var dragOrPlay = (_this.model.time.dragging || _this.model.time.playing) && this.model.marker.stack.which !== "none";

            //if(mergeStacked){
            this.stackedPointers.forEach(function (d) {
                var firstLast = _this._getFirstLastPointersInStack(d);
                _this.cached[d.key] = _this._getVerticesOfaMergedShape(firstLast);
                _this.values.color[d.key] = "_default";
                _this.values.axis_y[d.key] = _this._sumLeafPointersByMarker(d, "axis_y");
                d.yMax = firstLast.first.yMax;
            });
            //} else if (mergeGrouped || dragOrPlay){
            this.groupedPointers.forEach(function (d) {
                var firstLast = _this._getFirstLastPointersInStack(d);
                _this.cached[d.key] = _this._getVerticesOfaMergedShape(firstLast);
                _this.values.color[d.key] = _this.values.color[firstLast.first.KEY()];
                _this.values.axis_y[d.key] = _this._sumLeafPointersByMarker(d, "axis_y");
                d.yMax = firstLast.first.yMax;
            });
            //}

            if (!mergeStacked && !mergeGrouped && this.model.marker.stack.use === "property") {
                this.groupedPointers.forEach(function (d) {
                    var visible = d.values.filter(function (f) {
                        return !f.hidden;
                    });
                    d.yMax = visible[0].yMax;
                    d.values.forEach(function (e) {
                        e.yMaxGroup = d.yMax;
                    });
                });
            }


        },

        _getFirstLastPointersInStack: function (group) {
            var _this = this;

            var visible, visible2;

            if (group.values[0].values) {
                visible = group.values[0].values.filter(function (f) {
                    return !f.hidden;
                });
                visible2 = group.values[group.values.length - 1].values.filter(function (f) {
                    return !f.hidden;
                });
                var first = visible[0];
                var last = visible2[visible2.length - 1];
            } else {
                visible = group.values.filter(function (f) {
                    return !f.hidden;
                });
                var first = visible[0];
                var last = visible[visible.length - 1];
            }
            
            if (!visible.length || (visible2 && !visible2.length)) warn('mountain chart failed to generate shapes. check the incoming data');

            return {
                first: first,
                last: last
            };
        },

        _getVerticesOfaMergedShape: function (arg) {
            var _this = this;

            var first = arg.first.KEY();
            var last = arg.last.KEY();

            return _this.mesh.map(function (m, i) {
                var y = _this.cached[first][i].y0 + _this.cached[first][i].y - _this.cached[last][i].y0;
                var y0 = _this.cached[last][i].y0;
                return {
                    x: m,
                    y0: y0,
                    y: y
                };
            });
        },

        _spawnMasks: function () {
            var _this = this;

            var tailFatX = this._math.unscale(this.model.time.tailFatX);
            var tailCutX = this._math.unscale(this.model.time.tailCutX);
            var tailFade = this.model.time.tailFade;
            var k = 2 * Math.PI / (Math.log(tailFatX) - Math.log(tailCutX));
            var m = Math.PI - Math.log(tailFatX) * k;


            this.spawnMask = [];
            this.cosineShape = [];
            this.cosineArea = 0;

            this.mesh.map(function (dX, i) {
                _this.spawnMask[i] = dX < tailCutX ? 1 : (dX > tailFade * 7 ? 0 : Math.exp((tailCutX - dX) / tailFade))
                _this.cosineShape[i] = (dX > tailCutX && dX < tailFatX ? (1 + Math.cos(Math.log(dX) * k + m)) : 0);
                _this.cosineArea += _this.cosineShape[i];
            });
        },

        _spawn: function (values, d) {
            var _this = this;

            var norm = values.axis_y[d.KEY()];
            var sigma = _this._math.giniToSigma(values.size[d.KEY()]);
            var mu = _this._math.gdpToMu(values.axis_x[d.KEY()], sigma);

            if (!norm || !mu || !sigma) return [];

            var distribution = [];
            var acc = 0;

            this.mesh.map(function (dX, i) {
                distribution[i] = _this._math.pdf.lognormal(dX, mu, sigma);
                acc += _this.spawnMask[i] * distribution[i];
            });

            var result = this.mesh.map(function (dX, i) {
                return {
                    x: dX,
                    y0: 0,
                    y: norm * (distribution[i] * (1 - _this.spawnMask[i]) + _this.cosineShape[i] / _this.cosineArea * acc)
                }
            });

            return result;
        },

        _adjustMaxY: function (options) {
            if (!options) options = {};
            var _this = this;
            var method = this.model.time.yMaxMethod;

            if (method !== "immediate" && !options.force) return;

            if (method === "latest") _this.updateTime(_this.model.time.end);

            if (!_this.yMax) warn("Setting yMax to " + _this.yMax + ". You failed again :-/");
            this.yScale.domain([0, _this.yMax]);

            if (method === "latest") _this.updateTime();
        },

        redrawDataPoints: function () {
            var _this = this;
            var mergeGrouped = this.model.marker.group.merge;
            var mergeStacked = this.model.marker.stack.merge;
            var stackMode = this.model.marker.stack.which;
            //it's important to know if the chart is dragging or playing at the moment.
            //because if that is the case, the mountain chart will merge the stacked entities to save performance
            var dragOrPlay = (this.model.time.dragging || this.model.time.playing)
                //never merge when no entities are stacked
                && stackMode !== "none"
                //when the time is playing and stops in the end, the time.playing is set to false after the slider is stopped
                //so the mountain chat is stuck in the merged state. this line prevents it:
                && !(this.model.time.value - this.model.time.end==0 && !this.model.time.loop);

            this._adjustMaxY();

            this.mountainsMergeStacked.each(function (d) {
                var view = d3.select(this);
                var hidden = !mergeStacked;
                _this._renderShape(view, d.KEY(), hidden);
            })

            this.mountainsMergeGrouped.each(function (d) {
                var view = d3.select(this);
                var hidden = (!mergeGrouped && !dragOrPlay) || (mergeStacked && !_this.model.entities.isSelected(d));
                _this._renderShape(view, d.KEY(), hidden);
            });

            this.mountainsAtomic.each(function (d, i) {
                var view = d3.select(this);
                var hidden = d.hidden || ((mergeGrouped || mergeStacked || dragOrPlay) && !_this.model.entities.isSelected(d));
                _this._renderShape(view, d.KEY(), hidden);
            })

            if (stackMode === "none") {
                this.mountainsAtomic.sort(function (a, b) {
                    return b.yMax - a.yMax;
                });

            } else if (stackMode === "all") {
                // do nothing if everything is stacked

            } else {
                if (mergeGrouped || dragOrPlay) {
                    this.mountainsMergeGrouped.sort(function (a, b) {
                        return b.yMax - a.yMax;
                    });
                } else {
                    this.mountainsAtomic.sort(function (a, b) {
                        return b.yMaxGroup - a.yMaxGroup;
                    });
                }
            }


            // exporting shapes for shape preloader. is needed once in a while
            // if (!this.shapes) this.shapes = {}
            // this.shapes[this.model.time.value.getUTCFullYear()] = {
            //     yMax: d3.format(".2e")(_this.yMax),
            //     shape: _this.cached["all"].map(function (d) {return d3.format(".2e")(d.y);})
            // }

        },

        redrawDataPointsOnlyColors: function () {
            var _this = this;
            this.mountains.style("fill", function (d) {
                return _this.values.color[d.KEY()]?_this.cScale(_this.values.color[d.KEY()]):"transparent";
            });
        },

        _renderShape: function (view, key, hidden) {
            var stack = this.model.marker.stack.which;
            var _this = this;

            view.classed("vzb-hidden", hidden);

            if (hidden) {
                if (stack !== "none") view.style("stroke-opacity", 0);
                return;
            }

            var filter = {};
            filter[this.KEY] = key;
            if (this.model.entities.isSelected(filter)) {
                view.attr("d", this.area(this.cached[key].filter(function (f) {return f.y > _this.values.axis_y[key] * THICKNESS_THRESHOLD })));
            } else {
                view.attr("d", this.area(this.cached[key]));
            }

            if (this.model.marker.color.use === "indicator") view
                .style("fill", this.values.color[key] ? _this.cScale(this.values.color[key]) : "transparent");

            if (stack !== "none") view
                .transition().duration(Math.random() * 900 + 100).ease("circle")
                .style("stroke-opacity", .5);

            if (this.model.time.record) this._export.write({
                type: "path",
                id: key,
                time: this.model.time.value.getUTCFullYear(),
                fill: this.cScale(this.values.color[key]),
                d: this.area(this.cached[key])
            });
        },

        _setTooltip: function (tooltipText) {
            if (tooltipText) {
                var mouse = d3.mouse(this.graph.node()).map(function (d) { return parseInt(d); });

                //position tooltip
                this.tooltip.classed("vzb-hidden", false)
                    .attr("transform", "translate(" + (mouse[0]) + "," + (mouse[1]) + ")")
                    .selectAll("text")
                    .attr("text-anchor", "middle")
                    .attr("alignment-baseline", "middle")
                    .text(tooltipText)

                var contentBBox = this.tooltip.select("text")[0][0].getBBox();

                this.tooltip.select("rect")
                    .attr("width", contentBBox.width + 8)
                    .attr("height", contentBBox.height + 8)
                    .attr("x", -contentBBox.width - 25)
                    .attr("y", -contentBBox.height - 25)
                    .attr("rx", contentBBox.height * .2)
                    .attr("ry", contentBBox.height * .2);

                this.tooltip.selectAll("text")
                    .attr("x", -contentBBox.width - 25 + ((contentBBox.width + 8)/2))
                    .attr("y", -contentBBox.height - 25 + ((contentBBox.height + 11)/2)); // 11 is 8 for margin + 3 for strokes
                var translateX = (mouse[0] - contentBBox.width - 25) > 0 ? mouse[0] : (contentBBox.width + 25);
                var translateY = (mouse[1] - contentBBox.height - 25) > 0 ? mouse[1] : (contentBBox.height + 25);
                this.tooltip
                    .attr("transform", "translate(" + translateX + "," + translateY + ")");

            } else {

                this.tooltip.classed("vzb-hidden", true);
            }
        }

    });

    //MOUNTAIN CHART TOOL
    var MountainChart = Tool.extend('MountainChart', {

      /**
       * Initializes the tool (MountainChart Tool).
       * Executed once before any template is rendered.
       * @param {Object} placeholder Placeholder element for the tool
       * @param {Object} external_model Model as given by the external page
       */
      init: function(placeholder, external_model) {

        this.name = "mountainchart";

        //specifying components
        this.components = [{
          component: MountainChartComponent,
          placeholder: '.vzb-tool-viz',
          model: ["state.time", "state.entities", "state.marker", "language"] //pass models to component
        }, {
          component: TimeSlider,
          placeholder: '.vzb-tool-timeslider',
          model: ["state.time"]
        }, {
          component: Dialogs,
          placeholder: '.vzb-tool-dialogs',
          model: ['state', 'ui', 'language']
        }, {
          component: ButtonList,
          placeholder: '.vzb-tool-buttonlist',
          model: ['state', 'ui', 'language']
        }, {
          component: TreeMenu,
          placeholder: '.vzb-tool-treemenu',
          model: ['state.marker', 'language']
        }, {
          component: DataWarning,
          placeholder: '.vzb-tool-datawarning',
          model: ['language']
        }];

        //constructor is the same as any tool
        this._super(placeholder, external_model);
      }
    });

    //d3.svg.collisionResolver

    function collisionResolver() {
      return function collision_resolver() {
        var DURATION = 300;
        var labelHeight = {};
        var labelPosition = {};
        // MAINN FUNCTION. RUN COLLISION RESOLVER ON A GROUP g
        function resolver(g) {
          if(data == null) {
            console.warn(
              'D3 collision resolver stopped: missing data to work with. Example: data = {asi: {valueY: 45, valueX: 87}, ame: {valueY: 987, valueX: 767}}'
            );
            return;
          }
          if(selector == null) {
            console.warn('D3 collision resolver stopped: missing a CSS slector');
            return;
          }
          if(height == null) {
            console.warn('D3 collision resolver stopped: missing height of the canvas');
            return;
          }
          if(value == null) {
            console.warn(
              'D3 collision resolver stopped: missing pointer within data objects. Example: value = \'valueY\' ');
            return;
          }
          if(KEY == null) {
            console.warn('D3 collision resolver stopped: missing a key for data. Example: key = \'geo\' ');
            return;
          }
          g.each(function(d, index) {
            labelHeight[d[KEY]] = d3.select(this).select(selector)[0][0].getBBox().height;
          });
          labelPosition = resolver.calculatePositions(data, value, height, scale);
          //actually reposition the labels
          g.each(function(d, i) {
            if(data[d[KEY]][fixed])
              return;
            var resolvedY = labelPosition[d[KEY]] || scale(data[d[KEY]][value]) || 0;
            var resolvedX = null;
            if(handleResult != null) {
              handleResult(d, i, this, resolvedX, resolvedY);
              return;
            }
            d3.select(this).selectAll(selector).transition().duration(DURATION).attr('transform', 'translate(0,' +
              resolvedY + ')');
          });
        }

        // CALCULATE OPTIMIZED POSITIONS BASED ON LABELS' HEIGHT AND THEIR PROXIMITY (DELTA)
        resolver.calculatePositions = function(data, value, height, scale) {
          var result = {};
          var keys = Object.keys(data).sort(function(a, b) {
            return data[a][value] - data[b][value];
          });
          keys.forEach(function(d, index) {
            //initial positioning
            result[d] = scale(data[d][value]);
            // check the overlapping chain reaction all the way down
            for(var j = index; j > 0; j--) {
              // if overlap found shift the overlapped label downwards
              var delta = result[keys[j - 1]] - result[keys[j]] - labelHeight[keys[j]];
              if(delta < 0)
                result[keys[j - 1]] -= delta;
              // if the chain reaction stopped because found some gap in the middle, then quit
              if(delta > 0)
                break;
            }
          });
          // check if the lowest label is breaking the boundary...
          var delta = height - result[keys[0]] - labelHeight[keys[0]];
          // if it does, then
          if(delta < 0) {
            // shift the lowest up
            result[keys[0]] += delta;
            // check the overlapping chain reaction all the way up
            for(var j = 0; j < keys.length - 1; j++) {
              // if overlap found shift the overlapped label upwards
              var delta = result[keys[j]] - result[keys[j + 1]] - labelHeight[keys[j + 1]];
              if(delta < 0)
                result[keys[j + 1]] += delta;
              // if the chain reaction stopped because found some gap in the middle, then quit
              if(delta > 0)
                break;
            }
          }
          return result;
        };
        // GETTERS AND SETTERS
        var data = null;
        resolver.data = function(arg) {
          if(!arguments.length)
            return data;
          data = arg;
          return resolver;
        };
        var selector = null;
        resolver.selector = function(arg) {
          if(!arguments.length)
            return selector;
          selector = arg;
          return resolver;
        };
        var height = null;
        resolver.height = function(arg) {
          if(!arguments.length)
            return height;
          height = arg;
          return resolver;
        };
        var scale = d3.scale.linear().domain([
          0,
          1
        ]).range([
          0,
          1
        ]);
        resolver.scale = function(arg) {
          if(!arguments.length)
            return scale;
          scale = arg;
          return resolver;
        };
        var value = null;
        resolver.value = function(arg) {
          if(!arguments.length)
            return value;
          value = arg;
          return resolver;
        };
        var fixed = null;
        resolver.fixed = function(arg) {
          if(!arguments.length)
            return fixed;
          fixed = arg;
          return resolver;
        };
        var handleResult = null;
        resolver.handleResult = function(arg) {
          if(!arguments.length)
            return handleResult;
          handleResult = arg;
          return resolver;
        };
        var KEY = null;
        resolver.KEY = function(arg) {
          if(!arguments.length)
            return KEY;
          KEY = arg;
          return resolver;
        };
        return resolver;
      }();
    };

    //LINE CHART COMPONENT
    var LCComponent = Component.extend({

      init: function(config, context) {
        var _this = this;
        this.name = 'linechart';
        this.template = 'linechart.html';

        //define expected models for this component
        this.model_expects = [{
          name: "time",
          type: "time"
        }, {
          name: "entities",
          type: "entities"
        }, {
          name: "marker",
          type: "model"
        }, {
          name: "language",
          type: "language"
        }];


        this.model_binds = {
          'change:time.value': function() {
            if(!_this._readyOnce) return;
            _this.updateTime();
            _this.redrawDataPoints();
          },
          'change:time.playing': function() {
            // hide tooltip on touch devices when playing
            if (_this.model.time.playing && isTouchDevice() && !_this.tooltip.classed("vzb-hidden")) _this.tooltip.classed("vzb-hidden", true);
          },
          'change:marker': function(evt, path) {
            if(!_this._readyOnce) return;
            if(path.indexOf("which") > -1 || path.indexOf("use") > -1) return;
            _this.ready();
          }
        };

        this._super(config, context);

        this.xScale = null;
        this.yScale = null;

        this.xAxis = axisSmart().orient("bottom");
        this.yAxis = axisSmart().orient("left");

        this.isDataPreprocessed = false;
        this.timeUpdatedOnce = false;
        this.sizeUpdatedOnce = false;

        // default UI settings
        this.ui = extend({
          entity_labels: {},
          whenHovering: {}
        }, this.ui["vzb-tool-" + this.name]);

        this.ui.entity_labels = extend({
          min_number_of_entities_when_values_hide: 10
        }, this.ui.entity_labels);

        this.ui.whenHovering = extend({
          hideVerticalNow: true,
          showProjectionLineX: true,
          showProjectionLineY: true,
          higlightValueX: true,
          higlightValueY: true,
          showTooltip: true
        }, this.ui.whenHovering);

        this.getValuesForYear = memoize(this.getValuesForYear);
        this.getNearestKey = memoize(this.getNearestKey);
      },

      /*
       * domReady:
       * Executed after template is loaded
       * Ideally, it contains instantiations related to template
       */
      readyOnce: function() {
        var _this = this;

        this.element = d3.select(this.element);
        this.graph = this.element.select('.vzb-lc-graph');
        this.yAxisEl = this.graph.select('.vzb-lc-axis-y');
        this.xAxisEl = this.graph.select('.vzb-lc-axis-x');
        this.xTitleEl = this.graph.select('.vzb-lc-axis-x-title');
        this.yTitleEl = this.graph.select('.vzb-lc-axis-y-title');
        this.xValueEl = this.graph.select('.vzb-lc-axis-x-value');
        this.yValueEl = this.graph.select('.vzb-lc-axis-y-value');

        this.linesContainer = this.graph.select('.vzb-lc-lines');
        this.labelsContainer = this.graph.select('.vzb-lc-labels');

        this.verticalNow = this.labelsContainer.select(".vzb-lc-vertical-now");
        this.tooltip = this.element.select('.vzb-tooltip');
        //            this.filterDropshadowEl = this.element.select('#vzb-lc-filter-dropshadow');
        this.projectionX = this.graph.select("g").select(".vzb-lc-projection-x");
        this.projectionY = this.graph.select("g").select(".vzb-lc-projection-y");

        this.entityLines = null;
        this.entityLabels = null;
        this.totalLength_1 = {};

        this.KEY = this.model.entities.getDimension();

        //component events
        this.on("resize", function() {
          _this.updateSize();
          _this.updateTime();
          _this.redrawDataPoints();
        });
      },

      ready: function() {
        this.updateUIStrings();
        this.updateShow();
        this.updateTime();
        this.updateSize();
        this.redrawDataPoints();

        this.graph
          .on('mousemove', this.entityMousemove.bind(this, null, null, this))
          .on('mouseleave', this.entityMouseout.bind(this, null, null, this));
      },

      updateUIStrings: function() {
        this.translator = this.model.language.getTFunction();

        var titleStringX = this.translator("indicator/" + this.model.marker.axis_x.which);
        var titleStringY = this.translator("indicator/" + this.model.marker.axis_y.which);

        var xTitle = this.xTitleEl.selectAll("text").data([0]);
        xTitle.enter().append("text");
        xTitle
          .attr("text-anchor", "end")
          .attr("y", "-0.32em")
          .text(titleStringX);

        var yTitle = this.yTitleEl.selectAll("text").data([0]);
        yTitle.enter().append("text");
        yTitle
          .attr("y", "-0px")
          .attr("x", "-9px")
          .attr("dy", "-0.36em")
          .attr("dx", "-0.72em")
          .text(titleStringY);
      },

      /*
       * UPDATE SHOW:
       * Ideally should only update when show parameters change or data changes
       */
      updateShow: function() {
        var _this = this;
        var KEY = this.KEY;



        this.cached = {};

        //scales
        this.yScale = this.model.marker.axis_y.getScale();
        this.xScale = this.model.marker.axis_x.getScale();
        this.cScale = this.model.marker.color.getScale();

        this.yAxis.tickSize(6, 0)
          .tickFormat(this.model.marker.axis_y.tickFormatter);
        this.xAxis.tickSize(6, 0)
          .tickFormat(this.model.marker.axis_x.tickFormatter);

        this.collisionResolver = collisionResolver()
          .selector(".vzb-lc-label")
          .value("valueY")
          .scale(this.yScale)
          .KEY(KEY);

        //line template
        this.line = d3.svg.line()
          .interpolate("basis")
          .x(function(d) {
            return _this.xScale(d[0]);
          })
          .y(function(d) {
            return _this.yScale(d[1]);
          });
          
        this.all_values = this.model.marker.getFrames();
        this.all_steps = this.model.time.getAllSteps();

      },


      /*
       * UPDATE TIME:
       * Ideally should only update when time or data changes
       */
      updateTime: function() {
        var _this = this;
        var KEY = this.KEY;

        var time_1 = (this.time === null) ? this.model.time.value : this.time;
        this.time = this.model.time.value;
        this.duration = this.model.time.playing && (this.time - time_1 > 0) ? this.model.time.delayAnimations : 0;

        var timeDim = this.model.time.getDimension();
        var filter = {};

        filter[timeDim] = this.time;

        this.data = this.model.marker.getKeys(filter);
        this.values = this.model.marker.getFrame(this.time);
        this.prev_steps = this.all_steps.filter(function(f){return f < _this.time;});

        this.entityLines = this.linesContainer.selectAll('.vzb-lc-entity').data(this.data);
        this.entityLabels = this.labelsContainer.selectAll('.vzb-lc-entity').data(this.data);

        this.timeUpdatedOnce = true;

      },

      /*
       * RESIZE:
       * Executed whenever the container is resized
       * Ideally, it contains only operations related to size
       */
      updateSize: function() {

        var _this = this;
        var values = this.values;
        var KEY = this.KEY;

        var padding = 2;

        this.profiles = {
          "small": {
            margin: {
              top: 30,
              right: 20,
              left: 55,
              bottom: 30
            },
            tick_spacing: 60,
            text_padding: 8,
            lollipopRadius: 6,
            limitMaxTickNumberX: 5
          },
          "medium": {
            margin: {
              top: 40,
              right: 60,
              left: 65,
              bottom: 40
            },
            tick_spacing: 80,
            text_padding: 12,
            lollipopRadius: 7,
            limitMaxTickNumberX: 10
          },
          "large": {
            margin: {
              top: 50,
              right: 60,
              left: 70,
              bottom: 50
            },
            tick_spacing: 100,
            text_padding: 20,
            lollipopRadius: 9,
            limitMaxTickNumberX: 0 // unlimited
          }
        };

        var timeSliderProfiles = {
          small: {
            margin: {
              top: 9,
              right: 15,
              bottom: 10,
              left: 10
            }
          },
          medium: {
            margin: {
              top: 9,
              right: 15,
              bottom: 10,
              left: 20
            }
          },
          large: {
            margin: {
              top: 9,
              right: 15,
              bottom: 10,
              left: 25
            }
          }
        };

        this.activeProfile = this.profiles[this.getLayoutProfile()];
        this.margin = this.activeProfile.margin;
        this.tick_spacing = this.activeProfile.tick_spacing;


        //adjust right this.margin according to biggest label
        var lineLabelsText = this.model.marker.getKeys().map(function(d, i) {
          return values.label[d[KEY]];
        });

        var longestLabelWidth = 0;
        var lineLabelsView = this.linesContainer.selectAll(".samplingView").data(lineLabelsText);

        lineLabelsView
          .enter().append("text")
          .attr("class", "samplingView vzb-lc-labelname")
          .style("opacity", 0)
          .text(function(d) {
            return(d.length < 13) ? d : d.substring(0, 10) + '...';
          })
          .each(function(d) {
            if(longestLabelWidth > this.getComputedTextLength()) {
              return;
            }
            longestLabelWidth = this.getComputedTextLength();
          })
          .remove();

        this.margin.right = Math.max(this.margin.right, longestLabelWidth + this.activeProfile.text_padding + 20);


        //stage
        this.height = parseInt(this.element.style("height"), 10) - this.margin.top - this.margin.bottom;
        this.width = parseInt(this.element.style("width"), 10) - this.margin.left - this.margin.right;

        this.collisionResolver.height(this.height);

        this.graph
          .attr("width", this.width + this.margin.right + this.margin.left)
          .attr("height", this.height + this.margin.top + this.margin.bottom)
          .select("g")
          .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");


        if(this.model.marker.axis_y.scaleType !== "ordinal") {
          this.yScale.range([this.height, 0]);
        } else {
          this.yScale.rangePoints([this.height, 0], padding).range();
        }
        if(this.model.marker.axis_x.scaleType !== "ordinal" || 1) {
          this.xScale.range([0, this.width]);
        } else {
          this.xScale.rangePoints([0, this.width], padding).range();
        }


        this.yAxis.scale(this.yScale)
          .labelerOptions({
            scaleType: this.model.marker.axis_y.scaleType,
            timeFormat: this.model.time.timeFormat,
            toolMargin: {
              top: 0,
              right: this.margin.right,
              left: this.margin.left,
              bottom: this.margin.bottom
            },
            limitMaxTickNumber: 6
              //showOuter: true
          });

        this.xAxis.scale(this.xScale)
          .labelerOptions({
            scaleType: this.model.marker.axis_x.scaleType,
            timeFormat: this.model.time.timeFormat,
            toolMargin: this.margin,
            limitMaxTickNumber: this.activeProfile.limitMaxTickNumberX
              //showOuter: true
          });


        this.yAxisEl.call(this.yAxis);
        this.xAxisEl.call(this.xAxis);

        this.xAxisEl.attr("transform", "translate(0," + this.height + ")");
        this.xValueEl.attr("transform", "translate(0," + this.height + ")")
          .attr("y", this.xAxis.tickPadding() + this.xAxis.tickSize());

        this.xTitleEl.attr("transform", "translate(" + this.width + "," + this.height + ")");

        // adjust the vertical dashed line
        this.verticalNow.attr("y1", this.yScale.range()[0]).attr("y2", this.yScale.range()[1])
          .attr("x1", 0).attr("x2", 0);
        this.projectionX.attr("y1", _this.yScale.range()[0]);
        this.projectionY.attr("x2", _this.xScale.range()[0]);

        if(isTouchDevice()) {
          _this.tooltip.classed("vzb-hidden", true);
          _this.verticalNow.style("opacity", 1);
          _this.projectionX.style("opacity", 0);
          _this.projectionY.style("opacity", 0);
          _this.xAxisEl.call(_this.xAxis.highlightValue(_this.time));
          _this.yAxisEl.call(_this.yAxis.highlightValue("none"));
          _this.graph.selectAll(".vzb-lc-entity").each(function() {
            d3.select(this).classed("vzb-dimmed", false).classed("vzb-hovered", false);
          });

          _this.hoveringNow = null;
        }

        var opts = {
          rangeMax: this.xScale.range()[1],
          mRight: this.margin.right,
          profile: timeSliderProfiles[this.getLayoutProfile()]
        };
        this.parent.trigger('myEvent', opts);

        this.sizeUpdatedOnce = true;
      },

      /*
       * REDRAW DATA POINTS:
       * Here plotting happens
       */
      redrawDataPoints: function() {
        var _this = this;
        var KEY = this.KEY;
        var values = this.values;

        if(!this.timeUpdatedOnce) {
          this.updateTime();
        }

        if(!this.sizeUpdatedOnce) {
          this.updateSize();
        }

        this.entityLabels.exit().remove();
        this.entityLines.exit().remove();

        this.entityLines.enter().append("g")
          .attr("class", "vzb-lc-entity")
          .each(function(d, index) {
            var entity = d3.select(this);

            entity.append("path")
              .attr("class", "vzb-lc-line-shadow")
              .attr("transform", "translate(0,2)");

            entity.append("path")
              .attr("class", "vzb-lc-line");

          });

        this.entityLabels.enter().append("g")
          .attr("class", "vzb-lc-entity")
          .each(function(d, index) {
            var entity = d3.select(this);
            var label = values.label[d[KEY]];

            entity.append("circle")
              .attr("class", "vzb-lc-circle")
              .attr("cx", 0);

            var labelGroup = entity.append("g").attr("class", "vzb-lc-label");

            labelGroup.append("text")
              .attr("class", "vzb-lc-labelname")
              .attr("dy", ".35em");

            labelGroup.append("text")
              .attr("class", "vzb-lc-label-value")
              .attr("dy", "1.6em");
          });

        this.entityLines
          .each(function(d, index) {
            var entity = d3.select(this);
            var label = values.label[d[KEY]];

            var color = _this.cScale(values.color[d[KEY]]);
            var colorShadow = _this.model.marker.color.which == "geo.region"?
                _this.model.marker.color.getColorShade({
                  colorID: values.color[d[KEY]],
                  shadeID: "shade"
                })
                :
                d3.rgb(color).darker(0.5).toString();

            //TODO: optimization is possible if getValues would return both x and time
            //TODO: optimization is possible if getValues would return a limited number of points, say 1 point per screen pixel
            var xy = _this.prev_steps.map(function(frame, i) {
                    return [+frame, +_this.all_values[frame].axis_y[d[KEY]]];
                })
                .filter(function(d) {return !isNaN$1(d[1]);});
            
            _this.cached[d[KEY]] = {
              valueY: xy[xy.length - 1][1]
            };


            // the following fixes the ugly line butts sticking out of the axis line
            //if(x[0]!=null && x[1]!=null) xy.splice(1, 0, [(+x[0]*0.99+x[1]*0.01), y[0]]);
            var path2 = entity.select(".vzb-lc-line");
            var totalLength = path2.node().getTotalLength();

            var path1 = entity.select(".vzb-lc-line-shadow")
              .attr("stroke-dasharray", totalLength)
              .style("stroke", colorShadow)
              .attr("d", _this.line(xy));
            path2
              //.style("filter", "none")
              .attr("stroke-dasharray", totalLength)
              .style("stroke", color)
              .attr("d", _this.line(xy));

            // this section ensures the smooth transition while playing and not needed otherwise
            if(_this.model.time.playing) {

              if(_this.totalLength_1[d[KEY]] === null) {
                _this.totalLength_1[d[KEY]] = totalLength;
              }

              path1
                .interrupt()
                .attr("stroke-dashoffset", totalLength - _this.totalLength_1[d[KEY]])
                .transition()
                .delay(0)
                .duration(_this.duration)
                .ease("linear")
                .attr("stroke-dashoffset", 0);
              path2
                .interrupt()
                .attr("stroke-dashoffset", totalLength - _this.totalLength_1[d[KEY]])
                .transition()
                .delay(0)
                .duration(_this.duration)
                .ease("linear")
                .attr("stroke-dashoffset", 0);

              _this.totalLength_1[d[KEY]] = totalLength;
            } else {
              //reset saved line lengths
              _this.totalLength_1[d[KEY]] = null;

              path1
                .attr("stroke-dasharray", "none")
                .attr("stroke-dashoffset", "none");

              path2
                .attr("stroke-dasharray", "none")
                .attr("stroke-dashoffset", "none");
            }

          });

        this.entityLabels
          .each(function(d, index) {
            var entity = d3.select(this);
            var label = values.label[d[KEY]];

            var color = _this.cScale(values.color[d[KEY]]);
            var colorShadow = _this.model.marker.color.which == "geo.region"?
                _this.model.marker.color.getColorShade({
                  colorID: values.color[d[KEY]],
                  shadeID: "shade"
                })
                :
                d3.rgb(color).darker(0.5).toString();


            entity.select(".vzb-lc-circle")
              .style("fill", color)
              .transition()
              .duration(_this.duration)
              .ease("linear")
              .attr("r", _this.profiles[_this.getLayoutProfile()].lollipopRadius)
              .attr("cy", _this.yScale(_this.cached[d[KEY]].valueY) + 1);


            entity.select(".vzb-lc-label")
              .transition()
              .duration(_this.duration)
              .ease("linear")
              .attr("transform", "translate(0," + _this.yScale(_this.cached[d[KEY]].valueY) + ")");


            var value = _this.yAxis.tickFormat()(_this.cached[d[KEY]].valueY);
            var name = label.length < 13 ? label : label.substring(0, 10) + '...';
            var valueHideLimit = _this.ui.entity_labels.min_number_of_entities_when_values_hide;

            var t = entity.select(".vzb-lc-labelname")
              .style("fill", colorShadow)
              .attr("dx", _this.activeProfile.text_padding)
              .text(name + " " + (_this.data.length < valueHideLimit ? value : ""));

            entity.select(".vzb-lc-label-value")
              .style("fill", colorShadow)
              .attr("dx", _this.activeProfile.text_padding)
              .text("");

            if(_this.data.length < valueHideLimit) {

              var size = _this.xScale(_this.time) + t[0][0].getComputedTextLength() + _this.activeProfile.text_padding;
              var width = _this.width + _this.margin.right;

              if(size > width) {
                entity.select(".vzb-lc-labelname").text(name);
                entity.select(".vzb-lc-label-value").text(value);
              }
            }
          });

        this.labelsContainer
          .transition()
          .duration(_this.duration)
          .ease("linear")
          .attr("transform", "translate(" + _this.xScale(_this.time) + ",0)");

        this.verticalNow
          .style("opacity", this.time - this.model.time.start === 0 || _this.hoveringNow ? 0 : 1);


        if(!this.hoveringNow) {
          this.xAxisEl.call(
            this.xAxis.highlightValue(_this.time).highlightTransDuration(_this.duration)
          );
        }

        // Call flush() after any zero-duration transitions to synchronously flush the timer queue
        // and thus make transition instantaneous. See https://github.com/mbostock/d3/issues/1951
        if(_this.duration == 0) {
          d3.timer.flush();
        }

        // cancel previously queued simulation if we just ordered a new one
        // then order a new collision resolving
        clearTimeout(_this.collisionTimeout);
        _this.collisionTimeout = setTimeout(function() {
          _this.entityLabels.call(_this.collisionResolver.data(_this.cached));
        }, _this.model.time.delayAnimations * 1.5);

      },

      entityMousemove: function(me, index, context, closestToMouse) {
        var _this = context;
        var KEY = _this.KEY;
        var values = _this.values;

        var mouse = d3.mouse(_this.graph.node()).map(function(d) {
          return parseInt(d);
        });

        var resolvedTime = _this.xScale.invert(mouse[0] - _this.margin.left);
        if(_this.time - resolvedTime < 0) {
          resolvedTime = _this.time;
        } else if(resolvedTime < this.model.time['start']) {
          resolvedTime = this.model.time['start'];
        }
        var resolvedValue;
        var timeDim = _this.model.time.getDimension();
        
        var mousePos = mouse[1] - _this.margin.bottom;
        var data = this.getValuesForYear(resolvedTime);
        var nearestKey = this.getNearestKey(mousePos, data.axis_y, _this.yScale.bind(_this));
        resolvedValue = data.axis_y[nearestKey];
        if(!me) me = {};
        me[KEY] = nearestKey;

        _this.hoveringNow = me;

        _this.graph.selectAll(".vzb-lc-entity").each(function() {
          d3.select(this)
            .classed("vzb-dimmed", function(d) {
              return d[KEY] !== _this.hoveringNow[KEY];
            })
            .classed("vzb-hovered", function(d) {
              return d[KEY] === _this.hoveringNow[KEY];
            });
        });

        if(isNaN$1(resolvedValue)) return;

        var scaledTime = _this.xScale(resolvedTime);
        var scaledValue = _this.yScale(resolvedValue);

        if(_this.ui.whenHovering.showTooltip) {
          //position tooltip
          _this.tooltip
            //.style("right", (_this.width - scaledTime + _this.margin.right ) + "px")
            .style("left", (scaledTime + _this.margin.left) + "px")
            .style("bottom", (_this.height - scaledValue + _this.margin.bottom) + "px")
            .text(_this.yAxis.tickFormat()(resolvedValue))
            .classed("vzb-hidden", false);
        }

        // bring the projection lines to the hovering point
        if(_this.ui.whenHovering.hideVerticalNow) {
          _this.verticalNow.style("opacity", 0);
        }

        if(_this.ui.whenHovering.showProjectionLineX) {
          _this.projectionX
            .style("opacity", 1)
            .attr("y2", scaledValue)
            .attr("x1", scaledTime)
            .attr("x2", scaledTime);
        }
        if(_this.ui.whenHovering.showProjectionLineY) {
          _this.projectionY
            .style("opacity", 1)
            .attr("y1", scaledValue)
            .attr("y2", scaledValue)
            .attr("x1", scaledTime);
        }

        if(_this.ui.whenHovering.higlightValueX) _this.xAxisEl.call(
          _this.xAxis.highlightValue(resolvedTime).highlightTransDuration(0)
        );

        if(_this.ui.whenHovering.higlightValueY) _this.yAxisEl.call(
          _this.yAxis.highlightValue(resolvedValue).highlightTransDuration(0)
        );

        clearTimeout(_this.unhoverTimeout);

      },

      entityMouseout: function(me, index, context) {
        var _this = context;
        if(d3.event.relatedTarget && d3.select(d3.event.relatedTarget).classed('vzb-tooltip')) return;

        // hide and show things like it was before hovering
        _this.unhoverTimeout = setTimeout(function() {
          _this.tooltip.classed("vzb-hidden", true);
          _this.verticalNow.style("opacity", 1);
          _this.projectionX.style("opacity", 0);
          _this.projectionY.style("opacity", 0);
          _this.xAxisEl.call(_this.xAxis.highlightValue(_this.time));
          _this.yAxisEl.call(_this.yAxis.highlightValue("none"));

          _this.graph.selectAll(".vzb-lc-entity").each(function() {
            d3.select(this).classed("vzb-dimmed", false).classed("vzb-hovered", false);
          });

          _this.hoveringNow = null;
        }, 300);

      },

      getValuesForYear: function(year) {
        if(!isDate(year)) {
          year = this.model.time.timeFormat.parse(year);
        }
        return this.model.marker.getFrame(year);
      },

      /**
       * Returns key from obj which value has the smallest difference with val
       */
      getNearestKey: function(val, obj, fn) {
        var keys = Object.keys(obj);
        var resKey = keys[0];
        for(var i = 1; i < keys.length; i++) {
          var key = keys[i];
          if(Math.abs((fn ? fn(obj[key]) : obj[key]) - val) < Math.abs((fn ? fn(obj[resKey]) : obj[resKey]) - val)) {
            resKey = key;
          }
        }
        return resKey;
      }




      //        resolveLabelCollisions: function(){
      //            var _this = this;
      //
      //            // cancel previously queued simulation if we just ordered a new one
      //            clearTimeout(_this.collisionTimeout);
      //
      //            // place force layout simulation into a queue
      //            _this.collisionTimeout = setTimeout(function(){
      //
      //                _this.cached.sort(function(a,b){return a.value - b.value});
      //
      //                // update inputs of force layout -- fixed nodes
      //                _this.dataForceLayout.links.forEach(function(d,i){
      //                    var source = utils.find(_this.cached, {geo:d.source[KEY]});
      //                    var target = utils.find(_this.cached, {geo:d.target[KEY]});
      //
      //                    d.source.px = _this.xScale(source.time);
      //                    d.source.py = _this.yScale(source.value);
      //                    d.target.px = _this.xScale(target.time) + 10;
      //                    d.target.py = _this.yScale(target.value) + 10;
      //                });
      //
      //                // shift the boundary nodes
      //                _this.dataForceLayout.nodes.forEach(function(d){
      //                    if(d[KEY] == "upper_boundary"){d.x = _this.xScale(_this.time)+10; d.y = 0; return};
      //                    if(d[KEY] == "lower_boundary"){d.x = _this.xScale(_this.time)+10; d.y = _this.height; return};
      //                });
      //
      //                // update force layout size for better gravity
      //                _this.forceLayout.size([_this.xScale(_this.time)*2, _this.height]);
      //
      //                    // resume the simulation, fast-forward it, stop when done
      //                    _this.forceLayout.resume();
      //                    while(_this.forceLayout.alpha() > .01)_this.forceLayout.tick();
      //                    _this.forceLayout.stop();
      //            },  500)
      //        },
      //
      //
      //
      //        initLabelCollisionResolver: function(){
      //            var _this = this;
      //
      //            this.dataForceLayout = {nodes: [], links: []};
      //            this.ROLE_BOUNDARY = 'boundary node';
      //            this.ROLE_MARKER = 'node fixed to marker';
      //            this.ROLE_LABEL = 'node for floating label';
      //
      //            this.data = this.model.marker.label.getKeys({ time: this.time });
      //            this.data.forEach(function(d,i){
      //                _this.dataForceLayout.nodes.push({geo: d[KEY], role:_this.ROLE_MARKER, fixed: true});
      //                _this.dataForceLayout.nodes.push({geo: d[KEY], role:_this.ROLE_LABEL, fixed: false});
      //                _this.dataForceLayout.links.push({source: i*2, target: i*2+1 });
      //            })
      //            _this.dataForceLayout.nodes.push({geo: "upper_boundary", role:_this.ROLE_BOUNDARY, fixed: true});
      //            _this.dataForceLayout.nodes.push({geo: "lower_boundary", role:_this.ROLE_BOUNDARY, fixed: true});
      //
      //            this.forceLayout = d3.layout.force()
      //                .size([1000, 400])
      //                .gravity(.05)
      //                .charge(function(d){
      //                        switch (d.role){
      //                            case _this.ROLE_BOUNDARY: return -1000;
      //                            case _this.ROLE_MARKER: return -0;
      //                            case _this.ROLE_LABEL: return -1000;
      //                        }
      //                    })
      //                .linkDistance(10)
      //                //.linkStrength(1)
      //                .chargeDistance(30)
      //                .friction(.2)
      //                //.theta(.8)
      //                .nodes(this.dataForceLayout.nodes)
      //                .links(this.dataForceLayout.links)
      //                .on("tick", function(){
      //                    _this.dataForceLayout.nodes.forEach(function (d, i) {
      //                        if(d.fixed)return;
      //
      //                        if(d.x<_this.xScale(_this.time)) d.x = _this.xScale(_this.time);
      //                        if(d.x>_this.xScale(_this.time)+10) d.x--;
      //                    })
      //                })
      //                .on("end", function () {
      //
      //                    var entitiesOrderedByY = _this.cached
      //                        .map(function(d){return d[KEY]});
      //
      //                    var suggestedY = _this.dataForceLayout.nodes
      //                        .filter(function(d){return d.role==_this.ROLE_LABEL})
      //                        .sort(function(a,b){return b.y-a.y});
      //
      //                    _this.graph.selectAll(".vzb-lc-label")
      //                        .each(function (d, i) {
      //                            var geoIndex = _this.cached.map(function(d){return d[KEY]}).indexOf(d[KEY]);
      //                            var resolvedY = suggestedY[geoIndex].y || _this.yScale(_this.cached[geoIndex][geoIndex]) || 0;
      //                            d3.select(this)
      //                                .transition()
      //                                .duration(300)
      //                                .attr("transform", "translate(" + _this.xScale(_this.time) + "," + resolvedY + ")")
      //                        });
      //
      //
      //                })
      //                .start();
      //
      //        }


    });

    //LINE CHART TOOL
    var LineChart = Tool.extend('LineChart', {
        /**
         * Initialized the tool
         * @param {Object} placeholder Placeholder element for the tool
         * @param {Object} external_model Model as given by the external page
         */
        init: function(placeholder, external_model) {

          this.name = 'linechart';

          this.components = [{
            component: LCComponent,
            placeholder: '.vzb-tool-viz',
            model: ["state.time", "state.entities", "state.marker", "language"] //pass models to component
          }, {
            component: TimeSlider,
            placeholder: '.vzb-tool-timeslider',
            model: ["state.time"],
            ui: {show_value_when_drag_play: false, axis_aligned: true}
          }, {
            component: Dialogs,
            placeholder: '.vzb-tool-dialogs',
            model: ['state', 'ui', 'language']
          }, {
            component: ButtonList,
            placeholder: '.vzb-tool-buttonlist',
            model: ['state', 'ui', 'language']
          }, {
            component: TreeMenu,
            placeholder: '.vzb-tool-treemenu',
            model: ['state.marker', 'language']
          }];

          this._super(placeholder, external_model);
        },

        default_model: {
        state: {
          time: {
            start: 1990,
            end: 2012,
            value: 2012,
            step: 1,
            formatInput: "%Y"
          },
          //entities we want to show
          entities: {
            dim: "geo",
            show: {
              _defs_: {
                "geo": ["*"],
                "geo.cat": ["region"]
              }
            }
          },
          //how we show it
          marker: {
            space: ["entities", "time"],
            label: {
              use: "property",
              which: "geo.name"
            },
            axis_y: {
              use: "indicator",
              which: "gdp_pc",
              scaleType: "log"
            },
            axis_x: {
              use: "indicator",
              which: "time",
              scaleType: "time"
            },
            color: {
              use: "property",
              which: "geo.region"
            }
          }
        },

        data: {
          //reader: "waffle",
          reader: "csv",
          path: "data/waffles/basic-indicators.csv"
        },

        ui: {
          'vzb-tool-line-chart': {
            entity_labels: {
              min_number_of_entities_when_values_hide: 2 //values hide when showing 2 entities or more
            },
            whenHovering: {
              hideVerticalNow: 0,
              showProjectionLineX: true,
              showProjectionLineY: true,
              higlightValueX: true,
              higlightValueY: true,
              showTooltip: 0
            }
          }
        }
      }

      });

    var Trail = Class.extend({

      init: function(context) {
        this.context = context;
      },

      toggle: function(arg) {
        var _this = this.context;

        if(arg) {
          _this._trails.create();
          _this._trails.run(["resize", "recolor", "opacityHandler", "findVisible", "reveal"]);
        } else {
          _this._trails.run("remove");
          _this.model.entities.select.forEach(function(d) {
            d.trailStartTime = null;
          });
        }
      },

      create: function(selection) {
        var _this = this.context;
        var KEY = _this.KEY;

        //quit if the function is called accidentally
        if(!_this.model.time.trails || !_this.model.entities.select.length) return;

        var start = +_this.model.time.timeFormat(_this.model.time.start);
        var end = +_this.model.time.timeFormat(_this.model.time.end);
        var step = _this.model.time.step;
        var timePoints = [];
        for(var time = start; time <= end; time += step) timePoints.push(time);

        //work with entities.select (all selected entities), if no particular selection is specified
        selection = selection == null ? _this.model.entities.select : [selection];
        selection.forEach(function(d) {

          var trailSegmentData = timePoints.map(function(m) {
            return {
              t: _this.model.time.timeFormat.parse("" + m)
            }
          });

          if(_this.cached[d[KEY]] == null) _this.cached[d[KEY]] = {};

          _this.cached[d[KEY]].maxMinValues = {
            valueXmax: null,
            valueXmin: null,
            valueYmax: null,
            valueYmin: null,
            valueSmax: null
          };

          var maxmin = _this.cached[d[KEY]].maxMinValues;

          var trail = _this.entityTrails
            .filter(function(f) {
              return f[KEY] == "trail-" + d[KEY]
            })
            .selectAll("g")
            .data(trailSegmentData);

          trail.exit().remove();

          trail.enter().append("g")
            .attr("class", "vzb-bc-trailsegment")
            .on("mouseover", function(segment, index) {
              if(isTouchDevice()) return;
              var _key = d3.select(this.parentNode).data()[0][KEY];

              var pointer = {};
              pointer[KEY] = _key.replace("trail-", "");
              pointer.time = segment.t;

              _this._axisProjections(pointer);
              var text = _this.model.time.timeFormat(segment.t);
              var labelData = _this.entityLabels
                .filter(function(f) {
                  return f[KEY] == pointer[KEY]
                })
                .classed("vzb-highlighted", true)
                .datum();
              if(text !== labelData.trailStartTime) {
                var values = _this.model.marker.getFrame(pointer.time);
                var x = _this.xScale(values.axis_x[pointer[KEY]]);
                var y = _this.yScale(values.axis_y[pointer[KEY]]);
                var s = areaToRadius(_this.sScale(values.size[pointer[KEY]]));
                _this._setTooltip(text, x, y, s);
              }
              //change opacity to OPACITY_HIGHLT = 1.0;
              d3.select(this).style("opacity", 1.0);
            })
            .on("mouseout", function(segment, index) {
              if(isTouchDevice()) return;
              _this._axisProjections();
              _this._setTooltip();
              _this.entityLabels.classed("vzb-highlighted", false);
              d3.select(this).style("opacity", _this.model.entities.opacityRegular);
            })
            .each(function(segment, index) {
              var view = d3.select(this);
              view.append("circle");
              view.append("line");
            });


          trail.each(function(segment, index) {
            //update segment data (maybe for new indicators)

            segment.valueY = _this.model.marker.getFrame(segment.t).axis_y[d[KEY]];
            segment.valueX = _this.model.marker.getFrame(segment.t).axis_x[d[KEY]];
            segment.valueS = _this.model.marker.getFrame(segment.t).size[d[KEY]];
            segment.valueC = _this.model.marker.getFrame(segment.t).color[d[KEY]];

            //update min max frame: needed to zoom in on the trail
            if(segment.valueX > maxmin.valueXmax || maxmin.valueXmax == null) maxmin.valueXmax = segment.valueX;
            if(segment.valueX < maxmin.valueXmin || maxmin.valueXmin == null) maxmin.valueXmin = segment.valueX;
            if(segment.valueY > maxmin.valueYmax || maxmin.valueYmax == null) maxmin.valueYmax = segment.valueY;
            if(segment.valueY < maxmin.valueYmin || maxmin.valueYmin == null) maxmin.valueYmin = segment.valueY;
            if(segment.valueS > maxmin.valueSmax || maxmin.valueSmax == null) maxmin.valueSmax = segment.valueS;
          });

        });
      },


      run: function(actions, selection, duration) {
        var _this = this.context;
        var KEY = _this.KEY;


        //quit if function is called accidentally
        if((!_this.model.time.trails || !_this.model.entities.select.length) && actions != "remove") return;
        if(!duration) duration = 0;

        actions = [].concat(actions);

        //work with entities.select (all selected entities), if no particular selection is specified
        selection = selection == null ? _this.model.entities.select : [selection];
        selection.forEach(function(d) {

          var trail = _this.entityTrails
            .filter(function(f) {
              return f[KEY] == "trail-" + d[KEY]
            })
            .selectAll("g")

          //do all the actions over "trail"
          actions.forEach(function(action) {
            _this._trails["_" + action](trail, duration, d);
          })

        });
      },


      _remove: function(trail, duration, d) {
        trail.remove();
      },

      _resize: function(trail, duration, d) {
        var _this = this.context;

        if (_this.model.time.splash) {
          return;
        }

        trail.each(function(segment, index) {

          var view = d3.select(this);
          view.select("circle")
            //.transition().duration(duration).ease("linear")
            .attr("cy", _this.yScale(segment.valueY))
            .attr("cx", _this.xScale(segment.valueX))
            .attr("r", areaToRadius(_this.sScale(segment.valueS)));

          var next = this.parentNode.childNodes[(index + 1)];
          if(next == null) return;
          next = next.__data__;

          var lineLength = Math.sqrt(
              Math.pow(_this.xScale(segment.valueX) - _this.xScale(next.valueX),2) +
              Math.pow(_this.yScale(segment.valueY) - _this.yScale(next.valueY),2)
              )

          view.select("line")
            //.transition().duration(duration).ease("linear")
            .attr("x1", _this.xScale(next.valueX))
            .attr("y1", _this.yScale(next.valueY))
            .attr("x2", _this.xScale(segment.valueX))
            .attr("y2", _this.yScale(segment.valueY))
            .style("stroke-dasharray", lineLength)
            .style("stroke-dashoffset", areaToRadius(_this.sScale(segment.valueS)));
        });
      },

      _recolor: function(trail, duration, d) {
        var _this = this.context;

        trail.each(function(segment, index) {

          var view = d3.select(this);

          var strokeColor = _this.model.marker.color.which == "geo.region"?
            _this.model.marker.color.getColorShade({
              colorID: segment.valueC,
              shadeID: "shade"
            })
            :
            _this.cScale(segment.valueC);

          view.select("circle")
            //.transition().duration(duration).ease("linear")
            .style("fill", _this.cScale(segment.valueC));
          view.select("line")
            //.transition().duration(duration).ease("linear")
            .style("stroke", strokeColor);
        });
      },

      _opacityHandler: function(trail, duration, d) {
        var _this = this.context;

        trail.each(function(segment, index) {

          var view = d3.select(this);

          view
            //.transition().duration(duration).ease("linear")
            .style("opacity", d.opacity || _this.model.entities.opacityRegular);
        });
      },


      _findVisible: function(trail, duration, d) {
        var _this = this.context;
        var KEY = _this.KEY;

        var firstVisible = true;
        var trailStartTime = _this.model.time.timeFormat.parse("" + d.trailStartTime);

        trail.each(function(segment, index) {

          // segment is transparent if it is after current time or before trail StartTime
          segment.transparent = (segment.t - _this.time >= 0) || (trailStartTime - segment.t > 0)
            //no trail segment should be visible if leading bubble is shifted backwards
            || (d.trailStartTime - _this.model.time.timeFormat(_this.time) >= 0);

          if(firstVisible && !segment.transparent) {
            _this.cached[d[KEY]].labelX0 = segment.valueX;
            _this.cached[d[KEY]].labelY0 = segment.valueY;
            _this.cached[d[KEY]].scaledS0 = areaToRadius(_this.sScale(segment.valueS));
            firstVisible = false;
          }
        });
      },


      _reveal: function(trail, duration, d) {
        var _this = this.context;
        var KEY = _this.KEY;

        trail.each(function(segment, index) {

          var view = d3.select(this);

          view.classed("vzb-invisible", segment.transparent);

          if(segment.transparent) return;

          var next = this.parentNode.childNodes[(index + 1)];
          if(next == null) return;
          next = next.__data__;

          if(segment.t - _this.time <= 0 && _this.time - next.t <= 0) {
            next = _this.cached[d[KEY]];

            view.select("line")
              .attr("x2", _this.xScale(segment.valueX))
              .attr("y2", _this.yScale(segment.valueY))
              .attr("x1", _this.xScale(segment.valueX))
              .attr("y1", _this.yScale(segment.valueY))
              //.transition().duration(duration).ease("linear")
              .attr("x1", _this.xScale(next.valueX))
              .attr("y1", _this.yScale(next.valueY));
          } else {
            view.select("line")
              .attr("x2", _this.xScale(segment.valueX))
              .attr("y2", _this.yScale(segment.valueY))
              .attr("x1", _this.xScale(next.valueX))
              .attr("y1", _this.yScale(next.valueY));
          }
        });

      },


    });

    var PanZoom = Class.extend({

        init: function(context) {
            this.context = context;

            this.enabled = false;

            this.dragRectangle = d3.behavior.drag();
            this.zoomer = d3.behavior.zoom();

            this.dragRectangle
                .on("dragstart", this.drag().start)
                .on("drag", this.drag().go)
                .on("dragend", this.drag().stop);

            this.zoomer
                .scaleExtent([1, 100])
                .on("zoom", this.zoom().go)
                .on('zoomend', this.zoom().stop);

            this.zoomer.ratioX = 1;
            this.zoomer.ratioY = 1;

            context._zoomZoomedDomains = {x:{zoomedMin: null, zoomedMax: null}, y:{zoomedMin: null, zoomedMax: null}};
        },

        drag: function(){
            var _this = this.context;
            var self = this;

            return {
                start: function(d, i) {
                    if(!(d3.event.sourceEvent.ctrlKey || d3.event.sourceEvent.metaKey)) return;

                    this.ctrlKeyLock = true;
                    this.origin = {
                        x: d3.mouse(this)[0],
                        y: d3.mouse(this)[1]
                    };
                    _this.zoomRect.classed("vzb-invisible", false);

                },

                go: function(d, i) {
                    if(!this.ctrlKeyLock) return;
                    var origin = this.origin;
                    var mouse = {
                        x: d3.event.x,
                        y: d3.event.y
                    };

                    _this.zoomRect
                        .attr("x", Math.min(mouse.x, origin.x))
                        .attr("y", Math.min(mouse.y, origin.y))
                        .attr("width", Math.abs(mouse.x - origin.x))
                        .attr("height", Math.abs(mouse.y - origin.y));
                },

                stop: function(e) {
                    if(!this.ctrlKeyLock) return;
                    this.ctrlKeyLock = false;

                    _this.zoomRect
                        .attr("width", 0)
                        .attr("height", 0)
                        .classed("vzb-invisible", true);

                    this.target = {
                        x: d3.mouse(this)[0],
                        y: d3.mouse(this)[1]
                    };

                    self._zoomOnRectangle(
                        d3.select(this),
                        this.origin.x,
                        this.origin.y,
                        this.target.x,
                        this.target.y,
                        true, 500
                    );
                }
            };
        },

        zoom: function() {
            var _this = this.context;
            var zoomer = this.zoomer;
            var self = this;

            return {
                go: function() {

                    if(d3.event.sourceEvent != null && (d3.event.sourceEvent.ctrlKey || d3.event.sourceEvent.metaKey)) return;

                    //send the event to the page if fully zoomed our or page not scrolled into view
    //
    //                    if(d3.event.scale == 1)
    //
    //                    if(utils.getViewportPosition(_this.element.node()).y < 0 && d3.event.scale > 1) {
    //                        _this.scrollableAncestor.scrollTop += d3.event.sourceEvent.deltaY;
    //                        return;
    //                    }
                    if(d3.event.sourceEvent != null && _this.scrollableAncestor) {
                        if(d3.event.sourceEvent != null && !self.enabled){
                            _this.scrollableAncestor.scrollTop += d3.event.sourceEvent.deltaY;
                            zoomer.scale(1)
                            return;
                        }
                    }

                    _this.model._data.entities.clearHighlighted();
                    _this._setTooltip();

                    var zoom = d3.event.scale;
                    var pan = d3.event.translate;
                    var ratioY = zoomer.ratioY;
                    var ratioX = zoomer.ratioX;

                    _this.draggingNow = true;

                    //value protections and fallbacks
                    if(isNaN(zoom) || zoom == null) zoom = zoomer.scale();
                    if(isNaN(zoom) || zoom == null) zoom = 1;

                    var sourceEvent = d3.event.sourceEvent;

                    //TODO: this is a patch to fix #221. A proper code review of zoom and zoomOnRectangle logic is needed
                    /*
                     * Mouse wheel and touchmove events set the zoom value
                     * independently of axis ratios. If the zoom event was triggered
                     * by a mouse wheel event scrolling down or touchmove event with
                     * more than 1 contact that sets zoom to 1, then set the axis
                     * ratios to 1 as well, which will fully zoom out.
                     */
                    if(zoom === 1 && sourceEvent !== null &&
                        (sourceEvent.type === "wheel" && sourceEvent.deltaY > 0 ||
                         sourceEvent.type === "touchmove" && sourceEvent.touches.length > 1)) {
                        zoomer.ratioX = 1;
                        ratioX = 1;
                        zoomer.ratioY = 1;
                        ratioY = 1
                    }

                    if(isNaN(pan[0]) || isNaN(pan[1]) || pan[0] == null || pan[1] == null) pan = zoomer.translate();
                    if(isNaN(pan[0]) || isNaN(pan[1]) || pan[0] == null || pan[1] == null) pan = [0, 0];

                    // limit the zooming, so that it never goes below 1 for any of the axes
                    if(zoom * ratioY < 1) {
                        ratioY = 1 / zoom;
                        zoomer.ratioY = ratioY
                    }
                    if(zoom * ratioX < 1) {
                        ratioX = 1 / zoom;
                        zoomer.ratioX = ratioX
                    }

                    //limit the panning, so that we are never outside the possible range
                    if(pan[0] > 0) pan[0] = 0;
                    if(pan[1] > 0) pan[1] = 0;
                    if(pan[0] < (1 - zoom * ratioX) * _this.width) pan[0] = (1 - zoom * ratioX) * _this.width;
                    if(pan[1] < (1 - zoom * ratioY) * _this.height) pan[1] = (1 - zoom * ratioY) * _this.height;

                    var xPanOffset = _this.width * zoom * ratioX;
                    var yPanOffset = _this.height * zoom * ratioY;

                    var xRange = [0 * zoom * ratioX + pan[0], xPanOffset + pan[0]];
                    var yRange = [yPanOffset + pan[1], 0 * zoom * ratioY + pan[1]];

                    var xRangeBumped = _this._rangeBump(xRange);
                    var yRangeBumped = _this._rangeBump(yRange);

                    /*
                     * Shift xRange and yRange by the difference between the bumped
                     * ranges, which is scaled by the zoom factor. This accounts for
                     * the range bump, which controls a gutter around the
                     * bubblechart, while correctly zooming.
                     */
                    var xRangeMinOffset = (xRangeBumped[0] - xRange[0]) * zoom * ratioX;
                    var xRangeMaxOffset = (xRangeBumped[1] - xRange[1]) * zoom * ratioX;

                    var yRangeMinOffset = (yRangeBumped[0] - yRange[0]) * zoom * ratioY;
                    var yRangeMaxOffset = (yRangeBumped[1] - yRange[1]) * zoom * ratioY;

                    xRange[0] = xRange[0] + xRangeMinOffset;
                    xRange[1] = xRange[1] + xRangeMaxOffset;

                    yRange[0] = yRange[0] + yRangeMinOffset;
                    yRange[1] = yRange[1] + yRangeMaxOffset;

                    // Calculate the maximum xRange and yRange available.
                    var xRangeBounds = [0,  _this.width];
                    var yRangeBounds = [_this.height, 0];

                    var xRangeBoundsBumped = _this._rangeBump(xRangeBounds);
                    var yRangeBoundsBumped = _this._rangeBump(yRangeBounds);

                    /*
                     * Set the pan to account for the range bump by subtracting
                     * offsets and preventing panning past the range bump gutter.
                     */
                    if(xRange[0] > xRangeBoundsBumped[0]) pan[0] = xRangeBoundsBumped[0] - xRangeMinOffset;
                    if(xRange[1] < xRangeBoundsBumped[1]) pan[0] = xRangeBoundsBumped[1] - xRangeMaxOffset - xPanOffset;
                    if(yRange[0] < yRangeBoundsBumped[0]) pan[1] = yRangeBoundsBumped[0] - yRangeMinOffset - yPanOffset;
                    if(yRange[1] > yRangeBoundsBumped[1]) pan[1] = yRangeBoundsBumped[1] - yRangeMaxOffset;

                    zoomer.translate(pan);

                    /*
                     * Clamp the xRange and yRange by the amount that the bounds
                     * that are range bumped.
                     *
                     * Additionally, take the amount clamped on the end of the range
                     * and either subtract or add it to the range's other end. This
                     * prevents visible stretching of the range when only panning.
                     */
                    if(xRange[0] > xRangeBoundsBumped[0]) {
                        xRange[1] = xRange[1] - Math.abs(xRange[0] - xRangeBoundsBumped[0]);
                        xRange[0] = xRangeBoundsBumped[0];
                    }

                    if(xRange[1] < xRangeBoundsBumped[1]) {
                        xRange[0] = xRange[0] + Math.abs(xRange[1] - xRangeBoundsBumped[1]);
                        xRange[1] = xRangeBoundsBumped[1];
                    }

                    if(yRange[0] < yRangeBoundsBumped[0]) {
                        yRange[1] = yRange[1] + Math.abs(yRange[0] - yRangeBoundsBumped[0]);
                        yRange[0] = yRangeBoundsBumped[0];
                    }

                    if(yRange[1] > yRangeBoundsBumped[1]) {
                        yRange[0] = yRange[0] - Math.abs(yRange[1] - yRangeBoundsBumped[1]);
                        yRange[1] = yRangeBoundsBumped[1];
                    }

                    if(_this.model.marker.axis_x.scaleType === 'ordinal'){
                        _this.xScale.rangeBands(xRange);
                    }else{
                        _this.xScale.range(xRange);
                    }

                    if(_this.model.marker.axis_y.scaleType === 'ordinal'){
                        _this.yScale.rangeBands(yRange);
                    }else{
                        _this.yScale.range(yRange);
                    }

                    var formatter = function(n) { return d3.round(n, 2); };

                    var zoomedXRange = xRangeBoundsBumped;
                    var zoomedYRange = yRangeBoundsBumped;

                    /*
                     * Set the zoomed min/max to the correct value depending on if the
                     * min/max values lie within the range bound regions.
                     */
                    zoomedXRange[0] = xRangeBounds[0] > xRange[0] ? xRangeBounds[0] : xRange[0];
                    zoomedXRange[1] = xRangeBounds[1] < xRange[1] ? xRangeBounds[1] : xRange[1];
                    zoomedYRange[0] = yRangeBounds[0] < yRange[0] ? yRangeBounds[0] : yRange[0];
                    zoomedYRange[1] = yRangeBounds[1] > yRange[1] ? yRangeBounds[1] : yRange[1];

                    _this._zoomZoomedDomains = {
                        x: {
                         zoomedMin: formatter(_this.xScale.invert(zoomedXRange[0])),
                         zoomedMax: formatter(_this.xScale.invert(zoomedXRange[1]))
                        },
                        y: {
                         zoomedMin: formatter(_this.yScale.invert(zoomedYRange[0])),
                         zoomedMax: formatter(_this.yScale.invert(zoomedYRange[1]))
                        }
                    }

                    _this.model.marker.axis_x.set(_this._zoomZoomedDomains.x, null, false /*avoid storing it in URL*/);
                    _this.model.marker.axis_y.set(_this._zoomZoomedDomains.y, null, false /*avoid storing it in URL*/);

                    // Keep the min and max size (pixels) constant, when zooming.
                    //                    _this.sScale.range([utils.radiusToArea(_this.minRadius) * zoom * zoom * ratioY * ratioX,
                    //                                        utils.radiusToArea(_this.maxRadius) * zoom * zoom * ratioY * ratioX ]);

                    var optionsY = _this.yAxis.labelerOptions();
                    var optionsX = _this.xAxis.labelerOptions();
                    optionsY.limitMaxTickNumber = zoom * ratioY < 2 ? 7 : 14;
                    optionsY.transitionDuration = zoomer.duration;
                    optionsX.transitionDuration = zoomer.duration;

                    _this.xAxisEl.call(_this.xAxis.labelerOptions(optionsX));
                    _this.yAxisEl.call(_this.yAxis.labelerOptions(optionsY));
                    _this.redrawDataPoints(zoomer.duration);
                    _this._trails.run("resize", null, zoomer.duration);

                    zoomer.duration = 0;
                },

                stop: function(){
                    _this.draggingNow = false;

                    //Force the update of the URL and history, with the same values
                    _this.model.marker.axis_x.set(_this._zoomZoomedDomains.x, true, true);
                    _this.model.marker.axis_y.set(_this._zoomZoomedDomains.y, true, true);
                }
            };
        },

        expandCanvas: function(duration) {
            var _this = this.context;
            if (!duration) duration = _this.duration;

            var timeRounded = _this.model.time.timeFormat.parse( _this.model.time.timeFormat(_this.time) );

            var mmmX = _this.xyMaxMinMean.x[timeRounded];
            var mmmY = _this.xyMaxMinMean.y[timeRounded];
            var radiusMax = areaToRadius(_this.sScale(_this.xyMaxMinMean.s[timeRounded].max));

            /*
             * Use a range bumped scale to correctly accommodate the range bump
             * gutter.
             */
            var suggestedFrame = {
                x1: _this.xScale(mmmX.min) - radiusMax,
                y1: _this.yScale(mmmY.min) + radiusMax,
                x2: _this.xScale(mmmX.max) + radiusMax,
                y2: _this.yScale(mmmY.max) - radiusMax
            };

            var xBounds = [0, _this.width];
            var yBounds = [_this.height, 0];

            // Get the current zoom frame based on the current dimensions.
            var frame = {
                x1: xBounds[0],
                x2: xBounds[1],
                y1: yBounds[0],
                y2: yBounds[1]
            };

            var TOLERANCE = .0;

            /*
             * If there is no current zoom frame, or if any of the suggested frame
             * points extend outside of the current zoom frame, then expand the
             * canvas.
             */
            if(!_this.isCanvasPreviouslyExpanded ||
                suggestedFrame.x1 < frame.x1 * (1 - TOLERANCE) || suggestedFrame.x2 > frame.x2 * (1 + TOLERANCE) ||
                suggestedFrame.y2 < frame.y2 * (1 - TOLERANCE) || suggestedFrame.y1 > frame.y1 * (1 + TOLERANCE)) {
                /*
                 * If there is already a zoom frame, then clamp the suggested frame
                 * points to only zoom out and expand the canvas.
                 *
                 * If any of x1, x2, y1, or y2 is within the current frame
                 * boundaries, then clamp them to the frame boundaries. If any of
                 * the above values will translate into a data value that is outside
                 * of the possible data range, then clamp them to the frame
                 * coordinate that corresponds to the maximum data value that can
                 * be displayed.
                 */
                if (_this.isCanvasPreviouslyExpanded) {
                    /*
                     * Calculate bounds and bumped scale for calculating the data boundaries
                     * to which the suggested frame points need to be clamped.
                     */
                    var xBoundsBumped = _this._rangeBump(xBounds);
                    var yBoundsBumped = _this._rangeBump(yBounds);

                    var xScaleBoundsBumped = _this.xScale.copy()
                        .range(xBoundsBumped);
                    var yScaleBoundsBumped = _this.yScale.copy()
                        .range(yBoundsBumped);

                    var xDataBounds = [xScaleBoundsBumped.invert(xBounds[0]), xScaleBoundsBumped.invert(xBounds[1])];
                    var yDataBounds = [yScaleBoundsBumped.invert(yBounds[0]), yScaleBoundsBumped.invert(yBounds[1])];

                    if (suggestedFrame.x1 > 0)
                        suggestedFrame.x1 = 0;
                    else if (_this.xScale.invert(suggestedFrame.x1) < xDataBounds[0])
                        suggestedFrame.x1 = _this.xScale(xDataBounds[0]);

                    if (suggestedFrame.x2 < _this.width)
                        suggestedFrame.x2 = _this.width;
                    else if (_this.xScale.invert(suggestedFrame.x2) > xDataBounds[1])
                        suggestedFrame.x2 = _this.xScale(xDataBounds[1]);

                    if (suggestedFrame.y1 < _this.height)
                        suggestedFrame.y1 = _this.height;
                    else if (_this.yScale.invert(suggestedFrame.y1) < yDataBounds[0])
                        suggestedFrame.y1 = _this.yScale(yDataBounds[0]);

                    if (suggestedFrame.y2 > 0)
                        suggestedFrame.y2 = 0;
                    else if (_this.yScale.invert(suggestedFrame.y2) > yDataBounds[1])
                        suggestedFrame.y2 = _this.yScale(yDataBounds[1]);
                }

                _this.isCanvasPreviouslyExpanded = true;
                this._zoomOnRectangle(_this.element, suggestedFrame.x1, suggestedFrame.y1,
                    suggestedFrame.x2, suggestedFrame.y2, false, duration);
            } else {
                _this.redrawDataPoints(duration);
            }
        },

        zoomToMaxMin: function(zoomedMinX, zoomedMaxX, zoomedMinY, zoomedMaxY, duration){
            var _this = this.context;
            var minX = zoomedMinX;
            var maxX = zoomedMaxX;
            var minY = zoomedMinY;
            var maxY = zoomedMaxY;

            var xRangeBounds = [0, _this.width];
            var yRangeBounds = [_this.height, 0];

            var xDomain = _this.xScale.domain();
            var yDomain = _this.yScale.domain();

            // Clamp zoomed values to maximum and minimum values.
            if (minX < xDomain[0]) minX = xDomain[0];
            if (maxX > xDomain[1]) maxX = xDomain[1];
            if (minY < yDomain[0]) minY = yDomain[0];
            if (maxY > yDomain[1]) maxY = yDomain[1];

            /*
             * Define TOLERANCE value as Number.EPSILON if exists, otherwise use
             * ES6 standard value.
             */
            var TOLERANCE = Number.EPSILON ? Number.EPSILON : 2.220446049250313e-16;

            /*
             * Check if the range bump region is currently displayed, i.e. for the
             * minX range bump region, check:
             * _this.xScale.invert(xRangeBounds[0]) < _this.xScale.domain()[0]
             *
             * Also check if the given min/max values equal the domain edges.
             * If so, then set the min/max values according to their range bumped
             * values. These values are used to calculate the correct rectangle
             * points for zooming.
             */
            if (_this.xScale.invert(xRangeBounds[0]) < xDomain[0]
                && Math.abs(minX - xDomain[0]) < TOLERANCE) {
                minX = _this.xScale.invert(xRangeBounds[0]);
            }

            if (_this.xScale.invert(xRangeBounds[1]) > xDomain[1]
                && Math.abs(maxX - xDomain[1]) < TOLERANCE) {
                maxX = _this.xScale.invert(xRangeBounds[1]);
            }

            if (_this.yScale.invert(yRangeBounds[0]) < yDomain[0]
                && Math.abs(minY - yDomain[0]) < TOLERANCE) {
                minY = _this.yScale.invert(yRangeBounds[0]);
            }

            if (_this.yScale.invert(yRangeBounds[1]) > yDomain[1]
                && Math.abs(maxY - yDomain[1]) < TOLERANCE) {
                maxY = _this.yScale.invert(yRangeBounds[1]);
            }

            var xRange = [_this.xScale(minX), _this.xScale(maxX)];
            var yRange = [_this.yScale(minY), _this.yScale(maxY)];

            this._zoomOnRectangle(_this.element, xRange[0], yRange[0], xRange[1], yRange[1], false, duration);
        },

        _zoomOnRectangle: function(element, zoomedX1, zoomedY1, zoomedX2, zoomedY2, compensateDragging, duration) {
            var _this = this.context;
            var zoomer = this.zoomer;

            var x1 = zoomedX1;
            var y1 = zoomedY1;
            var x2 = zoomedX2;
            var y2 = zoomedY2;

            /*
             * When dragging to draw a rectangle, the translate vector has (x2 - x1)
             * added to zoomer.translate()[0], and (y2 - 1) added to
             * zoomer.translate()[1].
             *
             * We need to compensate for this addition when
             * zooming with a rectangle, because zooming with a rectangle will
             * update the translate vector with new values based on the rectangle
             * dimensions.
             */
            if(compensateDragging) {
                zoomer.translate([
                    zoomer.translate()[0] + x1 - x2,
                    zoomer.translate()[1] + y1 - y2
                ])
            }

            var xRangeBounds = [0, _this.width];
            var yRangeBounds = [_this.height, 0];

            var xDomain = _this.xScale.domain();
            var yDomain = _this.yScale.domain();

            /*
             * If the min or max of one axis lies in the range bump region, then
             * changing the opposite end of that axis must correctly scale and
             * maintain the range bump region.
             */
            if (_this.xScale.invert(x1) < xDomain[0]) {
                x1 = this._scaleCoordinate(x1, xRangeBounds[1] - x2, _this.xScale.range()[0], xRangeBounds[1]);
            } else if (_this.xScale.invert(x2) < xDomain[0]) {
                x2 = this._scaleCoordinate(x2, xRangeBounds[1] - x1, _this.xScale.range()[0], xRangeBounds[1]);
            }

            if (_this.xScale.invert(x2) > xDomain[1]) {
                x2 = this._scaleCoordinate(x2, x1 - xRangeBounds[0], _this.xScale.range()[1], xRangeBounds[0]);
            } else if (_this.xScale.invert(x1) > xDomain[1]) {
                x1 = this._scaleCoordinate(x1, x2 - xRangeBounds[0], _this.xScale.range()[1], xRangeBounds[0]);
            }

            if (_this.yScale.invert(y1) < yDomain[0]) {
                y1 = this._scaleCoordinate(y1, y2 - yRangeBounds[1], _this.yScale.range()[0], yRangeBounds[1]);
            } else if (_this.yScale.invert(y2) < yDomain[0]) {
                y2 = this._scaleCoordinate(y2, y1 - yRangeBounds[1], _this.yScale.range()[0], yRangeBounds[1]);
            }

            if (_this.yScale.invert(y2) > yDomain[1]) {
                y2 = this._scaleCoordinate(y2, yRangeBounds[0] - y1, _this.yScale.range()[1], yRangeBounds[0]);
            } else if (_this.yScale.invert(y1) > yDomain[1]) {
                y1 = this._scaleCoordinate(y1, yRangeBounds[0] - y2, _this.yScale.range()[1], yRangeBounds[0]);
            }

            if(Math.abs(x1 - x2) < 10 || Math.abs(y1 - y2) < 10) return;

            var maxZoom = zoomer.scaleExtent()[1];

            if(Math.abs(x1 - x2) > Math.abs(y1 - y2)) {
                var zoom = _this.height / Math.abs(y1 - y2) * zoomer.scale();

                /*
                 * Clamp the zoom scalar to the maximum zoom allowed before
                 * calculating the next ratioX and ratioY.
                 */
                if (zoom > maxZoom) zoom = maxZoom;

                var ratioX = _this.width / Math.abs(x1 - x2) * zoomer.scale() / zoom * zoomer.ratioX;
                var ratioY = zoomer.ratioY;
            } else {
                var zoom = _this.width / Math.abs(x1 - x2) * zoomer.scale();

                /*
                 * Clamp the zoom scalar to the maximum zoom allowed before
                 * calculating the next ratioX and ratioY.
                 */
                if (zoom > maxZoom) zoom = maxZoom;

                var ratioY = _this.height / Math.abs(y1 - y2) * zoomer.scale() / zoom * zoomer.ratioY;
                var ratioX = zoomer.ratioX;
            }

            var pan = [
                (zoomer.translate()[0] - Math.min(x1, x2)) / zoomer.scale() / zoomer.ratioX * zoom * ratioX,
                (zoomer.translate()[1] - Math.min(y1, y2)) / zoomer.scale() / zoomer.ratioY * zoom * ratioY
            ];

            zoomer.scale(zoom);
            zoomer.ratioY = ratioY;
            zoomer.ratioX = ratioX;
            zoomer.translate(pan);
            zoomer.duration = duration ? duration : 0;

            zoomer.event(element);
        },

        /*
         * Helper function that returns a scaled coordinate value based on the
         * distance between the given coordinate and the data boundary.
         */
        _scaleCoordinate: function(coordValue, scaleDifference, dataBoundary, viewportBoundary) {
            var scalar = scaleDifference / Math.abs(dataBoundary - viewportBoundary);
            return (coordValue - dataBoundary) * (1 - scalar) + dataBoundary;
        },

        reset: function(element) {
            var _this = this.context;
            _this.isCanvasPreviouslyExpanded = false;

            this.zoomer.scale(1);
            this.zoomer.ratioY = 1;
            this.zoomer.ratioX = 1;
            this.zoomer.translate([0, 0]);
            this.zoomer.duration = 0;
            this.zoomer.event(element || _this.element);
        },

        rerun: function(element) {
            var _this = this.context;
            this.zoomer.event(element || _this.element);
        }
    });

    //BUBBLE CHART COMPONENT
    var BubbleChartComp = Component.extend({

      /**
       * Initializes the component (Bubble Chart).
       * Executed once before any template is rendered.
       * @param {Object} config The config passed to the component
       * @param {Object} context The component's parent
       */
      init: function(config, context) {
        var _this = this;
        this.name = 'bubblechart';
        this.template = 'bubblechart.html';

        //define expected models for this component
        this.model_expects = [{
          name: "time",
          type: "time"
        }, {
          name: "entities",
          type: "entities"
        }, {
          name: "marker",
          type: "model"
        }, {
          name: "language",
          type: "language"
        }, {
          name: "ui",
          type: "model"
        }];

        //starts as splash if this is the option
        this._splash = config.ui.splash;

        this.model_binds = {
          'change:time': function(evt, original) {
            if(_this._splash !== _this.model.time.splash) {
              if(!_this._readyOnce) return;
              _this._splash = _this.model.time.splash;
              //TODO: adjust X & Y axis here
            }
          },
          'change:time.start': function(evt, original) {
            if(_this.model.marker.color.scaleType === 'time') {
              _this.model.marker.color.scale = null;
            }
          },
          "change:time.record": function() {
            //console.log("change time record");
            if(_this.model.time.record) {
              _this._export.open(this.element, this.name);
            } else {
              _this._export.reset();
            }
          },
          "change:time.trails": function(evt) {
            //console.log("EVENT change:time:trails");
            _this._trails.toggle(_this.model.time.trails);
            _this.redrawDataPoints();
          },
          "change:time.lockNonSelected": function(evt) {
            //console.log("EVENT change:time:lockNonSelected");
            _this.redrawDataPoints(500);
          },
          "change:marker": function(evt, path) {
            // bubble size change is processed separately
            if(!_this._readyOnce) return;

            if(path.indexOf("scaleType") > -1) {
              _this.ready();
              return;
            }

            if(path.indexOf("marker.size") !== -1) return;

            if(path.indexOf("domainMin") > -1 || path.indexOf("domainMax") > -1) {
              _this.updateSize();
              _this.updateMarkerSizeLimits();
              _this._trails.run("findVisible");
              _this.redrawDataPoints();
              _this._trails.run("resize");
              return;
            }
            if(path.indexOf("zoomedMin") > -1 || path.indexOf("zoomedMax") > -1) {
              if(_this.draggingNow)return;

              //avoid zooming again if values didn't change.
              //also prevents infinite loop on forced URL update from zoom.stop()
              if(_this._zoomZoomedDomains.x.zoomedMin == _this.model.marker.axis_x.zoomedMin
              && _this._zoomZoomedDomains.x.zoomedMax == _this.model.marker.axis_x.zoomedMax
              && _this._zoomZoomedDomains.y.zoomedMin == _this.model.marker.axis_y.zoomedMin
              && _this._zoomZoomedDomains.y.zoomedMax == _this.model.marker.axis_y.zoomedMax
              ) return;

                _this._panZoom.zoomToMaxMin(
                  _this.model.marker.axis_x.zoomedMin,
                  _this.model.marker.axis_x.zoomedMax,
                  _this.model.marker.axis_y.zoomedMin,
                  _this.model.marker.axis_y.zoomedMax,
                  500
              )
              return;
            }

            //console.log("EVENT change:marker", evt);
          },
          "change:entities.select": function() {
            if(!_this._readyOnce) return;
            //console.log("EVENT change:entities:select");
            _this.selectDataPoints();
            _this.redrawDataPoints();
            _this._trails.run(["resize", "recolor", "opacityHandler","findVisible", "reveal"]);
            _this.updateBubbleOpacity();
            _this._updateDoubtOpacity();
          },
          "change:entities.highlight": function() {
            if(!_this._readyOnce) return;
            //console.log("EVENT change:entities:highlight");
            _this.highlightDataPoints();
          },
          'change:time.value': function() {
            //console.log("EVENT change:time:value");
            _this.updateTime();
            _this._updateDoubtOpacity();

            _this._trails.run("findVisible");
            if(_this.model.time.adaptMinMaxZoom) {
              _this._panZoom.expandCanvas();
            } else {
              _this.redrawDataPoints();
            }
            _this._trails.run("reveal");
            _this.tooltipMobile.classed('vzb-hidden', true);
            //_this._bubblesInteract().mouseout();
          },
          'change:time.adaptMinMaxZoom': function() {
            //console.log("EVENT change:time:adaptMinMaxZoom");
            if(_this.model.time.adaptMinMaxZoom) {
              _this._panZoom.expandCanvas(500);
            } else {
              _this._panZoom.reset();
            }
          },
          'change:marker.size': function(evt, path) {
            //console.log("EVENT change:marker:size:max");
            if(!_this._readyOnce) return;
            if(path.indexOf("domainMin") > -1 || path.indexOf("domainMax") > -1) {
                _this.updateMarkerSizeLimits();
                _this._trails.run("findVisible");
                _this.redrawDataPointsOnlySize();
                _this._trails.run("resize");
                return;
            }
          },
          'change:marker.color.palette': function(evt, path) {
            if(!_this._readyOnce) return;
            //console.log("EVENT change:marker:color:palette");
            _this.redrawDataPointsOnlyColors();
            _this._trails.run("recolor");
          },
          'change:entities.opacitySelectDim': function() {
            _this.updateBubbleOpacity();
          },
          'change:entities.opacityRegular': function() {
            _this.updateBubbleOpacity();
            _this._trails.run("opacityHandler");
          },
          'ready': function() {
            // if(_this.model.marker.color.scaleType === 'time') {
            //   _this.model.marker.color.scale = null;
            //   utils.defer(function() {
            //     _this.trigger('ready');
            //   });
            // }
          }
        };

        this._super(config, context);

        this.xScale = null;
        this.yScale = null;
        this.sScale = null;
        this.cScale = null;

        this.xAxis = axisSmart();
        this.yAxis = axisSmart();


        this.cached = {};
        this.xyMaxMinMean = {};
        this.isCanvasPreviouslyExpanded = false;
        this.draggingNow = null;

        // default UI settings
        this.ui = extend({
          whenHovering: {},
          labels: {}
        }, this.ui["vzb-tool-" + this.name]);

        this.ui.whenHovering = extend({
          showProjectionLineX: true,
          showProjectionLineY: true,
          higlightValueX: true,
          higlightValueY: true
        }, this.ui.whenHovering);

        this.ui.labels = extend({
          autoResolveCollisions: false,
          dragging: true
        }, this.ui.labels);

        this._trails = new Trail(this);
        this._panZoom = new PanZoom(this);
        this._export = new Exporter(this);
        this._export
          .prefix("vzb-bc-")
          .deleteClasses(["vzb-bc-bubbles-crop", "vzb-hidden", "vzb-bc-year", "vzb-bc-zoom-rect",
            "vzb-bc-projection-x", "vzb-bc-projection-y", "vzb-bc-axis-c-title"
          ]);



        //            this.collisionResolver = d3.svg.collisionResolver()
        //                .value("labelY2")
        //                .fixed("labelFixed")
        //                .selector("text")
        //                .scale(this.yScale)
        //                .handleResult(this._repositionLabels);


        this.labelDragger = d3.behavior.drag()
          .on("dragstart", function(d, i) {
            d3.event.sourceEvent.stopPropagation();
            var KEY = _this.KEY;
            _this.druging = d[KEY];
          })
          .on("drag", function(d, i) {
            var KEY = _this.KEY;
            if(!_this.ui.labels.dragging) return;
            var cache = _this.cached[d[KEY]];
            cache.labelFixed = true;


            cache.labelX_ += d3.event.dx / _this.width;
            cache.labelY_ += d3.event.dy / _this.height;

            var resolvedX = _this.xScale(cache.labelX0) + cache.labelX_ * _this.width;
            var resolvedY = _this.yScale(cache.labelY0) + cache.labelY_ * _this.height;

            var resolvedX0 = _this.xScale(cache.labelX0);
            var resolvedY0 = _this.yScale(cache.labelY0);

            var lineGroup = _this.entityLines.filter(function(f) {
              return f[KEY] == d[KEY];
            });

            _this._repositionLabels(d, i, this, resolvedX, resolvedY, resolvedX0, resolvedY0, 0, lineGroup);
          })
          .on("dragend", function(d, i) {
            var KEY = _this.KEY;
            _this.druging = null;
            _this.model.entities.setLabelOffset(d, [
              _this.cached[d[KEY]].labelX_,
              _this.cached[d[KEY]].labelY_
            ]);
          });



      },




      _rangeBump: function(arg, undo) {
        var bump = this.activeProfile.maxRadius;
        undo = undo?-1:1;
        if(isArray(arg) && arg.length > 1) {
          var z1 = arg[0];
          var z2 = arg[arg.length - 1];

          //the sign of bump depends on the direction of the scale
          if(z1 < z2) {
            z1 += bump * undo;
            z2 -= bump * undo;
            // if the scale gets inverted because of bump, set it to avg between z1 and z2
            if(z1 > z2) z1 = z2 = (z1 + z2) / 2;
          } else if(z1 > z2) {
            z1 -= bump * undo;
            z2 += bump * undo;
            // if the scale gets inverted because of bump, set it to avg between z1 and z2
            if(z1 < z2) z1 = z2 = (z1 + z2) / 2;
          } else {
            warn("rangeBump error: the input scale range has 0 length. that sucks");
          }
          return [z1, z2];
        } else {
          warn("rangeBump error: input is not an array or empty");
        }
      },

    //  _marginUnBump: function(arg) {
    //    var bump = this.profiles[this.getLayoutProfile()].maxRadius;
    //    if(utils.isObject(arg)) {
    //      return {
    //        left: arg.left - bump,
    //        right: arg.right - bump,
    //        top: arg.top - bump,
    //        bottom: arg.bottom - bump
    //      };
    //    } else {
    //      utils.warn("marginUnBump error: input is not an object {left top bottom right}");
    //    }
    //  },


      /**
       * Executes right after the template is in place, but the model is not yet ready
       */
      readyOnce: function() {
        var _this = this;

        this.scrollableAncestor = findScrollableAncestor(this.element);
        this.element = d3.select(this.element);

        // reference elements
        this.graph = this.element.select('.vzb-bc-graph');
        this.yAxisElContainer = this.graph.select('.vzb-bc-axis-y');
        this.yAxisEl = this.yAxisElContainer.select('g');

        this.xAxisElContainer = this.graph.select('.vzb-bc-axis-x');
        this.xAxisEl = this.xAxisElContainer.select('g');

        this.yTitleEl = this.graph.select('.vzb-bc-axis-y-title');
        this.xTitleEl = this.graph.select('.vzb-bc-axis-x-title');
        this.sTitleEl = this.graph.select('.vzb-bc-axis-s-title');
        this.cTitleEl = this.graph.select('.vzb-bc-axis-c-title');
        this.yearEl = this.graph.select('.vzb-bc-year');

        this.year = new DynamicBackground(this.yearEl);

        this.yInfoEl = this.graph.select('.vzb-bc-axis-y-info');
        this.xInfoEl = this.graph.select('.vzb-bc-axis-x-info');
        this.dataWarningEl = this.graph.select('.vzb-data-warning');

        this.projectionX = this.graph.select(".vzb-bc-projection-x");
        this.projectionY = this.graph.select(".vzb-bc-projection-y");

        this.trailsContainer = this.graph.select('.vzb-bc-trails');
        this.bubbleContainerCrop = this.graph.select('.vzb-bc-bubbles-crop');
        this.bubbleContainer = this.graph.select('.vzb-bc-bubbles');
        this.labelsContainer = this.graph.select('.vzb-bc-labels');
        this.linesContainer = this.graph.select('.vzb-bc-lines');
        this.zoomRect = this.element.select('.vzb-bc-zoom-rect');
        this.eventArea = this.element.select('.vzb-bc-eventarea');

        this.entityBubbles = null;
        this.entityLabels = null;
        this.tooltip = this.element.select('.vzb-bc-tooltip');
        this.tooltipMobile = this.element.select('.vzb-tooltip-mobile');
        this.entityLines = null;
        //component events
        this.on("resize", function() {
          //console.log("EVENT: resize");
          _this.updateSize();
          _this.updateMarkerSizeLimits();
          _this._trails.run("findVisible");
          _this._panZoom.rerun(); // includes redraw data points and trail resize
        });

        //keyboard listeners
        d3.select("body")
          .on("keydown", function() {
            if(d3.event.metaKey || d3.event.ctrlKey) _this.element.select("svg").classed("vzb-zoomin", true);
          })
          .on("keyup", function() {
            if(!d3.event.metaKey && !d3.event.ctrlKey) _this.element.select("svg").classed("vzb-zoomin", false);
          });

        this.bubbleContainerCrop
          .call(this._panZoom.zoomer)
          .call(this._panZoom.dragRectangle)
          .on('dblclick.zoom', null)
          .on("mouseup", function() {
            _this.draggingNow = false;
          })

        d3.select(this.parent.placeholder)
          .onTap(function() {
            _this._panZoom.enabled = true;
    //        _this._bubblesInteract().mouseout();
    //        _this.tooltipMobile.classed('vzb-hidden', true);
          })
          .on("mousedown", function(){
            _this._panZoom.enabled = true;
          })
          .on("mouseleave", function(){
            clearTimeout(_this.timeoutMouseEnter);
            _this.timeoutMouseLeave = setTimeout(function(){_this._panZoom.enabled = false;}, 800)
          })
          .on("mouseenter", function(){
            clearTimeout(_this.timeoutMouseLeave);
            _this.timeoutMouseEnter = setTimeout(function(){_this._panZoom.enabled = true;}, 2000)
          });

        this.KEY = this.model.entities.getDimension();
        this.TIMEDIM = this.model.time.getDimension();

        this.updateUIStrings();

        this.wScale = d3.scale.linear()
          .domain(this.parent.datawarning_content.doubtDomain)
          .range(this.parent.datawarning_content.doubtRange);

        this.updateIndicators();
        this.updateEntities();
        this.updateTime();
        this.updateSize();
        this.updateMarkerSizeLimits();
        this.selectDataPoints();
        this.updateBubbleOpacity();
        this._updateDoubtOpacity();
        this._trails.create();
        this._panZoom.reset(); // includes redraw data points and trail resize
        this._trails.run(["recolor", "opacityHandler", "findVisible", "reveal"]);
        if(this.model.time.adaptMinMaxZoom) this._panZoom.expandCanvas();
      },

      ready: function() {

        this.updateUIStrings();

        this.updateEntities();
        this.updateBubbleOpacity();
        this.updateIndicators();
        this.updateSize();
        this.cached = {};
        this.updateMarkerSizeLimits();
        this._trails.create();
        this._trails.run("findVisible");
        this._panZoom.reset(); // includes redraw data points and trail resize
        this._trails.run(["recolor", "opacityHandler", "reveal"]);

        this._panZoom.zoomToMaxMin(
           this.model.marker.axis_x.zoomedMin,
           this.model.marker.axis_x.zoomedMax,
           this.model.marker.axis_y.zoomedMin,
           this.model.marker.axis_y.zoomedMax
        )

      },

      /*
       * UPDATE INDICATORS
       */
      updateIndicators: function() {
        var _this = this;

        //scales
        this.yScale = this.model.marker.axis_y.getScale();
        this.xScale = this.model.marker.axis_x.getScale();
        this.sScale = this.model.marker.size.getScale();
        this.cScale = this.model.marker.color.getScale();

        //            this.collisionResolver.scale(this.yScale);


        this.yAxis.tickFormat(_this.model.marker.axis_y.tickFormatter);
        this.xAxis.tickFormat(_this.model.marker.axis_x.tickFormatter);

        this.xyMaxMinMean = {
          x: this.model.marker.axis_x.gerLimitsPerFrame(),
          y: this.model.marker.axis_y.gerLimitsPerFrame(),
          s: this.model.marker.size.gerLimitsPerFrame()
        };
      },


      updateUIStrings: function() {
        var _this = this;

        this.translator = this.model.language.getTFunction();

        this.strings = {
          title: {
            Y: this.translator("indicator/" + this.model.marker.axis_y.which),
            X: this.translator("indicator/" + this.model.marker.axis_x.which),
            S: this.translator("indicator/" + this.model.marker.size.which),
            C: this.translator("indicator/" + this.model.marker.color.which)
          },
          unit: {
            Y: this.translator("unit/" + this.model.marker.axis_y.which),
            X: this.translator("unit/" + this.model.marker.axis_x.which),
            S: this.translator("unit/" + this.model.marker.size.which),
            C: this.translator("unit/" + this.model.marker.color.which)
          }
        }
        if(!!this.strings.unit.Y) this.strings.unit.Y = ", " + this.strings.unit.Y;
        if(!!this.strings.unit.X) this.strings.unit.X = ", " + this.strings.unit.X;
        if(!!this.strings.unit.S) this.strings.unit.S = ", " + this.strings.unit.S;
        if(!!this.strings.unit.C) this.strings.unit.C = ", " + this.strings.unit.C;

        var yTitle = this.yTitleEl.selectAll("text").data([0]);
        yTitle.enter().append("text");
        yTitle
          //.attr("y", "-6px")
          .on("click", function() {
            _this.parent
              .findChildByName("gapminder-treemenu")
              .markerID("axis_y")
              .alignX("left")
              .alignY("top")
              .updateView()
              .toggle();
          });

        var xTitle = this.xTitleEl.selectAll("text").data([0]);
        xTitle.enter().append("text");
        xTitle
          .on("click", function() {
            _this.parent
              .findChildByName("gapminder-treemenu")
              .markerID("axis_x")
              .alignX("left")
              .alignY("bottom")
              .updateView()
              .toggle();
          });

        var sTitle = this.sTitleEl.selectAll("text").data([0]);
        sTitle.enter().append("text");
        sTitle
          .attr("text-anchor", "end");

        setIcon(this.dataWarningEl, iconWarn).select("svg").attr("width", "0px").attr("height", "0px");
        this.dataWarningEl.append("text")
          .attr("text-anchor", "end")
          .text(this.translator("hints/dataWarning"));

        setIcon(this.yInfoEl, iconQuestion)
            .select("svg").attr("width", "0px").attr("height", "0px");

        setIcon(this.xInfoEl, iconQuestion)
          .select("svg").attr("width", "0px").attr("height", "0px");


        //TODO: move away from UI strings, maybe to ready or ready once
        this.yInfoEl.on("click", function() {
          window.open(_this.model.marker.axis_y.getMetadata().sourceLink, '_blank').focus();
        })
        this.xInfoEl.on("click", function() {
          window.open(_this.model.marker.axis_x.getMetadata().sourceLink, '_blank').focus();
        })
        this.dataWarningEl
          .on("click", function() {
            _this.parent.findChildByName("gapminder-datawarning").toggle();
          })
          .on("mouseover", function() {
            _this._updateDoubtOpacity(1);
          })
          .on("mouseout", function() {
            _this._updateDoubtOpacity();
          })
      },

      _updateDoubtOpacity: function(opacity) {
        if(opacity == null) opacity = this.wScale(+this.model.time.timeFormat(this.time));
        if(this.someSelected) opacity = 1;
        this.dataWarningEl.style("opacity", opacity);
      },

      /*
       * UPDATE ENTITIES:
       * Ideally should only update when show parameters change or data changes
       */
      updateEntities: function() {
        var _this = this;
        var KEY = this.KEY;
        var TIMEDIM = this.TIMEDIM;

        var getKeys = function(prefix) {
          prefix = prefix || "";
          return this.model.marker.getKeys()
            .map(function(d) {
              var pointer = {};
              pointer[KEY] = d[KEY];
              pointer[TIMEDIM] = endTime;
              pointer.sortValue = values.size[d[KEY]]||0;
              pointer[KEY] = prefix + d[KEY];
              return pointer;
            })
            .sort(function(a, b) {
              return b.sortValue - a.sortValue;
            })
        };

        // get array of GEOs, sorted by the size hook
        // that makes larger bubbles go behind the smaller ones
        var endTime = this.model.time.end;
        var values = this.model.marker.getFrame(endTime);
        this.model.entities.setVisible(getKeys.call(this));

        this.entityBubbles = this.bubbleContainer.selectAll('.vzb-bc-entity')
          .data(this.model.entities.getVisible(), function(d) {return d[KEY]})
          .order();

        //exit selection
        this.entityBubbles.exit().remove();

        //enter selection -- init circles
        this.entityBubbles.enter().append("circle")
          .attr("class", function(d) {
            return "vzb-bc-entity " + "bubble-" + d[KEY];
          })
          .on("mouseover", function(d, i) {
            if(isTouchDevice()) return;
            _this._bubblesInteract().mouseover(d, i);
          })
          .on("mouseout", function(d, i) {
            if(isTouchDevice()) return;

            _this._bubblesInteract().mouseout(d, i);
          })
          .on("click", function(d, i) {
            if(isTouchDevice()) return;

            _this._bubblesInteract().click(d, i);
          })
          .onTap(function(d, i) {
            d3.event.stopPropagation();
            _this._bubblesInteract().click(d, i);
          })
          .onLongTap(function(d, i) {});


        //TODO: no need to create trail group for all entities
        //TODO: instead of :append an :insert should be used to keep order, thus only few trail groups can be inserted
        this.entityTrails = this.bubbleContainer.selectAll(".vzb-bc-entity")
          .data(getKeys.call(this, "trail-"), function(d) {
            return d[KEY];
          });

        this.entityTrails.enter().insert("g", function(d) {
          return document.querySelector(".vzb-bc-bubbles ." + d[KEY].replace("trail-", "bubble-"));
        }).attr("class", function(d) {
          return "vzb-bc-entity " + d[KEY]
        });

      },

      _bubblesInteract: function() {
        var _this = this;
        var KEY = this.KEY;
        var TIMEDIM = this.TIMEDIM;

        return {
          mouseover: function(d, i) {
            _this.model.entities.highlightEntity(d);

            //show the little cross on the selected label
            _this.entityLabels
                .filter(function(f){return f[KEY] == d[KEY]})
                .select(".vzb-bc-label-x")
                .classed("vzb-transparent", false);
          },

          mouseout: function(d, i) {
            _this.model.entities.clearHighlighted();

            //hide the little cross on the selected label
            _this.entityLabels
                .filter(function(f){return f[KEY] == d[KEY]})
                .select(".vzb-bc-label-x")
                .classed("vzb-transparent", true);
          },

          click: function(d, i) {
            if(_this.draggingNow) return;
            var isSelected = _this.model.entities.isSelected(d);
            _this.model.entities.selectEntity(d);
            //return to highlighted state
            if(!isTouchDevice() && isSelected) {
                _this.model.entities.highlightEntity(d);
                _this.highlightDataPoints();
            }
          }
        }
      },




      /*
       * UPDATE TIME:
       * Ideally should only update when time or data changes
       */
      updateTime: function() {
        var _this = this;

        this.time_1 = this.time == null ? this.model.time.value : this.time;
        this.time = this.model.time.value;
        this.duration = this.model.time.playing && (this.time - this.time_1 > 0) ? this.model.time.delayAnimations : 0;
        this.year.setText(this.model.time.timeFormat(this.time));
        //this.yearEl.text(this.model.time.timeFormat(this.time));
      },

      /*
       * RESIZE:
       * Executed whenever the container is resized
       */
      updateSize: function() {


        var profiles = {
          small: {
            margin: { top: 30, right: 10, left: 40, bottom: 35 },
            padding: 2,
            minRadius: 0.5,
            maxRadius: 30,
            infoElHeight: 16,
            yAxisTitleBottomMargin: 6,
            xAxisTitleBottomMargin: 4
          },
          medium: {
            margin: { top: 40, right: 15, left: 60, bottom: 55 },
            padding: 2,
            minRadius: 1,
            maxRadius: 55,
            infoElHeight: 20,
            yAxisTitleBottomMargin: 6,
            xAxisTitleBottomMargin: 5
          },
          large: {
            margin: { top: 50, right: 20, left: 60, bottom: 60 },
            padding: 2,
            minRadius: 1,
            maxRadius: 70,
            infoElHeight: 22,
            yAxisTitleBottomMargin: 6,
            xAxisTitleBottomMargin: 5
          }
        };

        var presentationProfileChanges = {
          "medium": {
            margin: { top: 80, bottom: 80, left: 100 },
            yAxisTitleBottomMargin: 20,
            xAxisTitleBottomMargin: 20,
            infoElHeight: 26,
          },
          "large": {
            margin: { top: 80, bottom: 100, left: 100 },
            yAxisTitleBottomMargin: 20,
            xAxisTitleBottomMargin: 20,
            infoElHeight: 32,
          }
        }

        var _this = this;

        this.activeProfile = this.getActiveProfile(profiles, presentationProfileChanges);

        var margin = this.activeProfile.margin;
        var infoElHeight = this.activeProfile.infoElHeight;

        //stage
        this.height = parseInt(this.element.style("height"), 10) - margin.top - margin.bottom;
        this.width = parseInt(this.element.style("width"), 10) - margin.left - margin.right;

        //            this.collisionResolver.height(this.height);

        //graph group is shifted according to margins (while svg element is at 100 by 100%)
        this.graph
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        this.year.resize(this.width, this.height, Math.min(this.width/2.5, Math.max(this.height / 4, this.width / 4)));
        this.eventArea
          .attr("width", this.width)
          .attr("height", Math.max(0, this.height));

        //update scales to the new range
        if(this.model.marker.axis_y.scaleType !== "ordinal") {
          this.yScale.range(this._rangeBump([this.height, 0]));
        } else {
          this.yScale.rangePoints([this.height, 0], _this.activeProfile.padding).range();
        }
        if(this.model.marker.axis_x.scaleType !== "ordinal") {
          this.xScale.range(this._rangeBump([0, this.width]));
        } else {
          this.xScale.rangePoints([0, this.width], _this.activeProfile.padding).range();
        }

        //apply scales to axes and redraw
        this.yAxis.scale(this.yScale)
          .orient("left")
          .tickSize(6, 0)
          .tickSizeMinor(3, 0)
          .labelerOptions({
            scaleType: this.model.marker.axis_y.scaleType,
            timeFormat: this.model.time.timeFormat,
            toolMargin: margin,
            limitMaxTickNumber: 6,
            bump: this.activeProfile.maxRadius
          });

        this.xAxis.scale(this.xScale)
          .orient("bottom")
          .tickSize(6, 0)
          .tickSizeMinor(3, 0)
          .labelerOptions({
            scaleType: this.model.marker.axis_x.scaleType,
            timeFormat: this.model.time.timeFormat,
            toolMargin: margin,
            bump: this.activeProfile.maxRadius
          });


        this.bubbleContainerCrop
          .attr("width", this.width)
          .attr("height", Math.max(0, this.height));

        this.xAxisElContainer
          .attr("width", this.width + 1)
          .attr("height", this.activeProfile.margin.bottom)
          .attr("y", this.height - 1)
          .attr("x", -1);
        this.xAxisEl
          .attr("transform", "translate(1,1)");

        this.yAxisElContainer
          .attr("width", this.activeProfile.margin.left)
          .attr("height", Math.max(0, this.height))
          .attr("x", -this.activeProfile.margin.left);
        this.yAxisEl
          .attr("transform", "translate(" + (this.activeProfile.margin.left - 1) + "," + 0 + ")");

        this.yAxisEl.call(this.yAxis);
        this.xAxisEl.call(this.xAxis);

        this.projectionX.attr("y1", _this.yScale.range()[0] + this.activeProfile.maxRadius);
        this.projectionY.attr("x2", _this.xScale.range()[0] - this.activeProfile.maxRadius);



        // reset font size to remove jumpy measurement
        var sTitleText = this.sTitleEl.select("text")
          .style("font-size", null)
          .text(this.translator("buttons/size") + ": " + this.strings.title.S + ", " +
            this.translator("buttons/colors") + ": " + this.strings.title.C);

        // reduce font size if the caption doesn't fit
        var sTitleWidth = sTitleText.node().getBBox().width;
        var remainigHeight = this.height - 30;
        var font = parseInt(sTitleText.style("font-size")) * remainigHeight / sTitleWidth;
        sTitleText.style("font-size", sTitleWidth > remainigHeight? font + "px" : null);


        var yaxisWidth = this.yAxisElContainer.select("g").node().getBBox().width;
        this.yTitleEl
          .style("font-size", infoElHeight)
          .attr("transform", "translate(" + (-yaxisWidth) + ", -" + this.activeProfile.yAxisTitleBottomMargin + ")");

        this.xTitleEl
          .style("font-size", infoElHeight)
          .attr("transform", "translate(" + (0) + "," + (this.height + margin.bottom - this.activeProfile.xAxisTitleBottomMargin) + ")");

        this.sTitleEl
          .attr("transform", "translate(" + this.width + "," + 20 + ") rotate(-90)");


        var yTitleText = this.yTitleEl.select("text").text(this.strings.title.Y + this.strings.unit.Y);
        if(yTitleText.node().getBBox().width > this.width) yTitleText.text(this.strings.title.Y);

        var xTitleText = this.xTitleEl.select("text").text(this.strings.title.X + this.strings.unit.X);
        if(xTitleText.node().getBBox().width > this.width - 100) xTitleText.text(this.strings.title.X);

        if(this.yInfoEl.select('svg').node()) {
          var titleBBox = this.yTitleEl.node().getBBox();
          var translate = d3.transform(this.yTitleEl.attr('transform')).translate;

          this.yInfoEl.select('svg')
            .attr("width", infoElHeight)
            .attr("height", infoElHeight)
          this.yInfoEl.attr('transform', 'translate('
            + (titleBBox.x + translate[0] + titleBBox.width + infoElHeight * .4) + ','
            + (translate[1] - infoElHeight * 0.8) + ')');
        }

        if(this.xInfoEl.select('svg').node()) {
          var titleBBox = this.xTitleEl.node().getBBox();
          var translate = d3.transform(this.xTitleEl.attr('transform')).translate;

          this.xInfoEl.select('svg')
            .attr("width", infoElHeight)
            .attr("height", infoElHeight)
          this.xInfoEl.attr('transform', 'translate('
            + (titleBBox.x + translate[0] + titleBBox.width + infoElHeight * .4) + ','
            + (translate[1] - infoElHeight * 0.8) + ')');
        }

        this._resizeDataWarning();
      },

      _resizeDataWarning: function(){
        this.dataWarningEl
          .attr("transform", "translate("
            + (this.width) + ","
            + (this.height + this.activeProfile.margin.bottom - this.activeProfile.xAxisTitleBottomMargin)
            + ")");

        // reset font size to remove jumpy measurement
        var dataWarningText = this.dataWarningEl.select("text").style("font-size", null);

        // reduce font size if the caption doesn't fit
        var dataWarningWidth = dataWarningText.node().getBBox().width + dataWarningText.node().getBBox().height * 3;
        var remainingWidth = this.width - this.xTitleEl.node().getBBox().width - this.activeProfile.infoElHeight;
        var font = parseInt(dataWarningText.style("font-size")) * remainingWidth / dataWarningWidth;
        dataWarningText.style("font-size", dataWarningWidth > remainingWidth? font + "px" : null);

        // position the warning icon
        var warnBB = dataWarningText.node().getBBox();
        this.dataWarningEl.select("svg")
          .attr("width", warnBB.height * 0.75)
          .attr("height", warnBB.height * 0.75)
          .attr("x", -warnBB.width - warnBB.height * 1.2)
          .attr("y", - warnBB.height * 0.65);
      },

      updateMarkerSizeLimits: function() {
        var _this = this;
        var minRadius = this.activeProfile.minRadius;
        var maxRadius = this.activeProfile.maxRadius;

        this.minRadius = Math.max(maxRadius * this.model.marker.size.domainMin, minRadius);
        this.maxRadius = Math.max(maxRadius * this.model.marker.size.domainMax, minRadius);

        if(this.model.marker.size.scaleType !== "ordinal") {
          this.sScale.range([radiusToArea(_this.minRadius), radiusToArea(_this.maxRadius)]);
        } else {
          this.sScale.rangePoints([radiusToArea(_this.minRadius), radiusToArea(_this.maxRadius)], 0).range();
        }

      },

      redrawDataPointsOnlyColors: function() {
        var _this = this;
        var values, valuesNow;
        var KEY = this.KEY;

        valuesNow = this.model.marker.getFrame(this.time);

        if(this.model.time.lockNonSelected && this.someSelected) {
          var tLocked = this.model.time.timeFormat.parse("" + this.model.time.lockNonSelected);
          values = this.model.marker.getFrame(tLocked);
        } else {
          values = valuesNow;
        }

        this.entityBubbles.style("fill", function(d) {

          var cache = _this.cached[d[KEY]];

          var valueC = cache && _this.model.time.lockNonSelected ? valuesNow.color[d[KEY]] : values.color[d[KEY]];

          return valueC?_this.cScale(valueC):"transparent";
        });
      },

      redrawDataPointsOnlySize: function() {
        var _this = this;

        // if (this.someSelected) {
        //   _this.entityBubbles.each(function (d, index) {
        //     _this._updateBubble(d, index, d3.select(this), 0);
        //   });
        // } else {
        //   this.entityBubbles.each(function (d, index) {
        //     var valueS = _this.model.marker.size.getValue(d);
        //     if (valueS == null) return;

        //     d3.select(this).attr("r", utils.areaToRadius(_this.sScale(valueS)));
        //   });
        // }
        var values, valuesNow;
        var KEY = this.KEY;

        valuesNow = this.model.marker.getFrame(this.time);

        if(this.model.time.lockNonSelected && this.someSelected) {
          var tLocked = this.model.time.timeFormat.parse("" + this.model.time.lockNonSelected);
          values = this.model.marker.getFrame(tLocked);
        } else {
          values = valuesNow;
        }

        this.entityBubbles.each(function(d, index) {

          var cache = _this.cached[d[KEY]];

          var valueS = cache && _this.model.time.lockNonSelected ? valuesNow.size[d[KEY]] : values.size[d[KEY]];
          if(valueS == null) return;

          var scaledS = areaToRadius(_this.sScale(valueS));
          d3.select(this).attr("r", scaledS);

          //update lines of labels
          if(cache) {

            var resolvedX = _this.xScale(cache.labelX0) + cache.labelX_ * _this.width;
            var resolvedY = _this.yScale(cache.labelY0) + cache.labelY_ * _this.height;

            var resolvedX0 = _this.xScale(cache.labelX0);
            var resolvedY0 = _this.yScale(cache.labelY0);

            var lineGroup = _this.entityLines.filter(function(f) {
              return f[KEY] == d[KEY];
            });

            var select = find(_this.model.entities.select, function(f) {
              return f[KEY] == d[KEY]
            });

            var trailStartTime = _this.model.time.timeFormat.parse("" + select.trailStartTime);

            if(!_this.model.time.trails || trailStartTime - _this.time == 0) {
              cache.scaledS0 = scaledS;
            }

            _this.entityLabels.filter(function(f) {
              return f[KEY] == d[KEY]
            })
            .each(function(groupData) {
              _this._repositionLabels(d, index, this, resolvedX, resolvedY, resolvedX0, resolvedY0, 0, lineGroup);
            });
          }
        });
      },

      /*
       * REDRAW DATA POINTS:
       * Here plotting happens
       * debouncing to improve performance: events might trigger it more than 1x
       */
      redrawDataPoints: function(duration) {
        var _this = this;

        if(duration == null) duration = _this.duration;

        var TIMEDIM = this.TIMEDIM;
        var KEY = this.KEY;
        var values, valuesLocked;

        //get values for locked and not locked
        if(this.model.time.lockNonSelected && this.someSelected) {
          var tLocked = this.model.time.timeFormat.parse("" + this.model.time.lockNonSelected);
          valuesLocked = this.model.marker.getFrame(tLocked);
        }

        values = this.model.marker.getFrame(this.time);

        this.entityBubbles.each(function(d, index) {
          var view = d3.select(this);
          _this._updateBubble(d, values, valuesLocked, index, view, duration);

        }); // each bubble

        if(_this.ui.labels.autoResolveCollisions) {
          // cancel previously queued simulation if we just ordered a new one
          clearTimeout(_this.collisionTimeout);

          // place label layout simulation into a queue
          _this.collisionTimeout = setTimeout(function() {
            //  _this.entityLabels.call(_this.collisionResolver.data(_this.cached));
          }, _this.model.time.delayAnimations * 1.2)
        }

      },

      //redraw Data Points
      _updateBubble: function(d, values, valuesL, index, view, duration) {

        var _this = this;
        var TIMEDIM = this.TIMEDIM;
        var KEY = this.KEY;
        if(_this.model.time.lockNonSelected && _this.someSelected && !_this.model.entities.isSelected(d)) {
          values = valuesL;
        }

        var valueY = values.axis_y[d[KEY]];
        var valueX = values.axis_x[d[KEY]];
        var valueS = values.size[d[KEY]];
        var valueL = values.label[d[KEY]];
        var valueC = values.color[d[KEY]];

        // check if fetching data succeeded
        //TODO: what if values are ACTUALLY 0 ?
        if(!valueL || !valueY || !valueX || !valueS) {
          // if entity is missing data it should hide
          view.classed("vzb-invisible", true)

        } else {

          // if entity has all the data we update the visuals
          var scaledS = areaToRadius(_this.sScale(valueS));

          view.classed("vzb-invisible", false)
            .style("fill", valueC?_this.cScale(valueC):"transparent");


          if(duration) {
            view.transition().duration(duration).ease("linear")
              .attr("cy", _this.yScale(valueY))
              .attr("cx", _this.xScale(valueX))
              .attr("r", scaledS);
          } else {
            view.interrupt()
              .attr("cy", _this.yScale(valueY))
              .attr("cx", _this.xScale(valueX))
              .attr("r", scaledS);
          }

          if(this.model.time.record) _this._export.write({
            type: "circle",
            id: d[KEY],
            time: this.model.time.value.getUTCFullYear(),
            fill: valueC?_this.cScale(valueC):"transparent",
            cx: _this.xScale(valueX),
            cy: _this.yScale(valueY),
            r: scaledS
          });

          _this._updateLabel(d, index, valueX, valueY, scaledS, valueL, duration);

        } // data exists
      },


      _updateLabel: function(d, index, valueX, valueY, scaledS, valueL, duration) {
        var _this = this;
        var KEY = this.KEY;
        if(d[KEY] == _this.druging)
          return;

        if(_this.cached[d[KEY]] == null) _this.cached[d[KEY]] = {};

        var cached = _this.cached[d[KEY]];
        if(duration == null) duration = _this.duration;

        // only for selected entities
        if(_this.model.entities.isSelected(d) && _this.entityLabels != null) {

          var select = find(_this.model.entities.select, function(f) {
            return f[KEY] == d[KEY]
          });
          var trailStartTime = _this.model.time.timeFormat.parse("" + select.trailStartTime);

          if(!_this.model.time.trails || trailStartTime - _this.time > 0 || select.trailStartTime == null) {

            select.trailStartTime = _this.model.time.timeFormat(_this.time);
            //the events in model are not triggered here. to trigger uncomment the next line
            //_this.model.entities.triggerAll("change:select");
            cached.scaledS0 = scaledS;
            cached.labelX0 = valueX;
            cached.labelY0 = valueY;

          }

          var lineGroup = _this.entityLines.filter(function(f) {
            return f[KEY] == d[KEY];
          });
          // reposition label
          _this.entityLabels.filter(function(f) {
              return f[KEY] == d[KEY]
            })
            .each(function(groupData) {

              cached.valueX = valueX;
              cached.valueY = valueY;

              var limitedX, limitedY, limitedX0, limitedY0;
              if(cached.scaledS0 == null || cached.labelX0 == null || cached.labelX0 == null) { //initialize label once
                cached.scaledS0 = scaledS;
                cached.labelX0 = valueX;
                cached.labelY0 = valueY;
              }
              var labelGroup = d3.select(this);

              var text = labelGroup.selectAll(".vzb-bc-label-content")
                .text(valueL + (_this.model.time.trails ? " " + select.trailStartTime : ""));

              var rect = labelGroup.select("rect");

              var contentBBox = text[0][0].getBBox();
              if(!cached.contentBBox || cached.contentBBox.width != contentBBox.width) {
                cached.contentBBox = contentBBox;

                var labelCloseGroup = labelGroup.select(".vzb-bc-label-x")
                  .attr("x", /*contentBBox.height * .0 + */ 4)
                  .attr("y", contentBBox.height * -1);

                labelCloseGroup.select("circle")
                  .attr("cx", /*contentBBox.height * .0 + */ 4)
                  .attr("cy", contentBBox.height * -1)
                  .attr("r", contentBBox.height * .5);

                labelCloseGroup.select("svg")
                  .attr("x", -contentBBox.height * .5 + 4)
                  .attr("y", contentBBox.height * -1.5)
                  .attr("width", contentBBox.height)
                  .attr("height", contentBBox.height)

                rect.attr("width", contentBBox.width + 8)
                  .attr("height", contentBBox.height * 1.2)
                  .attr("x", -contentBBox.width - 4)
                  .attr("y", -contentBBox.height * .85)
                  .attr("rx", contentBBox.height * .2)
                  .attr("ry", contentBBox.height * .2);
              }

              limitedX0 = _this.xScale(cached.labelX0);
              limitedY0 = _this.yScale(cached.labelY0);

              cached.labelX_ = select.labelOffset[0] || (-cached.scaledS0 * .75 - 5) / _this.width;
              cached.labelY_ = select.labelOffset[1] || (-cached.scaledS0 * .75 - 11) / _this.height;

              limitedX = _this.xScale(cached.labelX0) + cached.labelX_ * _this.width;
              if(limitedX - cached.contentBBox.width <= 0) { //check left
                cached.labelX_ = (cached.scaledS0 * .75 + cached.contentBBox.width + 10) / _this.width;
                limitedX = _this.xScale(cached.labelX0) + cached.labelX_ * _this.width;
              } else if(limitedX + 15 > _this.width) { //check right
                cached.labelX_ = (_this.width - 15 - _this.xScale(cached.labelX0)) / _this.width;
                limitedX = _this.xScale(cached.labelX0) + cached.labelX_ * _this.width;
              }
              limitedY = _this.yScale(cached.labelY0) + cached.labelY_ * _this.height;
              if(limitedY - cached.contentBBox.height <= 0) { // check top
                cached.labelY_ = (cached.scaledS0 * .75 + cached.contentBBox.height) / _this.height;
                limitedY = _this.yScale(cached.labelY0) + cached.labelY_ * _this.height;
              } else if(limitedY + 10 > _this.height) { //check bottom
                cached.labelY_ = (_this.height - 10 - _this.yScale(cached.labelY0)) / _this.height;
                limitedY = _this.yScale(cached.labelY0) + cached.labelY_ * _this.height;
              }

              _this._repositionLabels(d, index, this, limitedX, limitedY, limitedX0, limitedY0, duration, lineGroup);

            })
        } else {
          //for non-selected bubbles
          //make sure there is no cached data
          if(_this.cached[d[KEY]] != null) {
            _this.cached[d[KEY]] = void 0;
          }
        }
      },

      _repositionLabels: function(d, i, context, resolvedX, resolvedY, resolvedX0, resolvedY0, duration, lineGroup) {

        var cache = this.cached[d[this.KEY]];

        var labelGroup = d3.select(context);

        var width = parseInt(labelGroup.select("rect").attr("width"));
        var height = parseInt(labelGroup.select("rect").attr("height"));
        var heightDelta = labelGroup.node().getBBox().height - height;

        if(resolvedX - width <= 0) { //check left
          cache.labelX_ = (width - this.xScale(cache.labelX0)) / this.width;
          resolvedX = this.xScale(cache.labelX0) + cache.labelX_ * this.width;
        } else if(resolvedX + 23 > this.width) { //check right
          cache.labelX_ = (this.width - 23 - this.xScale(cache.labelX0)) / this.width;
          resolvedX = this.xScale(cache.labelX0) + cache.labelX_ * this.width;
        }
        if(resolvedY - height * .75 - heightDelta <= 0) { // check top
          cache.labelY_ = (height * .75 + heightDelta - this.yScale(cache.labelY0)) / this.height;
          resolvedY = this.yScale(cache.labelY0) + cache.labelY_ * this.height;
        } else if(resolvedY + 13 > this.height) { //check bottom
          cache.labelY_ = (this.height - 13 - this.yScale(cache.labelY0)) / this.height;
          resolvedY = this.yScale(cache.labelY0) + cache.labelY_ * this.height;
        }

        if(duration) {
          labelGroup
            .transition().duration(duration).ease("linear")
            .attr("transform", "translate(" + resolvedX + "," + resolvedY + ")");
          lineGroup.transition().duration(duration).ease("linear")
            .attr("transform", "translate(" + resolvedX + "," + resolvedY + ")");
        } else {
          labelGroup
              .interrupt()
              .attr("transform", "translate(" + resolvedX + "," + resolvedY + ")");
          lineGroup
              .interrupt()
              .attr("transform", "translate(" + resolvedX + "," + resolvedY + ")");
        }

        var diffX1 = resolvedX0 - resolvedX;
        var diffY1 = resolvedY0 - resolvedY;
        var diffX2 = 0;
        var diffY2 = 0;

        var angle = Math.atan2(diffX1 + width / 2, diffY1 + height / 2) * 180 / Math.PI;
        // middle bottom
        if(Math.abs(angle) <= 45) {
          diffX2 = width / 2;
          diffY2 = 0
        }
        // right middle
        if(angle > 45 && angle < 135) {
          diffX2 = 0;
          diffY2 = height / 4;
        }
        // middle top
        if(angle < -45 && angle > -135) {
          diffX2 = width - 4;
          diffY2 = height / 4;
        }
        // left middle
        if(Math.abs(angle) >= 135) {
          diffX2 = width / 2;
          diffY2 = height / 2
        }

        var longerSideCoeff = Math.abs(diffX1) > Math.abs(diffY1) ? Math.abs(diffX1) / this.width : Math.abs(diffY1) / this.height;
        lineGroup.select("line").style("stroke-dasharray", "0 " + (cache.scaledS0 + 2) + " " + ~~(longerSideCoeff + 2) + "00%");

        lineGroup.selectAll("line")
          .attr("x1", diffX1)
          .attr("y1", diffY1)
          .attr("x2", -diffX2)
          .attr("y2", -diffY2);

      },


      selectDataPoints: function() {
        var _this = this;
        var KEY = this.KEY;

        //hide tooltip
        _this._setTooltip();


        _this.someSelected = (_this.model.entities.select.length > 0);


        this.entityLabels = this.labelsContainer.selectAll('.vzb-bc-entity')
          .data(_this.model.entities.select, function(d) {
            return(d[KEY]);
          });
        this.entityLines = this.linesContainer.selectAll('.vzb-bc-entity')
          .data(_this.model.entities.select, function(d) {
            return(d[KEY]);
          });


        this.entityLabels.exit()
          .each(function(d) {
            _this._trails.run("remove", d);
          })
          .remove();
        this.entityLines.exit()
          .each(function(d) {
            _this._trails.run("remove", d);
          })
          .remove();
        this.entityLines
          .enter().append('g')
          .attr("class", "vzb-bc-entity")
          .each(function(d, index) {
            d3.select(this).append("line").attr("class", "vzb-bc-label-line");
          });

        this.entityLabels
          .enter().append("g")
          .attr("class", "vzb-bc-entity")
          .call(_this.labelDragger)
          .each(function(d, index) {
            var view = d3.select(this);

    // Ola: Clicking bubble label should not zoom to countries boundary #811
    // It's too easy to accidentally zoom
    // This feature will be activated later, by making the label into a "context menu" where users can click Split, or zoom,.. hide others etc....

            view.append("rect");
    //          .on("click", function(d, i) {
    //            //default prevented is needed to distinguish click from drag
    //            if(d3.event.defaultPrevented) return;
    //
    //            var maxmin = _this.cached[d[KEY]].maxMinValues;
    //            var radius = utils.areaToRadius(_this.sScale(maxmin.valueSmax));
    //            _this._panZoom._zoomOnRectangle(_this.element,
    //              _this.xScale(maxmin.valueXmin) - radius,
    //              _this.yScale(maxmin.valueYmin) + radius,
    //              _this.xScale(maxmin.valueXmax) + radius,
    //              _this.yScale(maxmin.valueYmax) - radius,
    //              false, 500);
    //          });

            view.append("text").attr("class", "vzb-bc-label-content vzb-label-shadow");

            view.append("text").attr("class", "vzb-bc-label-content");

            var cross = view.append("g").attr("class", "vzb-bc-label-x vzb-transparent");
            setIcon(cross, iconClose);

            cross.insert("circle", "svg");

            cross.select("svg")
              .attr("class", "vzb-bc-label-x-icon")
              .attr("width", "0px")
              .attr("height", "0px");

            cross.on("click", function() {
              _this.model.entities.clearHighlighted();
              //default prevented is needed to distinguish click from drag
              if(d3.event.defaultPrevented) return;
              _this.model.entities.selectEntity(d);
            });

            _this._trails.create(d);
          })
          .on("mouseover", function(d) {
            if(isTouchDevice()) return;
            _this.model.entities.highlightEntity(d);
            _this.entityLabels.sort(function (a, b) { // select the labels and sort the path's
              if (a.geo != d.geo) return -1;          // a is not the hovered element, send "a" to the back
              else return 1;
            });
            d3.select(this).selectAll(".vzb-bc-label-x")
              .classed("vzb-transparent", false);
          })
          .on("mouseout", function(d) {
            if(isTouchDevice()) return;
            _this.model.entities.clearHighlighted();
            d3.select(this).selectAll(".vzb-bc-label-x")
              .classed("vzb-transparent", true);
          })
          .on("click", function(d) {
            if (!isTouchDevice()) return;
            var cross = d3.select(this).selectAll(".vzb-bc-label-x");
            cross.classed("vzb-transparent", !cross.classed("vzb-transparent"));
          });

      },


      _setTooltip: function(tooltipText, x, y, offset) {
        if(tooltipText) {
          var mouse = d3.mouse(this.graph.node()).map(function(d) {
            return parseInt(d)
          });
          var xPos, yPos, xSign = -1,
            ySign = -1,
            xOffset = 0,
            yOffset = 0;

          if(offset) {
            xOffset = offset * .71; // .71 - sin and cos for 315
            yOffset = offset * .71;
          }
          //position tooltip
          this.tooltip.classed("vzb-hidden", false)
            //.attr("style", "left:" + (mouse[0] + 50) + "px;top:" + (mouse[1] + 50) + "px")
            .selectAll("text")
            .text(tooltipText);

          var contentBBox = this.tooltip.select('text')[0][0].getBBox();
          if(x - xOffset - contentBBox.width < 0) {
            xSign = 1;
            x += contentBBox.width + 5; // corrective to the block Radius and text padding
          } else {
            x -= 5; // corrective to the block Radius and text padding
          }
          if(y - yOffset - contentBBox.height < 0) {
            ySign = 1;
            y += contentBBox.height;
          } else {
            y -= 11; // corrective to the block Radius and text padding
          }
          if(offset) {
            xPos = x + xOffset * xSign;
            yPos = y + yOffset * ySign; // 5 and 11 - corrective to the block Radius and text padding
          } else {
            xPos = x + xOffset * xSign; // .71 - sin and cos for 315
            yPos = y + yOffset * ySign; // 5 and 11 - corrective to the block Radius and text padding
          }
          this.tooltip.attr("transform", "translate(" + (xPos ? xPos : mouse[0]) + "," + (yPos ? yPos : mouse[1]) +
            ")")

          this.tooltip.select('rect').attr("width", contentBBox.width + 8)
            .attr("height", contentBBox.height * 1.2)
            .attr("x", -contentBBox.width - 4)
            .attr("y", -contentBBox.height * .85)
            .attr("rx", contentBBox.height * .2)
            .attr("ry", contentBBox.height * .2);

        } else {
          this.tooltip.classed("vzb-hidden", true);
        }
      },

      /*
       * Shows and hides axis projections
       */
      _axisProjections: function(d) {
        var TIMEDIM = this.TIMEDIM;
        var KEY = this.KEY;

        if(d != null) {

          var values = this.model.marker.getFrame(d[TIMEDIM]);
          var valueY = values.axis_y[d[KEY]];
          var valueX = values.axis_x[d[KEY]];
          var valueS = values.size[d[KEY]];
          var radius = areaToRadius(this.sScale(valueS));

          if(!valueY || !valueX || !valueS) return;

          if(this.ui.whenHovering.showProjectionLineX
            && this.xScale(valueX) > 0 && this.xScale(valueX) < this.width
            && (this.yScale(valueY) + radius) < this.height) {
            this.projectionX
              .style("opacity", 1)
              .attr("y2", this.yScale(valueY) + radius)
              .attr("x1", this.xScale(valueX))
              .attr("x2", this.xScale(valueX));
          }

          if(this.ui.whenHovering.showProjectionLineY
            && this.yScale(valueY) > 0 && this.yScale(valueY) < this.height
            && (this.xScale(valueX) - radius) > 0) {
            this.projectionY
              .style("opacity", 1)
              .attr("y1", this.yScale(valueY))
              .attr("y2", this.yScale(valueY))
              .attr("x1", this.xScale(valueX) - radius);
          }

          if(this.ui.whenHovering.higlightValueX) this.xAxisEl.call(
            this.xAxis.highlightValue(valueX)
          );

          if(this.ui.whenHovering.higlightValueY) this.yAxisEl.call(
            this.yAxis.highlightValue(valueY)
          );

        } else {

          this.projectionX.style("opacity", 0);
          this.projectionY.style("opacity", 0);
          this.xAxisEl.call(this.xAxis.highlightValue("none"));
          this.yAxisEl.call(this.yAxis.highlightValue("none"));

        }

      },

      /*
       * Highlights all hovered bubbles
       */
      highlightDataPoints: function() {
        var _this = this;
        var TIMEDIM = this.TIMEDIM;
        var KEY = this.KEY;

        this.someHighlighted = (this.model.entities.highlight.length > 0);

        this.updateBubbleOpacity();

        if(this.model.entities.highlight.length === 1) {
          var d = clone(this.model.entities.highlight[0]);

          if(_this.model.time.lockNonSelected && _this.someSelected && !_this.model.entities.isSelected(d)) {
            d[TIMEDIM] = _this.model.time.timeFormat.parse("" + _this.model.time.lockNonSelected);
          } else {
            d[TIMEDIM] = _this.model.time.timeFormat.parse("" + d.trailStartTime) || _this.time;
          }

          this._axisProjections(d);

          var values = _this.model.marker.getFrame(d[TIMEDIM]);

          //show tooltip
          var text = "";
          if(_this.model.entities.isSelected(d) && _this.model.time.trails) {
            text = _this.model.time.timeFormat(_this.time);
            var labelData = _this.entityLabels
              .filter(function(f) {
                return f[KEY] == d[KEY]
              })
              .classed("vzb-highlighted", true)
              .datum();
            text = text !== labelData.trailStartTime && _this.time === d[TIMEDIM] ? text : '';
          } else {
            text = _this.model.entities.isSelected(d) ? '': values.label[d[KEY]];
          }
          //set tooltip and show axis projections
          if(text) {
            var x = _this.xScale(values.axis_x[d[KEY]]);
            var y = _this.yScale(values.axis_y[d[KEY]]);
            var s = areaToRadius(_this.sScale(values.size[d[KEY]]));
            _this._setTooltip(text, x, y, s);
          }

          var selectedData = find(this.model.entities.select, function(f) {
            return f[KEY] == d[KEY];
          });
          if(selectedData) {
            var clonedSelectedData = clone(selectedData);
            //change opacity to OPACITY_HIGHLT = 1.0;
            clonedSelectedData.opacity = 1.0;
            this._trails.run(["opacityHandler"], clonedSelectedData);
          }
        } else {
          this._axisProjections();
          this._trails.run(["opacityHandler"]);
          //hide tooltip
          _this._setTooltip();
          _this.entityLabels.classed("vzb-highlighted", false);
        }
      },

      updateBubbleOpacity: function(duration) {
        var _this = this;
        //if(!duration)duration = 0;

        var OPACITY_HIGHLT = 1.0;
        var OPACITY_HIGHLT_DIM = .3;
        var OPACITY_SELECT = this.model.entities.opacityRegular;
        var OPACITY_REGULAR = this.model.entities.opacityRegular;
        var OPACITY_SELECT_DIM = this.model.entities.opacitySelectDim;

        this.entityBubbles
          //.transition().duration(duration)
          .style("opacity", function(d) {

            if(_this.someHighlighted) {
              //highlight or non-highlight
              if(_this.model.entities.isHighlighted(d)) return OPACITY_HIGHLT;
            }

            if(_this.someSelected) {
              //selected or non-selected
              return _this.model.entities.isSelected(d) ? OPACITY_SELECT : OPACITY_SELECT_DIM;
            }

            if(_this.someHighlighted) return OPACITY_HIGHLT_DIM;

            return OPACITY_REGULAR;
          });


        var someSelectedAndOpacityZero = _this.someSelected && _this.model.entities.opacitySelectDim < .01;

        // when pointer events need update...
        if(someSelectedAndOpacityZero != this.someSelectedAndOpacityZero_1) {
          this.entityBubbles.style("pointer-events", function(d) {
            return(!someSelectedAndOpacityZero || _this.model.entities.isSelected(d)) ?
              "visible" : "none";
          });
        }

        this.someSelectedAndOpacityZero_1 = _this.someSelected && _this.model.entities.opacitySelectDim < .01;
      }

    });

    var BubbleChart = Tool.extend('BubbleChart', {

      /**
       * Initializes the tool (Bubble Chart Tool).
       * Executed once before any template is rendered.
       * @param {Object} placeholder Placeholder element for the tool
       * @param {Object} external_model Model as given by the external page
       */
      init: function(placeholder, external_model) {

        this.name = "bubblechart";

        //specifying components
        this.components = [{
          component: BubbleChartComp,
          placeholder: '.vzb-tool-viz',
          model: ["state.time", "state.entities", "state.marker", "language", "ui"] //pass models to component
        }, {
          component: TimeSlider,
          placeholder: '.vzb-tool-timeslider',
          model: ["state.time"]
        }, {
          component: Dialogs,
          placeholder: '.vzb-tool-dialogs',
          model: ['state', 'ui', 'language']
        }, {
          component: ButtonList,
          placeholder: '.vzb-tool-buttonlist',
          model: ['state', 'ui', 'language']
        }, {
          component: TreeMenu,
          placeholder: '.vzb-tool-treemenu',
          model: ['state.marker', 'language']
        }, {
          component: DataWarning,
          placeholder: '.vzb-tool-datawarning',
          model: ['language']
        }];

        this._super(placeholder, external_model);

      },

      /**
       * Determines the default model of this tool
       */
      default_model: {
        state: {
          time: {
            round: "ceil",
            trails: true,
            lockNonSelected: 0,
            adaptMinMaxZoom: false
          },
          entities: {
            dim: "geo",
            show: {
              _defs_: {
                "geo": ["*"],
                "geo.cat": ["country"]
              }
            },
            opacitySelectDim: .3,
            opacityRegular: 1,
          },
          marker: {
            space: ["entities", "time"],
            type: "geometry",
            label: {
              use: "property",
              which: "geo.name"
            },
            axis_y: {
              use: "indicator",
              which: "lex"
            },
            axis_x: {
              use: "indicator",
              which: "gdp_pc"
            },
            color: {
              use: "property",
              which: "geo.region"
            },
            size: {
              use: "indicator",
              which: "pop"
            }
          }
        },
        data: {
          //reader: "waffle",
          reader: "csv",
          path: "data/waffles/basic-indicators.csv"
        },
        ui: {
          presentation: true 
        }
      }
    });

    /*!
     * VIZABI POP BY AGE Component
     */


    //POP BY AGE CHART COMPONENT
    var PopByAge$1 = Component.extend({

      /**
       * Initializes the component (Bar Chart).
       * Executed once before any template is rendered.
       * @param {Object} config The config passed to the component
       * @param {Object} context The component's parent
       */
      init: function(config, context) {
        this.name = 'popbyage';
        this.template = 'popbyage.html';

        //define expected models for this component
        this.model_expects = [{
          name: "time",
          type: "time"
        }, {
          name: "entities",
          type: "entities"
        }, {
          name: "age",
          type: "entities"
        }, {
          name: "marker",
          type: "model"
        }, {
          name: "language",
          type: "language"
        }];

        var _this = this;
        this.model_binds = {
          "change:time.value": function(evt) {
            _this._updateEntities();
          },
          "change:entities.show": function(evt) {
            console.log('Trying to change show');
          },
          "change:age.select": function(evt) {
            _this._selectBars();
          },
          "change:marker.color.palette": function (evt) {
            if (!_this._readyOnce) return;
            _this._updateEntities();
          },
          "change:marker.color.scaleType":function (evt) {
            if (!_this._readyOnce) return;
            _this._updateEntities();
          }
        };

        //contructor is the same as any component
        this._super(config, context);

        this.xScale = null;
        this.yScale = null;
        this.cScale = null;

        this.xAxis = axisSmart();
        this.yAxis = axisSmart();
      },

      /**
       * DOM is ready
       */
      readyOnce: function() {

        this.el = (this.el) ? this.el : d3.select(this.element);
        this.element = this.el;

        this.graph = this.element.select('.vzb-bc-graph');
        this.yAxisEl = this.graph.select('.vzb-bc-axis-y');
        this.xAxisEl = this.graph.select('.vzb-bc-axis-x');
        this.yTitleEl = this.graph.select('.vzb-bc-axis-y-title');
        this.bars = this.graph.select('.vzb-bc-bars');
        this.labels = this.graph.select('.vzb-bc-labels');

        this.title = this.element.select('.vzb-bc-title');
        this.year = this.element.select('.vzb-bc-year');

        //only allow selecting one at a time
        this.model.age.selectMultiple(true);

        var _this = this;
        this.on("resize", function() {
          _this._updateEntities();
        });
      },

      /*
       * Both model and DOM are ready
       */
      ready: function() {

        this.AGEDIM = this.model.age.getDimension();
        this.TIMEDIM = this.model.time.getDimension();

        this.updateUIStrings();
        this._updateIndicators();
        this.resize();
        this._updateEntities();
        this._updateEntities();
      },

      updateUIStrings: function() {
        this.translator = this.model.language.getTFunction();

        var titleStringY = this.translator("indicator/" + this.model.marker.axis_y.which);

        var yTitle = this.yTitleEl.selectAll("text").data([0]);
        yTitle.enter().append("text");
        yTitle
          .attr("y", "-6px")
          .attr("x", "-9px")
          .attr("dx", "-0.72em")
          .text(titleStringY);
      },

      /**
       * Changes labels for indicators
       */
      _updateIndicators: function() {
        var _this = this;
        this.duration = this.model.time.delayAnimations;
        this.yScale = this.model.marker.axis_y.getScale();
        this.xScale = this.model.marker.axis_x.getScale(false);
        this.yAxis.tickFormat(_this.model.marker.axis_y.tickFormatter);
        this.xAxis.tickFormat(_this.model.marker.axis_x.tickFormatter);
      },

      /**
       * Updates entities
       */
      _updateEntities: function() {

        var _this = this;
        var time = this.model.time;
        var ageDim = this.AGEDIM;
        var timeDim = this.TIMEDIM;
        var duration = (time.playing) ? time.delayAnimations : 0;

        var group_by = this.model.age.grouping || 1;
        //var group_offset = this.model.marker.group_offset ? Math.abs(this.model.marker.group_offset % group_by) : 0;

        var filter = {};
        filter[timeDim] = time.value;
        var markers = this.model.marker.getKeys(filter);
        var values$$ = this.model.marker.getValues(filter, [ageDim]);
        var domain = this.yScale.domain();

        this.cScale = this.model.marker.color.getScale();
        this.model.age.setVisible(markers);

        this.entityBars = this.bars.selectAll('.vzb-bc-bar')
          .data(markers);

        this.entityLabels = this.labels.selectAll('.vzb-bc-label')
          .data(markers);

        //exit selection
        this.entityBars.exit().remove();
        this.entityLabels.exit().remove();

        var highlight = this._highlightBar.bind(this);
        var unhighlight = this._unhighlightBars.bind(this)

        //enter selection -- init bars
        this.entityBars.enter().append("g")
          .attr("class", "vzb-bc-bar")
          .attr("id", function(d) {
            return "vzb-bc-bar-" + d[ageDim];
          })
          .on("mouseover", highlight)
          .on("mouseout", unhighlight)
          .on("click", function(d, i) {
            if(isTouchDevice()) return;
            _this.model.age.selectEntity(d);
          })
          .onTap(function(d) {
            d3.event.stopPropagation();
            _this.model.age.selectEntity(d);
          })    
          .append('rect');

        this.entityLabels.enter().append("g")
          .attr("class", "vzb-bc-label")
          .attr("id", function(d) {
            return "vzb-bc-label-" + d[ageDim];
          })
          .append('text')
          .attr("class", "vzb-bc-age");

        var one_bar_height = this.height / (domain[1] - domain[0]);
        var bar_height = one_bar_height * group_by; // height per bar is total domain height divided by the number of possible markers in the domain
        var first_bar_y_offset = this.height - bar_height;

        this.bars.selectAll('.vzb-bc-bar > rect')
          .attr("fill", function(d) {
            return _this._temporaryBarsColorAdapter(values$$, d, ageDim);
            //    return _this.cScale(values.color[d[ageDim]]);
          })
          .attr("shape-rendering", "crispEdges") // this makes sure there are no gaps between the bars, but also disables anti-aliasing
          .attr("x", 0)
          .transition().duration(duration).ease("linear")
          .attr("y", function(d, i) {
            return first_bar_y_offset - (d[ageDim] - domain[0]) * one_bar_height;
          })
          .attr("height", bar_height)
          .attr("width", function(d) {
            return _this.xScale(values$$.axis_x[d[ageDim]]);
          });

        this.labels.selectAll('.vzb-bc-label > .vzb-bc-age')
          .text(function(d, i) {
            var formatter = _this.model.marker.axis_x.tickFormatter;
            var yearOldsIn = _this.translator("popbyage/yearOldsIn");

            var age = parseInt(d[ageDim], 10);

            if(group_by > 1) {
              age = age + "-to-" + (age + group_by - 1);
            }

            return age + yearOldsIn + " " + _this.model.time.timeFormat(time.value) + ": " + formatter(values$$.axis_x[d[ageDim]]);
          })
          .attr("x", 7)
          .attr("y", function(d, i) {
            return first_bar_y_offset - (d[ageDim] - domain[0]) * one_bar_height - 10;
          })
          .style("fill", function(d) {
            var color = _this.cScale(values$$.color[d[ageDim]]);
            return d3.rgb(color).darker(2);
          });

        var label = values(values$$.label_name).reverse()[0]; //get last name

        //TODO: remove hack
        label = label === "usa" ? "United States" : "Sweden";

        this.title.text(label);

        this.year.text(this.model.time.timeFormat(this.model.time.value));

        //update x axis again
        //TODO: remove this when grouping is done at data level
        //var x_domain = this.xScale.domain();
        //var x_domain_max = Math.max.apply(null, utils.values(values.axis_x));
        //if(x_domain_max > this.xScale.domain()[1]) this.xScale = this.xScale.domain([x_domain[0], x_domain_max]);

        // should not be here
        var limits = this.model.marker.axis_x.getLimits(this.model.marker.axis_x.which);
        if (group_by == 1) {
          this.xScale = this.xScale.domain([limits.min, limits.max]);
        } else {
          var values$$ = values(values$$.axis_x);
          values$$.push(limits.max);
          this.xScale = this.xScale.domain([limits.min, Math.max.apply(Math, values$$)]);
        }
        this.resize();

      },

      _temporaryBarsColorAdapter: function(values$$, d, ageDim) {
        return this.cScale(values$$.color[d[ageDim]]);
      },

      /**
       * Highlight and unhighlight labels
       */
      _unhighlightBars: function() {
        if(isTouchDevice()) return;
          
        this.bars.classed('vzb-dimmed', false);
        this.bars.selectAll('.vzb-bc-bar.vzb-hovered').classed('vzb-hovered', false);
        this.labels.selectAll('.vzb-hovered').classed('vzb-hovered', false);
      },

      _highlightBar: function(d) {
        if(isTouchDevice()) return;
          
        this.bars.classed('vzb-dimmed', true);
        var curr = this.bars.select("#vzb-bc-bar-" + d[this.AGEDIM]);
        curr.classed('vzb-hovered', true);
        var label = this.labels.select("#vzb-bc-label-" + d[this.AGEDIM]);
        label.classed('vzb-hovered', true);
      },

      /**
       * Select Entities
       */
      _selectBars: function() {
        var _this = this;
        var AGEDIM = this.AGEDIM;
        var selected = this.model.age.select;

        this._unselectBars();

        if(selected.length) {
          this.bars.classed('vzb-dimmed-selected', true);
          forEach(selected, function(s) {
            _this.bars.select("#vzb-bc-bar-" + s[AGEDIM]).classed('vzb-selected', true);
            _this.labels.select("#vzb-bc-label-" + s[AGEDIM]).classed('vzb-selected', true);
          });
        }
      },

      _unselectBars: function() {
        this.bars.classed('vzb-dimmed-selected', false);
        this.bars.selectAll('.vzb-bc-bar.vzb-selected').classed('vzb-selected', false);
        this.labels.selectAll('.vzb-selected').classed('vzb-selected', false);
      },

      /**
       * Executes everytime the container or vizabi is resized
       * Ideally,it contains only operations related to size
       */
      resize: function() {

        var _this = this;

        this.profiles = {
          "small": {
            margin: {
              top: 70,
              right: 20,
              left: 40,
              bottom: 40
            },
            minRadius: 2,
            maxRadius: 40
          },
          "medium": {
            margin: {
              top: 80,
              right: 60,
              left: 60,
              bottom: 40
            },
            minRadius: 3,
            maxRadius: 60
          },
          "large": {
            margin: {
              top: 100,
              right: 60,
              left: 60,
              bottom: 40
            },
            minRadius: 4,
            maxRadius: 80
          }
        };

        this.activeProfile = this.profiles[this.getLayoutProfile()];
        var margin = this.activeProfile.margin;

        //stage
        this.height = parseInt(this.element.style("height"), 10) - margin.top - margin.bottom;
        this.width = parseInt(this.element.style("width"), 10) - margin.left - margin.right;

        this.graph
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        //update scales to the new range
        if(this.model.marker.axis_y.scaleType !== "ordinal") {
          this.yScale.range([this.height, 0]);
        } else {
          this.yScale.rangePoints([this.height, 0]).range();
        }
        if(this.model.marker.axis_x.scaleType !== "ordinal") {
          this.xScale.range([0, this.width]);
        } else {
          this.xScale.rangePoints([0, this.width]).range();
        }

        //apply scales to axes and redraw
        this.yAxis.scale(this.yScale)
          .orient("left")
          .tickSize(6, 6)
          .tickSizeMinor(3, 0)
          .labelerOptions({
            scaleType: this.model.marker.axis_y.scaleType,
            toolMargin: margin,
            limitMaxTickNumber: 6
          });

        this.xAxis.scale(this.xScale)
          .orient("bottom")
          .tickSize(6, 0)
          .tickSizeMinor(3, 0)
          .labelerOptions({
            scaleType: this.model.marker.axis_x.scaleType,
            toolMargin: margin,
            limitMaxTickNumber: 6
          });
        this.xAxisEl.attr("transform", "translate(0," + this.height + ")")
          .call(this.xAxis);

        this.yAxisEl.call(this.yAxis);
        //this.xAxisEl.call(this.xAxis);

        this.title.attr('x', margin.right).attr('y', margin.top / 2);

        this.year.attr('x', this.width + margin.left).attr('y', margin.top / 2);

      }
    });

    //BAR CHART TOOL
    var PopByAge = Tool.extend('PopByAge', {

      /**
       * Initializes the tool (Bar Chart Tool).
       * Executed once before any template is rendered.
       * @param {Object} placeholder Placeholder element for the tool
       * @param {Object} external_model Model as given by the external page
       */
      init: function(placeholder, external_model) {

        this.name = "popbyage";

        //specifying components
        this.components = [{
          component: PopByAge$1,
          placeholder: '.vzb-tool-viz',
          model: ["state.time", "state.entities", "state.entities_age", "state.marker", "language"] //pass models to component
        }, {
          component: TimeSlider,
          placeholder: '.vzb-tool-timeslider',
          model: ["state.time"]
        }, {
          component: Dialogs,
          placeholder: '.vzb-tool-dialogs',
          model: ['state', 'ui', 'language']
        }, {
          component: ButtonList,
          placeholder: '.vzb-tool-buttonlist',
          model: ['state', 'ui', 'language']
        }, {
          component: TreeMenu,
          placeholder: '.vzb-tool-treemenu',
          model: ['state.marker', 'language']
        }];

        //constructor is the same as any tool
        this._super(placeholder, external_model);
      },

      /**
       * Validating the tool model
       * @param model the current tool model to be validated
       */
      validate: function(model) {

        model = this.model || model;

        var time = model.state.time;
        var marker = model.state.marker.label;

        //don't validate anything if data hasn't been loaded
        if(!marker.getKeys() || marker.getKeys().length < 1) {
          return;
        }

        var dateMin = marker.getLimits(time.getDimension()).min;
        var dateMax = marker.getLimits(time.getDimension()).max;

        if(time.start < dateMin) {
          time.start = dateMin;
        }
        if(time.end > dateMax) {
          time.end = dateMax;
        }
      }
    });

    var FILE_CACHED$3 = {}; //caches files from this reader
    var FILE_REQUESTED$3 = {}; //caches files from this reader
    // temporal hack for https problem

    var WSReader = Reader.extend({

      /**
       * Initializes the reader.
       * @param {Object} reader_info Information about the reader
       */
      init: function (reader_info) {
        this._name = 'waffle';
        this._data = [];
        this._basepath = reader_info.path;
        this._parsers = reader_info.parsers;
        if (!this._basepath) {
          error$1("Missing base path for graph reader");
        }
      },

      /**
       * Reads from source
       * @param {Object} query to be performed
       * @param {String} language language
       * @returns a promise that will be resolved when data is read
       */
      read: function (query, language) {
        var _this = this;
        var p = new Promise();
        var path = this._basepath;

        path += '?' + _this._encodeQuery(query);

        _this._data = [];

        (function (query, p) {
          //if cached, retrieve and parse
          if (FILE_CACHED$3.hasOwnProperty(path)) {
            parse(FILE_CACHED$3[path]);
            return p;
          }
          //if requested by another hook, wait for the response
          if (FILE_REQUESTED$3.hasOwnProperty(path)) {
            FILE_REQUESTED$3[path].then(function () {
              parse(FILE_CACHED$3[path]);
            });
            return p;
          }
          //if not, request and parse
          FILE_REQUESTED$3[path] = new Promise();
          get(path, [], onSuccess, console.error.bind(console), true);
          function onSuccess(resp) {
            if (!resp) {
              error$1("Empty json: " + path, error);
              return;
            }

            //format data
            resp = mapRows(uzip(resp.data || resp), _this._parsers);

            //cache and resolve
            FILE_CACHED$3[path] = resp;
            FILE_REQUESTED$3[path].resolve();
            FILE_REQUESTED$3[path] = void 0;

            parse(resp);
          }

          return p;

          function uzip(table) {
            var rows = table.rows;
            var headers = table.headers;
            var result = new Array(rows.length);
            // unwrap compact data into json collection
            for (var i = 0; i < rows.length; i++) {
              result[i] = {};
              for (var j = 0; j < headers.length; j++) {
                result[i][headers[j]] = (rows[i][j] || '').toString();
                if (headers[j] === 'geo.cat') {
                  result[i][headers[j]] = [result[i][headers[j]]];
                }
              }
            }
            return result;
          }

          function parse(res) {

            var data = res;

            // sorting
            // one column, one direction (ascending) for now
            if(query.orderBy && data[0]) {
              if (data[0][query.orderBy]) {
                data.sort(function(a, b) {
                  return a[query.orderBy] - b[query.orderBy];
                });
              } else {
                p.reject("Cannot sort by " + query.orderBy + ". Column does not exist in result.");
              }
            }

            _this._data = data;
            p.resolve();
          }

        })(query, p);

        return p;
      },

      /**
       * Encode query parameters into readable string
       * @param {Object} query to be performed
       * @returns encoded query params
       * `select=geo,time,population&geo=afr,chn&time=1800,1950:2000,2015&geo.cat=country,region`
       */
      _encodeQuery: function (params) {
        var _params = deepExtend({}, params.where);
        _params.select = params.select;
        _params.gapfilling = params.gapfilling;

        // todo: WS doesn't support value `*` for geo parameter
        // remove this condition when geo will be removed from params.where (when you need all geo props)
        if (_params.geo && _params.geo.length === 1 && _params.geo[0] === '*') {
          delete _params.geo;
        }

        var result = [];

        // create `key=value` pairs for url query string
        Object.keys(_params).map(function (key) {
          var value = QueryEncoder.encodeQuery(_params[key]);
          if (value) {
            result.push(key + '=' + value);
          }
        });

        return result.join('&');
      },

      /**
       * Gets the data
       * @returns all data
       */
      getData: function () {
        return this._data;
      }
    });

    var QueryEncoder = (function() {
      return {
        encodeQuery: encodeQuery
      };

      function encodeQuery(param) {
        return mapParams()(param);
      }

      function mapParams(depth) {
        if (!depth) {
          return _map;
        }

        return _mapRange;
      }

      function _map(v, i) {
        // if falsy value
        if (!v) {
          return v;
        }

        // if value is string or number
        if (v.toString() === v || _isNumber(v)) {
          return v;
        }

        // if value is array
        if (Array.isArray(v)) {
          return v.map(mapParams(1)).join();
        }

        if (typeof v === 'object') {
          return _toArray(v).map(mapParams(1)).join();
        }

        return v;
      }

      function _mapRange(v) {
        return encodeURI(v).replace(/,/g, ':')
      }

      function _isNumber(value) {
        return parseInt(value, 10) == value;
      }

      function _toArray(object) {
        return Object.keys(object).map(function(key) {
          if (object[key] === true) {
            return [key];
          }

          return [key, object[key]];
        })
      }
    })();

    //  d3.scale.genericLog
    function genericLog() {
        return function d3_scale_genericLog(logScale) {
            var _this = this;
            var eps = 1;
            var delta = 5;
            var domain = logScale.domain();
            var range = logScale.range();
            var useLinear = false;
            var linScale = d3.scale.linear().domain([0,eps]).range([0,delta]);
            var abs = function(arg) {
                if(arg instanceof Array)
                    return arg.map(function(d) {
                        return Math.abs(d);
                    });
                return Math.abs(arg);
            };
            var oneside = function(arg) {
                var sign = Math.sign(arg[0]);
                for(var i = 0; i < arg.length; i++) {
                    if(Math.sign(arg[i]) != sign)
                        return false;
                }
                return true;
            };

            function scale(x) {
                var ratio = 1;
                var shiftNeg = 0;
                var shiftPos = 0;
                var shiftAll = 0;
                //console.log("DOMAIN log lin", logScale.domain(), linScale.domain());
                //console.log("RANGE log lin", logScale.range(), linScale.range());
                var domainPointingForward = domain[0] < domain[domain.length - 1];
                var rangePointingForward = range[0] < range[range.length - 1];
                if(d3.min(domain) < 0 && d3.max(domain) > 0) {
                    var minAbsDomain = d3.min(abs([
                        domain[0],
                        domain[domain.length - 1]
                    ]));
                    //var maxAbsDomain = d3.max(abs([ domain[0], domain[domain.length-1] ]));
                    //ratio shows how the + and - scale should fit as compared to a simple + or - scale
                    ratio = domainPointingForward != rangePointingForward ? (d3.max(range) + d3.max(range) - logScale(
                        Math.max(
                            eps, minAbsDomain))) / d3.max(range) : (d3.max(range) + logScale(Math.max(eps,
                        minAbsDomain))) / d3.max(
                        range);
                    if(domainPointingForward && !rangePointingForward) {
                        shiftNeg = (d3.max(range) + linScale(0)) / ratio;
                        // if the bottom is heavier we need to shift the entire chart
                        if(abs(domain[0]) > abs(domain[domain.length - 1]))
                            shiftAll -= logScale(Math.max(eps, minAbsDomain)) / ratio;
                    } else if(!domainPointingForward && !rangePointingForward) {
                        shiftAll = logScale(Math.max(eps, minAbsDomain)) / ratio;
                        //if the top is heavier we need to shift the entire chart
                        if(abs(domain[0]) < abs(domain[domain.length - 1]))
                            shiftAll += (d3.max(range) - logScale(Math.max(eps, minAbsDomain))) / ratio;
                    } else if(domainPointingForward && rangePointingForward) {
                        shiftAll = d3.max(range) / ratio;
                        // if the top is heavier we need to shift the entire chart
                        if(abs(domain[0]) < abs(domain[domain.length - 1]))
                            shiftAll -= (d3.max(range) - logScale(Math.max(eps, minAbsDomain))) / ratio;
                    } else if(!domainPointingForward && rangePointingForward) {
                        shiftNeg = (d3.max(range) + linScale(0)) / ratio;
                        //if the top is heavier we need to shift the entire chart
                        if(abs(domain[0]) < abs(domain[domain.length - 1]))
                            shiftAll -= logScale(Math.max(eps, minAbsDomain)) / ratio;
                    }
                } else if(d3.min(domain) < 0 && d3.max(domain) < 0) {
                    shiftNeg = d3.max(range);
                }
                if(x > eps)
                    return logScale(x) / ratio + shiftAll + shiftPos;
                if(x < -eps)
                    return -logScale(-x) / ratio + shiftAll + shiftNeg;
                if(0 <= x && x <= eps)
                    return linScale(x) / ratio + shiftAll + shiftPos;
                if(-eps <= x && x < 0)
                    return -linScale(-x) / ratio + shiftAll + shiftNeg;
            }

            scale.eps = function(arg) {
                if(!arguments.length)
                    return eps;
                eps = arg;
                scale.domain(domain);
                return scale;
            };
            scale.delta = function(arg) {
                if(!arguments.length)
                    return delta;
                delta = arg;
                scale.range(range);
                return scale;
            };
            scale.domain = function(_arg) {
                if(!arguments.length)
                    return domain;
                // this is an internal array, it will be modified. the input _arg should stay intact
                var arg = [];
                if(_arg.length != 2)
                    console.warn(
                        'generic log scale is best for 2 values in domain, but it tries to support other cases too'
                    );
                switch(_arg.length) {
                    // if no values are given, reset input to the default domain (do nothing)
                    case 0:
                        arg = domain;
                        break;
                        // use the given value as a center, get the domain /2 and *2 around it
                    case 1:
                        arg = [
                            _arg[0] / 2,
                            _arg[0] * 2
                        ];
                        break;
                        // two is the standard case. just use these
                    case 2:
                        arg = [
                            _arg[0],
                            _arg[1]
                        ];
                        break;
                        // use the edge values as domain, center as ±epsilon
                    case 3:
                        arg = [
                            _arg[0],
                            _arg[2]
                        ];
                        eps = abs(_arg[1]);
                        break;
                    default:
                        arg = [
                            _arg[0],
                            _arg[_arg.length - 1]
                        ];
                        eps = d3.min(abs(_arg.filter(function(d, i) {
                            return i != 0 && i != _arg.length - 1;
                        })));
                        break;
                }
                //if the domain is just a single value
                if(arg[0] == arg[1]) {
                    arg[0] = arg[0] / 2;
                    arg[1] = arg[1] * 2;
                }
                //if the desired domain is one-seded
                if(oneside(arg) && d3.min(abs(arg)) >= eps) {
                    //if the desired domain is above +epsilon
                    if(arg[0] > 0 && arg[1] > 0) {
                        //then fallback to a regular log scale. nothing special
                        logScale.domain(arg);
                    } else {
                        //otherwise it's all negative, we take absolute and swap the arguments
                        logScale.domain([-arg[1], -arg[0]]);
                    }
                    useLinear = false; //if the desired domain is one-sided and takes part of or falls within 0±epsilon
                } else if(oneside(arg) && d3.min(abs(arg)) < eps) {
                    //if the desired domain is all positive
                    if(arg[0] > 0 && arg[1] > 0) {
                        //the domain is all positive
                        //check the direction of the domain
                        if(arg[0] <= arg[1]) {
                            //if the domain is pointing forward
                            logScale.domain([
                                eps,
                                arg[1]
                            ]);
                            linScale.domain([
                                0,
                                eps
                            ]);
                        } else {
                            //if the domain is pointing backward
                            logScale.domain([
                                arg[0],
                                eps
                            ]);
                            linScale.domain([
                                eps,
                                0
                            ]);
                        }
                    } else {
                        //otherwise it's all negative, we take absolute and swap the arguments
                        //check the direction of the domain
                        if(arg[0] <= arg[1]) {
                            //if the domain is pointing forward
                            logScale.domain([
                                eps, -arg[0]
                            ]);
                            linScale.domain([
                                0,
                                eps
                            ]);
                        } else {
                            //if the domain is pointing backward
                            logScale.domain([-arg[1],
                                eps
                            ]);
                            linScale.domain([
                                eps,
                                0
                            ]);
                        }
                    }
                    useLinear = true; // if the desired domain is two-sided and fully or partially covers 0±epsilon
                } else if(!oneside(arg)) {
                    //check the direction of the domain
                    if(arg[0] <= arg[1]) {
                        //if the domain is pointing forward
                        logScale.domain([
                            eps,
                            d3.max(abs(arg))
                        ]);
                        linScale.domain([
                            0,
                            eps
                        ]);
                    } else {
                        //if the domain is pointing backward
                        logScale.domain([
                            d3.max(abs(arg)),
                            eps
                        ]);
                        linScale.domain([
                            eps,
                            0
                        ]);
                    }
                    useLinear = true;
                }
                //
                //console.log("LOG scale domain:", logScale.domain());
                //if(useLinear)console.log("LIN scale domain:", linScale.domain());
                domain = _arg;
                return scale;
            };
            scale.range = function(arg) {
                if(!arguments.length)
                    return range;
                if(arg.length != 2)
                    console.warn(
                        'generic log scale is best for 2 values in range, but it tries to support other cases too');
                switch(arg.length) {
                    // reset input to the default range
                    case 0:
                        arg = range;
                        break;
                        // use the only value as a center, get the range ±100 around it
                    case 1:
                        arg = [
                            arg[0] - 100,
                            arg[0] + 100
                        ];
                        break;
                        // two is the standard case. do nothing
                    case 2:
                        arg = arg;
                        break;
                        // use the edge values as range, center as delta
                    case 3:
                        delta = arg[1];
                        arg = [
                            arg[0],
                            arg[2]
                        ];
                        break;
                        // use the edge values as range, the minimum of the rest be the delta
                    default:
                        delta = d3.min(arg.filter(function(d, i) {
                            return i != 0 && i != arg.length - 1;
                        }));
                        arg = [
                            arg[0],
                            arg[arg.length - 1]
                        ];
                        break;
                }
                if(!useLinear) {
                    logScale.range(arg);
                } else {
                    if(arg[0] < arg[1]) {
                        //range is pointing forward
                        //check where domain is pointing
                        if(domain[0] < domain[domain.length - 1]) {
                            //domain is pointing forward
                            logScale.range([
                                delta,
                                arg[1]
                            ]);
                            linScale.range([
                                0,
                                delta
                            ]);
                        } else {
                            //domain is pointing backward
                            logScale.range([
                                0,
                                arg[1] - delta
                            ]);
                            linScale.range([
                                arg[1] - delta,
                                arg[1]
                            ]);
                        }
                    } else {
                        //range is pointing backward
                        //check where domain is pointing
                        if(domain[0] < domain[domain.length - 1]) {
                            //domain is pointing forward
                            logScale.range([
                                arg[0] - delta,
                                0
                            ]);
                            linScale.range([
                                arg[0],
                                arg[0] - delta
                            ]);
                        } else {
                            //domain is pointing backward
                            logScale.range([
                                arg[0],
                                delta
                            ]);
                            linScale.range([
                                delta,
                                0
                            ]);
                        }
                    }
                }
                //
                //console.log("LOG and LIN range:", logScale.range(), linScale.range());
                range = arg;
                return scale;
            };
            scale.copy = function() {
                return d3_scale_genericLog(d3.scale.log().domain([
                    1,
                    10
                ])).domain(domain).range(range).eps(eps).delta(delta);
            };
            return d3.rebind(scale, logScale, 'invert', 'base', 'rangeRound', 'interpolate', 'clamp', 'nice',
                'tickFormat',
                'ticks');
        }(d3.scale.log().domain([
            1,
            10
        ]));
    };

    function detectTouchEvent(element, onTap, onLongTap) {
      var start;
      var namespace = onTap ? '.onTap' : '.onLongTap';
      d3.select(element)
        .on('touchstart' + namespace, function(d, i) {
          start = d3.event.timeStamp;
        })
        .on('touchend' + namespace, function(d, i) {
          if(d3.event.timeStamp - start < 500)
            return onTap ? onTap(d, i) : undefined;
          return onLongTap ? onLongTap(d, i) : undefined;
        });
    }

    //d3.selection.prototype.onTap
    var onTap = function(callback) {
      return this.each(function() {
        detectTouchEvent(this, callback);
      })
    };

    //d3.selection.prototype.onLongTap
    var onLongTap = function(callback) {
      return this.each(function() {
        detectTouchEvent(this, null, callback);
      })
    };

    var FILE_CACHED$2 = {}; //caches files from this reader
    var FILE_REQUESTED$2 = {}; //caches files from this reader

    var JSONReader = Reader.extend({

      /**
       * Initializes the reader.
       * @param {Object} reader_info Information about the reader
       */
      init: function(reader_info) {
        this._name = 'json';
        this._data = [];
        this._basepath = reader_info.path;
        this._parsers = reader_info.parsers;
        if(!this._basepath) {
          error$1("Missing base path for json reader");
        };
      },

      /**
       * Reads from source
       * @param {Object} query to be performed
       * @param {String} language language
       * @returns a promise that will be resolved when data is read
       */
      read: function(query, language) {
        var _this = this;
        var p = new Promise();

        //this specific reader has support for the tag {{LANGUAGE}}
        var path = this._basepath.replace("{{LANGUAGE}}", language);
        _this._data = [];

        (function(query, p) {

          //if cached, retrieve and parse
          if(FILE_CACHED$2.hasOwnProperty(path)) {
            parse(FILE_CACHED$2[path]);
          }
          //if requested by another hook, wait for the response
          else if(FILE_REQUESTED$2.hasOwnProperty(path)) {
            FILE_REQUESTED$2[path].then(function() {
              parse(FILE_CACHED$2[path]);
            });
          }
          //if not, request and parse
          else {
            d3.json(path, function(error, res) {

              if(!res) {
                error$1("No permissions or empty file: " + path, error);
                return;
              }

              if(error) {
                error$1("Error Happened While Loading JSON File: " + path, error);
                return;
              }
              //fix JSON response
              res = format(res);

              //cache and resolve
              FILE_CACHED$2[path] = res;
              FILE_REQUESTED$2[path].resolve();
              FILE_REQUESTED$2[path] = void 0;

              parse(res);
            });
            FILE_REQUESTED$2[path] = new Promise();
          }

          function format(res) {
            //TODO: Improve local json filtering
            //make category an array and fix missing regions
            res = res[0].map(function(row) {
              row['geo.cat'] = [row['geo.cat']];
              row['geo.region'] = row['geo.region'] || row['geo'];
              return row;
            });

            //format data
            res = mapRows(res, _this._parsers);

            //TODO: fix this hack with appropriate ORDER BY
            //order by formatted
            //sort records by time
            var keys = Object.keys(_this._parsers);
            var order_by = keys[0];
            res.sort(function(a, b) {
              return a[order_by] - b[order_by];
            });
            //end of hack

            return res;
          }

          function parse(res) {
            var data = res;
            //rename geo.category to geo.cat
            var where = query.where;
            if(where['geo.category']) {
              where['geo.cat'] = clone(where['geo.category']);
              where['geo.category'] = void 0;
            }

            //format values in the dataset and filters
            where = mapRows([where], _this._parsers)[0];

            //make sure conditions don't contain invalid conditions
            var validConditions = [];
            forEach(where, function(v, p) {
              for(var i = 0, s = data.length; i < s; i++) {
                if(data[i].hasOwnProperty(p)) {
                  validConditions.push(p);
                  return true;
                }
              };
            });
            //only use valid conditions
            where = clone(where, validConditions);

            data = filterAny(data, where);

            //warn if filtering returns empty array
            if(data.length == 0) {
              p.reject("data reader returns empty array, that's bad");
              return;
            }

            //only selected items get returned
            data = data.map(function(row) {
              return clone(row, query.select);
            });

            _this._data = data;

            p.resolve();
          }

        })(query, p);

        return p;
      },

      /**
       * Gets the data
       * @returns all data
       */
      getData: function() {
        return this._data;
      }
    });

    /*!
     * Inline Reader
     * the simplest reader possible
     */

    var InlineReader = Reader.extend({
        init: function (reader_info) {
          this.name = "inline";
          this._super(reader_info);
        }
      });

    var FILE_CACHED$1 = {}; //caches files from this reader
    var FILE_REQUESTED$1 = {}; //caches files from this reader
    // temporal hack for https problem

    var GraphReader = Reader.extend({

      /**
       * Initializes the reader.
       * @param {Object} reader_info Information about the reader
       */
      init: function (reader_info) {
        this._name = 'graph';
        this._data = [];
        this._basepath = reader_info.path;
        this._parsers = reader_info.parsers;
        if (!this._basepath) {
          error$1("Missing base path for graph reader");
        }
      },

      /**
       * Reads from source
       * @param {Object} query to be performed
       * @param {String} language language
       * @returns a promise that will be resolved when data is read
       */
      read: function (query, language) {
        var _this = this;
        var p = new Promise();
        var path = this._basepath;
        //format time query if existing
        if (query.where.time) {
          var time = query.where.time[0];
          var t = typeof time.join !== 'undefined' && time.length === 2 ?
            // {from: time, to: time}
            JSON.stringify({from: getYear(time[0]), to: getYear(time[1])}) :
            getYear(time[0]);
          path += '?time=' + t;
        }

        function getYear(time) {
          if (typeof time === 'string') {
            return time;
          }

          return time.getUTCFullYear();
        }

        _this._data = [];

        (function (query, p) {
          //if cached, retrieve and parse
          if (FILE_CACHED$1.hasOwnProperty(path)) {
            parse(FILE_CACHED$1[path]);
            return p;
          }
          //if requested by another hook, wait for the response
          if (FILE_REQUESTED$1.hasOwnProperty(path)) {
            FILE_REQUESTED$1[path].then(function () {
              parse(FILE_CACHED$1[path]);
            });
            return p;
          }
          //if not, request and parse
          FILE_REQUESTED$1[path] = new Promise();
          get(path, [], onSuccess, console.error.bind(console), true);
          function onSuccess(resp) {
            if (!resp) {
              error$1("Empty json: " + path, error);
              return;
            }

            resp = format(uzip(resp.data));
            //cache and resolve
            FILE_CACHED$1[path] = resp;
            FILE_REQUESTED$1[path].resolve();
            FILE_REQUESTED$1[path] = void 0;

            parse(resp);
          }

          return p;

          function uzip(table) {
            var rows = table.rows;
            var headers = table.headers;
            var result = new Array(rows.length);
            // unwrap compact data into json collection
            for (var i = 0; i < rows.length; i++) {
              result[i] = {};
              for (var j = 0; j < headers.length; j++) {
                result[i][headers[j]] = (rows[i][j] || '').toString();
                if (headers[j] === 'geo.cat') {
                  result[i][headers[j]] = [result[i][headers[j]]];
                }
              }
            }
            return result;
          }

          function format(res) {
            //format data
            res = mapRows(res, _this._parsers);

            //TODO: fix this hack with appropriate ORDER BY
            //order by formatted
            //sort records by time
            var keys = Object.keys(_this._parsers);
            var order_by = keys[0];
            res.sort(function (a, b) {
              return a[order_by] - b[order_by];
            });
            //end of hack

            return res;
          }

          function parse(res) {

            var data = res;
            //rename geo.category to geo.cat
            var where = query.where;
            if (where['geo.category']) {
              where['geo.cat'] = clone(where['geo.category']);
              delete where['geo.category'];
            }

            //format values in the dataset and filters
            where = mapRows([where], _this._parsers)[0];

            //make sure conditions don't contain invalid conditions
            var validConditions = [];
            forEach(where, function (v, p) {
              for (var i = 0, s = data.length; i < s; i++) {
                if (data[i].hasOwnProperty(p)) {
                  validConditions.push(p);
                  return true;
                }
              }
            });
            //only use valid conditions
            where = clone(where, validConditions);

            //filter any rows that match where condition
            data = filterAny(data, where);

            //warn if filtering returns empty array
            if (data.length === 0) warn("data reader returns empty array, that's bad");

            //only selected items get returned
            data = data.map(function (row) {
              return clone(row, query.select);
            });
            _this._data = data;
            p.resolve();
          }

        })(query, p);

        return p;
      },

      /**
       * Gets the data
       * @returns all data
       */
      getData: function () {
        return this._data;
      }
    });

    var FILE_CACHED = {}; //caches files from this reader
    var FILE_REQUESTED = {}; //caches files from this reader

    var CSVReader = Reader.extend({

      /**
       * Initializes the reader.
       * @param {Object} reader_info Information about the reader
       */
      init: function(reader_info) {
        this._name = 'csv';
        this._data = [];
        this._basepath = reader_info.path;
        this._parsers = reader_info.parsers;
        if(!this._basepath) {
          error$1("Missing base path for csv reader");
        }
      },

      /**
       * Reads from source
       * @param {Object} query to be performed
       * @param {String} language language
       * @returns a promise that will be resolved when data is read
       */
      read: function(query, language) {
        var _this = this;
        var p = new Promise();

        //this specific reader has support for the tag {{LANGUAGE}}
        this.path = this._basepath.replace("{{LANGUAGE}}", language);

        //replace conditional tags {{<any conditional>}}
        this.path = this.path.replace(/{{(.*?)}}/g, function(match, capture) {
          capture = capture.toLowerCase();
          if(isArray(query.where[capture])) {
            return query.where[capture].sort().join('-');
          }
          return query.where[capture];
        });

        //if only one year, files ending in "-YYYY.csv"
        var loadPath = this.path;
        if(query.where.time && query.where.time[0].length === 1) {
          loadPath = loadPath.replace(".csv", "-" + query.where.time[0][0] + ".csv");
        }

        _this._data = [];

        (function(query, p) {

          // load and then read from the cache when loaded
          var loadPromise = _this.load(loadPath, parse);
          loadPromise.then(function() {
            parse(FILE_CACHED[loadPath]);
          })

          function parse(res) {

            var data = res;   

            //rename geo.category to geo.cat
            var where = query.where;
            if(where['geo.category']) {
              where['geo.cat'] = clone(where['geo.category']);
              where['geo.category'] = void 0;
            }

            // load (join) any properties if necessary
            var propertiesLoadPromise = _this.loadProperties(data, query);

            // once done, continue parsing
            propertiesLoadPromise.then(function() {

              //make sure conditions don't contain invalid conditions
              var validConditions = [];
              forEach(where, function(v, p) {
                for(var i = 0, s = data.length; i < s; i++) {
                  if(data[i].hasOwnProperty(p)) {
                    validConditions.push(p);
                    return true;
                  }
                };
              });

              // only use valid conditions
              where = clone(deepClone(where), validConditions);

              // 
              where = mapRows([where], _this._parsers)[0];

              //filter any rows that match where condition
              data = filterAny(data, where);

              //warn if filtering returns empty array
              if(data.length == 0) {
                p.reject("data reader returns empty array, that's bad");
                return;
              }

              //only selected items get returned
              data = data.map(function(row) {
                return clone(row, query.select);
              });

              // grouping
              data = _this.groupData(data, query);

              // sorting
              // one column, one direction (ascending) for now
              if(query.orderBy && data[0]) {
                if (data[0][query.orderBy]) {
                  data.sort(function(a, b) {
                    return a[query.orderBy] - b[query.orderBy];
                  });
                } else {
                  p.reject("Cannot sort by " + query.orderBy + ". Column does not exist in result.");
                }
              }

              _this._data = data;
              p.resolve();

            })


          }

        })(query, p);

        return p;
      },

      /**
       * Gets the data
       * @returns all data
       */
      getData: function() {
        return this._data;
      },


      format: function(res) {

        //make category an array
        res = res.map(function(row) {
          if(row['geo.cat']) {
            row['geo.cat'] = [row['geo.cat']];
          }
          return row;
        });

        //format data
        res = mapRows(res, this._parsers);

        return res;
      },

      load: function(path) {
        var _this = this;

        //if not yet cached or request, start a request
        if(!FILE_CACHED.hasOwnProperty(path) && !FILE_REQUESTED.hasOwnProperty(path)) {
          // load the csv
          d3.csv(path, function(error, res) {

            if(!res) {
              error$1("No permissions or empty file: " + path, error);
              return;
            }

            if(error) {
              error$1("Error Happened While Loading CSV File: " + path, error);
              return;
            }

            //fix CSV response
            res = _this.format(res);

            //cache and resolve
            FILE_CACHED[path] = res;
            FILE_REQUESTED[path].resolve();
            // commented this out because the promise needs to stay for future requests, indicating it is already in the cache
            // FILE_REQUESTED[path] = void 0; 

          });
          FILE_REQUESTED[path] = new Promise();
        }    
        // always return a promise, even if it is already in the cache
        return FILE_REQUESTED[path];
      },

      loadProperties: function(data, query) {
          var _this = this;

          // see if there are any properties used in the query and load them
          // At the moment properties are loaded and added to the data-set only when required but for every query. Maybe loading and adding them to the data-set once is better?
          var propertiesPromises = [];
          var propertiesByKey = {};

          // check both select and where for columns that actually refer to properties
          forEach(query.select, function(column) {
            checkForProperty(column);
          });
          forEach(query.where, function(values, column) {
            checkForProperty(column);
          });

          // load properties for each column referring to property in the dataset
            
          // The below process O(n*m*o) but both n and o are typically small: n = number of property-sets, m = size of data-set, o = number of columns in property-set
          // for each requested property-set
          forEach(propertiesByKey, function(properties, key) {
            properties[key] = true; // also retrieve the key-column
            propertiesPromises.push(loadProperties(properties, key));
          });

          return propertiesPromises.length ? Promise.all(propertiesPromises) : new Promise.resolve();


          function checkForProperty(column) {
            var split = column.split('.');
            if (split.length == 2) {
              propertiesByKey[split[0]] = propertiesByKey[split[0]] || [];
              propertiesByKey[split[0]].push(column);
            }
          }

          function loadProperties(queriedProperties, keyColumn) {

            /*
             * Code below is for a path to a file when properties are shared between datasets
             *

            // parse the url of the original csv
            var parser = document.createElement('a');
            parser.href = path;

            // construct the path of the file with properties of the key column
            var newpathname = parser.pathname.substr(0, parser.pathname.lastIndexOf('/') + 1) + key + "-properties.csv";
            var propertiesPath = parser.protocol + '//' + parser.host + newpathname + parser.search + parser.hash;
            */

            // get path of properties that are specific for the current data-set
            var propertiesPath = _this.path.replace(".csv", "-" + keyColumn + "-properties.csv");

            // load the file and return the promise for loading
            var processedPromise = new Promise();
            var loadPromise = _this.load(propertiesPath);
            loadPromise.then(function() {

              var properties = {};

              // load all the properties in a map with the keyColumn-value as keyColumn (e.g. properties['swe']['geo.name'] = 'Sweden')
              // this map is readable in O(1)
              forEach(FILE_CACHED[propertiesPath], function(object) {
                properties[object[keyColumn]] = object;
              }); 

              // go through each row of data
              forEach(data, function(row, index) { // e.g. row = { geo: se, pop: 1000, gdp: 5 }
                // copy each property that was queried to the matching data-row (matching = same keyColumn)
                forEach(queriedProperties, function(property) {
                    
                    // check if row exists in properties
                    if(properties[row[keyColumn]]){
                        row[property] = properties[row[keyColumn]][property];
                    }else{
                        // if not, then complain
                        warn(row[keyColumn] + " is missing from GEO-PROPERTIES.CSV");
                    }
                  
                })
              });
                
              processedPromise.resolve();

            });

            return processedPromise;
          }
      },

      groupData: function(data, query) {

        // nested object which will be used to find the right group for each datarow. Each leaf will contain a reference to a data-object for aggregration.
        var grouping_map = {}; 

        var filtered = data.filter(function(val, index) {

          var keep;
          var leaf = grouping_map; // start at the base

          // find the grouping-index for each grouping property (i.e. entity)
          var keys = Object.keys(query.grouping);
          var n = keys.length;
          for (var i = 0; i < n; i++) {
            var grouping = query.grouping[keys[i]];
            var entity = keys[i];

            var group_index;

            // TO-DO: only age is grouped together for now, should be more generic
            if (entity == 'age') {

              var group_by = grouping;
              var group_offset = 0;

              var group_nr = Math.floor((val[entity] - group_offset) / group_by); // group number
              var group_start = group_nr * group_by + group_offset; // number at which the group starts

              // if the group falls outside the where filter, make the group smaller
              if (group_start < query.where[entity][0][0])
                group_start = query.where[entity][0][0];   

              group_index = group_start;
              val[entity] = group_index;
            }

            // if this is not the last grouping property
            if (i < (n-1)) {

              // create if next grouping level doesn't exist yet
              if (!leaf[val[entity]])
                leaf[val[entity]] = {};
              // set leaf to next level to enable recursion
              leaf = leaf[val[entity]];

            } else {

              // if last grouping property: we are at the leaf and can aggegrate

              if (!leaf[val[entity]]) {

                // if the final leaf isn't set yet, start it by letting it refer to the current row in the data. We will keep this row in the data-set.
                leaf[val[entity]] = val;
                keep = true;

              } else {

                // if the final leaf was already set, aggregrate!
                leaf = leaf[val[entity]];
                // if the leaf already had values, apply the aggregrate functions for each property
                forEach(query.select, function(property, key) {
                  // TO-DO replace with more generic grouping/aggregrate
                  if (property == 'pop') {
                    // aggregrate the un-grouped data (now only sum population)
                    leaf[property] = parseFloat(leaf[property]) + parseFloat(val['pop']);
                  }
                });  
                keep = false;

              }

            }

          }

          // if this row will function as place for aggregration, keep it, otherwise, discard it through the filter.
          return keep;

        });
      
        return filtered;

      }

    });

    var _index = {
    csv : CSVReader,
    graph : GraphReader,
    inline : InlineReader,
    json : JSONReader,
    waffle : WSReader,
    };

    var readers = {
    	csv: CSVReader,
    	graph: GraphReader,
    	inline: InlineReader,
    	json: JSONReader,
    	waffle: WSReader,
    	default: _index
    };

    var Vzb = function(name, placeholder, external_model) {
      var tool = Tool.get(name);
      if(tool) {
        var t = new tool(placeholder, external_model);
        Vzb._instances[t._id] = t;
        return t;
      } else {
        error$1('Tool "' + name + '" was not found.');
      }
    };

    //stores reference to each tool on the page
    Vzb._instances = {};
    //stores global variables accessible by any tool or component
    Vzb._globals = globals;

    //TODO: clear all objects and intervals as well
    //garbage collection
    Vzb.clearInstances = function(id) {
      if(id) {
        Vzb._instances[id] = void 0;
      } else {
        for(var i in Vzb._instances) {
          Vzb._instances[i].clear();
        }
        Vzb._instances = {};
      }
    };

    //register available readers
    forEach(readers, function(reader, name) {
      Reader.register(name, reader);
    });

    //register available components
    forEach(components$1, function(component, name) {
      Component.register(name, component);
    });


    d3.scale.genericLog = genericLog;
    d3.selection.prototype.onTap = onTap;
    d3.selection.prototype.onLongTap = onLongTap;

    //makes all objects accessible
    Vzb.Tool = Tool;
    Vzb.Component = Component;
    Vzb.Model = Model;
    Vzb.Reader = Reader;
    Vzb.Events = EventSource;
    Vzb.utils = utils;

    var language = {
      id: "en",
      strings: {}
    };

    var locationArray = window.location.href.split("/");
    var localUrl = locationArray.splice(0, locationArray.indexOf("preview")).join("/");
    localUrl += "/home/vizabi/preview/";
    //TODO: remove hardcoded path from source code
    globals.gapminder_paths = {
      baseUrl: localUrl
    };

    //OVERWRITE OPTIONS

    BarChart.define('default_model', {
      state: {
        time: {
          start: "1800",
          end: "2012",
          value: "2000",
          step: 1,
          formatInput: "%Y"
        },
        entities: {
          dim: "geo",
          show: {
            _defs_: {
              "geo": ["usa", "swe", "nor"],
              "geo.cat": ["country", "unstate"]
            }
          }
        },
        marker: {
          space: ["entities", "time"],
          label: {
            use: "property",
            which: "geo.name"
          },
          axis_y: {
            use: "indicator",
            which: "population",
            scaleType: "log",
            allow: {
              scales: ["linear", "log"]
            }
          },
          axis_x: {
            use: "property",
            which: "geo.name",
            allow: {
              scales: ["ordinal"],
              names: ["!geo", "!_default"]
            }
          },
          color: {
            use: "property",
            which: "geo.region",
            scaleType: "ordinal"
          }
        }
      },
      data: {
        reader: "waffle",
        path: "http://waffle-server-dev.gapminderdev.org/api/graphs/stats/vizabi-tools"
        //reader: "csv",
        //path: globals.gapminder_paths.baseUrl + "data/waffles/dont-panic-poverty.csv"
      },
      language: language,
      ui: {
        presentation: false
      }
    });

    BarRankChart.define('default_model', {
      state: {
        time: {
          start: "1950",
          end: "2015",
          value: "2000",
          step: 1,
          formatInput: "%Y"
        },
        entities: {
          dim: "geo",
          show: {
            _defs_: {
              "geo.cat": ["country", "unstate"]
            }
          },
          opacitySelectDim: .3,
          opacityRegular: 1
        },
        entities_allpossible: {
          dim: "geo",
          show: {
            _defs_: {
              "geo": ["*"],
              "geo.cat": ["country", "unstate"]
            }
          }
        },
        marker_allpossible: {
          space: ["entities_allpossible"],
          label: {
            use: "property",
            which: "geo.name"
          }    
        },      
        marker: {
          space: ["entities", "time"],
          label: {
            use: "property",
            which: "geo.name"
          },
          axis_x: {
            use: "indicator",
            which: "population",
            scaleType: "log",
            allow: {
              scales: [
                "linear",
                "log"
              ]
            }
          },
          // should not be here because axis-y is not geo.name but order of population
          axis_y: {
            use: "property",
            which: "geo.name",
            scaleType: "log",
            allow: {
              scales: [
                "ordinal"
              ]
            }
          },
          color: {
            use: "property",
            which: "geo.region"
          }
        }
      },
      language: language,
      data: {
        reader: "waffle",
        path: "http://waffle-server-dev.gapminderdev.org/api/graphs/stats/vizabi-tools",
        //reader: "csv",
        //path: globals.gapminder_paths.baseUrl + "data/waffles/basic-indicators.csv"
        splash: true
      },
      ui: {
        presentation: false
      }
    });

    BubbleMap.define('datawarning_content', {
      title: "",
      body: "Comparing the size of economy across countries and time is not trivial. The methods vary and the prices change. Gapminder has adjusted the picture for many such differences, but still we recommend you take these numbers with a large grain of salt.<br/><br/> Countries on a lower income levels have lower data quality in general, as less resources are available for compiling statistics. Historic estimates of GDP before 1950 are generally also more rough. <br/><br/> Data for child mortality is more reliable than GDP per capita, as the unit of comparison, dead children, is universally comparable across time and place. This is one of the reasons this indicator has become so useful to measure social progress. But the historic estimates of child mortality are still suffering from large uncertainties.<br/><br/> Learn more about the datasets and methods in this <a href='http://www.gapminder.org/news/data-sources-dont-panic-end-poverty' target='_blank'>blog post</a>",
      doubtDomain: [1800, 1950, 2015],
      doubtRange: [1.0, .3, .2]
    });

    BubbleMap.define('default_model', {
      state: {
        time: {
          start: "1800",
          end: "2015",
          value: "2015",
          step: 1,
          speed: 300,
          formatInput: "%Y"
        },
        entities: {
          dim: "geo",
          opacitySelectDim: .3,
          opacityRegular: 1,
          show: {
            _defs_: {
              "geo.cat": ["country", "unstate"]
            }
          },
        },
        marker: {
          space: ["entities", "time"],
          label: {
            use: "property",
            which: "geo.name"
          },
          size: {
            use: "indicator",
            which: "population",
            scaleType: "linear",
            allow: {
              scales: ["linear", "log"]
            },
            domainMin: .04,
            domainMax: .90
          },
          lat: {
            use: "property",
            which: "geo.latitude"
          },
          lng: {
            use: "property",
            which: "geo.longitude"
          },
          color: {
            use: "property",
            which: "geo.region",
            scaleType: "ordinal",
            allow: {
              names: ["!geo.name"]
            }
          }
        }
      },
      data: {
        reader: "waffle",
        path: "http://waffle-server-dev.gapminderdev.org/api/graphs/stats/vizabi-tools",
        //reader: "csv",
        //path: globals.gapminder_paths.baseUrl + "data/waffles/dont-panic-poverty.csv",
        splash: true
      },
      language: language,
      ui: {
        presentation: false
      }
    });

    MountainChart.define('datawarning_content', {
      title: "Income data has large uncertainty!",
      body: "There are many different ways to estimate and compare income. Different methods are used in different countries and years. Unfortunately no data source exists that would enable comparisons across all countries, not even for one single year. Gapminder has managed to adjust the picture for some differences in the data, but there are still large issues in comparing individual countries. The precise shape of a country should be taken with a large grain of salt.<br/><br/> Gapminder strongly agrees with <a href='https://twitter.com/brankomilan' target='_blank'>Branko Milanovic</a> about the urgent need for a comparable global income survey, especially for the purpose of monitoring the UN poverty-goal.<br/><br/> We are constantly improving our datasets and methods. Please expect revision of this graph within the coming months. <br/><br/> Learn more about the datasets and methods in this <a href='http://www.gapminder.org/news/data-sources-dont-panic-end-poverty' target='_blank'>blog post</a>",
      doubtDomain: [1800, 1950, 2015],
      doubtRange: [1.0, .8, .6]
    });

    MountainChart.define('default_model', {
      state: {
        time: {
          start: 1800,
          end: 2015,
          value: 2015,
          step: 1,
          delay: 100,
          delayThresholdX2: 50,
          delayThresholdX4: 25,
          formatInput: "%Y",
          xLogStops: [1, 2, 5],
          yMaxMethod: "latest",
          probeX: 1.85,
          tailFatX: 1.85,
          tailCutX: .2,
          tailFade: .7,
          xScaleFactor: 1.039781626,
          //0.9971005335,
          xScaleShift: -1.127066411,
          //-1.056221322,
          xPoints: 50
        },
        entities: {
          dim: "geo",
          opacitySelectDim: .3,
          opacityRegular: .6,
          show: {
            _defs_: {
              "geo": ["*"],
              "geo.cat": ["unstate"]
            }
          }
        },
        entities_allpossible: {
          dim: "geo",
          show: {
            _defs_: {
              "geo": ["*"],
              "geo.cat": ["unstate"]
            }
          }
        },
        marker_allpossible: {
          space: ["entities_allpossible"],
          label: {
            use: "property",
            which: "geo.name"
          }    
        },
        marker: {
          space: ["entities", "time"],
          label: {
            use: "property",
            which: "geo.name"
          },
          axis_y: {
            use: "indicator",
            which: "population",
            scaleType: 'linear'
          },
          axis_x: {
            use: "indicator",
            which: "gdp_p_cap_const_ppp2011_dollar",
            scaleType: 'log',
            domainMin: .11, //0
            domainMax: 500 //100
          },
          size: {
            use: "indicator",
            which: "gini",
            scaleType: 'linear'
          },
          color: {
            use: "property",
            which: "geo.region",
            scaleType: "ordinal",
            allow: {
              names: ["!geo.name"]
            }
          },
          stack: {
            use: "constant",
            which: "all" // set a property of data or values "all" or "none"
          },
          group: {
            use: "property",
            which: "geo.region", // set a property of data
            manualSorting: ["asia", "africa", "americas", "europe"],
            merge: false
          }
        }
      },
      language: language,
      data: {
        reader: "waffle",
        path: "http://waffle-server-dev.gapminderdev.org/api/graphs/stats/vizabi-tools",
        //reader: "csv",
        //path: globals.gapminder_paths.baseUrl + "data/waffles/dont-panic-poverty.csv",
        splash: true
      },
      ui: {
        presentation: false
      }
    });


    LineChart.define('default_model', {
      state: {
        time: {
          start: 1800,
          end: 2012,
          value: 2012,
          step: 1,
          formatInput: "%Y"
        },
        //entities we want to show
        entities: {
          dim: "geo",
          show: {
            _defs_: {
              "geo": ["usa", "swe", "chn"],
              "geo.cat": ["country", "unstate"]
            }
          }
        },
        //how we show it
        marker: {
          space: ["entities", "time"],
          label: {
            use: "property",
            which: "geo.name"
          },
          axis_y: {
            use: "indicator",
            which: "gdp_p_cap_const_ppp2011_dollar",
            scaleType: "log"
          },
          axis_x: {
            use: "indicator",
            which: "time",
            scaleType: "time"
          },
          color: {
            use: "property",
            which: "geo.region",
            allow: {
              scales: ["ordinal"],
              names: ["!geo.name"]
            }
          }
        }
      },

      data: {
        reader: "csv",
        path: globals.gapminder_paths.baseUrl + "data/waffles/dont-panic-poverty.csv",
        splash: false
      },
      language: language,
      ui: {
        'vzb-tool-line-chart': {
          entity_labels: {
            min_number_of_entities_when_values_hide: 2 //values hide when showing 2 entities or more
          },
          whenHovering: {
            hideVerticalNow: 0,
            showProjectionLineX: true,
            showProjectionLineY: true,
            higlightValueX: true,
            higlightValueY: true,
            showTooltip: 0
          }
        },
        presentation: false
      }
    });

    BubbleChart.define('datawarning_content', {
      title: "",
      body: "Comparing the size of economy across countries and time is not trivial. The methods vary and the prices change. Gapminder has adjusted the picture for many such differences, but still we recommend you take these numbers with a large grain of salt.<br/><br/> Countries on a lower income levels have lower data quality in general, as less resources are available for compiling statistics. Historic estimates of GDP before 1950 are generally also more rough. <br/><br/> Data for child mortality is more reliable than GDP per capita, as the unit of comparison, dead children, is universally comparable across time and place. This is one of the reasons this indicator has become so useful to measure social progress. But the historic estimates of child mortality are still suffering from large uncertainties.<br/><br/> Learn more about the datasets and methods in this <a href='http://www.gapminder.org/news/data-sources-dont-panic-end-poverty' target='_blank'>blog post</a>",
      doubtDomain: [1800, 1950, 2015],
      doubtRange: [1.0, .3, .2]
    });

    BubbleChart.define('default_model', {

      state: {
        time: {
          start: "1800",
          end: "2015",
          value: "2015",
          step: 1,
          formatInput: "%Y",
          trails: true,
          lockNonSelected: 0,
          adaptMinMaxZoom: false
        },
        entities: {
          dim: "geo",
          show: {
            _defs_: {
              "geo.cat": ["country", "unstate"]
            }
          }
        },
        marker: {
          space: ["entities", "time"],
          type: "geometry",
          shape: "circle",
          label: {
            use: "property",
            which: "geo.name"
          },
          axis_y: {
            use: "indicator",
            which: "child_mortality_rate_per1000",
            scaleType: "linear",
            allow: {
              scales: ["linear", "log"]
            }
          },
          axis_x: {
            use: "indicator",
            which: "gdp_p_cap_const_ppp2011_dollar",
            scaleType: "log",
            allow: {
              scales: ["linear", "log"]
            }
          },
          color: {
            use: "property",
            which: "geo.region",
            scaleType: "ordinal",
            allow: {
              names: ["!geo.name"]
            }
          },
          size: {
            use: "indicator",
            which: "population",
            scaleType: "linear",
            allow: {
              scales: ["linear", "log"]
            },
            domainMin: .04,
            domainMax: .90
          }
        }
      },
      data: {
        reader: "waffle",
        path: "http://waffle-server-dev.gapminderdev.org/api/graphs/stats/vizabi-tools",
        //reader: "csv",
        //path: globals.gapminder_paths.baseUrl + "data/waffles/dont-panic-poverty.csv",
        splash: true
      },
      language: language,
      ui: {
        'vzb-tool-bubble-chart': {
          whenHovering: {
            showProjectionLineX: true,
            showProjectionLineY: true,
            higlightValueX: true,
            higlightValueY: true
          },
          labels: {
            autoResolveCollisions: true,
            dragging: true
          }
        },
        presentation: false
      }
    });

    PopByAge.define('default_model', {
      state: {
        time: {
          value: '2013',
          start: '1950',
          end: '2100'
        },
        entities: {
          dim: "geo",
          show: {
            _defs_: {
              "geo": ["usa"]
            }
          }
        },
        entities_age: {
          dim: "age",
          show: {
            _defs_: {
              "age": [
                  [0, 95]
                ] //show 0 through 100
            }
          },
          grouping: 5
        },
        marker: {
          space: ["entities", "entities_age", "time"],
          label: {
            use: "indicator",
            which: "age"
          },
          label_name: {
            use: "property",
            which: "geo"
          },
          axis_y: {
            use: "indicator",
            which: "age",
            // domain Max should be set manually as age max from entites_age plus one grouping value (95 + 5 = 100)
            // that way the last age group fits in on the scale
            domainMax: 100,
            domainMin: 0
          },
          axis_x: {
            use: "indicator",
            which: "population"
          },
          color: {
            use: "constant",
            which: "#ffb600",
            allow: {
              names: ["!geo.name"]
            }
          }
        }
      },
      data: {
        reader: "csv",
        path: globals.gapminder_paths.baseUrl + "data/waffles/{{geo}}.csv",
        splash: false
      },
      language: language,
      ui: {
        presentation: false
      }
    });

    //Waffle Server Reader custom path
    WSReader.define('basepath', "http://52.18.235.31:8001/values/waffle");

    //preloading mountain chart precomputed shapes
    MountainChartComponent.define("preload", function(done) {
      var shape_path = globals.gapminder_paths.baseUrl + "data/mc_precomputed_shapes.json";

      d3.json(shape_path, function(error, json) {
        if(error) return console.warn("Failed loading json " + shape_path + ". " + error);
        MountainChartComponent.define('precomputedShapes', json);
        done.resolve();
      });
    });

    //preloading bubble map country shapes
    BubbleMapComponent.define("preload", function(done) {
      var shape_path = globals.gapminder_paths.baseUrl + "data/world-50m.json";

      d3.json(shape_path, function(error, json) {
        if(error) return console.warn("Failed loading json " + shape_path + ". " + error);
        BubbleMapComponent.define('world', json);
        done.resolve();
      });
    });


    //preloading metadata for all charts
    Tool.define("preload", function(promise) {

      var _this = this;

      var metadata_path = Vzb._globals.gapminder_paths.baseUrl + "data/waffles/metadata.json";
      var globals = Vzb._globals;
        
      Vzb._globals.version = Vzb._version;
      Vzb._globals.build = Vzb._build;
        
      //TODO: concurrent
      //load language first
      this.preloadLanguage().then(function() {
        //then metadata
        d3.json(metadata_path, function(metadata) {

          globals.metadata = metadata;

          // TODO: REMOVE THIS HACK
          // We are currently saving metadata info to default state manually in order
          // to produce small URLs considering some of the info in metadata to be default
          // we need a consistent way to add metadata to Vizabi
          addMinMax("axis_x");
          addMinMax("axis_y");
          addPalettes("color");

          promise.resolve();

        });
      });

      // TODO: REMOVE THIS HACK (read above)
      function addPalettes(hook) {
        if(!_this.default_model.state || !_this.default_model.state.marker[hook] || !globals.metadata.color) {
          return;
        }
        var color = _this.default_model.state.marker[hook];
        var palette = globals.metadata.color.palettes['geo.region'];
        color.palette = extend({}, color.palette, palette);
      }

      function addMinMax(hook) {
        if(!_this.default_model.state || !_this.default_model.state.marker[hook]) {
          return;
        }
        var axis = _this.default_model.state.marker[hook];
        if(axis.use === "indicator" && globals.metadata.indicatorsDB[axis.which] && globals.metadata.indicatorsDB[axis.which].domain) {
          var domain = globals.metadata.indicatorsDB[axis.which].domain;
          axis.domainMin = axis.domainMin || domain[0];
          axis.domainMax = axis.domainMax || domain[1];
          axis.zoomedMin = axis.zoomedMin || axis.domainMin || domain[0];
          axis.zoomedMax = axis.zoomedMax || axis.domainMax || domain[1];
        }
      }

    });

    Tool.define("preloadLanguage", function() {
      var _this = this;
      var promise = new Promise();

      var langModel = this.model.language;
      var translation_path = Vzb._globals.gapminder_paths.baseUrl + "data/translation/" + langModel.id + ".json";

      if(langModel && !langModel.strings[langModel.id]) {
        d3.json(translation_path, function(langdata) {
          langModel.strings[langModel.id] = langdata;
          _this.model.language.strings.trigger("change");
          promise.resolve();
        });
      } else {
        this.model.language.strings.trigger("change");
        promise = promise.resolve();
      }

      return promise;

    });

    return Vzb;

}));
;(function (Vizabi) {Vizabi._version = "0.12.7"; Vizabi._build = "1455784101052";})(typeof Vizabi !== "undefined"?Vizabi:{});(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'bubblesize.html');s.innerHTML = '<div class="vzb-bs-holder"> <svg class="vzb-bs-svg"> <g class="vzb-bs-slider-wrap"> <g class="vzb-bs-slider"> </g> </g> </svg> </div> ';root.document.body.appendChild(s);}).call(this);(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'about.html');s.innerHTML = '<div class="vzb-dialog-modal"> <div class="vzb-dialog-title"> <%=t ( "buttons/about") %> </div> <div class="vzb-dialog-content"> <p class="vzb-about-text0"></p> <p class="vzb-about-text1"></p> <br/> <p class="vzb-about-version"></p> <p class="vzb-about-updated"></p> <br/> <p class="vzb-about-text2"></p> <br/> <p class="vzb-about-credits"></p> </div> <div class="vzb-dialog-buttons"> <div data-click="closeDialog" class="vzb-dialog-button vzb-label-primary"> OK </div> </div> </div> ';root.document.body.appendChild(s);}).call(this);(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'axes.html');s.innerHTML = '<div class="vzb-dialog-modal"> <span class="thumb-tack-class thumb-tack-class-ico-pin fa" data-dialogtype="axes" data-click="pinDialog"></span> <span class="thumb-tack-class thumb-tack-class-ico-drag fa" data-dialogtype="axes" data-click="dragDialog"></span> <div class="vzb-dialog-title"> <%=t ( "buttons/axes") %> </div> <div class="vzb-dialog-content"> <p class="vzb-dialog-sublabel"> <%=t ("buttons/axis_x") %> <span class="vzb-xaxis-selector"></span> </p> <div class="vzb-xaxis-minmax vzb-dialog-paragraph"></div> <p class="vzb-dialog-sublabel"> <%=t ("buttons/axis_y") %> <span class="vzb-yaxis-selector"></span> </p> <div class="vzb-yaxis-minmax vzb-dialog-paragraph"></div> <div class="vzb-axes-options"></div> </div> <div class="vzb-dialog-buttons"> <div data-click="closeDialog" class="vzb-dialog-button vzb-label-primary"> OK </div> </div> </div>';root.document.body.appendChild(s);}).call(this);(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'axesmc.html');s.innerHTML = '<div class="vzb-dialog-modal"> <span class="thumb-tack-class thumb-tack-class-ico-pin fa" data-dialogtype="axesmc" data-click="pinDialog"></span> <span class="thumb-tack-class thumb-tack-class-ico-drag fa" data-dialogtype="axesmc" data-click="dragDialog"></span> <div class="vzb-dialog-title"> <%=t ( "buttons/axes") %> </div> <div class="vzb-dialog-content"> <div class="vzb-yaxis-container"> <p class="vzb-dialog-sublabel"><%=t ( "hints/mount/maxYvalue") %></p> <form class="vzb-dialog-paragraph"> <label><input type="radio" name="ymax" value="immediate"><%=t ( "mount/maxYmode/immediate") %></label> <label><input type="radio" name="ymax" value="latest"><%=t ( "mount/maxYmode/latest") %></label> </form> </div> <div class="vzb-xaxis-container"> <p class="vzb-dialog-sublabel"> <%=t ( "hints/mount/logXstops") %> </p> <form class="vzb-dialog-paragraph"> <input type="checkbox" name="logstops" value="1">1 <input type="checkbox" name="logstops" value="2">2 <input type="checkbox" name="logstops" value="5">5 </form> </div> <p class="vzb-dialog-sublabel"> <%=t ( "hints/mount/xlimits") %> </p> <div class="vzb-xlimits-container vzb-dialog-paragraph"></div> <div class="vzb-probe-container"> <p class="vzb-dialog-sublabel"> <%=t ( "hints/mount/probe") %> </p> <input type="text" class="vzb-probe-field" name="probe"> </div> </div> <div class="vzb-dialog-buttons"> <div data-click="closeDialog" class="vzb-dialog-button vzb-label-primary"> OK </div> </div> </div>';root.document.body.appendChild(s);}).call(this);(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'colors.html');s.innerHTML = '<div class="vzb-dialog-modal"> <span class="thumb-tack-class thumb-tack-class-ico-pin fa" data-dialogtype="colors" data-click="pinDialog"></span> <span class="thumb-tack-class thumb-tack-class-ico-drag fa" data-dialogtype="colors" data-click="dragDialog"></span> <div class="vzb-dialog-title"> <%=t ( "buttons/colors") %> <span class="vzb-caxis-selector"></span> </div> <div class="vzb-dialog-content"> <div class="vzb-clegend-container"></div> </div> <div class="vzb-dialog-buttons"> <div data-click="closeDialog" class="vzb-dialog-button vzb-label-primary"> OK </div> </div> </div> ';root.document.body.appendChild(s);}).call(this);(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'find.html');s.innerHTML = '<div class="vzb-dialog-modal"> <span class="thumb-tack-class thumb-tack-class-ico-pin fa" data-dialogtype="find" data-click="pinDialog"></span> <span class="thumb-tack-class thumb-tack-class-ico-drag fa" data-dialogtype="find" data-click="dragDialog"></span> <div class="vzb-dialog-title"> <%=t ( "buttons/find") %> <span class="vzb-dialog-content vzb-find-filter"> <input id="vzb-find-search"type="text"/> </span> </div> <div class="vzb-dialog-content vzb-dialog-content-fixed vzb-dialog-scrollable"> <div class="vzb-find-list">  </div> </div> <div class="vzb-dialog-buttons"> <div class="vzb-dialog-bubbleopacity vzb-dialog-control"></div> <div id="vzb-find-deselect" class="vzb-dialog-button"> <%=t ( "buttons/deselect") %> </div> <div data-click="closeDialog" class="vzb-dialog-button vzb-label-primary"> <%=t ( "buttons/ok") %> </div> </div> </div> ';root.document.body.appendChild(s);}).call(this);(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'moreoptions.html');s.innerHTML = '<div class="vzb-dialog-modal"> <span class="thumb-tack-class thumb-tack-class-ico-pin fa" data-dialogtype="moreoptions" data-click="pinDialog"></span> <span class="thumb-tack-class thumb-tack-class-ico-drag fa" data-dialogtype="moreoptions" data-click="dragDialog"></span> <div class="vzb-dialog-options-buttonlist vzb-dialog-scrollable"> </div> <div class="vzb-dialog-title"> <%=t ("buttons/more_options") %> </div> <div class="vzb-dialog-content vzb-dialog-scrollable vzb-accordion"> </div> <div class="vzb-dialog-buttons"> <div data-click="closeDialog" class="vzb-dialog-button vzb-label-primary"> OK </div> </div> </div>';root.document.body.appendChild(s);}).call(this);(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'opacity.html');s.innerHTML = '<div class="vzb-dialog-modal"> <div class="vzb-dialog-title"> <%=t ( "buttons/opacity") %> </div> <div class="vzb-dialog-content"> <p class="vzb-dialog-sublabel"> <%=t ("buttons/opacityRegular") %> </p> <div class="vzb-dialog-bubbleopacity-regular"></div> <p class="vzb-dialog-sublabel"> <%=t ("buttons/opacityNonselect") %> </p> <div class="vzb-dialog-bubbleopacity-selectdim"></div> </div> </div> </div> ';root.document.body.appendChild(s);}).call(this);(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'presentation.html');s.innerHTML = '<div class="vzb-dialog-modal"> <div class="vzb-dialog-title"> <%=t ( "buttons/presentation") %> </div> <div class="vzb-dialog-content"> <div class="vzb-presentationmode-switch"></div> </div> </div> ';root.document.body.appendChild(s);}).call(this);(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'show.html');s.innerHTML = '<div class="vzb-dialog-modal"> <span class="thumb-tack-class thumb-tack-class-ico-pin fa" data-dialogtype="show" data-click="pinDialog"></span> <span class="thumb-tack-class thumb-tack-class-ico-drag fa" data-dialogtype="show" data-click="dragDialog"></span> <div class="vzb-dialog-title"> <%=t ( "buttons/show") %> <span class="vzb-dialog-content vzb-show-filter"> <input id="vzb-show-search" type="text"/> </span> </div> <div class="vzb-dialog-content vzb-dialog-content-fixed vzb-dialog-scrollable"> <p class="vzb-dialog-sublabel"> <%=t ( "hints/mount/onlyshowthefollowing") %> </p> <div class="vzb-show-list">  </div> </div> <div class="vzb-dialog-buttons"> <div id="vzb-show-deselect" class="vzb-dialog-button"> <%=t ( "buttons/deselect") %> </div> <div data-click="closeDialog" class="vzb-dialog-button vzb-label-primary"> <%=t ( "buttons/ok") %> </div> </div> </div> ';root.document.body.appendChild(s);}).call(this);(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'size.html');s.innerHTML = '<div class="vzb-dialog-modal"> <span class="thumb-tack-class thumb-tack-class-ico-pin fa" data-dialogtype="size" data-click="pinDialog"></span> <span class="thumb-tack-class thumb-tack-class-ico-drag fa" data-dialogtype="size" data-click="dragDialog"></span> <div class="vzb-dialog-title"> <%=t ( "buttons/size") %> <span class="vzb-saxis-selector"></span> </div> <div class="vzb-dialog-content"> <div class="vzb-dialog-bubblesize"></div> </div> <div class="vzb-dialog-buttons"> <div data-click="closeDialog" class="vzb-dialog-button vzb-label-primary"> OK </div> </div> </div> ';root.document.body.appendChild(s);}).call(this);(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'speed.html');s.innerHTML = '<div class="vzb-dialog-modal"> <div class="vzb-dialog-title"> <%=t ( "buttons/speed") %> </div> <div class="vzb-dialog-content"> <div class="vzb-dialog-placeholder"></div> </div> </div> ';root.document.body.appendChild(s);}).call(this);(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'stack.html');s.innerHTML = '<div class="vzb-dialog-modal"> <span class="thumb-tack-class thumb-tack-class-ico-pin fa" data-dialogtype="stack" data-click="pinDialog"></span> <span class="thumb-tack-class thumb-tack-class-ico-drag fa" data-dialogtype="stack" data-click="dragDialog"></span> <div class="vzb-dialog-title"> <%=t ( "buttons/stack") %> </div> <div class="vzb-dialog-content vzb-dialog-scrollable">  <form class="vzb-howtostack vzb-dialog-paragraph"> <label> <input type="radio" name="stack" value="none"> <%=t ( "mount/stacking/none") %> </label> <label> <input type="radio" name="stack" value="geo.region"> <%=t ( "mount/stacking/region") %> </label> <label> <input type="radio" name="stack" value="all"> <%=t ( "mount/stacking/world") %> </label> </form> <form class="vzb-howtomerge vzb-dialog-paragraph"> <p class="vzb-dialog-sublabel"> <%=t ( "hints/mount/howtomerge") %> </p> <label> <input type="radio" name="merge" value="none"> <%=t ( "mount/merging/none") %> </label> <label> <input type="radio" name="merge" value="grouped"> <%=t ( "mount/merging/region") %> </label> <label> <input type="radio" name="merge" value="stacked"> <%=t ( "mount/merging/world") %> </label> </form> <form class="vzb-manual-sorting"> <p class="vzb-dialog-sublabel"> <%=t ( "mount/manualSorting") %> </p> <div class="vzb-dialog-draggablelist vzb-dialog-control"></div> </form> </div> <div class="vzb-dialog-buttons"> <div data-click="closeDialog" class="vzb-dialog-button vzb-label-primary">OK</div> </div> </div>';root.document.body.appendChild(s);}).call(this);(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'minmaxinputs.html');s.innerHTML = '<div class="vzb-mmi-holder"> <span class="vzb-mmi-domainmin-label"></span> <input type="text" class="vzb-mmi-domainmin" name="min"> <span class="vzb-mmi-domainmax-label"></span> <input type="text" class="vzb-mmi-domainmax" name="max"> <br class="vzb-mmi-break"/> <span class="vzb-mmi-zoomedmin-label"></span> <input type="text" class="vzb-mmi-zoomedmin" name="min"> <span class="vzb-mmi-zoomedmax-label"></span> <input type="text" class="vzb-mmi-zoomedmax" name="max"> </div>';root.document.body.appendChild(s);}).call(this);(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'timeslider.html');s.innerHTML = '<div class="vzb-timeslider vzb-ts-loading"> <div class="vzb-ts-slider-wrapper"> <svg class="vzb-ts-slider"> <g> <g class="vzb-ts-slider-axis"></g> <g class="vzb-ts-slider-slide"> <circle class="vzb-ts-slider-handle"></circle> <text class="vzb-ts-slider-value"></text> </g> </g> </svg> </div>  <div class="vzb-ts-btns"> <button class="vzb-ts-btn-loading vzb-ts-btn"> <div class="vzb-loader"></div> </button> <button class="vzb-ts-btn-play vzb-ts-btn"> <svg class="vzb-icon vzb-icon-play" viewBox="3 3 42 42" xmlns="http://www.w3.org/2000/svg"> <path xmlns="http://www.w3.org/2000/svg" d="M24 4C12.95 4 4 12.95 4 24s8.95 20 20 20 20-8.95 20-20S35.05 4 24 4zm-4 29V15l12 9-12 9z"/> </svg> </button> <button class="vzb-ts-btn-pause vzb-ts-btn"> <svg class="vzb-icon vzb-icon-pause" viewBox="3 3 42 42" xmlns="http://www.w3.org/2000/svg"> <path xmlns="http://www.w3.org/2000/svg" d="M24 4C12.95 4 4 12.95 4 24s8.95 20 20 20 20-8.95 20-20S35.05 4 24 4zm-2 28h-4V16h4v16zm8 0h-4V16h4v16z"/> </svg> </button> </div> </div> ';root.document.body.appendChild(s);}).call(this);(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'barchart.html');s.innerHTML = ' <svg class="vzb-barchart"> <g class="vzb-bc-graph"> <g class="vzb-bc-bars"></g> <g class="vzb-bc-bar-labels"></g> <g class="vzb-bc-axis-y-title"></g> <text class="vzb-bc-year"></text> <g class="vzb-bc-axis-x-title"></g> <g class="vzb-bc-axis-x"></g> <g class="vzb-bc-axis-y"></g> <g class="vzb-bc-axis-labels">  </g> </g> </svg> ';root.document.body.appendChild(s);}).call(this);(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'barrank.html');s.innerHTML = ' <div class="vzb-barrankchart"> <svg class="vzb-br-header"> <text class="vzb-br-title"></text> <text class="vzb-br-total"></text> </svg> <div class="barsviewport"> <svg class="vzb-br-bars-svg"> <g class="vzb-br-bars"></g> </svg> </div> </div> ';root.document.body.appendChild(s);}).call(this);(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'bubblechart.html');s.innerHTML = ' <div class="vzb-bubblechart"> <svg class="vzb-bubblechart-svg"> <g class="vzb-bc-graph"> <g class="vzb-bc-year"></g> <svg class="vzb-bc-axis-x"> <g></g> </svg> <svg class="vzb-bc-axis-y"> <g></g> </svg> <line class="vzb-bc-projection-x"></line> <line class="vzb-bc-projection-y"></line> <svg class="vzb-bc-bubbles-crop"> <rect class="vzb-bc-eventarea"></rect> <g class="vzb-bc-lines"></g> <g class="vzb-bc-trails"></g> <g class="vzb-bc-bubbles"></g> <g class="vzb-bc-labels"></g> </svg> <g class="vzb-bc-axis-y-title"></g> <g class="vzb-bc-axis-x-title"></g> <g class="vzb-bc-axis-s-title"></g> <g class="vzb-bc-axis-c-title"></g> <g class="vzb-bc-axis-y-info"> </g> <g class="vzb-bc-axis-x-info"> </g> <g class="vzb-data-warning"> <svg></svg> <text></text> </g> <rect class="vzb-bc-zoom-rect"></rect> <g class="vzb-bc-tooltip vzb-hidden"> <rect class="vzb-bc-tooltip-border"></rect> <text class="vzb-bc-tooltip-text"></text> </g> </g> </svg>  <div class="vzb-tooltip vzb-hidden vzb-tooltip-mobile"></div> </div> ';root.document.body.appendChild(s);}).call(this);(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'bubblemap.html');s.innerHTML = ' <div class="vzb-bubblemap"> <svg class="vzb-bmc-map-background"> <g class="vzb-bmc-map-graph"></g> </svg> <svg class="vzb-bubblemap-svg"> <g class="vzb-bmc-graph"> <g class="vzb-bmc-year"></g> <g class="vzb-bmc-lines"></g> <g class="vzb-bmc-bubbles"></g> <g class="vzb-bmc-labels"></g> <g class="vzb-bmc-bubble-labels"></g> <g class="vzb-bmc-axis-y-title"> <text></text> </g> <g class="vzb-bmc-axis-c-title"> <text></text> </g> <g class="vzb-bmc-axis-info"> </g> <g class="vzb-data-warning"> <svg></svg> <text></text> </g> <g class="vzb-bmc-tooltip vzb-hidden"> <rect class="vzb-bmc-tooltip-border"></rect> <text class="vzb-bmc-tooltip-text"></text> </g> </g> </svg> </div> ';root.document.body.appendChild(s);}).call(this);(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'linechart.html');s.innerHTML = ' <div class="vzb-linechart"> <svg class="vzb-lc-graph"> <g> <g class="vzb-lc-axis-x"></g> <text class="vzb-lc-axis-x-value"></text> <text class="vzb-lc-axis-y-value"></text> <svg class="vzb-lc-lines"></svg> <g class="vzb-lc-axis-y"></g> <line class="vzb-lc-projection-x"></line> ; <line class="vzb-lc-projection-y"></line> ; <g class="vzb-lc-labels"> <line class="vzb-lc-vertical-now"></line> ; </g> <g class="vzb-lc-axis-y-title"></g> <g class="vzb-lc-axis-x-title"></g> </g>  </svg> <div class="vzb-tooltip vzb-hidden"></div> </div> ';root.document.body.appendChild(s);}).call(this);(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'mountainchart.html');s.innerHTML = ' <div class="vzb-mountainchart"> <svg class="vzb-mountainchart-svg"> <g class="vzb-mc-graph"> <rect class="vzb-mc-eventarea"></rect> <g class="vzb-mc-year"></g> <g class="vzb-mc-mountains-mergestacked"></g> <g class="vzb-mc-mountains-mergegrouped"></g> <g class="vzb-mc-mountains"></g> <g class="vzb-mc-mountains-labels"></g> <g class="vzb-mc-axis-y-title"> <text></text> </g> <g class="vzb-mc-axis-x-title"> <text></text> </g> <g class="vzb-mc-axis-info"> </g> <g class="vzb-data-warning"> <svg></svg> <text></text> </g> <g class="vzb-mc-axis-x"></g> <g class="vzb-mc-axis-labels"></g> <g class="vzb-mc-probe"> <text class="vzb-shadow vzb-mc-probe-value-ul"></text> <text class="vzb-shadow vzb-mc-probe-value-ur"></text> <text class="vzb-shadow vzb-mc-probe-value-dl"></text> <text class="vzb-shadow vzb-mc-probe-value-dr"></text> <text class="vzb-mc-probe-value-ul"></text> <text class="vzb-mc-probe-value-ur"></text> <text class="vzb-mc-probe-value-dl"></text> <text class="vzb-mc-probe-value-dr"></text> <text class="vzb-mc-probe-extremepoverty"></text> <line></line> </g> <g class="vzb-mc-tooltip vzb-hidden"> <rect class="vzb-bc-tooltip-border"></rect> <text class="vzb-bc-tooltip-text"></text> </g> </g> </svg> </div> ';root.document.body.appendChild(s);}).call(this);(function() {var root = this;var s = root.document.createElement('script');s.type = 'text/template';s.setAttribute('id', 'popbyage.html');s.innerHTML = ' <svg class="vzb-popbyage"> <g class="vzb-bc-header"> <text class="vzb-bc-title"></text> <text class="vzb-bc-year"></text> </g> <g class="vzb-bc-graph"> <g class="vzb-bc-bars"></g> <g class="vzb-bc-labels"></g> <text class="vzb-bc-axis-y-title"></text> <g class="vzb-bc-axis-x"></g> <g class="vzb-bc-axis-y"></g> <g class="vzb-bc-axis-labels">  </g> </g> </svg> ';root.document.body.appendChild(s);}).call(this);