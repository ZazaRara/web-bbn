<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import = "java.text.SimpleDateFormat"%>
<%@page import="org.ikin.bbn.*"%>

<%
String q = request.getParameter("q");
String k = request.getParameter("k");

 koneksiDatabase kb = new koneksiDatabase();
 Connection kon = kb.getKoneksi();
 

 String a = "SELECT "+q+" as nama, COALESCE([2012], 0) as n2012,COALESCE([2013], 0) as n2013,"
         + "COALESCE([2014], 0) as n2014,COALESCE([2015], 0) as n2015,"
         + "COALESCE([2016], 0) as n2016 FROM ( "
         + "SELECT a."+q+", (a.biaya_notis + COALESCE(b.selisih,0)) as biaya, YEAR(a.tgl_kirim) as tahun "
         + "FROM bbn.v_detail_kirim_uang_asli a "
         + "LEFT JOIN temp.kirim_uang_notis b ON b.no_unik = a.NO_UNIK WHERE nama_rider IS NOT NULL"
         + ") src "
         + "PIVOT ("
         + "SUM(biaya) FOR tahun IN ([2012], [2013], [2014], [2015], [2016])) AS pvt ORDER BY "+q;

  if(k.equals("unit")){
      a = "SELECT "+q+" as nama, COALESCE([2012], 0) as n2012,COALESCE([2013], 0) as n2013,"
         + "COALESCE([2014], 0) as n2014,COALESCE([2015], 0) as n2015,"
         + "COALESCE([2016], 0) as n2016 FROM ( "
         + "SELECT a."+q+", (a.biaya_notis) as biaya, YEAR(a.tgl_kirim) as tahun "
         + "FROM bbn.v_detail_kirim_uang_asli a "
         + "WHERE nama_rider IS NOT NULL"
         + ") src "
         + "PIVOT ("
         + "count(biaya) FOR tahun IN ([2012], [2013], [2014], [2015], [2016])) AS pvt ORDER BY "+q;
  } else if(k.equals("rider")){
      a = "SELECT "+q+" as nama, COALESCE([2012], 0) as n2012,COALESCE([2013], 0) as n2013,"
         + "COALESCE([2014], 0) as n2014,COALESCE([2015], 0) as n2015,"
         + "COALESCE([2016], 0) as n2016 FROM ( "
         + "SELECT a."+q+", (a.biaya_rider) as biaya, YEAR(a.tgl_kirim) as tahun "
         + "FROM bbn.v_detail_kirim_uang_asli a WHERE nama_rider IS NOT NULL"
         + ") src "
         + "PIVOT ("
         + "SUM(biaya) FOR tahun IN ([2012], [2013], [2014], [2015], [2016])) AS pvt ORDER BY "+q;
  } else if(k.equals("tagihan")){
      a = "SELECT "+q+" as nama, COALESCE([2012], 0) as n2012,COALESCE([2013], 0) as n2013,"
         + "COALESCE([2014], 0) as n2014,COALESCE([2015], 0) as n2015,"
         + "COALESCE([2016], 0) as n2016 FROM ( "
         + "SELECT a."+q+", (a.biaya_arista) as biaya, YEAR(a.tgl_kirim) as tahun "
         + "FROM bbn.v_detail_kirim_uang_asli a WHERE nama_rider IS NOT NULL"
         + ") src "
         + "PIVOT ("
         + "SUM(biaya) FOR tahun IN ([2012], [2013], [2014], [2015], [2016])) AS pvt ORDER BY "+q;
  } else if(k.equals("rider_all")){
      a = "SELECT "+q+" as nama, COALESCE([2012], 0) as n2012,COALESCE([2013], 0) as n2013,"
         + "COALESCE([2014], 0) as n2014,COALESCE([2015], 0) as n2015,"
         + "COALESCE([2016], 0) as n2016 FROM ( "
         + "SELECT a."+q+", (a.biaya_notis + COALESCE(b.selisih,0)+a.biaya_rider) as biaya, YEAR(a.tgl_kirim) as tahun "
         + "FROM bbn.v_detail_kirim_uang_asli a "
         + "LEFT JOIN temp.kirim_uang_notis b ON b.no_unik = a.NO_UNIK WHERE nama_rider IS NOT NULL"
         + ") src "
         + "PIVOT ("
         + "sum(biaya) FOR tahun IN ([2012], [2013], [2014], [2015], [2016])) AS pvt ORDER BY "+q;
  } else if(k.equals("tagihan_all")){
      a = "SELECT "+q+" as nama, COALESCE([2012], 0) as n2012,COALESCE([2013], 0) as n2013,"
         + "COALESCE([2014], 0) as n2014,COALESCE([2015], 0) as n2015,"
         + "COALESCE([2016], 0) as n2016 FROM ( "
         + "SELECT a."+q+", (a.biaya_notis + COALESCE(b.selisih,0)+a.biaya_arista) as biaya, YEAR(a.tgl_kirim) as tahun "
         + "FROM bbn.v_detail_kirim_uang_asli a "
         + "LEFT JOIN temp.kirim_uang_notis b ON b.no_unik = a.NO_UNIK WHERE nama_rider IS NOT NULL"
         + ") src "
         + "PIVOT ("
         + "sum(biaya) FOR tahun IN ([2012], [2013], [2014], [2015], [2016])) AS pvt ORDER BY "+q;
  }
 
 String js = kb.getDataJSONFromSQL(kon, a);

 out.print( "["+js+"]" );
out.flush();

%>
