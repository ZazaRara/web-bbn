<%@page import="org.apache.poi.hssf.usermodel.*" %>
<%@page import="org.apache.poi.ss.usermodel.*" %>
<%@page import="java.io.*" %>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="org.ikin.bbn.*"%>
<%@page import="java.text.*"%>



<%
String q = request.getParameter("q");
String k = request.getParameter("k");
String qq = request.getParameter("qq");
String kk = request.getParameter("kk");

ikin.org.apa.SQL xx = new ikin.org.apa.SQL();
koneksiDatabase kb = new koneksiDatabase();
Connection kon = kb.getKoneksi();
Connection con = kb.getKoneksiLO();


DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
Calendar cal = Calendar.getInstance();
String tgl = dateFormat.format(cal.getTime());
	   
// create a small spreadsheet
HSSFWorkbook wb = new HSSFWorkbook();
HSSFSheet sheet = wb.createSheet();
HSSFRow row = sheet.createRow(0);
HSSFCell cell = row.createCell(0);
HSSFCell cjudul = null;

CellStyle cs = wb.createCellStyle();
CellStyle cs2 = wb.createCellStyle();
CellStyle cs3 = wb.createCellStyle();
Font f = wb.createFont();
Font f2 = wb.createFont();
Font f3 = wb.createFont();
//set font 1 to 12 point type
f.setFontHeightInPoints((short) 14);
f2.setFontHeightInPoints((short) 10);
f3.setFontHeightInPoints((short) 10);
// make it bold
//arial is the default font
f.setBoldweight(Font.BOLDWEIGHT_BOLD);
f3.setBoldweight(Font.BOLDWEIGHT_BOLD);
cs.setFont(f);
cs2.setFont(f2);
cs3.setFont(f3);

cell.setCellStyle(cs);
cell.setCellValue(qq);
row = sheet.createRow(1);
cell = row.createCell(0);
cell.setCellStyle(cs);
cell.setCellValue(kk);

String data[][] = {{"NO", "2"},
    {"Keterangan", "15"},
    {"2012", "8"},
   {"2013", "8"},
    {"2014", "8"},
   {"2015", "8"},
   {"2016", "8"}
                  };
kb.createHeaderTabelExcel(data, sheet, row, cjudul, cs3, 3);

String a = "SELECT "+q+" as nama, COALESCE([2012], 0) as n2012,COALESCE([2013], 0) as n2013,"
         + "COALESCE([2014], 0) as n2014,COALESCE([2015], 0) as n2015,"
         + "COALESCE([2016], 0) as n2016 FROM ( "
         + "SELECT a."+q+", (a.biaya_notis + COALESCE(b.selisih,0)) as biaya, YEAR(a.tgl_kirim) as tahun "
         + "FROM bbn.v_detail_kirim_uang_asli a "
         + "LEFT JOIN temp.kirim_uang_notis b ON b.no_unik = a.NO_UNIK WHERE nama_rider IS NOT NULL"
         + ") src "
         + "PIVOT ("
         + "SUM(biaya) FOR tahun IN ([2012], [2013], [2014], [2015], [2016])) AS pvt ORDER BY "+q;

  if(k.equals("unit")){
      a = "SELECT "+q+" as nama, COALESCE([2012], 0) as n2012,COALESCE([2013], 0) as n2013,"
         + "COALESCE([2014], 0) as n2014,COALESCE([2015], 0) as n2015,"
         + "COALESCE([2016], 0) as n2016 FROM ( "
         + "SELECT a."+q+", (a.biaya_notis) as biaya, YEAR(a.tgl_kirim) as tahun "
         + "FROM bbn.v_detail_kirim_uang_asli a "
         + "WHERE nama_rider IS NOT NULL"
         + ") src "
         + "PIVOT ("
         + "count(biaya) FOR tahun IN ([2012], [2013], [2014], [2015], [2016])) AS pvt ORDER BY "+q;
  } else if(k.equals("rider")){
      a = "SELECT "+q+" as nama, COALESCE([2012], 0) as n2012,COALESCE([2013], 0) as n2013,"
         + "COALESCE([2014], 0) as n2014,COALESCE([2015], 0) as n2015,"
         + "COALESCE([2016], 0) as n2016 FROM ( "
         + "SELECT a."+q+", (a.biaya_rider) as biaya, YEAR(a.tgl_kirim) as tahun "
         + "FROM bbn.v_detail_kirim_uang_asli a WHERE nama_rider IS NOT NULL"
         + ") src "
         + "PIVOT ("
         + "SUM(biaya) FOR tahun IN ([2012], [2013], [2014], [2015], [2016])) AS pvt ORDER BY "+q;
  } else if(k.equals("tagihan")){
      a = "SELECT "+q+" as nama, COALESCE([2012], 0) as n2012,COALESCE([2013], 0) as n2013,"
         + "COALESCE([2014], 0) as n2014,COALESCE([2015], 0) as n2015,"
         + "COALESCE([2016], 0) as n2016 FROM ( "
         + "SELECT a."+q+", (a.biaya_arista) as biaya, YEAR(a.tgl_kirim) as tahun "
         + "FROM bbn.v_detail_kirim_uang_asli a WHERE nama_rider IS NOT NULL"
         + ") src "
         + "PIVOT ("
         + "SUM(biaya) FOR tahun IN ([2012], [2013], [2014], [2015], [2016])) AS pvt ORDER BY "+q;
  } else if(k.equals("rider_all")){
      a = "SELECT "+q+" as nama, COALESCE([2012], 0) as n2012,COALESCE([2013], 0) as n2013,"
         + "COALESCE([2014], 0) as n2014,COALESCE([2015], 0) as n2015,"
         + "COALESCE([2016], 0) as n2016 FROM ( "
         + "SELECT a."+q+", (a.biaya_notis + COALESCE(b.selisih,0)+a.biaya_rider) as biaya, YEAR(a.tgl_kirim) as tahun "
         + "FROM bbn.v_detail_kirim_uang_asli a "
         + "LEFT JOIN temp.kirim_uang_notis b ON b.no_unik = a.NO_UNIK WHERE nama_rider IS NOT NULL"
         + ") src "
         + "PIVOT ("
         + "sum(biaya) FOR tahun IN ([2012], [2013], [2014], [2015], [2016])) AS pvt ORDER BY "+q;
  } else if(k.equals("tagihan_all")){
      a = "SELECT "+q+" as nama, COALESCE([2012], 0) as n2012,COALESCE([2013], 0) as n2013,"
         + "COALESCE([2014], 0) as n2014,COALESCE([2015], 0) as n2015,"
         + "COALESCE([2016], 0) as n2016 FROM ( "
         + "SELECT a."+q+", (a.biaya_notis + COALESCE(b.selisih,0)+a.biaya_arista) as biaya, YEAR(a.tgl_kirim) as tahun "
         + "FROM bbn.v_detail_kirim_uang_asli a "
         + "LEFT JOIN temp.kirim_uang_notis b ON b.no_unik = a.NO_UNIK WHERE nama_rider IS NOT NULL"
         + ") src "
         + "PIVOT ("
         + "sum(biaya) FOR tahun IN ([2012], [2013], [2014], [2015], [2016])) AS pvt ORDER BY "+q;
  }
kb.setTabelExcel(kon, a, sheet, row, cell, 4 );

// write it as an excel attachment
ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
wb.write(outByteStream);
byte [] outArray = outByteStream.toByteArray();
response.setContentType("application/ms-excel");
response.setContentLength(outArray.length);
response.setHeader("Expires:", "0"); // eliminates browser caching
response.setHeader("Content-Disposition", "attachment; filename=Rekap Pengajuan BBN "+qq+kk+"per tanggal "+tgl+".xls");
OutputStream outStream = response.getOutputStream();
outStream.write(outArray);
outStream.flush();

%>