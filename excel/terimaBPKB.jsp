<%@page import="org.apache.poi.hssf.usermodel.*" %>
<%@page import="org.apache.poi.ss.usermodel.*" %>
<%@page import="java.io.*" %>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="org.ikin.bbn.*"%>
<%@page import="java.text.*"%>



<%
String q = request.getParameter("q");
String k = request.getParameter("k");
String y = request.getParameter("y");

ikin.org.apa.SQL xx = new ikin.org.apa.SQL();
koneksiDatabase kb = new koneksiDatabase();
Connection kon = kb.getKoneksi();
Connection con = kb.getKoneksiLO();


DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
Calendar cal = Calendar.getInstance();
String tgl = dateFormat.format(cal.getTime());
	   
// create a small spreadsheet
HSSFWorkbook wb = new HSSFWorkbook();
HSSFSheet sheet = wb.createSheet();
HSSFRow row = sheet.createRow(0);
HSSFCell cell = row.createCell(0);
HSSFCell cjudul = null;

CellStyle cs = wb.createCellStyle();
CellStyle cs2 = wb.createCellStyle();
CellStyle cs3 = wb.createCellStyle();
Font f = wb.createFont();
Font f2 = wb.createFont();
Font f3 = wb.createFont();
//set font 1 to 12 point type
f.setFontHeightInPoints((short) 14);
f2.setFontHeightInPoints((short) 10);
f3.setFontHeightInPoints((short) 10);
// make it bold
//arial is the default font
f.setBoldweight(Font.BOLDWEIGHT_BOLD);
f3.setBoldweight(Font.BOLDWEIGHT_BOLD);
cs.setFont(f);
cs2.setFont(f2);
cs3.setFont(f3);

cell.setCellStyle(cs);
cell.setCellValue("LAPORAN TERIMA BPKB");
row = sheet.createRow(1);
cell = row.createCell(0);
cell.setCellStyle(cs);
cell.setCellValue("Tanggal Unduh : "+tgl);

String data[][] = {{"NO", "2"},
    {"NAMA KONSUMEN", "12"},
    {"NO. RANGKA", "8"},
    {"NO. MESIN", "4"},
    {"PLAT", "3"},
    {"NO. BPKB", "4"},
    {"TGL CEKLIST", "5"}
                  };
kb.createHeaderTabelExcel(data, sheet, row, cjudul, cs3, 3);

String sql = "SELECT nama, no_rangka, no_mesin, nopol, no_bpkb, tgl_cek_bpkb FROM bbn.v_terima_bpkb WHERE tgl_cek_bpkb >= '"+q+"' AND tgl_cek_bpkb <= '"+k+"' AND kode_cabang = '"+y+"' ORDER BY tgl_cek_bpkb, nama";

kb.setTabelExcel(kon, sql, sheet, row, cell, 4 );

// write it as an excel attachment
ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
wb.write(outByteStream);
byte [] outArray = outByteStream.toByteArray();
response.setContentType("application/ms-excel");
response.setContentLength(outArray.length);
response.setHeader("Expires:", "0"); // eliminates browser caching
response.setHeader("Content-Disposition", "attachment; filename=Laporan Terima BPKB "+tgl+".xls");
OutputStream outStream = response.getOutputStream();
outStream.write(outArray);
outStream.flush();

%>