var bbn = (function () {

    this.getMenu = function (callback, url, ev) {

        $.ajax({
            type: "GET",
            url: "/home/data/getMenu.jsp?user=" + url + "&q=" + ev,
            dataType: "json"

        }).done(function (data) {

            //callback(data);    

            var l, le = 0, lx, h;

            $.each(data, function (i, item) {


                l = Object.keys(item.sub).length;
                if (le < l) {
                    le = l;
                }

                //   $('#menu-samping').append('<li><a><i class="fa fa-bar-chart-o"></i> '+item.name+' <span class="fa fa-chevron-down"></span></a><ul class="nav child_menu" style="display: none;"><div id="menu_'+i+'"></div></ul></li>');

                $.each(item.sub, function (x, submenu) {
                    //  console.log('item'+i, Object.keys(item.sub[i]).length); 

                    if (submenu.namesub.startsWith("0")) {

                        $('#' + item.name).append('<li>' +
                                '<a href="' + submenu.url + '" target="_blank">' +
                                submenu.namesub.replace("0", "") + '</a></li>');

                    } else if (submenu.namesub.startsWith("1")) {

                        $('#' + item.name).append('<li>' +
                                '<a href="#" id="menu_' + i + '_' + x + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                                submenu.namesub.replace("1", "") + '</a></li>');
                        //'+submenu.url+'
                        $('#' + item.name).on("click", "#menu_" + i + "_" + x, function () {
                            bbn.showContent('konten', submenu.url, submenu.namesub);
                        })
                    } else {

                        $('#' + item.name).append('<li>' +
                                '<a href="#" id="menu_' + i + '_' + x + '">' +
                                submenu.namesub + '</a></li>');
                        //'+submenu.url+'
                        $('#' + item.name).on("click", "#menu_" + i + "_" + x, function () {
                            bbn.showContent('konten', submenu.url, submenu.namesub);
                        })
                    }
                })
            })
        });
    }
    this.tebelKirimUang = function (id, tgl_dari, tgl_mulai, filter, data) {


        $("#" + id).empty();
        $('#' + id).append("<tr><td colspan='6'><img src='img/loading.gif' height='100px' /><br /><strong>Sedang memuat...</strong></td></tr>");
        $.ajax({
            type: "GET",
            url: "/home/data/kirimUang.jsp?q=" + tgl_dari + "&k=" + tgl_mulai + "&a=" + filter + "&b=" + data,
            dataType: "json",
        }).done(function (data) {
            $("#" + id).empty();
            if (data.length == 0) {
                $('#' + id).append("<tr><td colspan='6'>DATA TIDAK DITEMUKAN</td></tr>");
            } else {
                var unit = 0;
                var rider = 0;
                var proses = 0;
                $.each(data, function (data, n) {
                    unit += parseInt(n.jml_unit);
                    rider += parseInt(n.biaya_rider);
                    proses += parseInt(n.biaya_proses);

                    $('#' + id).append(
                            '<tr role="row" class="abu-muda" data-id="' + n.no + '">' +
                            '<td style="text-align: center">' + n.no +
                            '</td><td style="text-align: left">' + n.filter +
                            '</td><td style="text-align: center">' + n.tahun +
                            '</td><td style="text-align: right">' + toRp(n.jml_unit) +
                            '</td><td style="text-align: right">' + toRp(n.biaya_rider) +
                            '</td><td style="text-align: right">' + toRp(n.biaya_proses) +
                            "</td></tr>"
                            );

                });

                $('#' + id).append(
                        '<tr role="row" class="abu-muda">' +
                        '<td style="text-align: center">' +
                        '</td><td style="text-align: left">' +
                        '</td><td style="text-align: center">' +
                        '</td><td style="text-align: right"><b>' + toRp(unit) + '</b>' +
                        '</td><td style="text-align: right"><b>' + toRp(rider) + '</b>' +
                        '</td><td style="text-align: right"><b>' + toRp(proses) + '</b>' +
                        "</td></tr>"
                        );
            }
//alert('JSON load SUKSES');
        }).fail(function () {
            $("#" + id).empty();
            $('#' + id).append("<tr><td colspan='6'>ERROR LOADING</td></tr>");
            //      alert('Error Loading');
        });
    }
    this.tebelKirimUangNotice = function (id, tgl_dari, tgl_mulai, filter, data) {


        $("#" + id).empty();
        $('#' + id).append("<tr><td colspan='6'><img src='img/loading.gif' height='100px' /><br /><strong>Sedang memuat...</strong></td></tr>");
        $.ajax({
            type: "GET",
            url: "/home/data/kirimUangNotice.jsp?q=" + tgl_dari + "&k=" + tgl_mulai + "&a=" + filter + "&b=" + data,
            dataType: "json",
        }).done(function (data) {
            $("#" + id).empty();
            if (data.length == 0) {
                $('#' + id).append("<tr><td colspan='6'>DATA TIDAK DITEMUKAN</td></tr>");
            } else {
                var unit = 0;
                var rider = 0;
                var proses = 0;
                $.each(data, function (data, n) {
                    unit += parseInt(n.jml_unit);
                    rider += parseInt(n.biaya_notis);


                    $('#' + id).append(
                            '<tr role="row" class="abu-muda" data-id="' + n.no + '">' +
                            '<td style="text-align: center">' + n.no +
                            '</td><td style="text-align: left">' + n.filter +
                            '</td><td style="text-align: center">' + n.tahun +
                            '</td><td style="text-align: right">' + toRp(n.jml_unit) +
                            '</td><td style="text-align: right">' + toRp(n.biaya_notis) +
                            "</td></tr>"
                            );

                });

                $('#' + id).append(
                        '<tr role="row" class="abu-muda">' +
                        '<td style="text-align: center">' +
                        '</td><td style="text-align: left">' +
                        '</td><td style="text-align: center">' +
                        '</td><td style="text-align: right"><b>' + toRp(unit) + '</b>' +
                        '</td><td style="text-align: right"><b>' + toRp(rider) + '</b>' +
                        "</td></tr>"
                        );
            }
//alert('JSON load SUKSES');
        }).fail(function () {
            $("#" + id).empty();
            $('#' + id).append("<tr><td colspan='6'>ERROR LOADING</td></tr>");
            //      alert('Error Loading');
        });
    }
    this.tebelKirimUangProgresif = function (id, tgl_dari, tgl_mulai, filter, data) {


        $("#" + id).empty();
        $('#' + id).append("<tr><td colspan='6'><img src='img/loading.gif' height='100px' /><br /><strong>Sedang memuat...</strong></td></tr>");
        $.ajax({
            type: "GET",
            url: "/home/data/kirimUangProgresif.jsp?q=" + tgl_dari + "&k=" + tgl_mulai + "&a=" + filter + "&b=" + data,
            dataType: "json",
        }).done(function (data) {
            $("#" + id).empty();
            if (data.length == 0) {
                $('#' + id).append("<tr><td colspan='6'>DATA TIDAK DITEMUKAN</td></tr>");
            } else {
                var unit = 0;
                var rider = 0;
                var proses = 0;
                $.each(data, function (data, n) {
                    unit += parseInt(n.jml_unit);
                    rider += parseInt(n.transfer);


                    $('#' + id).append(
                            '<tr role="row" class="abu-muda" data-id="' + n.no + '">' +
                            '<td style="text-align: center">' + n.no +
                            '</td><td style="text-align: left">' + n.filter +
                            '</td><td style="text-align: center">' + n.tahun +
                            '</td><td style="text-align: right">' + toRp(n.jml_unit) +
                            '</td><td style="text-align: right">' + toRp(n.transfer) +
                            "</td></tr>"
                            );

                });

                $('#' + id).append(
                        '<tr role="row" class="abu-muda">' +
                        '<td style="text-align: center">' +
                        '</td><td style="text-align: left">' +
                        '</td><td style="text-align: center">' +
                        '</td><td style="text-align: right"><b>' + toRp(unit) + '</b>' +
                        '</td><td style="text-align: right"><b>' + toRp(rider) + '</b>' +
                        "</td></tr>"
                        );
            }
//alert('JSON load SUKSES');
        }).fail(function () {
            $("#" + id).empty();
            $('#' + id).append("<tr><td colspan='6'>ERROR LOADING</td></tr>");
            //      alert('Error Loading');
        });
    }

    this.tebelKirimUangJasa = function (id, tgl_dari, tgl_mulai, filter, data) {


        $("#" + id).empty();
        $('#' + id).append("<tr><td colspan='6'><img src='img/loading.gif' height='100px' /><br /><strong>Sedang memuat...</strong></td></tr>");
        $.ajax({
            type: "GET",
            url: "/home/data/kirimUangJasa.jsp?q=" + tgl_dari + "&k=" + tgl_mulai + "&a=" + filter + "&b=" + data,
            dataType: "json",
        }).done(function (data) {
            $("#" + id).empty();
            if (data.length == 0) {
                $('#' + id).append("<tr><td colspan='6'>DATA TIDAK DITEMUKAN</td></tr>");
            } else {
                var unit = 0;
                var rider = 0;
                var proses = 0;
                $.each(data, function (data, n) {
                    unit += parseInt(n.jml_unit);
                    rider += parseInt(n.biaya_jasa);


                    $('#' + id).append(
                            '<tr role="row" class="abu-muda" data-id="' + n.no + '">' +
                            '<td style="text-align: center">' + n.no +
                            '</td><td style="text-align: left">' + n.filter +
                            '</td><td style="text-align: center">' + n.tahun +
                            '</td><td style="text-align: right">' + toRp(n.jml_unit) +
                            '</td><td style="text-align: right">' + toRp(n.biaya_jasa) +
                            "</td></tr>"
                            );

                });

                $('#' + id).append(
                        '<tr role="row" class="abu-muda">' +
                        '<td style="text-align: center">' +
                        '</td><td style="text-align: left">' +
                        '</td><td style="text-align: center">' +
                        '</td><td style="text-align: right"><b>' + toRp(unit) + '</b>' +
                        '</td><td style="text-align: right"><b>' + toRp(rider) + '</b>' +
                        "</td></tr>"
                        );
            }
//alert('JSON load SUKSES');
        }).fail(function () {
            $("#" + id).empty();
            $('#' + id).append("<tr><td colspan='6'>ERROR LOADING</td></tr>");
            //      alert('Error Loading');
        });
    }
    this.tebelResumeTagihanLastFive = function (id) {


        $("#" + id).empty();
        $('#' + id).append("<img src='img/loading.gif' height='100px' /><br /><strong>Sedang memuat...</strong>");
        $.ajax({
            type: "GET",
            url: "/home/data/resumeTagihanLast5.jsp",
            dataType: "json",
        }).done(function (data) {
            $("#" + id).empty();
            if (data.length == 0) {
                $('#' + id).append("DATA TIDAK DITEMUKAN");
            } else {

                $.each(data, function (data, n) {

                    $('#' + id).append(
                            '<li class="media event">' +
                            '<a class="pull-left border-aero profile_thumb"><i class="fa fa-user aero"></i></a>' +
                            '<div class="media-body">' +
                            '<a class="title" href="#">Biaya : ' + toRp(n.biaya_notice) + '</a>' +
                            '<p>Jasa : ' + toRp(n.jasa) + '</p>' +
                            '<p>Cabang : ' + toRp(n.jml_cabang) + ' -- Unit : ' + toRp(n.unit) + '</p>' +
                            '</div>' +
                            '</li>'
                            );
                });


            }
//alert('JSON load SUKSES');
        }).fail(function () {
            $("#" + id).empty();
            $('#' + id).append("ERROR LOADING");
            //      alert('Error Loading');
        });
    }

    this.tebelMonitoringTagihan = function (id, data) {


        $("#" + id).empty();
        $('#' + id).append("<tr><td colspan='6'><img src='img/loading.gif' height='100px' /><br /><strong>Sedang memuat...</strong></td></tr>");
        $.ajax({
            type: "GET",
            url: "/home/data/monitoringTagihan.jsp?q=" + data,
            dataType: "json",
        }).done(function (data) {
            $("#" + id).empty();
            if (data.length == 0) {
                $('#' + id).append("<tr><td colspan='7'>DATA TIDAK DITEMUKAN</td></tr>");
            } else {
                var unit = 0;
                var rider = 0;
                var proses = 0;
                $.each(data, function (data, n) {
                    //  unit += parseInt(n.jml_unit);
                    //   rider += parseInt(n.transfer);


                    $('#' + id).append(
                            '<tr role="row" class="abu-muda" data-id="' + n.no + '">' +
                            '<td style="text-align: center">' + n.no +
                            '</td><td style="text-align: left">' + n.nama +
                            '</td><td style="text-align: center">' + toRp(n.proses_cabang) +
                            '</td><td style="text-align: center">' + toRp(n.proses_samsat) +
                            '</td><td style="text-align: center; color:red;"><b>' + toRp(n.proses_cek) + '</b>' +
                            '</td><td style="text-align: center">' + toRp(n.siap) +
                            '</td><td style="text-align: center"><a href="/lfserver/Monitoring_Tagihan?kode_cabang=' + n.kode_cabang + '" target="_blank" >Detail</a>' +
                            "</td></tr>"
                            );

                });


            }
//alert('JSON load SUKSES');
        }).fail(function () {
            $("#" + id).empty();
            $('#' + id).append("<tr><td colspan='7'>ERROR LOADING</td></tr>");
            //      alert('Error Loading');
        });
    }

    this.tebelBerkelit = function (id, data) {


        $("#" + id).empty();
        $('#' + id).append("<tr><td colspan='6'><img src='img/loading.gif' height='100px' /><br /><strong>Sedang memuat...</strong></td></tr>");
        $.ajax({
            type: "GET",
            url: "/home/data/monitoringBerkelit.jsp?q=" + data,
            dataType: "json",
        }).done(function (data) {
            $("#" + id).empty();
            if (data.length == 0) {
                $('#' + id).append("<tr><td colspan='7'>DATA TIDAK DITEMUKAN</td></tr>");
            } else {
                var unit = 0;
                var rider = 0;
                var proses = 0;
                $.each(data, function (data, n) {
                    //  unit += parseInt(n.jml_unit);
                    //   rider += parseInt(n.transfer);


                    $('#' + id).append(
                            '<tr role="row" class="abu-muda" data-id="' + n.no + '">' +
                            '<td style="text-align: center">' + n.no +
                            '<td style="text-align: left">' + n.no_rangka +
                            '</td><td style="text-align: left">' + n.nama +
                            '</td><td style="text-align: left">' + n.no_surat_tugas +
                            '</td><td style="text-align: left">' + n.kode_rider +
                            '</td><td style="text-align: left">' + n.kode_cabang +
                            '</td><td style="text-align: left">' + n.kode_samsat +
                            '</td><td style="text-align: left">' + n.alasan +
                            "</td></tr>"
                            );

                });


            }
//alert('JSON load SUKSES');
        }).fail(function () {
            $("#" + id).empty();
            $('#' + id).append("<tr><td colspan='7'>ERROR LOADING</td></tr>");
            //      alert('Error Loading');
        });
    }

    this.tebelHargaNotice = function (id, data) {


        $("#" + id).empty();
        $('#' + id).append("<tr><td colspan='12'><img src='img/loading.gif' height='100px' /><br /><strong>Sedang memuat...</strong></td></tr>");
        $.ajax({
            type: "GET",
            url: "/home/data/hargaNotice.jsp?q=" + data,
            dataType: "json",
        }).done(function (data) {
            $("#" + id).empty();
            if (data.length == 0) {
                $('#' + id).append("<tr><td colspan='12'>DATA TIDAK DITEMUKAN</td></tr>");
            } else {
                var unit = 0;
                var rider = 0;
                var proses = 0;
                $.each(data, function (data, n) {
                    //  unit += parseInt(n.jml_unit);
                    //   rider += parseInt(n.transfer);


                    $('#' + id).append(
                            '<tr role="row" class="abu-muda" data-id="' + n.no + '">' +
                            '<td style="text-align: center">' + n.no +
                            '<td style="text-align: left">' + n.KODE_KENDARAAN +
                            '</td><td style="text-align: left">' + n.NAMA_KENDARAAN +
                            '</td><td style="text-align: center">' + n.TYPE +
                            '</td><td style="text-align: center">' + toRp(n.BIAYA_NOTIS) +
                            '</td><td style="text-align: center">' + toRp(n.prog_2) +
                            '</td><td style="text-align: center">' + toRp(n.prog_3) +
                            '</td><td style="text-align: center">' + toRp(n.prog_4) +
                            '</td><td style="text-align: center">' + toRp(n.prog_5) +
                            "</td></tr>"
                            );

                });


            }
//alert('JSON load SUKSES');
        }).fail(function () {
            $("#" + id).empty();
            $('#' + id).append("<tr><td colspan='12'>ERROR LOADING</td></tr>");
            //      alert('Error Loading');
        });
    }
    this.tabelPerformaCabang = function (id, q, k) {


        $("#" + id).empty();
        $('#' + id).append("<tr><td colspan='3'><img src='img/loading.gif' height='100px' /><br /><strong>Sedang memuat...</strong></td></tr>");
        $.ajax({
            type: "GET",
            url: "/home/data/detailPerformaCabang.jsp?q=" + q + "&k=" + k,
            dataType: "json",
        }).done(function (data) {
            $("#" + id).empty();
            if (data.length == 0) {
                $('#' + id).append("<tr><td colspan='3'>DATA TIDAK DITEMUKAN</td></tr>");
            } else {

                $.each(data, function (data, n) {
                    //  unit += parseInt(n.jml_unit);
                    //   rider += parseInt(n.transfer);


                    $('#' + id).append(
                            '<tr role="row" class="abu-muda" data-id="' + n.no + '">' +
                            '<td style="text-align: center">' + n.no +
                            '</td><td style="text-align: left">' + n.nama +
                            '</td><td style="text-align: center">' + toRp(n.jml) +
                            '</td></tr>'
                            );

                });


            }
//alert('JSON load SUKSES');
        }).fail(function () {
            $("#" + id).empty();
            $('#' + id).append("<tr><td colspan='7'>ERROR LOADING</td></tr>");
            //      alert('Error Loading');
        });
    }

    this.tabelDetailPengajuanCabang = function (id, q, k) {


        $("#" + id).empty();
        $('#' + id).append("<tr><td colspan='3'><img src='img/loading.gif' height='100px' /><br /><strong>Sedang memuat...</strong></td></tr>");
        $.ajax({
            type: "GET",
            url: "/home/data/detailPengajuanCabang.jsp?q=" + q + "&k=" + k,
            dataType: "json",
        }).done(function (data) {
            $("#" + id).empty();
            if (data.length == 0) {
                $('#' + id).append("<tr><td colspan='3'>DATA TIDAK DITEMUKAN</td></tr>");
            } else {

                $.each(data, function (data, n) {
                    $('#' + id).append(
                            '<tr role="row" class="abu-muda" data-id="' + n.no + '">' +
                            '<td style="text-align: center">' + n.no +
                            '</td><td style="text-align: left">' + n.no_surat_tugas +
                            '</td><td style="text-align: left">' + n.nama +
                            '</td><td style="text-align: center">' + toRp(n.transfer) +
                            '</td></tr>'
                            );

                });


            }
//alert('JSON load SUKSES');
        }).fail(function () {
            $("#" + id).empty();
            $('#' + id).append("<tr><td colspan='7'>ERROR LOADING</td></tr>");
            //      alert('Error Loading');
        });
    }
    this.tabelDetailDashboardPengajuanCabang = function (id, q, k) {


        $("#" + id).empty();
        $('#' + id).append("<tr><td colspan='7'><img src='img/loading.gif' height='100px' /><br /><strong>Sedang memuat...</strong></td></tr>");
        $.ajax({
            type: "GET",
            url: "/home/data/detailUnitPengajuanCabang.jsp?q=" + q + "&k=" + k,
            dataType: "json",
        }).done(function (data) {
            $("#" + id).empty();
            if (data.length == 0) {
                $('#' + id).append("<tr><td colspan='7'>DATA TIDAK DITEMUKAN</td></tr>");
            } else {

                $.each(data, function (data, n) {
                    $('#' + id).append(
                            '<tr role="row" class="abu-muda" data-id="' + n.no + '">' +
                            '<td style="text-align: center">' + n.no +
                            '</td><td style="text-align: left">' + n.no_rangka +
                            '</td><td style="text-align: left">' + n.nama +
                            '</td><td style="text-align: left">' + n.no_surat_tugas +
                            //   '</td><td style="text-align: left">' + n.no_faktur +
                            '</td><td style="text-align: left">' + n.nama_cabang +
                            '</td><td style="text-align: left">' + n.nama_samsat +
                            '</td></tr>'
                            );

                });


            }
//alert('JSON load SUKSES');
        }).fail(function () {
            $("#" + id).empty();
            $('#' + id).append("<tr><td colspan='7'>ERROR LOADING</td></tr>");
            //      alert('Error Loading');
        });
    }
    this.tabelDetailDashboardPengajuanCabangRider = function (id, q, k) {


        $("#" + id).empty();
        $('#' + id).append("<tr><td colspan='7'><img src='img/loading.gif' height='100px' /><br /><strong>Sedang memuat...</strong></td></tr>");
        $.ajax({
            type: "GET",
            url: "/home/data/detailUnitPengajuanCabangRider.jsp?q=" + q + "&k=" + k,
            dataType: "json",
        }).done(function (data) {
            $("#" + id).empty();
            if (data.length == 0) {
                $('#' + id).append("<tr><td colspan='7'>DATA TIDAK DITEMUKAN</td></tr>");
            } else {

                $.each(data, function (data, n) {
                    $('#' + id).append(
                            '<tr role="row" class="abu-muda" data-id="' + n.no + '">' +
                            '<td style="text-align: center">' + n.no +
                            '</td><td style="text-align: left">' + n.no_rangka +
                            '</td><td style="text-align: left">' + n.nama +
                            '</td><td style="text-align: left">' + n.no_surat_tugas +
                            //   '</td><td style="text-align: left">' + n.no_faktur +
                            '</td><td style="text-align: left">' + n.nama_cabang +
                            '</td><td style="text-align: left">' + n.nama_samsat +
                            '</td></tr>'
                            );

                });


            }
//alert('JSON load SUKSES');
        }).fail(function () {
            $("#" + id).empty();
            $('#' + id).append("<tr><td colspan='7'>ERROR LOADING</td></tr>");
            //      alert('Error Loading');
        });
    }
    this.kotakMasuk = function (id, q, k, s) {


        $("#" + id).empty();
        $('#' + id).append("<img src='img/loading.gif' height='100px' /><br /><strong>Sedang memuat...</strong>");
        $.ajax({
            type: "GET",
            url: "/home/data/getKotakTugasEvent.jsp?q=" + q + "&k=" + k + "&s=" + s,
            dataType: "json",
        }).done(function (data) {
            $("#" + id).empty();
            if (data.length == 0) {
                $('#' + id).append("TIDAK ADA TUGAS / AKTIFITAS");
            } else {

                $.each(data, function (data, n) {

                    $('#' + id).append(
                            '<li class="media event">' +
                            '<a class="pull-left border-aero profile_thumb"><i class="fa fa-user aero"></i></a>' +
                            '<div class="media-body">' +
                            '<a class="title" href="/lfserver?DFS__Action=RouteGetForm&DFS__EventID=' + n.eventid + '_' + n.STEPCOUNT + '&DFS__DataSource=1&DFS__FormType=crp"><b>' + n.SUBJECT + '</b>' +
                            '<p>Tanggal : ' + n.TGL + '</p>' +
                            '</a>' +
                            '</div>' +
                            '</li>'
                            );
                });


            }
//alert('JSON load SUKSES');
        }).fail(function () {
            $("#" + id).empty();
            $('#' + id).append("ERROR LOADING");
            //      alert('Error Loading');
        });
    }
    this.dataDashboardFinance = function (id, userid) {
        $.ajax({
            type: "GET",
            url: "/home/data/financeDashboard.jsp?q=" + userid,
            dataType: "json"

        }).done(function (data) {

            $('#dashbord_01').append(
                    '<div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">' +
                    '<div class="left"></div>' +
                    '<div class="right">' +
                    '<span class="count_top"><i class="fa fa-calculator"></i> Siap Kirim Uang</span>' +
                    '<div class="count" id="resume_akan">' + toRp(data.resume_akan) + '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">' +
                    '   <div class="left"></div>' +
                    '   <div class="right">' +
                    '       <span class="count_top"><i class="fa fa-calendar"></i> Kirim Uang Kemarin</span>' +
                    '       <div class="count" id="resume_kirim">' + toRp(data.resume_kirim) + '</div>' +
                    '   </div>' +
                    '</div>' +
                    '<div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">' +
                    '<div class="left"></div>' +
                    '<div class="right">' +
                    '<span class="count_top"><i class="fa fa-money"></i> Siap Tagih</span>' +
                    '<div class="count green" id="resume_tagih">' + toRp(data.resume_tagih) + '</div>' +
                    '</div>' +
                    ' </div>' +
                    '<div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">' +
                    '   <div class="left"></div>' +
                    '   <div class="right">' +
                    '       <span class="count_top"><i class="fa fa-clock-o"></i> Proses Samsat</span>' +
                    '      <div class="count red" id="resume_pending">' + toRp(data.resume_pending) + '</div>' +
                    '   </div>' +
                    '</div>'
                    );

            $('#rekap_bawah').append(
                    '<div class="col-md-2 tile animated flipInY">' +
                    '<span>Total Kirim Uang</span>' +
                    '<h2 id="total_unit">' + toRp(data.total_kirim) + '</h2>' +
                    '</div>' +
                    '<div class="col-md-2 tile animated flipInY">' +
                    '   <span>Total Unit Kendaraan</span>' +
                    '   <h2>' + toRp(data.total_unit) + '</h2>' +
                    '</div>' +
                    '<div class="col-md-2 tile animated flipInY">' +
                    '   <span>Total Biaya Notice</span>' +
                    '   <h2>' + toRp(data.total_notice) + '</h2>' +
                    '</div>' +
                    '<div class="col-md-2 tile animated flipInY">' +
                    '   <span>Total Biaya Proses</span>' +
                    '  <h2>' + toRp(data.total_proses) + '</h2>' +
                    '</div>' +
                    '<div class="col-md-2 tile animated flipInY">' +
                    '    <span>Total Jasa Rider</span>' +
                    '   <h2>' + toRp(data.total_jasa) + '</h2>' +
                    '</div>' +
                    '<div class="col-md-2 tile animated flipInY">' +
                    '   <span>Total Progresif</span>' +
                    '   <h2>' + toRp(data.total_progresif) + '</h2>' +
                    '</div>');
        });
    }
    this.dataDashboardTransfer = function (id, userid) {
        $.ajax({
            type: "GET",
            url: "/home/data/financeDashboard.jsp?q=" + userid,
            dataType: "json"

        }).done(function (data) {

            $('#dashbord_01').append(
                    '<div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">' +
                    '<div class="left"></div>' +
                    '<div class="right">' +
                    '<span class="count_top"><i class="fa fa-calculator"></i> Siap Kirim Uang</span>' +
                    '<div class="count" id="resume_akan">' + toRp(data.resume_akan) + '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">' +
                    '<div class="left"></div>' +
                    '<div class="right">' +
                    '<span class="count_top"><i class="fa fa-clock-o"></i> Proses Samsat</span>' +
                    '<div class="count red" id="resume_tagih">' + toRp(data.resume_pending) + '</div>' +
                    '</div>' +
                    ' </div>' +
                    '<div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">' +
                    '   <div class="left"></div>' +
                    '   <div class="right">' +
                    '       <span class="count_top"><i class="fa fa-calendar"></i> Kirim Uang Kemarin</span>' +
                    '       <div class="count" id="resume_kirim">' + toRp(data.resume_kirim) + '</div>' +
                    '   </div>' +
                    '</div>' +
                    '<div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">' +
                    '   <div class="left"></div>' +
                    '   <div class="right">' +
                    '       <span class="count_top"><i class="fa fa-money"></i> Kirim Uang Hari ini</span>' +
                    '      <div class="count green" id="resume_pending">' + toRp(data.resume_today) + '</div>' +
                    '   </div>' +
                    '</div>'
                    );

            $('#rekap_bawah').append(
                    '<div class="col-md-2 tile animated flipInY">' +
                    '<span>Total Kirim Uang</span>' +
                    '<h2 id="total_unit">' + toRp(data.total_kirim) + '</h2>' +
                    '</div>' +
                    '<div class="col-md-2 tile animated flipInY">' +
                    '   <span>Total Unit Kendaraan</span>' +
                    '   <h2>' + toRp(data.total_unit) + '</h2>' +
                    '</div>' +
                    '<div class="col-md-2 tile animated flipInY">' +
                    '   <span>Total Biaya Notice</span>' +
                    '   <h2>' + toRp(data.total_notice) + '</h2>' +
                    '</div>' +
                    '<div class="col-md-2 tile animated flipInY">' +
                    '   <span>Total Biaya Proses</span>' +
                    '  <h2>' + toRp(data.total_proses) + '</h2>' +
                    '</div>' +
                    '<div class="col-md-2 tile animated flipInY">' +
                    '    <span>Total Jasa Rider</span>' +
                    '   <h2>' + toRp(data.total_jasa) + '</h2>' +
                    '</div>' +
                    '<div class="col-md-2 tile animated flipInY">' +
                    '   <span>Total Progresif</span>' +
                    '   <h2>' + toRp(data.total_progresif) + '</h2>' +
                    '</div>');
        });
    }
    this.dataDashboardRider = function (id, userid) {
        $.ajax({
            type: "GET",
            url: "/home/data/riderDashboard.jsp?q=" + userid,
            dataType: "json"

        }).done(function (data) {

            $('#dashbord_01').append(
                    '<div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">' +
                    '<div class="left"></div>' +
                    '<div class="right">' +
                    '<span class="count_top"><i class="fa fa-calculator"></i> Pengajuan Baru</span>' +
                    '<a href="#" id="a">' +
                    '<div class="count" id="resume_akan">' + toRp(data.proses_baru) + ' UNIT</div>' +
                    '</a>' +
                    //  '<a href="#" id="a"><span class="count_bottom"> Lihat Detail</span></a>' +
                    '</div>' +
                    '</div>' +
                    '<div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">' +
                    '<div class="left"></div>' +
                    '<div class="right">' +
                    '<span class="count_top"><i class="fa fa-clock-o"></i> Proses Samsat</span>' +
                    '<a href="#" id="b">' +
                    '<div class="count blue" id="resume_tagih">' + toRp(data.proses_samsat) + ' UNIT</div>' +
                    '</a>' +
                    '</div>' +
                    ' </div>' +
                    '<div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">' +
                    '   <div class="left"></div>' +
                    '   <div class="right">' +
                    '       <span class="count_top"><i class="fa fa-clock-o"></i> Menunggu Ceklist Cabang</span>' +
                    '<a href="#" id="c">' +
                    '       <div class="count green" id="resume_kirim">' + toRp(data.proses_cek) + ' UNIT</div>' +
                    '</a>' +
                    '   </div>' +
                    '</div>' +
                    '<div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">' +
                    '   <div class="left"></div>' +
                    '   <div class="right">' +
                    '       <span class="count_top"><i class="fa fa-clock-o"></i> Sedang Proses > 5 hari</span>' +
                    '<a href="#" id="d">' +
                    '      <div class="count red" id="resume_pending">' + toRp(data.proses_pending) + ' UNIT</div>' +
                    '</a>' +
                    '   </div>' +
                    '</div>'
                    );



            $('#rekap_bawah').append(
                    '<div class="col-md-2 tile animated flipInY">' +
                    '<span>Total Kirim Uang</span>' +
                    '<h2 id="total_unit">' + toRp(data.total_kirim) + '</h2>' +
                    '</div>' +
                    '<div class="col-md-2 tile animated flipInY">' +
                    '   <span>Total Unit Kendaraan</span>' +
                    '   <h2>' + toRp(data.total_unit) + '</h2>' +
                    '</div>' +
                    '<div class="col-md-2 tile animated flipInY">' +
                    '   <span>Total Biaya Notice</span>' +
                    '   <h2>' + toRp(data.total_notice) + '</h2>' +
                    '</div>' +
                    '<div class="col-md-2 tile animated flipInY">' +
                    '   <span>Total Biaya Proses</span>' +
                    '  <h2>' + toRp(data.total_proses) + '</h2>' +
                    '</div>' +
                    '<div class="col-md-2 tile animated flipInY">' +
                    '   <span>Total Progresif</span>' +
                    '   <h2>' + toRp(data.total_progresif) + '</h2>' +
                    '</div>');

            $("#a").click(function () {
                bbn.tabelDetailDashboardPengajuanCabangRider('t-body4', userid, 'BARU');
                $('#judul4').empty();
                $('#judul4').append('status Pengajuan Baru');
                $("#myModalDetailDashboard").modal();
            });
            $("#b").click(function () {
                bbn.tabelDetailDashboardPengajuanCabangRider('t-body4', userid, 'SAMSAT');
                $('#judul4').empty();
                $('#judul4').append('status Proses Samsat');
                $("#myModalDetailDashboard").modal();
            });
            $("#c").click(function () {
                bbn.tabelDetailDashboardPengajuanCabangRider('t-body4', userid, 'CEK');
                $('#judul4').empty();
                $('#judul4').append('status Menunggu Ceklist');
                $("#myModalDetailDashboard").modal();
            });
            $("#d").click(function () {
                bbn.tabelDetailDashboardPengajuanCabangRider('t-body4', userid, 'PENDING');
                $('#judul4').empty();
                $('#judul4').append('status Proses > 5 hari');
                $("#myModalDetailDashboard").modal();
            });
        });
    }
    this.dataDashboardCabang = function (id, userid) {
        $.ajax({
            type: "GET",
            url: "/home/data/adminCabangDashboard.jsp?q=" + userid,
            dataType: "json"

        }).done(function (data) {

            $('#dashbord_01').append(
                    '<div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">' +
                    '<div class="left"></div>' +
                    '<div class="right">' +
                    '<span class="count_top"><i class="fa fa-clock-o"></i> Menunggu Persetujuan KaCab</span>' +
                    '<a href="#" id="a">' +
                    '<div class="count" id="resume_akan">' + toRp(data.proses_baru) + ' BERKAS</div>' +
                    //  '<a href="#" id="a"><span class="count_bottom"> Lihat Detail</span></a>' +
                    '</a>' +
                    '</div>' +
                    '</div>' +
                    '<div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">' +
                    '<div class="left"></div>' +
                    '<div class="right">' +
                    '<span class="count_top"><i class="fa fa-clock-o"></i> Pengajuan Baru</span>' +
                    '<a href="#" id="e">' +
                    '<div class="count blue" id="resume_tagih">' + toRp(data.proses_admin) + ' UNIT</div>' +
                    '</a>' +
                    '</div>' +
                    ' </div>' +
                    '<div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">' +
                    '<div class="left"></div>' +
                    '<div class="right">' +
                    '<span class="count_top"><i class="fa fa-clock-o"></i> Menunggu Proses Samsat</span>' +
                    '<a href="#" id="b">' +
                    '<div class="count blue" id="resume_tagih">' + toRp(data.proses_samsat) + ' UNIT</div>' +
                    '</a>' +
                    '</div>' +
                    ' </div>' +
                    '<div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">' +
                    '   <div class="left"></div>' +
                    '   <div class="right">' +
                    '       <span class="count_top"><i class="fa fa-clock-o"></i> Belum Ceklist Terima STNK</span>' +
                    '<a href="#" id="c">' +
                    '       <div class="count green" id="resume_kirim">' + toRp(data.proses_cek) + ' UNIT</div>' +
                    '</a>' +
                    '   </div>' +
                    '</div>'
                    /**    
                     '<div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">' +
                     '   <div class="left"></div>' +
                     '   <div class="right">' +
                     '       <span class="count_top"><i class="fa fa-clock-o"></i> Sedang Proses > 5 hari</span>' +
                     '<a href="#" id="d">'+
                     '      <div class="count red" id="resume_pending">' + toRp(data.proses_pending) + ' UNIT</div>' +
                     '</a>'+
                     '   </div>' +
                     '</div>'
                     **/
                    );


            $("#e").click(function () {
                bbn.tabelDetailDashboardPengajuanCabang('t-body4', userid, 'BARU');
                $('#judul4').empty();
                $('#judul4').append('status Pengajuan Baru');
                $("#myModalDetailDashboard").modal();
            });
            $("#b").click(function () {
                bbn.tabelDetailDashboardPengajuanCabang('t-body4', userid, 'SAMSAT');
                $('#judul4').empty();
                $('#judul4').append('status Proses Samsat');
                $("#myModalDetailDashboard").modal();
            });
            $("#c").click(function () {
                bbn.tabelDetailDashboardPengajuanCabang('t-body4', userid, 'CEK');
                $('#judul4').empty();
                $('#judul4').append('status Menunggu Ceklist');
                $("#myModalDetailDashboard").modal();
            });
            $("#d").click(function () {
                bbn.tabelDetailDashboardPengajuanCabang('t-body4', userid, 'PENDING');
                $('#judul4').empty();
                $('#judul4').append('status Proses > 5 hari');
                $("#myModalDetailDashboard").modal();
            });

        });
    }

    this.dataDashboardIndah = function (id, userid) {
        $.ajax({
            type: "GET",
            url: "/home/data/indahDashboard.jsp?q=" + userid,
            dataType: "json"

        }).done(function (data) {

            $('#dashbord_01').append(
                    '<div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">' +
                    '<div class="left"></div>' +
                    '<div class="right">' +
                    '<span class="count_top"><i class="fa fa-calculator"></i> Menunggu Kirim Uang</span>' +
                    '<div class="count" id="resume_akan">' + toRp(data.proses_baru) + ' UNIT</div>' +
                    //  '<a href="#" id="a"><span class="count_bottom"> Lihat Detail</span></a>' +
                    '</div>' +
                    '</div>' +
                    '<div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">' +
                    '<div class="left"></div>' +
                    '<div class="right">' +
                    '<span class="count_top"><i class="fa fa-clock-o"></i> Menunggu Input Nopol</span>' +
                    '<div class="count blue" id="resume_tagih">' + toRp(data.proses_samsat) + ' UNIT</div>' +
                    '</div>' +
                    ' </div>' +
                    '<div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">' +
                    '   <div class="left"></div>' +
                    '   <div class="right">' +
                    '       <span class="count_top"><i class="fa fa-clock-o"></i> Menunggu Ceklist Cabang</span>' +
                    '       <div class="count green" id="resume_kirim">' + toRp(data.proses_cek) + ' UNIT</div>' +
                    '   </div>' +
                    '</div>' +
                    '<div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">' +
                    '   <div class="left"></div>' +
                    '   <div class="right">' +
                    '       <span class="count_top"><i class="fa fa-clock-o"></i> Sedang Proses > 9 hari</span>' +
                    '<a href="#" id="c">' +
                    '      <div class="count red" id="resume_pending">' + toRp(data.proses_pending10) + ' UNIT</div>' +
                    '</a>' +
                    '   </div>' +
                    '</div>'
                    );

            $("#c").click(function () {
                bbn.tabelDetailDashboardPengajuanIndah('t-body4', userid, 'PENDING10');
                $('#judul4').empty();
                $('#judul4').append('status Proses > 10 hari');
                $("#myModalDetailDashboard").modal();
            });


        });
    }
    this.aa = function (variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
                return pair[1];
            }
        }
        return(false);
    }


    function toRp(angka) {
        var rev = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2 = '';
        for (var i = 0; i < rev.length; i++) {
            rev2 += rev[i];
            if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
                rev2 += '.';
            }
        }
        return rev2.split('').reverse().join('');
    }
    function toRpInt(angka) {
        var rev = angka.toString().split('').reverse().join('');
        var rev2 = '';
        for (var i = 0; i < rev.length; i++) {
            rev2 += rev[i];
            if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
                rev2 += '.';
            }
        }
        return rev2.split('').reverse().join('');
    }
    this.showContent = function (id, url, judul) {
        $('#judulatas').empty();
        $('#judulatas').append(judul.replace("1", ""));
        $('#' + id).empty();
        $('#' + id).append('<iframe id="idframe" frameborder="0" src="' + url + '" width="100%" height="590" />');

    }

    return this;

})();


